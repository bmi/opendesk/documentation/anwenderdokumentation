# Aufgaben anzeigen

Erfahren Sie, wie die Aufgaben eines Aufgabenordners auf unterschiedliche Weise angezeigt werden können.

Öffnen Sie zuerst in der Ordneransicht einen Aufgabenordner. Klicken Sie danach in der Liste auf eine Aufgabe. Genauere Angaben zur Aufgabe werden in der Detailansicht angezeigt.

![Detailansicht Aufgabe](media/0b1f798dfcd8539e45143e1f56077ab319d495dc.PNG "Detailansicht Aufgabe")

## Optionen

- Möchten Sie die Liste der Aufgaben sortieren oder nur unerledigte Aufgaben anzeigen lassen, klicken Sie rechts über der Liste auf die **3-Punkte-Schaltfläche**
- Mit einem Doppelklick auf eine Aufgabe in der Liste wird diese in einem neuen Fenster geöffnet

![Aufgaben sortieren](media/281ad575ce2e4c04ebec05bb49c4a3635734b240.PNG "Aufgaben sortieren")
