# Erste Schritte

Um mit dem Anlegen und Verwalten von Aufgaben zu beginnen, wählen Sie in der Kategorie **Kommunikation &amp; Organisation** das Untermodul **Aufgaben** aus.

![Kommunikation und Organisation](media/825a1ccca867905f5f5550dade1ca08af047f618.PNG "Kommunikation und Organisation")

Nachdem Sie das Modul geöffnet haben, werden die zuletzt erstellten Aufgaben aufgelistet. Bei der ersten Verwendung ist diese Liste noch leer. Sie können Ihre Aufgaben nach Dringlichkeit, Status, Fälligkeitstermin, Betreff und Priorität sowie alphabetisch auf- und absteigend sortieren (**3-Punkte-Schaltfläche**).

![Aufgaben anordnen](media/9e3144b0b6d6aa675264bdf1b4a951b56f4f2078.PNG "Aufgaben anordnen")

Die Aufgaben-Werkzeugleiste enthält verschiedene Funktionen zum Hinzufügen, Bearbeiten und Verwalten von Aufgaben. Sie haben hier folgende Möglichkeiten:

- neue Aufgaben anlegen
- Aufgaben bearbeiten
- das Fälligkeitsdatum einer Aufgabe ändern
- Aufgaben als erledigt markieren
- ausgewählte Aufgaben löschen
- persönliche Ordner anzeigen lassen
- neue Ordner hinzufügen

![Möglichkeiten mit Aufgaben](media/bbbff1f44cc57e4c50aa8d8b12d57544ce13a7be.PNG "Möglichkeiten mit Aufgaben")
