# Aufgaben in einen anderen Ordner verschieben

Sie können Aufgaben in einen anderen Ordner verschieben. Wählen Sie dafür zuerst in der Liste eine oder mehrere Aufgaben aus. Klicken Sie dann in der Werkzeugleiste auf das **Dreipunkt-Menü** und wählen Sie **Verschieben** aus.

![Aufgabe verschieben](media/779dbefbc7f510006b56a64eed839cf280b30f72.png "Aufgabe verschieben")

**Hinweis:** Um mehrere Aufgaben auszuwählen, halte Sie die Strg-Taste gedrückt und klicken Sie nacheinander auf die gewünschten Aufgaben.

Klicken Sie im Fenster **Verschieben** auf den kleinen Pfeil bei **Meine Aufgaben** , um die Ordnerliste aufzuklappen. Wählen Sie den Ordner aus, in den Sie die Aufgabe bzw. Aufgaben verschieben möchten. Bei Bedarf können Sie einen neuen Ordner anlegen, indem Sie auf **Ordner anlegen** klicken. Klicken Sie abschließend auf **Verschieben**.

![Aufgabe verschieben](media/beb7119226313f490678b177e9eaf6898537bfc8.png "Aufgabe verschieben")

**Hinweis:** Sie können die gewählten Objekte auch verschieben, indem Sie die Objekte in der Ordneransicht mit gedrückter Maustaste per Drag &amp; Drop auf einen Ordner ziehen.
