# Aufgaben als erledigt markieren

Wenn Sie eine Aufgabe abgeschlossen haben, können Sie diese als erledigt markieren. Wählen Sie dazu in der Liste eine oder mehrere Aufgaben aus. Klicken Sie dann in der Werkzeugleiste auf **Als erledigt markieren**.

![Aufgabe als erledigt markieren](media/de09c6614d02a17205fef703e92ea5b3cc24bcd8.png "Aufgabe als erledigt markieren")

Um diese Auswahl zu einem späteren Zeitpunkt rückgängig zu machen, klicken Sie an gleicher Stelle in der Werkzeugleiste auf **Als unerledigt markieren**.

![Aufgabe als unerledigt markieren](media/aa68c71931a29ac86441ddb1309ef16373fd69da.png "Aufgabe als unerledigt markieren")
