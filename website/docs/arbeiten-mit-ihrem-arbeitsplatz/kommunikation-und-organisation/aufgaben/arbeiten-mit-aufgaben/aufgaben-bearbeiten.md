# Aufgaben bearbeiten

Sie können Aufgaben auch nach dem Erstellen noch bearbeiten. Wählen Sie dazu in der Liste die zu bearbeitende Aufgabe aus. Danach klicken Sie in der Werkzeugleiste auf **Bearbeiten**.

![Aufgabe bearbeiten](media/5d2ed1bb2d6b148462c0bcd26143b4d701534b83.png "Aufgabe bearbeiten")

Die Daten der Aufgabe werden angezeigt. Nun können Sie die gewünschten Daten bearbeiten. Für die meisten Einstellmöglichkeiten müssen Sie zunächst auf **Fomular erweitern** klicken. Nähere Informationen zu den einzelnen Optionen finden Sie im Abschnitt **Neue Aufgabe anlegen**.

![Aufgabe bearbeiten](media/96d826a9c9a4767c1ffdc2e60385d14c3d31bbaf.png "Aufgabe bearbeiten")

Klicken Sie abschließend auf **Speichern.**

![Aufgabe speichern](media/6fe0ddae29cb80372ab4170406b4cfc19ccadf66.png "Aufgabe speichern")
