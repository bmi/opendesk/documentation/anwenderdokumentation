# Serienaufgabe anlegen

Legen Sie zuerst eine neue Aufgabe an. Sie finden dazu einen Anleitungsartikel unter **Neue Aufgabe anlegen.** Möchten Sie hingegen eine bestehende Aufgabe in eine Serienaufgabe umwandeln, so wählen Sie die Aufgabe in der Liste aus und klicken Sie oben in der Menüleiste auf **Aufgabe bearbeiten.**

So legen Sie im Aufgabenbearbeitungsfenster eine Serienaufgabe an:

Klicken Sie zunächst auf **Formular erweitern**.

![Serienaufgabe anlegen](media/96d826a9c9a4767c1ffdc2e60385d14c3d31bbaf.png "Serienaufgabe anlegen")

Aktivieren Sie jetzt den Haken bei **Wiederholen** . Es erscheint ein Vorschlag dazu, in welchem Intervall die Aufgabe wiederholt werden soll. Die Standardeinstellung ist eine wöchentliche Wiederholung am aktuellen Wochentag (z. B. jeden Montag). Klicken Sie auf den Vorschlag, um Ihre eigene Einstellung vorzunehmen.

![Serienaufgabe anlegen - Wiederholung des Termins](media/052017d61f4c42b7df0cd26945b50a9c9dbe20fc.png "Serienaufgabe anlegen - Wiederholung des Termins")

Ein Fenster öffnet sich, in dem Sie die Wiederholungsparameter genauer einstellen können:

Im Dropdown-Menü **Wiederholen** bestimmen Sie das Intervall zwischen den Terminen. Je nach dem, was Sie hier ausgewählt haben, stehen Ihnen weitere Optionen zur Verfügung, um das Intervall genauer einzugrenzen. Bei der Auswahl **Wöchentlich** können Sie z. B. bestimmen, an welchen Wochentagen die Aufgabe fällig ist und ob dies für jede Woche oder etwa alle 2 oder 3 Wochen gelten soll. Zuletzt können Sie bestimmen, ob und wann die Aufgabe enden soll.

Haben Sie Ihre gewünschten Änderungen vorgenommen, klicken Sie auf **Anwenden**.

![Serienaufgabe anlegen - Wiederholung bearbeiten](media/b90b5c6a9b7fd5f44de4ddc7b387beb9e6540699.png "Serienaufgabe anlegen - Wiederholung bearbeiten")
