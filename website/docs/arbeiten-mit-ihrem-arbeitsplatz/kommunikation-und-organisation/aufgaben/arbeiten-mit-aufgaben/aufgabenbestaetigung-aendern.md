# Aufgabenbestätigung ändern

Sie können die Antwort der Aufgabeneinladung auch noch nachträglich ändern. Klicken Sie dafür in die entsprechende Aufgabe in der Werkzeugleiste auf das Dreipunkt-Menü für weitere Optionen und klicken Sie auf **Bestätigung ändern**.

![Aufgabenbestästigung ändern](media/779dbefbc7f510006b56a64eed839cf280b30f72.png "Aufgabenbestästigung ändern")

Möchten Sie die Bestätigung oder Ablehnung der Aufgabe ändern, können Sie bei Bedarf noch eine **Anmerkung** eingeben.

![Aufgabeneinladung, Anmerkung](media/4dc88ab72325e0adacd2d9018ea435aa90f086b7.png "Aufgabeneinladung, Anmerkung")
