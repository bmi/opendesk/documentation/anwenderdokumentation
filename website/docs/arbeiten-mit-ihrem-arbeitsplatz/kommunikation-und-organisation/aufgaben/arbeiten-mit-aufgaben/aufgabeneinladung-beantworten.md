# Aufgabeneinladung beantworten

Wenn Sie eine Aufgabeneinladung erhalten haben, können Sie diese direkt aus dem Infobereich beantworten. Klicken Sie dazu zuerst in der Menüleiste auf das Symbol **Benachrichtigungen** oben rechts in der Ecke. Dieses Symbol ist nur sichtbar, wenn es tatsächlich neue Benachrichtigungen gibt.

![Aufgabeneinladung beantworten](media/0949730d8e6dd4d64babb5591c7d5f5f04d6f410.png "Aufgabeneinladung beantworten")

Jetzt wird Ihnen der Infobereich angezeigt, in dem Sie sehen können, um welche Aufgabe es geht. Klicken Sie unterhalb der Einladung auf **Bestätigen, Vielleicht** oder **Ablehnen**.

![Aufgabeneinladung beantworten](media/3b371d42e05553eb4b72afea1578bda3cb56327a.png "Aufgabeneinladung beantworten")

Möchten Sie die Bestätigung oder Ablehnung der Aufgabe ändern, können Sie bei Bedarf noch eine **Anmerkung** eingeben.

![Aufgabeneinladung, Anmerkung](media/4dc88ab72325e0adacd2d9018ea435aa90f086b7.png "Aufgabeneinladung, Anmerkung")
