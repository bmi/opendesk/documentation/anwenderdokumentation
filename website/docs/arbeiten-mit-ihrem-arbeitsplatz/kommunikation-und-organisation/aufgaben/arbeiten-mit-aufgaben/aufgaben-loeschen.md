# Aufgaben löschen

Sie können Aufgaben unwiderruflich löschen. Wählen Sie dazu in der Liste eine oder mehrere Aufgaben aus, welche Sie löschen möchten. Klicken Sie nun in der Werkzeugleiste auf **Löschen.**

![Aufgabe löschen](media/de09c6614d02a17205fef703e92ea5b3cc24bcd8.png "Aufgabe löschen")

Zuletzt müssen Sie noch bestätigen, dass die Aufgabe gelöscht werden soll. Klicken Sie dazu bei der Abfrage erneut auf **Löschen**.

![Aufgabe löschen, Abfrage](media/4186f0b727aaa1ee875241993f5d166717c436f6.png "Aufgabe löschen, Abfrage")
