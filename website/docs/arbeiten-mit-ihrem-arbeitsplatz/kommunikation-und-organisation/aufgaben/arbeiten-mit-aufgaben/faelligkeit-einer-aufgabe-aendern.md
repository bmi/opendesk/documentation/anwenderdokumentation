# Fälligkeit einer Aufgabe ändern

So ändern Sie die Fälligkeit einer Aufgabe:

Zuerst wählen Sie in der Liste eine Aufgabe, deren Fälligkeit Sie ändern möchten. Klicken Sie anschließend in der Werkzeugleiste auf **Fälligkeitsdatum ändern**. Es öffnet sich ein Dropdown-Menü. Darin kannen Sie die gewünschte Fälligkeit auswählen.

![Fälligkeitsdatum einer Aufgabe ändern](media/d37e334107a07ca94c1825d5a87747674143d5b5.png "Fälligkeitsdatum einer Aufgabe ändern")
