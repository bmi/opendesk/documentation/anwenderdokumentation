# Aufgaben suchen

Um nach bestimmten Aufgaben zu suchen, klicken Sie zunächst oben in der Mitte in das Eingabefeld **In Aufgaben suchen**. Geben Sie nun einen Suchbegriff ein. Daraufhin öffnet sich ein Dropdown-Menü. Durch eine Auswahl in diesem Menü können Sie eingrenzen, welche Aufgabendaten nach dem Suchbegriff durchsucht werden sollen.

![Aufgabe suchen](media/699a846d302fb783e688c57fa56a0f3372e2fe5c.png "Aufgabe suchen")

- Um alle Daten zu durchsuchen (Betreff, Beschreibung und Anhänge), klicken Sie auf den Suchbegriff oder drücken Sie die Eingabetaste, ohne eine weitere Auswahl im Menü zu treffen (Teilnehmerinnen und Teilnehmer der Aufgabe werden mit dieser Suchmethode jedoch nicht erfasst)
- Um nach Aufgaben zu suchen, die eine bestimmte Teilnehmerin bzw. ein bestimmter Teilnehmer hat, geben Sie den Namen in das Suchfeld ein und wählen Sie die gewünschte Person aus
- Um weitere Suchkriterien einstellen zu können klicken Sie auf den Pfeil neben dem Suchfeld. Hier öffnet sich jetzt ein Dropdown-Menü. Im Dropdown-Menü können Sie jetzt in der aktuellen Liste und in allen Listen suchen. Außerdem können Sie nach enthaltenen Wörtern und anhand des Datums suchen, welches in der entsprechenden Aufgabe angegeben ist

![Aufgabe erweiterte Suche](media/8a0a1faf11e1b66eaf54cc8b1381e5df34d00e7f.png "Aufgabe erweiterte Suche")

Die gefundenen Aufgaben, die Ihren Suchbegriff enthalten, werden in der Liste angezeigt. Um die Suche auf bestimmte Aufgaben zu beschränken, klicken Sie neben **Suchergebnisse** auf das Dreipunkt-Menü.

![Aufgabensuche verfeinern, Hamburger-Menü](media/f30a337d58a52fed67cb3991b8d362af43c5671a.png "Aufgabensuche verfeinern, Hamburger-Menü")

Folgende Optionen stehen Ihnen zur Verfügung:

- Unter dem **Dreipunkt-Menü** können Sie nach **Dringlichkeit** , **Status** , **Fälligkeitsstatus** , **Betreff** und **Priorität** sortieren. Damit können Sie die Suche nach Aufgaben eingrenzen.
- Außerdem können Sie die Sortierreihenfolge der Aufgaben festlegen. Es kann entweder **Aufsteigend** oder **Absteigend** ausgewählt werden.
- Auch nach erledigten Aufgaben kann die Suche eingegrenzt werden. Setzen Sie hier den Haken bei dem Punkt **Erledigte Aufgaben anzeigen.**

![Aufgabensuche verfeinern, Dropdown-Menü](media/7a272dc37b0320f038d5ba99aae79d2214c098f2.png "Aufgabensuche verfeinern, Dropdown-Menü")

Wenn Sie weiterhin zu viele Ergebnisse erhalten, um die gewünschten Aufgaben zu finden, fügen Sie im Suchfeld weitere Suchbegriffe hinzu. Gehen Sie dabei vor wie oben beschrieben. So können Sie die Suche weiter eingrenzen.

Möchten Sie die Suche beenden, klicken Sie ebenfalls im Suchfeld auf das Symbol **Entfernen (X)**.

![Suche beenden](media/c11568c13bb25d62743c52207894c330a000aa2f.png "Suche beenden")
