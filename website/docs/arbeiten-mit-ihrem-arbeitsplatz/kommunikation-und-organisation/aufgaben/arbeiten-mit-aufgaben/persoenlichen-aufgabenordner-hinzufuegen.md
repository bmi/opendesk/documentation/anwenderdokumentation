# Persönlichen Aufgabenordner hinzufügen

Sie können unterhalb von **Meine Listen** weitere persönliche Aufgabenordner anlegen, um Ihre Aufgaben zu organisieren. Klicken Sie dafür zuerst in der Ordneransicht auf das **Plus(+)** neben **Meine Listen** und wählen Sie **Neuen Aufgabenliste hinzufügen** aus.

![Aufgabenliste hinzufügen](media/8344f2033f89647a6ac33cbb41323dc4a09a4dcd.png "Aufgabenliste hinzufügen")

Ein Menü öffnet sich. Hier können Sie nun eine neue Liste hinzufügen bzw. diese Liste benennen und entweder als persönliche Liste oder als öffentliche Liste hinzufügen. Wählen Sie dafür die Checkbox **Als öffentlichen Ordner hinzufügen** aus.

![Aufgabenliste hinzufügen, benennen](media/f3ed59c5c9bec013d6921581edaeaa6b73735585.png "Aufgabenliste hinzufügen, benennen")
