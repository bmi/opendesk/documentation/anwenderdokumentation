# Neue Aufgaben anlegen

- [Neue Aufgaben anlegen](neue-aufgaben-anlegen/neue-aufgaben-anlegen.md)
- [Erinnerung für die Aufgabe einrichten](neue-aufgaben-anlegen/erinnerung-fuer-die-aufgabe-einrichten.md)
- [Status einer Aufgabe einstellen](neue-aufgaben-anlegen/status-einer-aufgabe-einstellen.md)
- [Priorität einer Aufgabe einstellen](neue-aufgaben-anlegen/prioritaet-einer-aufgabe-einstellen.md)
- [Sichtbarkeit einer Aufgabe einstellen](neue-aufgaben-anlegen/sichtbarkeit-einer-aufgabe-einstellen.md)
- [Mitglieder einer Aufgabe hinzufügen](neue-aufgaben-anlegen/mitglieder-einer-aufgabe-hinzufuegen.md)
