# Erinnerung für die Aufgabe einrichten

Sie können beim Anlegen einer Aufgabe auch eine Erinnerung für diese Aufgabe einrichten. Folgen Sie dazu den ersten Schritten unter [Neue Aufgaben anlegen](neue-aufgaben-anlegen.md), klicken Sie jedoch noch nicht auf Anlegen. Klicken Sie stattdessen unten auf **Formular erweitern** und klappen **dann** das Dropdown-Menü unter **Erinnerung** aus. Hier könen Sie wählen, wann die Erinnerung für Ihre Aufgabe kommen soll.

![Formular erweitern](media/75dd840b347db20c0349580ae7c1051e73d5d9a5.PNG "Formular erweitern")

![Erinnerung](media/ffb192497871f3786217113330061d083d6eabbf.PNG "Erinnerung")

Wenn Sie **Manuelle Eingabe** auswählen, können Sie bei **Datum für Erinnerung** selbst festlegen, wann die Erinnerung genau kommen soll. Geben Sie ein Datum und eine Uhrzeit ein und klicken Sie abschließend auf **Anlegen**.

![Zeitangabe Erinnerung](media/337a91f1c24694b4d030fd45f57b28f6dcbedf4a.PNG "Zeitangabe Erinnerung")
