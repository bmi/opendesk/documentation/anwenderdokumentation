# Mitglieder einer Aufgabe hinzufügen

Sie haben die Möglichkeit, Teilnehmerinnen und Teilnehmer zu einer von Ihnen erstellten **Aufgabe** hinzuzufügen oder aus ihr zu entfernen.

Wählen Sie die Aufgabe aus, zu der Sie Teilnehmerinnen und Teilnehmer hinzufügen möchten, und klicken Sie in der Menüleiste auf **Aufgabe bearbeiten** . Alternativ können Sie diese Einstellung auch bereits beim [Anlegen der Aufgabe](neue-aufgaben-anlegen.md) vornehmen.

![Aufgabe bearbeiten](media/761714b3cbfd4a2d38d3256c0f2876a70d9353c0.PNG "Aufgabe bearbeiten")

Es öffnet sich ein neues Fenster mit Betreff und Beschreibung Ihrer **Aufgabe**. Klicken Sie hier auf **Formular erweitern**, um weitere Eigenschaften anzuzueigen. Sie müssen ein wenig nach unten scrollen, um zum Teilnehmerinnen- und Teilnehmerbereich zu gelangen.

![Teilnehmerbereich](media/d97731965cc29bb72c9b1a254685a9370a6eeecc.png "Teilnehmerbereich")

Sie können Kontakte hinzufügen, indem Sie einen Namen in die Suchleiste **Kontakte hinzufügen** eingeben. Je genauer Sie den Namen eingeben, desto schneller finden Sie den Kontakt. Wenn Sie den gewünschten Kontakt gefunden haben, klicken Sie ihn an und er wird automatisch zu Ihrer Aufgabe hinzugefügt. Wiederholen Sie diesen Vorgang, bis Sie alle gewünschten Kontakte hinzugefügt haben.

**Hinweis:** Wenn Sie nur einen Teil des vollständigen Namens eingeben oder mehrere Personen denselben Namen haben, kann es passieren, dass Ihnen mehrere Vorschläge angezeigt werden. Achten Sie in diesem Fall darauf, dass Sie die richtige Person auswählen.

![Mitglieder hinzufügen](media/e4483781a25549c9da491fe91d80e057173e0daf.PNG "Mitglieder hinzufügen")

Möchten Sie die Zuweisung eines Kontaktes zu Ihrer Aufgabe aufheben, klicken Sie auf das **Papierkorb-Symbol** neben dem Kontakt. Wiederholen Sie diesen Vorgang, bis alle gewünschten Kontakte aus der Aufgabe gelöscht sind.

![Mitglieder entfernen](media/835c955a48e65ba68fe3d7043b14dbb281527d50.PNG "Mitglieder entfernen")

Klicken Sie auf **Speichern**, wenn Sie mit der Bearbeitung der Aufgabe fertig sind.

Wenn Sie zu einer Aufgabe eingeladen wurdesn, erhälten Sie eine Benachrichtigung, in der Sie die Aufgabe ablehnen oder bestätigen können. Es öffnet sich ein Fenster, indem Sie die Aufgabe ablehnen, als vielleicht markieren oder bestätigen und in dem Sie einen Kommentar hinzufügen können. Wenn Sie diese ablehnen, verschwindet die Benachrichtigung und es passiert nichts. Wenn Sie sie aber bestätigen oder als vielleicht markieren, erscheint die Aufgabe in Ihrem Aufgabenbereich.

![Aufgabeneinladung](media/82e0a0335547f339dbbc26f0f8d287aceb315986.PNG "Aufgabeneinladung")

![Aufgabe annehmen](media/cedccb01c7fa28816a1e5c85824d63b8eb11956c.PNG "Aufgabe annehmen")

**Hinweis**: Wenn Sie die **Aufgabe** slebst erstellt haben, erhalten Sie keine Bestätigungs- oder Ablehnungsbenachrichtigung, sonder nur eine Erinnerung. Diese kann zeitlich verzögert bei Ihnen ankommen.
