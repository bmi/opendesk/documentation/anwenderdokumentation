# Sichtbarkeit einer Aufgabe einstellen

Sie können auswählen, ob eine **Aufgabe** für andere Teilnehmerinnen und Teilnehmer sichtbar sein soll. Dies können Sie gleich beim Anlegen der Aufgabe im Fenster **Aufgabe erstellen** erledigen. Möchten Sie hingegen für eine bestehende **Aufgabe** die **Priorität** einstellen, so wählen Sie die **Aufgabe** in der Liste aus und klicken oben in der Menüleiste auf **Aufgabe bearbeiten**.

Klicken Sie danach in beiden Fällen, falls noch nicht geschehen, auf **Formular erweitern**. Rechts neben **Status** und **Priorität** können Sie unter **Typ** die **Aufgabe** als **Privat** markieren. So können andere Teilnehmerinnen und Teilnehmer Ihre Aufgabe nicht sehen. Lassen Sie den Haken hingegen weg, wenn Sie möchten, dass Ihre **Aufgabe** für andere sichtbar ist.

![Aufgabe auf Privat stellen](media/173008cd5b667796e985fc0105b0a2c8a2411f22.png "Aufgabe auf Privat stellen")
