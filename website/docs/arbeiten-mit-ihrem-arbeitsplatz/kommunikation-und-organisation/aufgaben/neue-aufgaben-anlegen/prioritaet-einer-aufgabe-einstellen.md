# Priorität einer Aufgabe einstellen

Sie können einer Aufgabe eine Priorität zuweisen. Dies können Sie gleich beim Anlegen der Aufgabe im Fenster **Aufgabe erstellen** erledigen. Möchten Sie hingegen für eine bestehende Aufgabe die Priorität einstellen, so wählen Sie die Aufgabe in der Liste aus und klicken oben in der Menüleiste auf **Bearbeiten**.

Klicken Sie danach in beiden Fällen, falls noch nicht geschehen, auf **Formular erweitern**. Rechts neben den Statuseinstellungen können Sie das Menü **Priorität** aufrufen und die gewünschte Priorität auswählen.

![Priorität einstellen](media/d6aa7687b76e1639ad1e8a10a64caa828a03c477.PNG "Priorität einstellen")
