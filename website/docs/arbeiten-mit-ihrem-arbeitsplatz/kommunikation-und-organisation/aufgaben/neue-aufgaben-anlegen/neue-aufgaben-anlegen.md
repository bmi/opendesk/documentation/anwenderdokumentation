# Neue Aufgaben anlegen

Über die Menüleiste im Aufgabenbereich können Sie eine neue Aufgabe anlegen. Klicken Sie hierzu in der Werkzeugleiste auf **Neue Aufgabe**.

![Neue Aufgabe](media/37d834f4f08e47fbaabf31987bd27b86a4bbd009.PNG "Neue Aufgabe")

Es öffnet sich ein neues Fenster. Jetzt können Sie einen **Betreff** und bei Bedarf auch eine nähere **Beschreibung** eingeben.

![Neue Aufgabe erstellen](media/d03ca8160bff6901bfec441fc9733ead7dc04b95.PNG "Neue Aufgabe erstellen")

Klicken Sie auf **Anlegen** um Ihre Aufgabe zu speichern.

![Anlegen klicken](media/c2976b4dce81711acfe06c90a4abba2d3e109446.PNG "Anlegen klicken")
