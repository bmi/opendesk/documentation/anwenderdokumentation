# Status einer Aufgabe einstellen

Im Fenster **Aufgabe erstellen** haben Sie die Möglichkeit, den Status Ihrer erstellten Aufgabe zu ändern. Klicken Sie dazu, falls noch nicht geschehen, auf **Formular erweitern** und navigieren zu **Status**.

![Formular erweitern](media/75dd840b347db20c0349580ae7c1051e73d5d9a5.PNG "Formular erweitern")

![Statusangabe](media/24288a4faa646e617c6e4d000446cc7d57d19824.png "Statusangabe")

Hier können Sie auswählen, welchen **Status** Ihre **Aufgabe** erhalten soll. Wählen Sie hierfür einen passenden Status aus dem Dropdown-Menü:

- Nicht begonnen
- In Bearbeitung
- Erledigt
- Warten
- Verschoben

![Status auswählen](media/2588375457698a08e81bb4117df22cff10a81526.PNG "Status auswählen")

Sie können auch angeben, wie weit Ihre **Aufgabe** bereits fortgeschritten ist. Wählen Sie hierzu bei **Fortschritt in %** über die Plus- und Minusschaltflächen den passenden Prozentsatz aus.

![Fortschritt angeben](media/1476de850a8339bf4e97e357ff98100020ad20e6.png "Fortschritt angeben")
