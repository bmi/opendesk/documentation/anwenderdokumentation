# Termin importieren

Wenn eine Datei im iCal-Format vorliegt, können Sie Termine auch importieren.

Wählen Sie in der Ordneransicht den Kalender aus, in den Sie Termine importieren möchten. Klicken Sie neben dem Kalender auf das Symbol **Aktionen** und auf **Importieren** . Im Fenster **Aus Datei importieren** klicken Sie auf **Datei hochladen**.

![Kalender-importieren-Menue](media/aa9809decd7add8f1f6c4ea062a9335abf8a5025.jpg "Kalender-importieren-Menue")

Wählen Sie nun eine Datei im iCal-Format. Um auch Termine zu importieren, die die gleiche Identifikationsnummer haben wie bereits vorhandene Termine, aktivieren Sie vorab **Vorhandene Termine ignorieren**. Klicken Sie nun auf **Importieren**. Die ausgewählten Termine werden jetzt zu Ihrem Kalender hinzugefügt.

**Achtung**: Alle Teilnehmerinnen und Teilnehmer der importierten Termine werden entfernt. Stattdessen werden Sie als teilnehmende Person hinzugefügt.
