# Termin exportieren

Sie können Termine im Format iCalendar exportieren. Dieses Format können Sie zum Beispiel verwenden, wenn Sie Termine mit anderen Kalender-Anwendungen austauschen möchten. Sie können einzelne Termine oder alle Termine eines Kalenders exportieren.

## Einzeltermin exportieren

Sie haben folgende Möglichkeiten, einzelne Termine zu exportieren:

- Klicken Sie in der Kalenderansicht auf einen Termin. Klicken dann im Pop-up auf das Symbol **Weitere Aktionen**

Wählen Sie in der Listenansicht einen oder mehrere Termine aus. Klicken Sie dann in der Werkzeugleiste auf das Symbol **Weitere Aktionen**  Klicken Sie nun auf **Exportieren**. Es öffnet sich ein Fenster. Hier wählen Sie die Funktion zum Speichern der Exportdatei.

## Alle Termine exportieren

Wenn Sie alle Termine eines Kalenders exportieren möchten, wählen Sie zunächst in der Ordneransicht einen persönlichen oder einen öffentlichen Kalender aus, von dem Sie die Termine exportieren möchten. Klicken Sie neben dem Ordnernamen auf das Symbol **Aktionen** und auf **Exportieren**. Ein Fenster öffnet sich. Wählen Sie hier die Funktion zum Speichern der Exportdatei.
