# Organisatorin oder Organisator ändern

Wenn Sie die organisierende Person eines (Serien-)Termins sind, der mindestens 2 weitere Teilnehmende hat, so können Sie die organisierende Person ändern.

In folgenden Fällen können Sie die organisierende Person nicht ändern:

- Wenn ein Termin externe Teilnehmende hat
- Wenn es sich um einen geänderten Einzeltermin einer Terminserie handelt - **Hinweis:** Diese Funktion ist je nach Konfiguration unter Umständen nicht verfügbar.

Sie haben folgende Möglichkeiten, die organisierende Person eines Termins zu ändern:

- Klicken Sie in der Kalenderansicht auf einen Termin. Klicken Sie dann im Pop-up auf das Symbol **Weitere Aktionen**

Wählen in der Listenansicht einen oder mehrere Termine aus  Klicken Sie dann in der Werkzeugleiste auf das Symbol **Weitere Aktionen** und auf **Organisator ändern**. Haben Sie eine Terminserie ausgewählt, müssen Sie festlegen, ob die Änderungen für den einzelnen Termin oder für die gesamte Terminserie gelten sollen. Im Fenster **Organisator ändern** geben Sie nun die E-Mail-Adresse der neuen Organisatorin oder des neuen Organisators ein.
