# Terminerinnerung ändern

Terminerinnerungen können nachträglich verändert werden, indem Sie eine Erinnerung anpassen, entfernen oder eine weitere Erinnerung hinzufügen.

**Hinweis:** Diese Funktion ist nur verfügbar, wenn Sie keine vollständigen Bearbeitungsrechte für den Termin besitzen. Andernfalls können Sie die Terminerinnerung ändern, indem Sie das Terminbearbeitungsfenster verwenden.

Folgende Möglichkeiten haben Sie, um eine Terminerinnerung zu ändern:

- Klicken Sie in der Kalenderansicht auf den gewünschten Termin. Ein Pop-up öffnet sich

Wählen Sie in der Listenansicht den gewünschten Termin aus. Öffnen Sie ihn mit einem Doppelklick. Der Termin wird in einem Fenster angezeigt  Klicken Sie nun auf das Symbol **Weitere Aktionen** und auf **Erinnerung ändern**. Das Fenster **Erinnerungen ändern** öffnet sich. Wählen Sie eine Erinnerung aus. Dadurch öffnet sich das Fenster **Erinnerungen bearbeiten**. Bei Bedarf können Sie jetzt eine vorhandene Erinnerung bearbeiten, löschen oder eine neue Erinnerung hinzufügen.
