# Termin als stellvertretende Person anlegen, bearbeiten und verwalten

Je nach den Berechtigungen, die Ihnen eine andere nutzende Person erteilt hat, können Sie in deren Kalender folgende Funktionen ausführen:

- Vorhandene Termine ansehen
- Neue Termine im Namen der nutzenden Person anlegen

Vorhandene Termine im Namen der nutzenden Person bearbeiten, verwalten oder löschen  Sie können ebenfalls als stellvertretende Person Termine anlegen oder bearbeiten. Dafür öffnen Sie in der Ordneransicht unterhalb von **Freigegebene Kalender** den Ordner der nutzenden Person, die Sie zur stellvertretenden Person bestimmt hat. Klicken Sie nun in der Werkzeugleiste auf **Neuer Termin**. Sie werden gefragt, ob Sie diesen Termin als stellvertretende Person im freigegebenen Kalender einer anderen nutzenden Person anlegen wollen. Klicken Sie auf **im Auftrag des Eigentümers**.

Die nutzende Person, für die Sie die Stellvertretung übernehmen, wird per E-Mail darüber informiert, dass Sie einen Termin angelegt, bearbeitet oder gelöscht haben. Wie man Termine anlegt, bearbeitet oder löscht, erfahren Sie im Kapitel **Termin einstellen**.
