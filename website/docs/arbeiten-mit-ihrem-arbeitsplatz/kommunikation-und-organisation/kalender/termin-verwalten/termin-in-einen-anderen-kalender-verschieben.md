# Termin in einen anderen Kalender verschieben

Sie können Termine in andere Kalender verschieben. Die einzige Voraussetzung hierfür ist, dass Sie im Zielkalender die Berechtigung zum Anlegen von Objekten haben.

Sie haben folgende Möglichkeiten, Termine zu verschieben:

- Klicken Sie in der Kalenderansicht auf einen Termin. Klicken Sie dann im Pop-up auf das Symbol **Weitere Aktionen (Drei Punkte)**
- Wählen Sie in der Listenansicht einen oder mehrere Termine aus. Klicken Sie dann in der Werkzeugleiste auf das Symbol **Weitere Aktionen (Drei Punkte)**

![Termin verschieben](media/60f8f8d972fb0d82d366f4afb1dca2bcb3b54fde.jpg "Termin verschieben")

Klicken Sie auf **Verschieben** . Das Fenster **Verschieben** öffnet sich. Wählen Sie hier den gewünschten Kalender aus. Bei Bedarf können Sie auch einen neuen Kalender anlegen, indem Sie auf **Ordner anlegen** klicken. Klicken Sie abschließend auf **Verschieben**.

![Termin-verschieben-Zielkalender](media/4e19f51dbcefc4718b2d020e00d2ccdb32f128b2.jpg "Termin-verschieben-Zielkalender")
