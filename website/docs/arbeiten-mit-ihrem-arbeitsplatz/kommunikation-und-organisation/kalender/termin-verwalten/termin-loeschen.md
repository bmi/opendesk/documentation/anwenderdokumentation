# Termin löschen

## Termin löschen

Termine aus Ihren privaten Kalendern können Sie nur löschen, wenn Sie auch die organisierende Person des Termins sind.

**Hinweis:** Wenn Sie einen Termin löschen, ist dieser Termin endgültig gelöscht und kann nicht mehr wiederhergestellt werden.

Sie haben 2 Möglichkeiten, einen Termin zu löschen:

- Klicken Sie in der Kalenderansicht auf den Termin, den Sie löschen möchten. Klicken Sie dann im Pop-up auf **Löschen**

Wählen Sie in der Listenansicht einen oder mehrere Termine aus, die Sie löschen möchten. Klicken Sie dann in der Werkzeugleiste auf **Löschen**  Bestätigen Sie abschließend, dass Sie den Termin löschen möchten.

## Serientermine löschen

Wenn Sie einen Serientermin gewählt haben, werden Sie gefragt, welche Termine der Terminserie gelöscht werden sollen:

- Wenn Sie den ersten Termin der Terminserie gewählt haben, können Sie entweder nur den ersten Termin oder die gesamte Terminserie löschen
- Wenn Sie einen Termin innerhalb der Terminserie gewählt haben, können Sie entweder nur den gewählten Termin oder den gewählten Termin und alle zukünftigen Termine der Terminserie löschen

Wenn Sie den letzten Termin der Terminserie gewählt haben, können Sie nur den letzten Termin löschen. In diesem Fall werden Sie nicht gefragt, welche Termine gelöscht werden sollen  Wenn Sie gleichzeitig auch die organisierende Person des Termins sind, können Sie die anderen Teilnehmerinnen und Teilnehmer über den Grund für die Löschung informieren, indem Sie eine Nachricht eingeben. Das Eingabefeld wird angezeigt, wenn folgende Bedingungen erfüllt sind:

- Der Termin hat mindestens 2 Teilnehmende

In den Kalender-Einstellungen ist **Benachrichtigung bei Terminänderungen** aktiviert. Die Nachricht wird als E-Mail gesendet.
