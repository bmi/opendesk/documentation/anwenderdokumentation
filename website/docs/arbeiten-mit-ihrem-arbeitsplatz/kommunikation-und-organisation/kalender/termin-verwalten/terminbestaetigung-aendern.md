# Terminbestätigung ändern

Sie können Ihre Terminbestätigung auch nachträglich ändern. Dabei gibt es folgende Optionen:

- Sie können den Termin kommentarlos bestätigen oder ablehnen

Sie können den Bestätigungsstatus ändern. Dabei können Sie einen Kommentar eingeben, den andere Teilnehmerinnen und Teilnehmer des Termins lesen können.

**Hinweis:** Je nach Konfiguration Ihrer privaten Kalender können Sie eine Terminbestätigung nur ändern, wenn Sie selbst teilnehmende Person des Termins sind.

Sie haben folgende Möglichkeiten, um Ihren Bestätigungsstatus nachträglich zu ändern:

- Klicken Sie in der Kalenderansicht auf den gewünschten Termin und im geöffneten Pop-up auf **Status ändern**

Wählen Sie in der Listenansicht den gewünschten Termin aus und öffnen ihn mit einem Doppelklick. Klicken Sie anschließend in der Werkzeugleiste auf **Status ändern**  Handelt es sich um eine Terminserie, so legen Sie fest, ob die Änderungen nur für einen einzelnen Termin oder für die gesamte Terminserie gelten sollen. Bei Bedarf können Sie im Fenster **Bestätigungsstatus ändern** eine Anmerkung verfassen. Klicken Sie auf eine der Schaltflächen **Ablehnen, Vorläufig** oder **Akzeptieren**.
