# Eigene, öffentliche und freigegebene Kalender

In der linken Seitenansicht sehen Sie den Mini-Kalender und in der Ordneransicht sind Ihre Kalender dargestellt.

![Kalender-Ordneransicht](media/7f2d579c22706f18d47a249aade3520b3fbdafbf.png "Kalender-Ordneransicht")

## Mein Kalender

Über das Plus (+) neben **Meine Kalender** können Sie einen neuen Kalender erstellen, in dem Sie **Neuen Kalender hinzufügen** klicken oder für andere Zwecke auf **Neuen Ressourcenkalender hinzufügen** und **Neue Ressourcennkalendergruppe hinzufügen** klicken.

![Neuer Kalender hinzufügen](media/28d50266f65f5cdcb9ede6f73a7699e9ab6b9f44.png "Neuer Kalender hinzufügen")

## Öffentliche Kalender

Über **Öffentliche Kalender** werden alle öffentlichen Termine und freigegebene Kalender angezeigt.
