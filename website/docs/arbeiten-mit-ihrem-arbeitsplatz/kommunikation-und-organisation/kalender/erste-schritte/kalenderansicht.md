# Kalenderansicht

In der Kalenderansicht können Sie anpassen, in welchem Umfang Ihr Kalender angezeigt werden soll. Sie können zwischen Tag, Arbeitswoche, Woche, Monat, Jahr und Liste auswählen.

![Kalender-Ansicht](media/49302f26835d16abbc751105627b819b5d7e3ed0.png "Kalender-Ansicht")
