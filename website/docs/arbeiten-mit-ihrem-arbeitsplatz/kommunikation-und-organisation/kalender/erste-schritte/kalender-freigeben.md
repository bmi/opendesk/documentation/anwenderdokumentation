# Kalender freigeben

Sie können Ihren eigenen Kalender für andere Personen freigeben. Öffnen Sie das Hamburger-Menü neben dem freizugebenden Kalender und klicken Sie dann bei den angezeigten Optionen auf **Freigeben/Berechtigungen**.

![Kalender freigeben](media/f50102c564d96aa9ce8eca553edf6a6401b30714.png "Kalender freigeben")

Nun können Sie bestimmen, wer auf den Kalender zugreifen kann. Sie können Personen einladen und spezifische Rechte für den Kalenderzugriff festlegen.

![Berechtigung für ausgewählten Kalender](media/943d5070d723d8f4f8a55f3175e7588f1dbcacd7.png "Berechtigung für ausgewählten Kalender")
