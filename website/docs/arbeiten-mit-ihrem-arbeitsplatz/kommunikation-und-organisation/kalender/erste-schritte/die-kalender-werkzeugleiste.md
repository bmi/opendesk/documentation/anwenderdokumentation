# Die Kalender-Werkzeugleiste

## Der Kalender und die Werkzeugleiste

Die Benutzeroberfläche des Kalenders bietet Ihnen die Möglichkeit, einen Überblick über alle geplanten Termine zu bekommen. Über die gekennzeichnete Schaltfläche mit den 9 Punkten (**Alle Applikationen**) ist es Ihnen möglich, die Programme des Hauptmenüs auszuwählen, falls Sie zu einer anderen Anwendung wechseln möchten.

![Alle Applikationen](media/0e6beaf2b198f9dd0f325bce6361060ba167c795.PNG "Alle Applikationen")

Die **Kalender-Werkzeugleiste** enthält folgende Funktionen:
- Neuer Termin: Anlegen eines neuen Termins
- Heute: Wechsel auf das aktuelle Datum in der Kalenderansicht

Ansicht: Wechsel auf Tag, Arbeitswoche, Woche oder Monat  Die Funktion **Heute** ist jedoch nur verfügbar, wenn Sie als **Ansicht** Tag, Arbeitswoche, Woche oder Monat geöffnet haben.

![Kalender-Werkzeugleiste](media/f54f6867831d533f667d97e3f3b4aa7c062f77e5.png "Kalender-Werkzeugleiste")
