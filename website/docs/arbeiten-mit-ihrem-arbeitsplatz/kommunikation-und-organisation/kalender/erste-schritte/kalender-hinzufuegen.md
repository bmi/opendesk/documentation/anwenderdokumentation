# Kalender hinzufügen

## Kalender hinzufügen

Sie können beliebig viele Kalender erstellen, zum Beispiel für unterschiedliche Projekte. Sie können diese auch flexibel öffnen.

Einen neuen Kalender erstellen Sie über den Punkt **Neuen Kalender hinzufügen**. Hier können Sie entscheiden, ob es ein persönlicher Kalender oder ein Kalender für andere Zwecke werden soll. Wenn das der Fall ist, können Sie auf **Neuen Ressourcenkalender hinzufügen** oder **Neue Ressourcenkalendergruppe hinzufügen** klicken.

![Kalender hinzufügen](media/28d50266f65f5cdcb9ede6f73a7699e9ab6b9f44.png "Kalender hinzufügen")

## Persönlichen Kalender hinzufügen

Möchten Sie einen persönlichen Kalender erstellen, wählen Sie **Neuen Kalender hinzufügen** aus. Ein neues Fenster öffnet sich und Sie können den neuen Kalender benennen. In diesem Fenster besteht ebenfalls die Möglichkeit, Ihren persönlichen Kalender in einen öffentlichen Kalender umzuwandeln. Dieses Feld wird also leer gelassen, wenn Sie den Kalender nur für sich selbst erstellen möchten. Durch einen Klick auf **Hinzufügen** wird der neue Kalender erstellt und in Ihre Liste aller Kalender mit aufgenommen.

![Persönlichen Kalender hinzufügen](media/683051ed53d093ba816ebca09870c165c75d9e46.png "Persönlichen Kalender hinzufügen")
