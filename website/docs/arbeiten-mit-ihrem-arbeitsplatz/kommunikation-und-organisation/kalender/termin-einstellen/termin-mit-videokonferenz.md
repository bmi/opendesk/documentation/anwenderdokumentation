# Termin mit Videokonferenz

Für einen Termin mit mehreren Personen können Sie einen Videokonferenzraum einrichten. Dabei wird ein Link für den geplanten Konferenzraum generiert und ein Raum im Modul **Videokonferenzen** eingerichtet.

![Videokonferenz anlegen](media/a7523b815151e29fb5eac8f69e29d6c9ac9a250b.png "Videokonferenz anlegen")
