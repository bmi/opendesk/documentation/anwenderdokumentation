# Terminsichtbarkeit einstellen

Sie haben die Möglichkeit, Ihren Termin für andere Nutzerinnen und Nutzer zu verbergen. Wenn Sie möchten, dass andere Nutzerinnen und Nutzer diesen Termin einsehen können, wählen Sie **Standard** aus. Möchten Sie, dass der Termin von anderen Nutzerinnern und Nutzern nicht eingesehen werden kann, wählen Sie **Privat** oder **Geheim** aus.

**Privat:** Nutzerinnen und Nutzer, die nicht am Termin teilnehmen, sehen nur den Zeitpunkt des Termins, nicht den Inhalt.

**Geheim:** Nutzerinnen und Nutzer, die nicht am Termin teilnehmen, wird der Termin nicht angezeigt.

Sie können außerdem eine **Erinnerung** für den Termin angeben, sodass die entsprechende Teilnehmerin bzw. der entsprechende Teilnehmer an den Termin erinnert wird. Sie können den gewünschten Kalender auswählen, in dem der Termin erscheinen soll und Sie können die **Terminfarbe** auswählen, in der der Termin angezeigt werden soll.

Darüber hinaus haben Sie die Option eine Kategorie Ihres Termins anzugeben, welche Ihnen erscheint, wenn Sie auf den rechten Pfeil neben **Kategorie hinzufügen** klicken. Die Kategorien sind voreingestellt und es kann **Predefined**, **Important**, **Business**, **Private** und **Meeting** ausgewählt werden.

Um dem Termin Anhänge hinzuzufügen, klicken Sie entweder auf **Anhänge hinzufügen** oder **Von Dateien hinzufügen.**

![Sichtbarkeit des Termins](media/e06e3c18927a25b8337c68829d9e054d963b8caa.png "Sichtbarkeit des Termins")
