# Termin planen

Über **Planung** können Sie vor Erstellen eines Termins bereits die Verfügbarkeit der Personen prüfen, die an Ihrem Termin teilnehmen sollen.

![Terminplanung](media/0da8c3247f0cbc84961112ab193e4c2921ff7737.png "Terminplanung")

Über das Feld **Teilnehmer hinzufügen** können Sie diese auswählen.

![Terminplanung](media/fe690690bfc960c197a54942fac9f127fd0059e0.png "Terminplanung")

Haben Sie ein offenes Zeitfenster für alle Beteiligten gefunden, halten Sie die linke Maustaste gedrückt und ziehen Sie über den gewünschten Zeitraum im Kalendertag, an dem der Termin stattfinden soll. Klicken Sie nun auf **Termin anlegen**. Die von Ihnen zuvor ausgewählte Zeit wird automatisch in die Felder **Beginnt am** und **Endet am** eingefügt.

![Terminplanung](media/6597a2a868c1d4a204e99248eeadf8c13e16623b.png "Terminplanung")

Über **Optionen** können Sie in diesem Fenster auch Änderungen vornehmen. So können Sie zum Beispiel über **Kompakt** die Darstellung des ausgewählten Kalendertages verkleinern oder über **Feinraster anzeigen** den Kalendertag auf viertelstündige Blöcke aufteilen. Den Datumsbereich können Sie auf Woche oder Monat einstellen. Sie können ebenfalls bestimmen, ob auch Nichtarbeitszeiten für die Terminauswahl angezeigt werden sollen oder nicht.

![Terminplanung Optionen](media/cf38b62f477318473e64f15d9757c49b3131d6fd.png "Terminplanung Optionen")
