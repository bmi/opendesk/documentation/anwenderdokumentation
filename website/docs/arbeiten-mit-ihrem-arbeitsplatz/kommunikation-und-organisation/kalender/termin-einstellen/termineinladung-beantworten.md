# Termineinladung beantworten

Wurden Sie zu einem Termin eingeladen, erhalten Sie eine oder beide der folgenden Benachrichtigungen:

- Der Infobereich informiert Sie über diesen Termin

Sie erhalten eine E-Mail mit der Termineinladung  Termineinladungen können zugesagt, vorläufig zugesagt oder abgelehnt werden. Dieser sogenannte Bestätigungsstatus kann auch zu einem späteren Zeitpunkt noch geändert werden.

## Terminbeantwortung im Infobereich

Wenn Sie die Termineinladung über den Infobereich beantworten möchten, klicken Sie in der Menüleiste auf das Symbol **Benachrichtigungen**. Der Infobereich wird nun angezeigt. Hier können Sie nun unterhalb der Einladung auf **Bestätigen, Vielleicht** oder **Ablehnen** klicken. Wenn Sie den Termin bestätigt haben, wird er in Ihrem Kalender eingetragen. Sie können den Termin auch im Kalender zusagen oder absagen. Klicken Sie dafür im entsprechenden Tag auf den Termin und beantworten Sie den Termin dort mit **Bestätigen**, **Vielleicht** oder **Ablehnen**.

![Termineinladung](media/78cd75b0ad23837318faa679b7ee150ca2893453.png "Termineinladung")

Wenn Sie auf das Dreipunkte-Menü neben den Antwort-Möglichkeiten klicken, können Sie eine Bemerkung zum Termin eingeben.

![Terminbemerkung](media/cc76ef3e8322e468a48fd82ff94a843724d24027.png "Terminbemerkung")

## Terminbeantwortung in der Einladungs-E-Mail

Sie können die Termineinladung auch per E-Mail beantworten. In der App **E-Mail** wird die Termineinladung angezeigt. Sie können in der Detailansicht unterhalb von **Termindetails anzeigen** einen Kommentar einfügen. Auch hier haben Sie die Auswahl zwischen **Bestätigen, Vielleicht** und **Ablehnen**. Wie im Abschnitt „Terminbeantwortung im Infobereich“ beschrieben, wird der Termin in Ihrem Kalender eingetragen, wenn Sie diesen bestätigen.

**Hinweis:** In den Kalender-Einstellungen können Sie bestimmen, ob Einladungs-E-Mails automatisch gelöscht werden sollen, wenn diese von Ihnen bestätigt oder abgelehnt wurden.
