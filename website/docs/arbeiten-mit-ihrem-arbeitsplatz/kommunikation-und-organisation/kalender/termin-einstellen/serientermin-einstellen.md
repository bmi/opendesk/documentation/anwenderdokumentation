# Serientermin einstellen

Möchtest Sie, dass ein Termin regelmäßig in einem bestimmten zeitlichen Intervall stattfindet, können Sie einen Serientermin einrichten. Aktivieren Sie dafür **Wiederholen.** Nun erscheint der aktuelle Wochentag in violetter Schrift.

Möchten Sie die Wiederholungszeit verändern, klicken Sie auf den violetten Wochentag

![Serientermin bearbeiten](media/f2f3ae67eb45b3886d71114c70e33485f329648d.png "Serientermin bearbeiten")

Es öffnet sich ein neues Fenster. Hier können nun die terminlichen Aspekte des Termins bearbeitet werden. Haben Sie Ihre gewünschten Änderungen vorgenommen, klicken Sie auf **Anwenden**.

![Serientermin bearbeiten](media/752246ddbac3d55756bb09a34dbbd16bf59c6570.png "Serientermin bearbeiten")
