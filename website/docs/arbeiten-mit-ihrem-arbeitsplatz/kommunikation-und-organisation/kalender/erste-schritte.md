# Erste Schritte

Nach Auswählen des Moduls **Kalender** öffnet sich die Kalenderansicht des aktuellen Monats. Einmal ist für Sie großflächig die aktuelle Kalenderwoche angezeigt. Im Minikalender links daneben haben Sie außerdem eine Darstellung des aktuellen Monats. So haben Sie direkt einen Überblick über anstehende Termine.

### Hier geht es weiter mit:

- [Kalender freigeben](erste-schritte/kalender-freigeben.md)
- [Kalender hinzufügen](erste-schritte/kalender-hinzufuegen.md)
- [Kalenderansicht](erste-schritte/kalenderansicht.md)
- [Eigene, öffentliche und freigegebene Kalender](erste-schritte/eigene-oeffentliche-und-freigegebene-kalender.md)
- [Die Kalender-Werkzeugleiste](erste-schritte/die-kalender-werkzeugleiste.md)
