# Termin verwalten

Hier erfahren Sie, wie Sie Ihre Termine verwalten. Beispielsweise können Sie Inhalte von Terminen verändern sowie Termine oder ganze Terminserien aus dem Kalender löschen.

### Hier geht es weiter mit:

- [Terminbestätigung ändern](termin-verwalten/terminbestaetigung-aendern.md)
- [Terminerinnerung ändern](termin-verwalten/terminerinnerung-aendern.md)
- [Termin als stellvertretende Person anlegen, bearbeiten und verwalten](termin-verwalten/termin-als-stellvertretende-person-anlegen-bearbeiten-und-verwalten.md)
- [Organisatorin oder Organisator ändern](termin-verwalten/organisatorin-oder-organisator-aendern.md)
- [Termin löschen](termin-verwalten/termin-loeschen.md)
- [Termin in einen anderen Kalender verschieben](termin-verwalten/termin-in-einen-anderen-kalender-verschieben.md)
- [Termin importieren](termin-verwalten/termin-importieren.md)
- [Termin exportieren](termin-verwalten/termin-exportieren.md)
