# Termin drucken

Sie können Termine und Kalender ausdrucken. Folgende Möglichkeiten gibt es:
- Sie können ein Kalenderblatt mit Terminen drucken
- Sie können die Daten von Terminen drucken
- Sie können eine detaillierte oder kompakte Liste von Terminen drucken

### Hier geht es weiter mit:

- [Ein Kalenderblatt drucken](termin-drucken/ein-kalenderblatt-drucken.md)
- [Daten von Terminen drucken](termin-drucken/daten-von-terminen-drucken.md)
