# Daten von Terminen drucken

Um die Daten von Terminen auszudrucken, haben Sie 2 Möglichkeiten:

- Klicken Sie in der Kalenderansicht auf einen Termin. Klicken Sie dann im Pop-up auf das Symbol **Weitere Aktionen** und dann auf **Drucken**
- Wählen Sie in der Listenansicht einen oder mit gedrückter **Strg-Taste** mehrere Termine aus. Klicken Sie dann in der Werkzeugleiste auf das Symbol **Weitere Aktionen** und dann auf **Drucken** . Hier können Sie noch zwischen der kompakten und der detaillierten Ansicht zum Drucken wählen

![Daten-von-Terminen-drucken](media/d75a99e17e8f05d75c0d78b7471e705a41b0438a.jpg "Daten-von-Terminen-drucken")
