# Ein Kalenderblatt drucken

Um ein Kalenderblatt mit Terminen zu drucken, wählen Sie den gewünschten Kalender und in der Menüleiste oben rechts die gewünschte **Ansicht** (Tag, Arbeitswoche, Woche, Monat, Jahr, Liste) aus. Öffnen Sie die **Einstellungen** (Zahnradsymbol) oben rechts neben Ihrem Profilbild bzw. Namenskürzel. Wählen Sie **Drucken**. Es wird eine Druckvorschau angezeigt und Sie können die Druckoptionen des Druckers anpassen.

![Kalender-Ansichtsauswahl- Arbeitswoche ist ausgewählt](media/8f67f756f707878c21d999db984ac5593ea53562.jpg "Kalender-Ansichtsauswahl")

![Termin-drucken](media/a720f694f06e528c2bbd041e7525bc205a1fef3e.jpg "Termin-drucken")
