# Kontakte verwalten

- [Neuen Kontakt hinzufügen](kontakte-verwalten/neuen-kontakt-hinzufuegen.md)
- [Kontakt löschen](kontakte-verwalten/kontakt-loeschen.md)
- [Kontakt verschieben oder kopieren](kontakte-verwalten/kontakt-verschieben-oder-kopieren.md)
- [Kontakt als vCard versenden](kontakte-verwalten/kontakt-als-vcard-versenden.md)
- [Kontakt zu einem Termin einladen](kontakte-verwalten/kontakt-zu-einem-termin-einladen.md)
- [Neuen Termin anlegen](kontakte-verwalten/neuen-termin-anlegen.md)
