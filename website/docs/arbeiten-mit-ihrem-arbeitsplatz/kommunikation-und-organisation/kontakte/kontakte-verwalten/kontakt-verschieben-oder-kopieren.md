# Kontakt verschieben oder kopieren

Sie können Kontakte oder Verteilerlisten in ein anderes Adressbuch verschieben oder kopieren. Das globale Adressbuch ist hiervon allerdings ausgenommen.

So verschieben oder kopieren Sie Kontakte in ein anderes Adressbuch:

Wählen Sie in der Liste einen oder mehrere Kontakte oder Verteilerlisten aus, welche Sie kopieren oder verschieben möchten. Klicken Sie anschließend in der Werkzeugleiste auf das Symbol **Weitere Aktionen (Hamburger-Menü)** . Danach können Sie auf **Verschieben** oder **Kopieren** klicken. Jetzt öffnet sich ein Fenster, in welchem Sie ein Adressbuch auswählen. Bei Bedarf können Sie auch ein neues Adressbuch anlegen, indem Sie auf **Ordner anlegen** klicken. Klicken Sie nun auf **Verschieben** beziehungsweise **Kopieren**.

**Tipp:** Sie können die gewählten Kontakte bzw. Verteilerlisten auch per Drag &amp; Drop verschieben, indem Sie diese in der Ordneransicht einfach mit gedrückter Maustaste auf ein Adressbuch ziehen.

![Kontakt verschieben oder kopieren](media/04f1c0996d180f08bb34e8cad7c778deec299d9d.png "Kontakt verschieben oder kopieren")

![Kontakt verschieben](media/66d38ab4439ebde2e5d70ea78e72e70e86c02e50.png "Kontakt verschieben")
