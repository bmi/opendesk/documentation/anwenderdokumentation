# Kontakt löschen

Möchten Sie einen bereits bestehenden Kontakt löschen, müssen Sie zunächst den gewünschten Kontakt auswählen, sodass dieser violett hinterlegt ist. Anschließend wählen Sie in der oberen Leiste das Icon **Löschen** aus.

![Kontakt löschen](media/8481991695cd7697aa47e69c4eb7052ac970a36d.png "Kontakt löschen")

Anschließend werden Sie gefragt, ob der Kontakt wirklich gelöschen werden soll. Wenn Sie dies tun möchten, klicken Sie auf **Löschen**.

![Kontakt löschen](media/1e0d2477699e927509115b18ab47edfcb53f38f7.png "Kontakt löschen")
