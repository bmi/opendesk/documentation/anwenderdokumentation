# Neuen Termin anlegen

So können Sie einen neuen Termin anlegen:

Zuerst öffnen Sie in der Ordneransicht einen Kalender, in dem Sie die Berechtigung zum Anlegen von Objekten haben. Danach klicken Sie in der Werkzeugleiste auf **Einladen**.

![Zu einem Termin einladen](media/4b8de26ed7fc39cefafe0a37e97d34ba94aed3e7.png "Zu einem Termin einladen")

Zuerst müssen Sie einen **Titel** eingeben.

![Neuen Termin anlegen, Titel festlegen](media/c3c074f213189c8717c5a129058fa94300676395.png "Neuen Termin anlegen, Titel festlegen")

Um den Beginn und das Ende des Termins festzulegen, müssen Sie unterhalb von **Beginnt am** und **Endet am** folgenden Aktionen ausführen:

- Klicken Sie auf ein Datum. Geben Sie ein Datum ein oder wählen Sie ein Datum aus der Datumsauswahl. Bei ganztägigen Terminen aktivierst du **Ganztägig**
- Klicken Sie auf eine Uhrzeit. Geben Sie die Uhrzeit ein, oder wählen Sie eine Uhrzeit aus der Liste
- Wenn gewünscht, können Sie die Zeitzone für die Start- oder Endzeit festlegen, indem Sie neben einer Uhrzeit auf die Zeitzonen-Schaltfläche klicken. Sie können für die Startzeit und Endzeit unterschiedliche Zeitzonen angeben

![Termin anlegen, Uhrzeit festlegen](media/0fa0bea2547a7fa7e8d5ab0125760928c5543743.png "Termin anlegen, Uhrzeit festlegen")
- Aktivieren Sie **Wiederholen**, wenn sich der Termin periodisch wiederholen soll. Nun erscheint der aktuelle Wochentag in violetter Schrift. Möchten Sie die Wiederholungszeit verändern, klicken Sie auf den violetten Wochentag. Es öffnet sich ein neues Fenster. Hier können nun die terminlichen Aspekte des Termins bearbeitet werden. Haben Sie Ihre gewünschten Änderungen vorgenommen, klicken Sie auf **Anwenden**.

![Termin anlegen, Wiederholung bearbeiten](media/752246ddbac3d55756bb09a34dbbd16bf59c6570.png "Termin anlegen, Wiederholung bearbeiten")

Möchten Sie den Termin in einer Videokonferenz abhalten, so wählen Sie unter **Konferenz** die Möglichkeit **Videokonferenz** aus. Das System erstellt einen Konferenzraum und generiert den dazugehörigen Link automatisch.

![Termin anlegen, Videokonferenz auswählen](media/a7523b815151e29fb5eac8f69e29d6c9ac9a250b.png "Termin anlegen, Videokonferenz auswählen")

Geben Sie bei Bedarf nun den **Ort** und eine **Beschreibung** ein.

Unter **Teilnehmer** haben Sie die Option, weitere Teilnehmerinnen und Teilnehmer für den Termin einzuladen. Außerdem haben sehen Sie unter **Teilnehmer** , welche Teilnehmerinnen und Teilnehmer sich bereits in dem Termin befinden. Um weitere Teilnehmerinnen und Teilnehmer in diesen Termin einzuladen, klicken Sie dafür neben dem Feld **Teilnehmer und Ressourcen** auf das Symbol **Kontakt auswählen**.

![Termin anlegen, Ort, Beschreibung, Teilnehmerinnen und Teilnehmer einladen](media/faa44b97d4e0a575ed77af6775df646f08ca046a.png "Termin anlegen, Ort, Beschreibung, Teilnehmerinnen und Teilnehmer einladen")

Nun öffnet sich ein neues Fenster mit Ihren Kontakten. Möchten Sie Kontakte hinzufügen, tippen Sie in das Eingabefeld **Suchen** den entsprechenden Namen des Kontakts ein und wählen ihn anschließend aus. Die ausgewählten Kontakte haben nun einen grauen Hintergrund. Klicke auf **Wählen** um deine gewünschten Kontakte zu dem Termin hinzuzufügen. Sie haben außerdem die Möglichkeit Kontakte zu filtern und in Ihren verschiedenen Adressbüchern nach Kontakten zu suchen. Die Schaltflächen befinden sich jeweils rechts neben dem Eingabefeld **Suchen**.

![Globale Adressliste, Suchen, Filtern, Adressliste](media/210167904fea7e4d14104273f77f8b0db392a3b1.png "Globale Adressliste, Suchen, Filtern, Adressliste")

Wenn Sie die Sichtbarkeit Ihres Termins einstellen möchten, können Sie unter **Sichtbarkeit** auf den rechten Pfeil in der Schaltfläche klicken und **Standard**, **Privat** oder **Geheim** auswählen. Sie können außerdem eine **Erinnerung** für den Termin angeben, sodass die entsprechende Teilnehmerin bzw. der entsprechende Teilnehmer an den Termin erinnert wird. Sie können den gewünschten Kalender auswählen, in dem der Termin erscheinen soll und sie können die **Terminfarbe** auswählen, in der der Termin angezeigt werden soll. Darüber hinaus haben Sie die Option eine Kategorie Ihres Termins anzugeben, welche Ihnen erscheint, wenn Sie auf den rechten Pfeil neben **Kategorie hinzufügen** klicken. Die Kategorien sind voreingestellt und es kann **Predefined**, **Important**, **Business**, **Private** und **Meeting** ausgewählt werden. Um dem Termin Anhänge hinzuzufügen, klicken Sie entweder auf **Anhänge hinzufügen** oder **Von Dateien hinzufügen.**

![Termin anlegen, weitere Funktionen](media/e06e3c18927a25b8337c68829d9e054d963b8caa.png "Termin anlegen, weitere Funktionen")

Haben Sie nun Ihren Termin fertig konfiguriert, müssen Sie ihn noch speichern.Sie speichern den Termin, indem Sie auf **Anlegen** klicken.

![Termin anlegen](media/eec945390db2dadcddff544789b3e1e658e88067.png "Termin anlegen")
