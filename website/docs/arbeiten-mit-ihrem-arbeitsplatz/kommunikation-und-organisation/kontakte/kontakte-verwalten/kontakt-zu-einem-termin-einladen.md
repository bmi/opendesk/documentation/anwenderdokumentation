# Kontakt zu einem Termin einladen

Sie können direkt aus dem Modul **Kontakte** Termine anlegen und Kontakte oder ganze Verteilerlisten zu diesen Terminen einladen. Wählen Sie zu Beginn einen oder mehrere Kontakte oder Verteilerlisten aus und klicken Sie anschließend in der Werkzeugleiste auf **Zu Termin Einladen**. Wenn Sie einen einzelnen Kontakt auswählen möchten, klicken Sie alternativ oben in der Detailansicht des Kontaktes auf **Einladen**. Als letzten Schritt können Sie die Angaben zum Anlegen des Termins vervollständigen. Nähere Angaben hierzu finden Sie unter [Termin einstellen](../../kalender/termin-einstellen/termin-einstellen.md).

![Kontakt zu einem Termin einladen](media/b57726f793631e8cbbb446bf291b8103a579f82d.png "Kontakt zu einem Termin einladen")

![Termin anlegen](media/a53d86bc88ecf016f2be84d0775f28fccac27767.png "Termin anlegen")
