# Neuen Kontakt hinzufügen

Das Modul Kontakte ermöglicht die Verwaltung aller Adressbücher und persönlichen Kontakte.

Das Anlegen neuer und das Bearbeiten vorhandener Kontakte und Adressen erfolgt über die Menüleiste.

Über den Menüpunkt **Neuer Kontakt** besteht auch die Möglichkeit, Verteilerlisten zu erstellen.

Wenn Sie einen neuen Kontakt hinzufügen möchten, navigieren Sie links zu **Meine Adressbücher** und wählen Sie ein Adressbuch aus. Klicken Sie nun auf die violette Schaltfläche **Neuer Kontakt**. Anschließend öffnet sich ein neues Fenster. Jetzt haben Sie die Möglichkeit, die gewünschten Kontaktdaten einzugeben.

![Neuen Kontakt hinzufügen](media/0c9b1b6ed2b10d7c9a9830c5c56c55124be30b57.png "Neuen Kontakt hinzufügen")

![Kontaktdaten eingeben](media/858e974efefd79d63f9f87a7e44888b981db3eb3.png "Kontaktdaten eingeben")

Wenn Sie einem Kontakt ein Foto hinzufügen möchten, klicken Sie auf das leere Kontaktfoto. Das Fenster **Kontaktfoto ändern** öffnet sich. Wenn Sie ein vorhandenes Foto hochladen möchten, klicken Sie auf **Ein Foto hochladen.** Möchten Sie direkt ein neues Foto mithilfe der Gerätekamera aufnehmen, klicken Sie auf **Ein Foto machen**. Bei Bedarf können Sie den Bildausschnitt festlegen, indem Sie den Regler unter dem Foto verwenden und das Foto verschieben oder drehen.

Wenn Sie auf **Anwenden** klicken, wird das Foto hinzugefügt.

![Kontaktfoto ändern](media/3a3a43dbc9e8f1d6a554ff48c7a630d256a94b3b.png "Kontaktfoto ändern")

Um ein Foto zu bearbeiten oder zu löschen, klicken Sie zunächst auf das Foto. Damit gelangen Sie in eine Editor-Ansicht, in der Sie das Bild über die entsprechenden Schaltflächen drehen können. Verwenden Sie den horizontalen Regler, um die Zoomstufe zu verändern. Wenn Sie näher an das Bild herangezoomt haben, können Sie im Folgenden den Bildausschnitt verändern, indem Sie auf das Bild klicken und es mit gedrückter Maustaste verschieben. Klicken Sie auf **Anwenden**, sobald Sie mit Ihren Änderungen zufrieden sind, oder wählen Sie **Foto entfernen** , wenn das Bild ganz gelöscht werden soll.

![Kontaktfoto bearbeiten](media/8fe9d7fa5a6300c1caeb752079522a954f97013e.png "Kontaktfoto bearbeiten")

![Kontaktdaten eingeben](media/b2c01466192b8ff4502aa49b0559b64133c0ed1f.png "Kontaktdaten eingeben")

![Persönliche Informationen hinzufügen](media/9a51b2d41a9401e74bb851de62c7a5133d1d6a70.png "Persönliche Informationen hinzufügen")

Der Firma bzw. der Abteilung können auch weitere Informationen hinzugefügt werden. Klicken Sie dafür auf **Geschäftsinformationen hinzufügen**. Es erscheint ein Dropdown-Menü und Sie können die Informationen um weitere Punkte ergänzen:

- **Position**
- **Beruf**
- **Raumnummer**
- **Manager**
- **Assistent**

![Geschäftsinformationen hinzufügen](media/3bbc9da118d5e56bd3c6e9939a0b1bf673b6d1ef.png "Geschäftsinformationen hinzufügen")

Sollten Sie mehrer E-Mail-Adressen besitzen, können Sie diese ebenfalls hinzufügen. Klicken Sie dafür auf **E-Mail, Telefon, Fax hinzufügen**. Es erscheint ein Dropdown-Menü und Sie können die Informationen um weitere Punkte ergänzen:

- **E-Mail 2**
- **E-Mail 3**
- **Instantmessenger 1**
- **Instantmessenger 2**
- **Mobiltelefon (2)**
- **Telefon (geschäftlich)**
- **Telefon (geschäftlich 2)**
- **Telefon (privat)**
- **Telefon (privat 2)**
- **Telefon (Zentrale)**
- **Telefon (weiteres)**
- **Fax**
- **Fax (privat)**
- **Fax(2)**

![E-Mail, Telefon, Fax hinzufügen](media/0b406ab1528f7e695f5a60bb3f7192c70a652b55.png "E-Mail, Telefon, Fax hinzufügen")

Darüber hinaus haben Sie die Option eine Kategorie Ihres Termins anzugeben, welche Ihnen erscheint, wenn Sie auf den rechten Pfeil neben **Kategorie hinzufügen** klicken. Die Kategorien sind voreingestellt und es kann **Predefined**, **Important**, **Business**, **Private** und **Meeting** ausgewählt werden. Möchten Sie nützliche Notizen hinzufügen können Sie im Textfeld Notiz Ihre Angaben machen. Sollte dieser Kontakt als privater Kontakt eingestuft werden, können Sie die Checkbox **Dies ist ein privater Kontakt und kann nicht freigegeben werden** anklicken. Außerdem haben Sie die Möglichkeit Benutzerfelder hinzuzufügen. Das können Sie einstellen, indem Sie auf **Benutzerfelder hinzufügen** klicken. Um dem Termin Anhänge hinzuzufügen, klicken Sie entweder auf **Anhänge hinzufügen** oder **Von Dateien hinzufügen.**

![Kategorie, Notizen, Anhänge hinzufügen](media/620783f92c4a3db370178ba646d58e7c49c90aea.png "Kategorie, Notizen, Anhänge hinzufügen")
