# Kontakt als vCard versenden

So können Sie Kontakte oder Verteilerlisten als vCard-Anhang in einer E-Mail versenden:

Zu Beginn wählen Sie in der Liste einen Kontakt, eine Verteilerliste oder mehrere Kontakte oder Verteilerlisten aus. Anschließend klicken Sie in der Werkzeugleiste auf das Symbol **Weitere Aktionen (Hamburger-Menü).** Wenn sie jetzt auf auf **Als vCard senden klicken** , wird eine vCard versendet.

![Kontakt als vCard versenden](media/04f1c0996d180f08bb34e8cad7c778deec299d9d.png "Kontakt als vCard versenden")

Anschließend können Sie die Angaben der E-Mail vervollständigen. Wie in einer regulären E-Mail können Sie folgende Daten eingeben:

- E-Mail-Adresse, an die der Kontakt versendet werden soll
- Betreff
- Text der E-Mail

Anhänge  Um die E-Mail mit der vCard abzusenden, klicken Sie auf **Senden**.

![vCard versenden](media/f18a2ece38d71155c01fe2f473a55f969e1936a2.png "vCard versenden")
