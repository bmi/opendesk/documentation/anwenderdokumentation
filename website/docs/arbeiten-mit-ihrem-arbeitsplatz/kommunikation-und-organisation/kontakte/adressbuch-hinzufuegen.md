# Adressbuch hinzufügen

Mithilfe von Adressbüchern können Sie Ihre Kontakte organisieren, zum Beispiel unterteilt in berufliche und private Kontakte. Erfahren Sie, wie Sie Adressbücher anlegen, Kontakte aus externen Adressbüchern anwenden und die Anzeige von freigegebenen Adressbüchern bestimmen können.

### Hier geht es weiter mit:

- [Persönliches Adressbuch hinzufügen](adressbuch-hinzufuegen/persoenliches-adressbuch-hinzufuegen.md)
- [Öffentliche Adressbücher](adressbuch-hinzufuegen/externe-adressbuecher-abonieren.md)
