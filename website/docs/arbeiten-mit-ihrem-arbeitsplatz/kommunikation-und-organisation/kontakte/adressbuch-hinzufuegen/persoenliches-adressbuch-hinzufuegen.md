# Persönliches Adressbuch hinzufügen

Unter **Meine Adressbücher** können Sie persönliche Adressbücher anlegen. Um ein neues Adressbuch anzulegen, klicken Sie auf das **Plus (+)** neben **Meine Adressbücher.**

![Adressbuch anlegen](media/d1ab51e40e4e106027be2dc3175a112f55455ca1.png "Adressbuch anlegen")

Wählen Sie jetzt das **Plus (+)** , um ein neues Adressbuch zu erstellen.

![Neues Adressbuch hinzufügen](media/9512f959675708ad4d05e1484718f4cf69deae3c.png "Neues Adressbuch hinzufügen")

Es erscheint ein neues Fenster, in dem Sie Ihr neues Adressbuch benennen können. Soll das Adressbuch öffentlich einsehbar sein, setzen Sie den Haken bei **Als öffentlichen Ordner hinzufügen.** Klicken Sie abschließend auf **Hinzufügen**.

![Neues Adressbuch hinzufügen](media/e69b72344b7f0e11154514a25cbcf3c632632d4b.png "Neues Adressbuch hinzufügen")
