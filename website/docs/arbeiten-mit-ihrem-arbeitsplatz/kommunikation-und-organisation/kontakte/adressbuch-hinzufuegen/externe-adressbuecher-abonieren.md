# Öffentliche Adressbücher

Sie können innerhalb des Moduls **Kontakte** öffentliche Adressbücher einsehen, welche in der Seitenansicht untereinander aufgelistet sind.

![Öffentliche Adressbücher Auflistung](media/9de3b5c229a61ac70c95e976a1adfd873717a218.png "Öffentliche Adressbücher Auflistung")

Sie haben die Möglichkeit öffentliche Adressbücher zu Ihren Favoriten hinzuzufügen, Ihre Berechtigungen für dieses Adressbuch einzusehen, das Adressbuch zu exportieren und es auszublenden. Klicken Sie dafür auf das Dreipunkt-Menü neben dem entsprechenden Adressbuch und klicken Sie auf die gewünschte Funktion.

![Öffentliche Adressbücher Funktionen](media/6c5bdcbf943ec0d1a53b9d9f092372d6fa2fe9d2.png "Öffentliche Adressbücher Funktionen")
