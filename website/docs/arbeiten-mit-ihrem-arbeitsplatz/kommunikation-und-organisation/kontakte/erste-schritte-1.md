# Erste Schritte

Mit dem Modul **Kontakte** haben Sie ein Werkzeug, um verschiedene Adressbücher zu verwalten. Die darin gespeicherten Adressen können Sie beim Versenden von E-Mails, bei der Termin- oder Aufgabenplanung und an vielen weiteren Stellen einsetzen.

## Allgemeiner Aufbau des Moduls

1.Adressbuch-Werkzeugleiste

![Werkzeugleiste](media/caa177aa22838ac390bccd82ff3c8c956177b550.png "Werkzeugleiste")

2.Adressbuch-Ordneransicht

![Adressbuch-Ordneransicht](media/218c0c53bfc2cbaa44ac093e49090596b5b7c4c0.png "Adressbuch-Ordneransicht")

3.Adressbuch-Navigationsbereich

![Adressbuch- Navigationsbereich](media/6b1fec9d28abc8f08bfde35b448808a0f41e7f27.png "Adressbuch- Navigationsbereich")

4.Adressbuch-Anzeigebereich

![Adressbuch-Anzeigebereich](media/2a9b6b949470613f76661bb6157c91d633796fb9.png "Adressbuch-Anzeigebereich")
