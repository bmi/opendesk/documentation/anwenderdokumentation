# Kontakte oder Verteilerlisten bearbeiten

**Hinweis:** Die folgenden Schritte sind nur in Adressbüchern möglich, in denen Sie die Berechtigung zum Anlegen von Objekten hast. Das globale Adressbuch ist hiervon z. B. ausgenommen.

Wählen Sie zuerst in der Liste einen Kontakt oder eine Verteilerliste aus. Klicken Sie danach in der Werkzeugleiste auf **Bearbeiten**.

![Werkzeugleiste Verteilerliste bearbeiten](media/caa177aa22838ac390bccd82ff3c8c956177b550.png "Werkzeugleiste Verteilerliste bearbeiten")

Es öffnet sich ein Fenster, in dem Sie die Daten des Kontaktes bzw. der Verteilerliste einsehen und bearbeiten können. Haben Sie alle gewünschten Änderungen vorgenommen, klicken Sie auf **Speichern**.

![](media/ccf87765da60c04e05b9f5c531d71fc48debf060.png "")
