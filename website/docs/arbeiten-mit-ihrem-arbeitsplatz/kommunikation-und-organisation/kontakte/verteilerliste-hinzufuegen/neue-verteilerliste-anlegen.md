# Neue Verteilerliste anlegen

Öffnen Sie als erstes ein Adressbuch in der Ordneransicht. Klicken Sie dann in der Werkzeugleiste auf **Neuer Kontakt** und anschließend auf **Neue Verteilerliste**.

![Verteilerliste anlegen](media/f16d0994b05f33c8466eb1d5b1b38bdeb7124072.png "Verteilerliste anlegen")

**Hinweis:** Diese Schritte sind nur in Adressbüchern möglich, in denen Sie die Berechtigung zum Anlegen von Objekten haben. Das globale Adressbuch ist hiervon z. B. ausgenommen.

![Verteilerliste anlegen](media/9bcd6835734a510c867311f58fbeba6b79b3fc42.png "Verteilerliste anlegen")

Geben Sie unter **Name** einen Namen für die Verteilerliste ein. Danach geben Sie im Eingabefeld unter der Überschrift **Teilnehmer** die E-Mail-Adresse oder den Namen einer Teilnehmerin oder eines Teilnehmers ein.

- Während der Eingabe werden passende Vorschläge für Sie angezeigt. Um einen Vorschlag zu übernehmen, klicken Sie darauf

Um Kontakte aus einem Adressbuch zu wählen, klicken Sie rechts im Eingabefeld auf das Symbol **Kontakte auswählen**  Falls Sie weitere Kontakte hinzufügen möchten, wiederholen Sie diese letzten Schritte entsprechend. Möchten Sie einen Kontakt entfernen, klicken Sie neben dem Kontakt auf das Papierkorb-Symbol.

Klicken Sie abschließend auf **Liste erstellen**.

![Verteilerliste erstellen](media/d2a16b8dc7af79e0c63ee25dd352463a370ad696.png "Verteilerliste erstellen")
