# Erste Schritte

Mit **Kommunikation &amp; Organisation** wird Ihnen ein vollwertiger E-Mail-Dienst zur Verfügung gestellt. Benutzerinnen und Benutzer erhalten Zugriff auf ein personenbezogenes **Postfach**, auf einen persönlichen **Kalender**, auf ein zentrales **Adressbuch** und auf eine **Aufgabenverwaltung**.

![Das Modul Kommunikation &amp; Organisation](media/d592e637ba8509495b9848d62f4eb281c1a65723.png "Das Modul Kommunikation &amp; Organisation")

Nach Auswählen des **Untermoduls E-Mail** öffnet sich die typische Ansicht eines E-Mail-Clients. Auf der linken Seite befinden sich die Ordner:

- Posteingang: Dieser Ordner speichert standardmäßig alle eingehenden E-Mails
- Entwürfe: Hier finden Sie alle E-Mails, die Sie gespeichert und noch nicht versendet haben
- Gesendet: Dieser Ordner enthält alle E-Mails, die Sie gesendet haben
- Spam: Hier werden alle E-Mails einsortiert, die als Spam-Mails markiert worden sind

Papierkorb: Hier liegen alle E-Mails, die Sie gelöscht haben  Je nach Konfiguration können weitere Ordner vorhanden sein:
- Freigegebene E-Mail-Ordner, die Sie abonniert haben

Ordner von funktionalen E-Mail-Konten, die Ihnen von der Administratorin bzw. dem Administrator zugewiesen wurden  Über die Schaltfläche **Ordnerspezifische Aktionen (Plus)** unter **Meine Ordner** können Sie einen neuen Ordner oder ein weiteres E-Mail-Konto hinzufügen.

Im mittleren Bereich des Fensters werden die E-Mails des aktuell ausgewählten Ordners als Liste angezeigt. Mit einem Doppelklick können sie geöffnet werden.

Im rechten Bereich finden Sie die Vorschau der jeweils ausgewählten E-Mail.

![Übersicht über die verschiedenen Bereiche des E-Mail-Moduls](media/ad3ca2d0d1635b948fd0086bf82feb9e495b9620.png "Übersicht E-Mail-Modul")

### Hier geht es weiter mit:

- [Einstellungen](erste-schritte/grundeinstellungen.md)
- [E-Mail schreiben](erste-schritte/e-mail-schreiben.md)
- [E-Mail beantworten](erste-schritte/e-mail-beantworten-1.md)
- [E-Mail weiterleiten](erste-schritte/e-mail-weiterleiten.md)
