# E-Mail-Ordner erstellen

Sie können beliebig viele eigene Ordner anlegen, um Ihre E-Mails zu sortieren. Dafür steht Ihnen der Bereich **Meine Ordner** zur Verfügung, Sie können jedoch auch in den vorhandenen Standard-Ordnern (ausgenommen Papierkorb) Unterordner erstellen.

Wählen Sie in der Ordneransicht den Ordner aus, in dem Sie einen neuen Unterordner anlegen möchten. Klicken Sie neben dem Ordnernamen auf die Schaltfläche **Aktionen (drei Punkte)** bzw. für den Bereich **Meine Ordner** auf **Ordnerspezifische Aktionen (Plus)**. Alternativ können Sie mit der rechten Maustaste auf den Ordnernamen klicken. Wählen Sie dann im Dropdown-Menü **Neuen Ordner hinzufügen**.

![Neuen Ordner über das Dropdown-Menü hinzufügen](media/500f0c9ebe65871c190566034338828526a36e05.png "Neuen Ordner hinzufügen")

Ein Fenster öffnet sich. Hier können Sie den neuen Ordner benennen und auf **Hinzufügen** klicken. Daraufhin wird ein neuer Ordner mit dem soeben vergebenen Namen angelegt.

![Neuen Ordner benennen und hinzufügen](media/cc7bdd72491e4e0b38990fc49fd1d4112736663b.png "Neuen Ordner benennen und hinzufügen")
