# E-Mail-Erinnerung anlegen

Sie können Erinnerungen für empfangene E-Mails einrichten, z. B. wenn Sie zu einem bestimmten Termin antworten müssen. Klicken Sie dazu in der Werkzeugleiste auf die Schaltfläche **Weitere Aktionen (drei Punkte)** . Wählen Sie im Menü den Punkt **Erinnerung**.

![Der Punkt &quot;Erinnerung&quot; im Dropdown-Menü &quot;Weitere Aktionen&quot;](media/6cb96d875d65dda8c333e68f86ed99bf07f0f337.png "Erinnerung im Dropdown-Menü")

Sie gelangen in das Fenster **Mich erinnern** . Hier können Sie einen Betreff für die Erinnerung sowie eine Notiz hinzufügen, in der Sie beispielsweise genauer definieren, um was es geht. Auch können Sie hier festlegen, wann Sie die Erinnerung erhalten möchten.

Mit Klick auf **Erinnerung erstellen** wird eine neue Aufgabe angelegt, wie sie auch im **Aufgaben-Modul** verwendet wird. Sie können auch zunächst auf **Weitere Optionen** klicken, um Ihre Erinnerung noch genauer zu konfigurieren. Hier können Sie z. B. einen Status oder eine Priorität für die Aufgabe festlegen sowie Teilnehmerinnen und Teilnehmer hinzufügen. Nähere Angaben hierzu finden Sie im Abschnitt [Neue Aufgaben anlegen](../../aufgaben/neue-aufgaben-anlegen.md).

![Das Fenster &quot;Mich erinnern&quot;](media/e4861b6c3bf7bad693941e6e8284d0149d4d1abe.png "Mich erinnern")
