# E-Mail-Regeln

Eine E-Mail-Regel hilft Ihnen, eingehende E-Mails automatisch zu organisieren. Mithilfe von Regeln können Sie zum Beispiel folgende Aktionen auslösen:

- Bestimmte E-Mails werden automatisch in einem bestimmten E-Mail-Ordner abgelegt
- Bestimmte E-Mails werden automatisch verworfen

Bestimmte E-Mails werden automatisch als gelesen markiert  Abwesenheitsbenachrichtigung und automatische Weiterleitung sind besondere, voreingestellte Formen von E-Mail-Regeln. Näheres hierzu finden Sie im Abschnitt **Weiteres**.

Im Folgenden erfahren Sie, wie Sie Ihre eigenen Regeln definieren können.

## Neue Regel erstellen

Eine Regel enthält folgende Bestandteile:

- einen Namen
- eine oder mehrere Bedingungen

eine oder mehrere Aktionen – die Aktionen werden ausgeführt, wenn eine Bedingung oder alle Bedingungen zutreffen  Um eine Regel zu erstellen, klicken Sie zunächst auf die Schaltfläche **Einstellungen (Zahnradsymbol)** oben rechts neben Ihrem Namenskürzel bzw. Profilbild. Klicken Sie dann auf **Alle Einstellungen**.

![Die Schaltfläche Einstellungen öffnet ein Dropdown-Menü mit der Option &quot;Alle Einstellungen&quot;](media/d6d685abbc52021b536c7dc0cf44b29ba4ca4658.png "Alle Einstellungen")

Sie gelangen in ein neues Fenster. Hier ist bereits der Menüpunkt **E-Mail** vorausgewählt. Scrollen Sie im rechten Bereich nach unten bis zum Reiter **Regeln** und öffnen Sie diesen mit einem Mausklick. Klicken Sie nun auf **Neue Regel hinzufügen**.

![Der Reiter &quot;Regeln&quot; in den E-Mail-Einstellungen und die Schaltfläche &quot;Neue Regel hinzufügen&quot;](media/9263746de2d266136a594b62e91f0ce82fdca8b4.png "Der Regeln-Reiter")

Bestimmen Sie im Fenster **Neue Regel erstellen** zunächst einen **Namen** für die Regel.

![Das Fenster &quot;Neue Regel erstellen&quot;](media/d7bfce1c80b59ab0414d28ad0418edac41f6c43a.png "Das Fenster &quot;Neue Regel erstellen&quot;")

## Bedingung hinzufügen

Mithilfe einer Bedingung bestimmen Sie, für welche E-Mails eine Aktion ausgeführt werden soll. Klicken Sie dazu auf **Bedingung hinzufügen** . Wählen Sie im Dropdown-Menü einen E-Mail-Bestandteil aus. Beispielsweise kann die Regel für bestimmte Absenderinnen oder Absender ( **Von** ) gelten. Oder Sie können einen **Betreff** oder **Textbestandteil** von E-Mails angeben, etwa wenn E-Mails mit bestimmten Stichwörtern für Sie nicht relevant sind und Sie diese gar nicht erst in Ihrem Posteingang sehen möchten. Sie können auch mehrere Bedingungen hintereinander auswählen, wenn Sie z. B. E-Mails von einer bestimmten Person filtern möchten, die ein bestimmtes Stichwort enthalten.

![Dropdown-Menü mit Auswahlmöglichkeiten für Bedingungen](media/aec2ff25e55e464fb35b1065d6c2603cf3da90a5.png "Bedingungen")

Beachten Sie dabei folgende **Hinweise:**

- Wenn Sie einen E-Mail-Bestandteil verwenden möchten, der nicht in der Liste enthalten ist, wählen Sie einen Header. Geben Sie unter **Name** einen Header-Eintrag ein. Sie können die Header-Einträge einer E-Mail lesen, indem Sie sich die Quelldaten anzeigen lassen
- Wenn Sie Personen in BCC berücksichtigen möchten (also Personen, die Blindkopien erhalten), wählen Sie als Bedingung **Envelope** und dann **An** . Envelope umfasst alle Empfängerinnen und Empfänger einer E-Mail, die entweder in An, CC oder BCC angegeben sind

Wenn Sie in der Bedingung das Datum des Empfangs verwenden möchten, wählen Sie **Aktuelles Datum**  Um eine Bedingung zu löschen, klicken Sie neben der Bedingung auf die Schaltfläche **Löschen (Papierkorbsymbol)**.

## Verschachtelte Bedingungen

Sobald Sie mindestens eine Bedingung ausgewählt haben, erhalten Sie die Möglichkeit, unter **Bedingung hinzufügen** die Option **Verschachtelte Bedingungen** auszuwählen. Nun können Sie im Dropdown-Menü wählen zwischen **fortsetzen, wenn irgendeine dieser Bedingungen erfüllt ist** und **fortsetzen, wenn alle diese Bedingungen erfüllt sind** . Sie können nun im umrandeten Kasten neue, untergeordnete Bedingungen hinzufügen. Sobald Sie den Kasten verlassen und darunter weitere Bedingungen hinzufügen, sind diese von den verschachtelten Bedingungen abhängig.

Die Option **fortsetzen, wenn irgendeine dieser Bedingungen erfüllt ist** ist hilfreich, wenn nur eine von mehreren Bedingungen erfüllt sein muss. Ohne diese Funktion müssten Sie für jede der Bedingungen eine eigene Regel hinzufügen. Beispiel: Sie möchten alle E-Mails von einer bestimmten E-Mail-Adresse filtern, die eines von mehreren Stichwörtern erhalten. Sind beispielsweise Anfragen zum Thema Presse für Sie ebenso wenig relevant wie solche zum Thema Marketing, so können Sie automatisch alle E-Mails filtern, die einen dieser Begriffe enthalten.

Wenn Sie **fortsetzen, wenn alle diese Bedingungen erfüllt sind** auswählen, können Sie verhindern, dass Folgebedingungen ohne Ausnahme gelten. Bedingungen, die Sie weiter unten außerhalb des umrandeten Kastens hinzufügen, werden also nur dann berücksichtigt, wenn alle verschachtelten Bedingungen innerhalb des Kastens ebenfalls erfüllt sind.

![Beispiel für verschachtelte Bedingungen](media/38d288a1d92008dc9fb7f3c3a243c3d8f51789fa.png "Verschachtelte Bedingungen")

## Aktion hinzufügen

Wenn Sie alle gewünschten Bedingungen eingestellt haben, klicken Sie auf **Aktion hinzufügen** . Hier bestimmen Sie, was mit den E-Mails passieren soll, die alle Bedingungen erfüllen. Auch hier können Sie wieder mehrere Optionen hintereinander auswählen.

![Dropdown-Menü mit Auswahlmöglichkeiten für Aktionen](media/0a7426274edc0f34911a9f589cddffde4d351f15.png "Aktionen")

Wählen Sie im Dropdown-Menü eine Aktion. Je nach Aktion werden weitere Schaltflächen und Eingabefelder angezeigt. Verwenden Sie diese Bedienelemente, um die Aktion fertigzustellen. Um eine Aktion zu löschen, klicken Sie neben der Aktion auf die Schaltfläche **Löschen (Papierkorbsymbol)**.

## Beispiel

Sie erhalten von der E-Mail-Adresse testbenutzer1@testdomain.de immer wieder Anfragen zum Thema Presse. Sie sind für solche Anfragen jedoch gar nicht zuständig, denn diese müssen an die Pressestelle gerichtet werden. Geben Sie also als Bedingungen unter **Von** die E-Mail-Adresse und unter **Betreff** (oder auch **Textbestandteil** ) den Begriff „Presse“ ein. Erhalten Sie solche Anfragen häufiger von wechselnden Absenderinnen und Absendern und möchten diese generell ablehnen, lassen Sie die Bedingung **Von** einfach aus. Als Aktion können Sie nun beispielsweise die Option **Ablehnen mit Begründung** auswählen und in der Begründung an die Pressestelle verweisen. Wählen Sie als weitere Aktion **Verwerfen** , um diese E-Mails gar nicht erst in Ihrem Posteingang zu sehen.

Klicken Sie zum Fertigstellen der neuen Regel auf **Speichern**.

![Beispielregel](media/530539b7c7f6be69bcdcf39c88a5019fe685b290.png "Beispielregel")
