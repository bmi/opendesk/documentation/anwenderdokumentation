# E-Mail löschen

## E-Mail löschen

Sie können einzelne E-Mails oder ganze Konversationen löschen. Die von Ihnen gelöschten E-Mails werden in den Papierkorb verschoben und können erst dort unwiderruflich entfernt werden.

![Papierkorb-Schaltfläche zum Löschen von E-Mails](media/2ad4c39db187eaab2a1bd7f781b4deb9a836aaf0.png "Papierkorb-Schaltfläche")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Löschen**.

![E-Mail löschen über Dropdown-Menü](media/84bf67f96e9eba9272485741371649dbdbc2565d.png "E-Mail löschen über Dropdown-Menü")

## Alle E-Mails eines Ordners löschen

Möchten Sie einen ganzen Ordner leeren, wählen Sie zunächst im Ordnermenü den Ordner aus, dessen E-Mails gelöscht werden sollen. Sie haben 2 Möglichkeiten, den gewünschten E-Mail-Ordner zu leeren:

- Klicken Sie im Ordnermenü mit der rechten Maustaste auf den Ordner und wählen Sie dann im Dropdown-Menü **Alle Nachrichten löschen**.

Klicken Sie oberhalb der E-Mail-Liste neben dem Ordnernamen auf die Schaltfläche **Weitere Optionen für Nachrichten (drei Punkte)** und dann auf **Alle Nachrichten auswählen** . Nun können Sie die Nachrichten mit der Schaltfläche **Löschen (Papierkorb-Symbol)** löschen.  Nach dem Ausführen werden die E-Mails in den Ordner **Papierkorb** verschoben.

![Alle Nachrichten eines Ordners löschen](media/578f58a2a6691174acfe2b58f90826f107addd9b.png "Alle Nachrichten löschen")

![Bestätigungsdialog, um einen Ordner zu leeren](media/0a1712da6a9274b476537455349d618a6269189b.png "Ordner leeren")

![Alle Nachrichten zum Löschen auswählen](media/965bafad7bb37dfa8dfe73dca936690268a99427.png "Alle Nachrichten auswählen")

## Gelöschte E-Mail wiederherstellen

Wenn Sie zuvor gelöschte E-Mails wiederherstellen möchten, öffnen Sie in der Ordneransicht den Ordner **Papierkorb**. Wählen Sie jetzt die E-Mail aus, die Sie wiederherstellen möchten. Sie können auch mehrere E-Mails auswählen, indem Sie die Strg-Taste gedrückt halten und die einzelnen E-Mails nacheinander anklicken.

Klicken Sie in der Werkzeugleiste auf die Schaltfläche **Verschieben**. Alternativ können Sie mit der rechten Maustaste auf die gewünschten E-Mails klicken und **Verschieben** auswählen.

![Die Verschieben-Schaltfläche](media/9d053cae5c5ff716860571078deb11e182e4a6a0.png "Die Verschieben-Schaltfläche")

![Verschieben im Dropdown-Menü](media/a14b4b7a0041e7575a43c9dc5cf459fbdbe6c1fc.png "Verschieben im Dropdown-Menü")

Wählen Sie im Dialog **Verschieben** den Ordner, in den Sie die wiederhergestellten E-Mails legen möchten. Klicken Sie dann auf die Schaltfläche **Verschieben** . Die E-Mails wurden im gewählten Ordner wiederhergestellt.

![Der Verschieben-Dialog, in dem der Zielordner ausgewählt wird](media/ac933128f3768a3bf70a40879e9a47dcd4e30775.png "Der Verschieben-Dialog")

## E-Mail endgültig löschen

Wenn Sie einzelne E-Mails endgültig löschen möchten, öffnen Sie zunächst in der Ordneransicht den Ordner **Papierkorb**. Wählen Sie nun eine oder mehrere E-Mails aus und klicken Sie in der Werkzeugleiste auf die Schaltfläche **Löschen (Papierkorb-Symbol)**. Bestätigen Sie im folgenden Dialog Ihre Auswahl mit einem weiteren Klick auf **Löschen**.

![Dialog, in dem Sie bestätigen, dass Sie E-Mails unwiderruflich löschen wollen](media/e25319498cb8c2dd2a5bd373c69c25b909f4e919.png "Unwiderruflich löschen")

## Papierkorb leeren

Wenn Sie sämtliche E-Mails im Papierkorb unwiderruflich löschen möchten, wählen Sie zunächst in der Ordneransicht den Ordner **Papierkorb** . Klicken Sie nun neben dem Ordnernamen auf die Schaltfläche **Aktionen für Papierkorb (drei Punkte)** und dann auf **Papierkorb leeren** . Bestätigen Sie, wenn Sie sämtliche E-Mails im Papierkorb löschen möchten.

![Dropdown-Menü, über das Sie den Papierkorb leeren können](media/5d8feb824abf52171f5b531561e5ddbc192bcb4f.png "Papierkorb leeren")
