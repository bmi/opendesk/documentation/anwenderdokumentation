# E-Mail importieren

E-Mails, die als eml-Datei vorliegen, können Sie importieren. Dies kann z. B. hilfreich sein, wenn Sie E-Mails zuvor aus einem anderen E-Mail-Programm exportiert haben und in Ihre aktuelle Arbeitsumgebung integrieren möchten. Öffnen Sie dafür in der Ordneransicht den E-Mail-Ordner, in den die E-Mail importiert werden soll. Öffnen Sie parallel in einem anderen Fenster den Dateiexplorer und navigieren Sie zum Ordner, in dem die eml-Datei gespeichert ist. Ziehen Sie nun die eml-Datei von dort mit gedrückter Maustaste in die E-Mail-Liste. Die eml-Datei wird in den gewählten E-Mail-Ordner geladen und ist fortan dort aufgelistet und abrufbar.

![Eine E-Mail im eml-Format wird mit gedrückter Maustaste in einen Ordner importiert](media/4150ca7910da99c0eb15aaae0065acfab1b2b386.png "E-Mail importieren")
