# Adressen sammeln

## Adressen automatisch sammeln

Sie können einstellen, dass neue E-Mail-Adressen beim Senden oder Lesen von E-Mails automatisch gesammelt werden. Klicken Sie dafür zunächst auf die Schaltfläche **Einstellungen (Zahnradsymbol)** oben rechts neben Ihrem Namenskürzel bzw. Profilbild. Klicken Sie dann auf **Alle Einstellungen**.

![Die Schaltfläche Einstellungen öffnet ein Dropdown-Menü mit der Option &quot;Alle Einstellungen&quot;](media/d6d685abbc52021b536c7dc0cf44b29ba4ca4658.png "Alle Einstellungen")

Ein neues Fenster öffnet sich. Auf der linken Seite finden Sie ein Menü, in dem der Unterpunkt **E-Mail** vorausgewählt ist. Scrollen Sie im rechten Hauptbereich ganz nach unten und klicken Sie auf den Reiter **Erweiterte Einstellungen** . Setzen Sie hier per Mausklick einen Haken bei den Punkten **Beim Versenden Kontakte automatisch im Ordner "Gesammelte Adressen" speichern** und/oder **Beim Lesen Kontakte automatisch im Ordner "Gesammelte Adressen" speichern**.

![Der Reiter &quot;Erweiterte Einstellungen&quot; in den E-Mail-Einstellungen](media/3cfcf0d48dceaa1af259144b3ec4f2de84813a29.png "Erweiterte E-Mail-Einstellungen")

## Adressen manuell hinzufügen

Sie können E-Mail-Adressen auch manuell Ihrem Adressbuch hinzufügen. Wählen Sie dazu die gewünschte E-Mail aus. Klicken Sie in der Detailansicht auf den Namen der Absenderin bzw. des Absenders oder der Empfängerin bzw. des Empfängers.

![Absenderin/Absender in der Detailansicht einer E-Mail auswählen](media/1a2c21a3797e3ecab39cdec26dff53c7bf43f550.png "Absenderin/Absender auswählen")

Sie gelangen in ein neues Fenster, das den Austausch mit dieser Person in der letzten Zeit anzeigt. Klicken Sie oben im Fenster auf die Schaltfläche **Zum Adressbuch hinzufügen**.

![Die Schaltfläche &quot;Zum Adressbuch hinzufügen&quot; im Kontaktfenster](media/e0f1a6b202878b1585208bb88d587a1081da2b7f.png "Schaltfläche &quot;Zum Adressbuch hinzufügen&quot;")
