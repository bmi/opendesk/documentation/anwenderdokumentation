# E-Mail Kategorien

Sie können E-Mails verschiedenen Kategorien zuweisen und so besser organisieren.

## Kategorien erstellen

Um E-Mails verschiedenen Kategorien zuzuweisen, müssen zunächst die verschiedenen Kategorien erstellt werden. Klicken Sie dafür im Posteingang auf den Button **Kategorien festlegen** und anschließend auf **Kategorien verwalten**.

![Kategorie festlegen-Button](media/e28f7be21fb236b8cfc0e3b378a8f58b6d0c6749.PNG "Kategorie festlegen-Button")

![Kategorie festlegen](media/8312bc55da70d33c6d18af29717a5f2f3aef53af.PNG "Kategorie festlegen")

Es öffnet sich ein Fenster, in dem bestehende Kategorien bearbeitet und gelöscht werden können. Mit einem Klick auf **Neue Kategorie** können Sie eine neue Kategorie erstellen.

![Kategorien verwalten](media/946ce1e4ac1def0f795f3116448cccb9fad1bed6.PNG "Kategorien verwalten")

Um die Kategorie zu erstellen, müssen Sie dieser einen Namen, ein Symbol und eine Farbe zuweisen.

![Kategorie erstellen](media/586791d4eeaae94234163776114b26e9aae6af75.PNG "Kategorie erstellen")

## E-Mail einer Kategorie zuweisen

Um E-Mails einer Kategorie zuzuweisen, klicken Sie auf die entsprechende Mail, dann auf **Kategorie festlegen** und anschließend auf die Kategorie. Die Mail ist jetzt dieser Kategorie zugewiesen.

![Mail einer Kategorie zuweisen](media/e58c0b7f372375b872c188c3fb183ac4e4a257e1.PNG "Mail einer Kategorie zuweisen")

Mit einem Klick auf eine Kategorie, sehen Sie alle Mails, die Sie dieser Kategorie zugewiesen haben.
