# E-Mail drucken

Der Inhalt einer oder mehrerer E-Mails kann ausgedruckt werden. Wählen Sie dafür zunächst eine E-Mail aus der Liste aus. Sie können auch mehrere E-Mails auswählen, indem Sie die **Strg-Taste** gedrückt halten und die einzelnen E-Mails nacheinander anklicken. Klicken Sie dann in der Werkzeugleiste auf die Schaltfläche **Weitere Aktionen (drei Punkte)**. Wählen Sie im Menü den Punkt **Drucken**.

![E-Mail drucken aus  dem Dropdown-Menü in der Detailansicht](media/c06fc7dfbf18abdcb480c54a76c75ccaf3a60e7c.png "E-Mail drucken \(Detailansicht\)")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Drucken**.

![E-Mail drucken über das Dropdown-Menü in der E-Mail-Liste](media/8c82d6da0f6a3f054024cefeceafbba9f4ef4c65.png "E-Mail drucken \(E-Mail-Liste\)")

Sie gelangen in das Fenster **Drucken**. Hier können Sie die Druckvorschau einsehen und bei Bedarf weitere Druckeinstellungen vornehmen:

- Wählen Sie den gewünschten Drucker aus
- Legen Sie fest, wie viele Kopien der E-Mail gedruckt werden sollen
- Wählen Sie zwischen Hoch- und Querformat
- Legen Sie fest, ob die gesamte E-Mail oder nur ein bestimmter Seitenbereich gedruckt werden soll

Wählen Sie zwischen Farbdruck und Schwarzweißdruck  Sie können das Formular mit einem Klick auf **Weitere Einstellungen** erweitern und erhalten so die folgenden Zusatzoptionen:
- Wählen Sie das gewünschte Papierformat
- Wählen Sie die Skalierung, d. h. wie groß die E-Mail gedruckt werden soll
- Wählen Sie, wie viele Seiten der E-Mail auf ein Blatt Druckerpapier passen sollen
- Legen Sie fest, ob Ränder auf den Seiten freigelassen werden und wie groß diese sein sollen

Legen Sie fest, ob Kopf- und Fußzeilen sowie Hintergrundgrafiken gedruckt werden sollen  Klicken Sie abschließend auf **Drucken**. Der Druckauftrag wird daraufhin mit den gewünschten Einstellungen an den Drucker gesendet.

**Hinweis:** Wenn Sie als Drucker **Microsoft Print to PDF** auswählen, wird die E-Mail als PDF-Datei gespeichert.

![Der Drucken-Dialog](media/d2421c43a87a65390b124b5660616a96812a6de1.png "Der Drucken-Dialog")

![Weitere Einstellungen im Drucken-Dialog](media/fc70b40510830888230c0cf40f71839c22285bbe.png "Weitere Einstellungen im Drucken-Dialog")
