# E-Mail als gelesen oder ungelesen markieren

## Einzelne E-Mails als gelesen oder ungelesen markieren

Wählen Sie zunächst eine E-Mail aus der Liste aus, die Sie als gelesen oder ungelesen markieren möchten. Sie können auch mehrere E-Mails auswählen, indem Sie die **Strg-Taste** gedrückt halten und die einzelnen E-Mails nacheinander anklicken. Klicken Sie dann in der Werkzeugleiste auf die Schaltfläche **Weitere Aktionen**. Wählen Sie **Als ungelesen markieren** oder **Als gelesen markieren**.

![Als ungelesen markieren über das Dropdown-Menü (Werkzeugleiste)](media/0e00f9009ac1f285551c3bf77416f4120115d5d0.png "Als ungelesen markieren über das Dropdown-Menü \(Werkzeugleiste\)")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Als ungelesen markieren** oder **Als gelesen markieren**.

![Als ungelesen markieren über das Dropdown-Menü (E-Mail-Liste)](media/c0ed07e5d281c5d033afcb9242d271856f04040a.png "Als ungelesen markieren über das Dropdown-Menü \(E-Mail-Liste\)")

## Alle Nachrichten eines Ordners als gelesen markieren

Möchten Sie den Inhalt eines ganzen Ordners als gelesen markieren, wählen Sie zunächst im Ordnermenü den gewünschten Ordner aus. Sie haben 2 Möglichkeiten, die E-Mails als gelesen zu markieren:

- Klicken Sie im Ordnermenü mit der rechten Maustaste auf den Ordner und wählen Sie dann im Dropdown-Menü **Alle Nachrichten als gelesen markieren**
- Klicken Sie oberhalb der E-Mail-Liste neben dem Ordnernamen auf die Schaltfläche **Weitere Optionen für Nachrichten (drei Punkte)** und dann auf **Alle Nachrichten als gelesen markieren**

![Alle E-Mails über das Dropdown-Menü als gelesen markieren (E-Mail-Liste)](media/f12e57e99ef245f11df5d55ecf5bd0705ef708be.png "Alle als gelesen markieren \(E-Mail-Liste\)")

![Alle E-Mails über das Dropdown-Menü als gelesen markieren (Ordnermenü)](media/8cc7e5aa4738511dfa4ba1310f509f3c9fb386c4.png "Alle als gelesen markieren \(Ordnermenü\)")
