# E-Mail verschieben oder kopieren

Sie haben verschiedene Möglichkeiten, E-Mails zu verschieben oder zu kopieren:

- Sie können einzelne E-Mails oder ganze Konversationen in einen anderen E-Mail-Ordner verschieben oder kopieren
- Sie können alle E-Mails eines E-Mail-Ordners verschieben

## E-Mails verschieben

Um Ihre E-Mails zu verschieben, wählen Sie zunächst eine E-Mail aus der Liste aus. Sie können auch mehrere E-Mails auswählen, indem Sie die **Strg-Taste** gedrückt halten und die einzelnen E-Mails nacheinander anklicken. Klicken Sie dann in der Werkzeugleiste auf die Schaltfläche **Verschieben**.

![E-Mails auswählen und verschieben](media/4bb485386d0a7862c805112526cdf785367426b7.png "E-Mails auswählen und verschieben")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Verschieben**.

![E-Mail verschieben über Dropdown-Menü](media/a14b4b7a0041e7575a43c9dc5cf459fbdbe6c1fc.png "E-Mail verschieben über Dropdown-Menü")

Wählen Sie im Dialog **Verschieben** den Ordner, in den Sie die ausgewählten E-Mails legen möchten. Klicken Sie dann auf die Schaltfläche **Verschieben**. Die E-Mails wurden in den gewählten Ordner verschoben.

![Der Verschieben-Dialog, in dem der Zielordner ausgewählt wird](media/ac933128f3768a3bf70a40879e9a47dcd4e30775.png "Der Verschieben-Dialog")

## Alle E-Mails eines Ordners verschieben

Möchten Sie den Inhalt eines ganzen Ordners verschieben, wählen Sie zunächst im Ordnermenü den Ordner aus, dessen E-Mails verschoben werden sollen. Sie haben 2 Möglichkeiten, die E-Mails zu verschieben:

- Klicken Sie im Ordnermenü mit der rechten Maustaste auf den Ordner und wählen Sie dann im Dropdown-Menü **Alle Nachrichten verschieben**
- Klicken Sie oberhalb der E-Mail-Liste neben dem Ordnernamen auf die Schaltfläche **Weitere Optionen für Nachrichten (drei Punkte)** und dann auf **Alle Nachrichten verschieben**

![E-Mails verschieben über das Dropdown-Menü (Ordnerliste)](media/8576cd9274e5f5bb0e782c3cc52c1c7b8fae9ceb.png "E-Mails verschieben über das Dropdown-Menü \(Ordnerliste\)")

![E-Mails verschieben über das Dropdown-Menü (E-Mail-Liste)](media/a013e3416101e5ce686b89fa073db6fe60b4a2d1.png "E-Mails verschieben über das Dropdown-Menü \(E-Mail-Liste\)")

In beiden Fällen öffnet sich ein Dialog. Wählen Sie hier den gewünschten Zielordner aus und bestätigen Sie Ihre Auswahl mit einem Klick auf **Alle verschieben**. Nach dem Ausführen werden die E-Mails in den Ordner **Papierkorb** verschoben.

![Dialog zum Verschieben aller Nachrichten eines Ordners](media/c64bb38f0f28f2e0141db8269299a2b4a85416e0.png "Alle Nachrichten verschieben")

## E-Mails kopieren

Um Ihre E-Mails zu kopieren, wählen Sie zunächst eine E-Mail aus der Liste aus. Sie können auch mehrere E-Mails auswählen, indem Sie die **Strg-Taste** gedrückt halten und die einzelnen E-Mails nacheinander anklicken. Klicken Sie dann in der Werkzeugleiste auf die Schaltfläche **Weitere Aktionen (drei Punkte)** und dann auf **Kopieren**.

![E-Mails kopieren über das Dropdown-Menü](media/a89b4f3a604c1dcfabed461a0220fd2c7b6b9718.png "E-Mails kopieren über das Dropdown-Menü")

Wählen Sie im Dialog **Kopieren** den Ordner, in den Sie die ausgewählten E-Mails kopieren möchten. Klicken Sie dann auf die Schaltfläche **Kopieren**. Die E-Mails wurden in den gewählten Ordner kopiert.

![Der Kopieren-Dialog, in dem der Zielordner ausgewählt wird](media/f0d7be3fa90dcb46676473e490fd028c023b3ed3.png "Der Kopieren-Dialog")
