# E-Mail schreiben

## Verfassen einer E-Mail

Um eine E-Mail zu schreiben, klicken Sie auf **Neue E-Mail**.

![Die Schaltfläche Neue E-Mail](media/658e26c418c5d49acd19d1eecaad5cd57dae4cc7.png "Neue E-Mail")

Sie gelangen in ein neues Fenster, in dem Sie die E-Mail verfassen und bearbeiten können.

![Fenster, in dem eine neue E-Mail verfasst wird](media/5ae6d4ba8f292b63e05bb6969bd4ccdc97e6c82e.png "E-Mail verfassen")

Das erste Feld wird beim Öffnen dieses Fensters automatisch mit Ihrer E-Mail-Adresse gefüllt. Mit Klick auf den Pfeil rechts neben Ihrer E-Mail-Adresse öffnet sich ein Dropdown-Menü, in dem Sie auswählen können, ob Ihr Name angezeigt werden soll. Sie können dort über den Punkt **Namen bearbeiten** auch einen anderen Namen eingeben, der Empfängerinnen und Empfängern angezeigt werden soll.

Im Feld **An** können Sie die E-Mail-Adresse der Empfängerin bzw. des Empfängers händisch eintippen und mit der Eingabetaste übernehmen. Während des Tippens werden Ihnen passende Vorschläge aus dem Adressbuch angezeigt, die Sie mit einem Mausklick oder mit der Eingabetaste auswählen können. Falls Sie die E-Mail-Adresse der Empfängerin bzw. des Empfängers zuvor schon in Ihrem Adressbuch abgespeichert haben oder ein gemeinsames Adressbuch mit der Person nutzen, können Sie stattdessen auch auf das **Adresslisten-Symbol** ganz rechts klicken.

Wählen Sie hier zunächst rechts im Dropdown-Menü aus, welche **Adressliste** durchsucht werden soll. Geben Sie dann die Namen von gewünschten Personen in das Suchfeld ein. Daraufhin tauchen Ergebnisvorschläge weiter unten auf. Wählen Sie hier die gesuchte Person mit einem Mausklick aus und wiederholen Sie den Vorgang, bis Sie alle gewünschten Empfängerinnen und Empfänger ausgewählt haben. Klicken Sie abschließend auf **Wählen**. Die E-Mail-Adressen werden übernommen und das Feld „An“ wird damit ausgefüllt. Sie gelangen zurück in das Fenster **Neue E-Mail**.

Hier haben Sie oben rechts außerdem die Möglichkeit, andere Personen in **CC** (Kopie) oder **BCC** (Blindkopie) zu setzen. Wenn Sie Personen in CC setzen, sieht die Empfängerin bzw. der Empfänger, dass andere eine Kopie der E-Mail bekommen haben. Die E-Mail-Adressen sind dann auch für alle Beteiligten sichtbar. Möchten Sie dies vermeiden (beispielsweise aus Datenschutzgründen), setzen Sie die gewünschten Personen in BCC.

Mit Klick auf AW können Sie das Eingabefeld **Antwort an** einblenden. Dies kann hilfreich sein, wenn die Empfängerin bzw. der Empfänger nicht Ihnen selbst, sondern beispielsweise Ihrer Führungskraft oder Urlaubsvertretung antworten soll. Geben Sie in einem solchen Fall im Feld **Antwort an** die E-Mail-Adresse der Person an, welche die Antwort erhalten soll. Gehen Sie dabei genauso vor wie oben für das Feld **An** beschrieben.

Beschreiben Sie im Feld **Betreff** kurz, worum es in der E-Mail gehen soll. Darunter haben Sie nun ein beliebig langes Textfeld zur Verfügung, in dem Sie Ihre E-Mail schreiben können.

## E-Mail formatieren

Die Werkzeugleiste unten im Fenster ermöglicht es Ihnen, E-Mails ähnlich wie in einem Textverarbeitungsprogramm zu formatieren. Folgende Funktionen stehen Ihnen von rechts nach links zur Verfügung:

- Sie können Wörter fett oder kursiv darstellen sowie unterstreichen
- Sie können Auflistungen oder Nummerierungen einfügen
- Sie können den Einzug vergrößern oder verkleinern
- Sie können die Schriftgröße verändern
- Sie können alle Formatierungen gesammelt entfernen und auf den Standard zurücksetzen
- Sie können Links und Bilder einfügen

Sie können die Text- und Hintergrundfarbe individuell anpassen  Wenn Sie die Werkzeugleiste nicht benötigen, weil Sie z. B. mit Tastenkombinationen arbeiten, können Sie diese mit dem Symbol **Werkzeugleiste ausblenden** (großes **T** neben der Schaltfläche **Senden**) ausblenden.

![Werkzeugleiste zum Formatieren von E-Mails](media/804b6aeb52eddac91f88f7aa7eec5f354b7cfc50.png "Werkzeugleiste für E-Mails")

## Anhänge hinzufügen

Selbstverständlich können Sie auch sämtliche Arten von Dateien an Ihre E-Mail anhängen. Klicken Sie dazu auf die Schaltfläche **Anhänge (Büroklammer-Symbol)** ganz unten.

![Die Schaltfläche Anhänge und benachbarte Schaltflächen](media/b82a64287224f2e86fbd1abf8b85ccd32bf6b5de.png "Anhänge")

Wenn Sie eine Datei anhängen möchten, die auf Ihrem Computer gespeichert ist, wählen Sie **Lokale Datei hinzufügen** . Sie gelangen in ein Explorer-Fenster, in dem Sie zum Speicherort der Datei navigieren können. Wählen Sie dort die gewünschte Datei aus und klicken Sie auf **Öffnen**.

Über **Von Dateien hinzufügen** können Sie Dateien anhängen, die Sie im Modul **Dateien** online gespeichert haben. Alternativ können Sie auch nur zu einer solchen Datei verlinken (**Link von Dateien hinzufügen**).

## Weitere Optionen

Über die Schaltfläche **Vorlagen** ganz unten im Fenster können Sie Vorlagen in Ihre E-Mail einfügen. Wenn Sie noch keine Vorlagen gespeichert haben, führt Sie die Schaltfläche in den **Vorlagen-Bereich der E-Mail-Einstellungen** . Hier können Sie zur Zeitersparnis häufig genutzte Textteile als Vorlagen anlegen und verwalten. Auf diese können Sie fortan über die Vorlagen-Schaltfläche zugreifen.

Mit einem Klick auf die Schaltfläche **Optionen (drei Punkte)** weiter rechts können Sie außerdem folgende Einstellungen vornehmen:

- Sie können Ihre Signatur anfügen
- Wenn Sie noch keine Signatur haben, können Sie mit **Signatur bearbeiten** eine anlegen
- Sie können unter dem Punkt **Wichtigkeit** eine Priorität für Ihre E-Mail festlegen
- Sie können Ihre Visitenkarte anhängen
- Sie können eine Lesebestätigung der E-Mail anfordern
- Sie können die E-Mail elektronisch signieren
- Falls vorhanden, können Sie einen öffentlichen Schlüssel anhängen
- Sie können HTML für die E-Mail ausschalten (**Nur Text**). Die E-Mail wird damit als reiner Text ausgegeben und etwaige Formatierungen gehen verloren. Die Werkzeugleiste kann dann nicht mehr genutzt werden. Diese Option ist nur in Ausnahmefällen relevant
- Sie können diese E-Mail als Entwurf speichern und schließen
