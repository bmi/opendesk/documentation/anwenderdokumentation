# Einstellungen

Vor der aktiven Nutzung des E-Mail-Programms sollten Sie einige **Einstellungen überprüfen und** auf Ihre Bedürfnisse **anpassen** . Um in die Einstellungen zu gelangen, klicken Sie auf die Schaltfläche **Einstellungen (Zahnradsymbol)** oben rechts neben Ihrem Namenskürzel bzw. Profilbild. Klicken Sie dann auf **Alle Einstellungen**.

![Die Schaltfläche Einstellungen öffnet ein Dropdown-Menü mit der Option &quot;Alle Einstellungen&quot;](media/d6d685abbc52021b536c7dc0cf44b29ba4ca4658.png "Alle Einstellungen")

Ein neues Fenster öffnet sich. Auf der linken Seite finden Sie ein Menü, in dem der Unterpunkt **E-Mail** vorausgewählt ist. Navigieren Sie per Mausklick zu den gewünschten Unterseiten des Menüs.

![Eine Übersicht über das Einstellungen-Menü](media/f4cd263c289ec6809f71a1403064bd174802c908.png "Das Einstellungen-Menü")

Die einzelnen Unterseiten sind in verschiedene, thematisch sortierte Reiter unterteilt. Einer dieser Reiter wird beim Auswählen der jeweiligen Unterseite automatisch geöffnet. Beim Beispiel **E-Mail** ist dies der Reiter **Lesen**.

Klicken Sie zur besseren Übersicht in den oberen Bereich, in dem Sie die Überschrift **Lesen** sowie rechts einen **Pfeil** finden. Der Reiter wird eingeklappt und Sie sehen alle verfügbaren Reiter auf einen Blick. Öffnen Sie mit einem weiteren Mausklick die gewünschten Reiter.

![Eine Übersicht über alle Reiter der E-Mail-Unterseite in den Einstellungen](media/e97d864b06bd461020b1acce18eb8d622db1e37f.png "Reiter E-Mail")

Die wichtigsten Einstellungen werden im Folgenden vorgestellt.

## Allgemein

Hier können Sie unter **Theme** das Design des Programms an Ihre Vorlieben anpassen.

Weiter unten können Sie Ihre **Sprache und Zeitzone** ändern sowie **Erweiterte Einstellungen** vornehmen.

## Benachrichtigungen

Hier legen Sie fest, welche Benachrichtigungen Sie erhalten möchten. So können Sie beispielsweise auswählen, ob bei eingehenden E-Mails ein Ton abgespielt werden soll oder ob vor anstehenden Terminen ein Countdown angezeigt werden soll. Folgende Reiter stehen Ihnen hier zur Verfügung:

- Desktop-Benachrichtigungen
- Infobereich
- E-Mail
- Kalender
- Aufgaben

## Sicherheit

Unter **Ihre Geräte** sehen Sie, welche Ihrer Geräte gerade im Arbeitsplatz angemeldet sind.

Besonders wichtig ist der Punkt **Externe Bilder in E-Mails**. Denn das Laden extern verlinkter Bilder – also Bilder, die in der E-Mail eingebunden sind, aber separat von einem Server heruntergeladen werden müssen – stellt immer ein potentielles Sicherheitsrisiko dar. Empfehlenswert ist deshalb, die Standardeinstellung beizubehalten (**Vor dem Anzeigen externer Bilder fragen**). Unter **Für folgende Absender externe Bilder immer anzeigen** können Sie jedoch die E-Mail-Adressen von Absenderinnen und Absendern eintragen, denen Sie vertrauen.

Unter **Erweiterte Einstellungen** können Sie auswählen, ob und nach welchem Zeitintervall Sie automatisch abgemeldet werden möchten.

## Konten

Verwalten Sie Unter **Ihre Konten** Ihre E-Mail-Konten. Sie können ein bereits eingerichtetes Konto bearbeiten oder löschen. Ebenso können Sie ein neues E-Mail-Konto hinzufügen.

Sie können in diesem Bereich auch **Abonnements** für externe Daten verwalten.

## E-Mail

Hier können Sie zahlreiche Einstellungen dazu vornehmen, wie Ihnen E-Mails angezeigt werden sollen (**Lesen**).

Legen Sie hier auch **Signaturen** an, in der Sie den Empfängerinnen und Empfängern Ihrer E-Mails Ihre zusätzlichen Kontaktdaten mitteilen.

Unter **Verfassen &amp; Antworten** können Sie beispielsweise einen Textstil festlegen, der für Ihre E-Mails automatisch voreingestellt werden soll.

Sie können außerdem zur Zeitersparnis häufig genutzte Textteile als **Vorlagen** anlegen und verwalten. Auf diese können Sie fortan über die Vorlagen-Schaltfläche im E-Mail-Editor zugreifen (Näheres dazu finden Sie unter **Erste Schritte – E-Mail schreiben**).

Unter **Regeln** können Sie eine Abwesenheitsbenachrichtigung einstellen und die automatische Weiterleitung eingehender E-Mails einrichten. Es stehen Ihnen außerdem vielfältige Optionen beim Anlegen individueller Regeln zur Verfügung, wenn Sie beispielsweise E-Mails mit bestimmten Stichwörtern automatisch löschen möchten.

Zuletzt können Sie hier noch **Erweiterte Einstellungen** vornehmen.

## Kalender

Legen Sie unter **Ihre Woche** Ihre übliche **Arbeitszeit** fest und richten Sie die Standardeinstellungen für **Terminerinnerungen** ein.

In diesem Bereich können Sie außerdem **Zusätzliche Zeitzonen** hinzufügen und **Erweiterte Einstellungen** zum Kalender vornehmen.

## Adressbuch

Stellen Sie ein, in welchem Format Namen in Adressbüchern angezeigt werden sollen und ob Sie Adressen aus Adressbüchern mit einem Kartendienst verknüpfen möchten. Unter **Erweiterte Einstellungen** können Sie auswählen, welche freigegebenen Adressbücher Sie abonnieren möchten.

## Portal

Unter **Portal-Widgets** können Sie einstellen, welche Widgets Ihnen auf dem Portal angezeigt werden sollen. Sie können Widgets hinzufügen, bearbeiten, deaktivieren und löschen

Unter **Erweiterte Einstellungen** können Sie auswählen, ob die Ansicht auf Smartphones auf die Widget-Übersicht reduziert werden soll.

## Guard

Hier können Sie den Guard einrichten und nach der Einrichtung weitere **Einstellungen** vornehmen. Der Guard bietet Ihnen beispielsweise Optionen zur Verschlüsselung von E-Mails und zur Verwaltung von Passwörtern.

## Ressourcen

Hier errstellen und verwalten Sie Ressourcen.
