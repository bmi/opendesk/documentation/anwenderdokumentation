# E-Mail beantworten

Wenn Sie eine E-Mail von einer Person empfangen haben und darauf antworten möchten, klicken Sie in der Detailansicht der E-Mail auf die Schaltfläche **Antworten**. Nutzen Sie die Funktion **Antwort an alle Empfänger**, um auch den Personen in CC bzw. BCC zu antworten.

![Die Schaltflächen Antwort an Absender und Antwort an alle Empfänger](media/0a19ad5cc9c2935e264b7dee1445374bcf17584b.png "E-Mail beantworten")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Antworten**, um der Absenderin bzw. dem Absender zu antworten, oder **Allen antworten**, um auch die Personen in CC/BCC einzubeziehen.

![Dropdown-Menü für E-Mails, in dem die Funktionen Antworten und Allen antworten zur Verfügung stehen](media/069a3e8c85e589276c6b4e973e10b8c1146fffe1.png "Dropdown-Menü E-Mail")
