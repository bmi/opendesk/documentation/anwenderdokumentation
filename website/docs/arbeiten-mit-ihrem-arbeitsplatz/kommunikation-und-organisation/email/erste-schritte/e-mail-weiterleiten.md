# E-Mail weiterleiten

Empfangene E-Mails können Sie an beliebige andere Personen weiterleiten. Klicken Sie dafür in der Werkzeugleiste oberhalb der Detailansicht auf die Schaltfläche **Weiterleiten**.

![](media/957992736a67ef3de093f0a30cb444cd708e2dd4.png "")

Ein neues Fenster öffnet sich. Hier können Sie die gewünschten Empfängerinnen und Empfänger auswählen sowie bei Bedarf den Inhalt der E-Mail ergänzen. Nähere Angaben zu den Funktionen dieses Fensters finden Sie unter [E-Mail schreiben](e-mail-schreiben.md). Klicken Sie abschließend auf **Senden**.

![E-Mail weiterleiten](media/6a7ca43f7d1e1f45fded8501e5a55846d07732a9.png "E-Mail weiterleiten")

Sie können alternativ auch in der E-Mail-Liste mit der rechten Maustaste auf die gewünschte E-Mail klicken. Damit öffnet sich ein Dropdown-Menü. Wählen Sie **Weiterleiten** und folgen Sie den obigen Anweisungen.

![Dropdown-Menü für E-Mails, in dem die Funktion Weiterleiten zur Verfügung steht](media/069a3e8c85e589276c6b4e973e10b8c1146fffe1.png "Dropdown-Menü E-Mail")
