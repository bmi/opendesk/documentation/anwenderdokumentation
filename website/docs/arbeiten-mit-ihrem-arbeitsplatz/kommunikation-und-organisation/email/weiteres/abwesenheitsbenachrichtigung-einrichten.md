# Abwesenheitsbenachrichtigung einrichten

Abwesenheitsbenachrichtigungen sind nützlich, wenn Sie für bestimmte Zeiträume z. B. urlaubsbedingt Ihre E-Mails nicht abrufen können. Eine Abwesenheitsbenachrichtigung wird den Absenderinnen und Absendern eingehender E-Mails während Ihrer Abwesenheit automatisch zugestellt. So können Sie Absenderinnen und Absender ganz einfach über Ihre Abwesenheit informieren und Angaben zu Ihrer Vertretung machen.

Um eine Abwesenheitsbenachrichtigung einzurichten, klicken Sie zunächst auf die Schaltfläche **Einstellungen (Zahnradsymbol)** oben rechts neben Ihrem Namenskürzel bzw. Profilbild. Klicken Sie dann auf **Abwesenheitsbenachrichtigung**.

![Die Schaltfläche Einstellungen öffnet ein Dropdown-Menü mit der Option &quot;Alle Einstellungen&quot;](media/d6d685abbc52021b536c7dc0cf44b29ba4ca4658.png "Alle Einstellungen")

Sie gelangen in ein neues Fenster. Hier ist bereits der Menüpunkt **E-Mail** vorausgewählt. Scrollen Sie im rechten Bereich nach unten bis zum Reiter **Regeln** und öffnen Sie diesen mit einem Mausklick. Klicken Sie nun auf **Abwesenheitsbenachrichtigung**.

![Der Reiter &quot;Regeln&quot; in den E-Mail-Einstellungen und die Schaltfläche &quot;Abwesenheitsbenachrichtigung&quot;](media/9263746de2d266136a594b62e91f0ce82fdca8b4.png "Der Regeln-Reiter")

Sie gelangen in ein neues Fenster. Um die Details Ihrer Abwesenheitsbenachrichtigung einstellen zu können, müssen Sie diese zunächst aktivieren. Klicken Sie dazu auf die Schaltfläche **Abwesenheitsbenachrichtigung aktivieren/deaktivieren** oben links.

![Das Fenster &quot;Abwesenheitsbenachrichtigung&quot;](media/3b925ed8131d73961f1794c59e6f12da80b9521a.png "Das Fenster &quot;Abwesenheitsbenachrichtigung&quot;")

Setzen Sie einen Haken bei **Abwesenheitsbenachrichtigung nur für diese Dauer versenden** , wenn Sie den Zeitraum Ihrer Abwesenheit vorher festlegen möchten. Sie können dann bei **Beginn** und **Ende** jeweils das Datum auswählen, an dem Ihre Abwesenheit beginnt bzw. endet. Klicken Sie dazu in das entsprechende Feld und tippen Sie das Datum entweder von Hand ein oder wählen Sie es aus dem Kalender aus, der sich öffnet.

Lassen Sie den Haken an dieser Stelle weg, wenn die Abwesenheitsbenachrichtigung ab sofort gelten soll und Sie noch nicht genau wissen, wie lange Sie nicht erreichbar sein werden, z. B. bei Krankheit.

![Dauer der Abwesenheit einstellen](media/4809df0480b6815c8af07faf9bfd662cc23eca82.png "Dauer der Abwesenheit einstellen")

Geben Sie nun einen **Betreff** sowie die eigentliche **Nachricht** ein. Informieren Sie Absenderinnen und Absender z. B. über die Dauer Ihrer Abwesenheit und eine eventuelle Vertretung oder Weiterleitung. Sie können hier beliebige weitere Angaben machen, die in Bezug auf Ihre Abwesenheit relevant sind.

Unter **Erweiterte Optionen anzeigen** können Sie bei Bedarf festlegen, wie viele Tage verstreichen sollen, bis die gleiche Absenderin bzw. der gleiche Absender nochmals Ihre Abwesenheitsbenachrichtigung erhält. Falls Sie mehrere E-Mail-Konten eingerichtet haben, können Sie hier außerdem festlegen, von welchem der Konten die Benachrichtigung versendet werden soll.

Klicken Sie abschließend auf **Änderungen übernehmen**.

![Betreff, Nachricht und erweiterte Optionen für Abwesenheitsbenachrichtigungen](media/71d38dff149e11587352cbd725f5d838b8fdd094.png "Abwesenheitsoptionen")

Wenn eine Abwesenheitsbenachrichtigung aktiv ist, wird in der E-Mail-Anwendung oberhalb der E-Mail-Liste ein Hinweistext angezeigt. Klicken Sie auf diesen Hinweistext, um in das Bearbeitungsfenster zu gelangen und Ihre Abwesenheitsbenachrichtigung zu ändern.

**Tipp:** Die Abwesenheitsbenachrichtigung ist eine besondere Art der E-Mail-Regel. Sie können die Abwesenheitsbenachrichtigung daher auch bei den Einstellungen für **E-Mail-Regeln** bearbeiten. Wie Sie dorthin gelangen, finden Sie im Abschnitt **E-Mail verwalten – E-Mail-Regeln**.

![Der Hinweistext &quot;Ihre Abwesenheitsbenachrichtigung ist aktiv&quot; über der E-Mail-Liste](media/91fdb82f4921614293b07dbb3c223d45d449dd49.png "Hinweistext Abwesenheitsbenachrichtigung")
