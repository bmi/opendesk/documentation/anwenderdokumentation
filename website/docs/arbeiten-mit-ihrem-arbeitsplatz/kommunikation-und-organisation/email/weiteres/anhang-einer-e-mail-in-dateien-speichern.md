# Anhang einer E-Mail in Dateien speichern

Sie können E-Mail-Anhänge im Modul **Dateien** speichern. Wählen Sie dazu in der E-Mail-Liste eine E-Mail mit Anhängen aus.

In der Detailansicht wird Ihnen neben dem **Büroklammersymbol** angezeigt, wie viele Anhänge die E-Mail hat. Wenn Sie alle Anhänge im Modul Dateien speichern möchten, klicken Sie auf **In Dateien speichern** weiter rechts. Sie können aber auch einzelne Anhänge speichern. Klicken Sie dazu links auf die Anhänge, um eine Übersicht der einzelnen Dateien aufzuklappen. Nun können Sie mit der rechten Maustaste auf die gewünschten Anhänge klicken und **In Dateien speichern** auswählen.

![Die Schaltfläche &quot;In Dateien speichern&quot; für alle Anhänge](media/741f46a58bf884c1da1c59859238bb397741c697.png "In Dateien speichern")

![Die Schaltfläche &quot;In Dateien speichern&quot; für einzelne Anhänge](media/b9f107c1bd102d12c5fae0c3233426eb92d4b28f.png "In Dateien speichern")

Nun öffnet sich ein Fenster, in dem Sie den Speicherort für die gewünschten Anhänge auswählen können. Sie können auch einen neuen Ordner anlegen, in dem die Anhänge gespeichert werden.

Wenn Sie Ihre gespeicherten Anhänge abrufen möchten, müssen Sie in das Modul **Dateien** wechseln. Navigieren Sie dafür zur Startseite Ihres Arbeitsplatzes und wählen Sie unter **Produktivität** das Modul **Dateien** aus. Wechseln Sie nun unter **Alle Dateien** zu dem Ordner, den Sie zuvor ausgewählt haben. Hier finden Sie Ihre gespeicherten Anhänge.
