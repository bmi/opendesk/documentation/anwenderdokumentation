# E-Mail automatisch weiterleiten

Sie haben die Möglichkeit, eingehende E-Mails automatisch an eine andere E-Mail-Adresse weiterleiten zu lassen. Klicken Sie dafür zunächst auf die Schaltfläche **Einstellungen (Zahnradsymbol)** oben rechts neben Ihrem Namenskürzel bzw. Profilbild. Klicken Sie dann auf **Alle Einstellungen**.

![Alle Einstellungen im Dropdown-Menü](media/d6d685abbc52021b536c7dc0cf44b29ba4ca4658.png "Alle Einstellungen")

Sie gelangen in ein neues Fenster. Hier ist bereits der Menüpunkt **E-Mail** vorausgewählt. Scrollen Sie im rechten Bereich nach unten bis zum Reiter **Regeln** und öffnen Sie diesen mit einem Mausklick. Klicken Sie nun auf **Automatische Weiterleitung**.

![Der Reiter &quot;Regeln&quot; in den E-Mail-Einstellungen und die Schaltfläche &quot;Abwesenheitsbenachrichtigung&quot;](media/9263746de2d266136a594b62e91f0ce82fdca8b4.png "Der Regeln-Reiter")

Im folgenden Dialog müssen Sie zunächst auf die Schaltfläche **Automatische Weiterleitung aktivieren/deaktivieren** klicken. Geben Sie dann im Textfeld die E-Mail-Adresse ein, an die Ihre E-Mails automatisch weitergeleitet werden sollen.

Sie können außerdem einstellen, ob Sie eine **Kopie der Nachricht behalten** möchten. Wenn nach der automatischen Weiterleitung weitere Filterregeln angewendet werden sollen, aktivieren Sie **Gegen nachfolgende Regeln prüfen** . Näheres zu Regeln finden Sie unter **E-Mail verwalten – E-Mail-Regeln**.

Klicken Sie abschließend auf **Änderungen übernehmen** . Die automatische Weiterleitung wird als E-Mail-Regel eingetragen.

![Der Dialog &quot;Automatische Weiterleitung&quot;](media/0b9666ea2eea57e12140daf631da347e79c9f755.png "Automatische Weiterleitung")

Wenn Sie später Ihre Einstellungen ändern oder die automatische Weiterleitung deaktivieren möchten, navigieren Sie wieder zum Reiter **Regeln** in den E-Mail-Einstellungen, wie oben beschrieben.
