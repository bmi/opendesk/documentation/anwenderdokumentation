# E-Mail stellvertretend senden, bearbeiten und verwalten

Je nachdem, welche Berechtigungen Sie und andere Nutzerinnen und Nutzer haben, können Sie folgende Funktionen ausführen:

- Im Posteingang der Nutzerinnen und Nutzer E-Mails lesen
- Im Posteingang der Nutzerinnen und Nutzer E-Mails bearbeiten, verwalten oder löschen
- Im Namen der Nutzerinnen und Nutzer E-Mails senden

## E-Mail stellvertretend senden

Wenn Sie eine E-Mail stellvertretend versenden möchten, haben Sie folgende Möglichkeiten:

- Klicken Sie auf **Neue E-Mail** . Klicken Sie nun im E-Mail-Bearbeitungsfenster neben **Von** auf die E-Mail-Addresse. Wählen Sie die Person aus, für die Sie stellvertretend die E-Mail versenden möchten

Öffnen Sie in der Ordneransicht unter **Freigegebene Ordner** den Posteingang der Person, die Sie als Stellvertreterin oder Stellvertreter bestimmt hat. Klicken Sie nun auf **Neue E-Mail** . Neben **Im Auftrag von** ist die Person eingetragen, für die Sie stellvertretend die E-Mail versenden möchten  Verfassen und versenden Sie Ihre E-Mail, wie unter **Erste Schritte – E-Mail schreiben** beschrieben.

**Hinweis:** Je nach Konfiguration enthält der Text der E-Mail am Ende einen Hinweis darauf, dass die E-Mail von einer Stellvertreterin bzw. einem Stellvertreter gesendet wurde.

Die Empfängerin bzw. der Empfänger sieht im Anzeigebereich mehrere Absenderinnen bzw. Absender:

- Die Person, für die Sie stellvertretend die E-Mail versendet haben

Die Stellvertreterin bzw. den Stellvertreter (in diesem Fall Sie)  Wenn die Empfängerin bzw. der Empfänger auf die E-Mail antwortet, wird die Antwort sowohl an Sie als auch an die Person gesendet, für die Sie stellvertretend die E-Mail versendet haben.

## E-Mail stellvertretend lesen, bearbeiten oder organisieren

Öffnen Sie in der Ordneransicht unter **Freigegebene Ordner** den Posteingang der Person, die Sie zur Stellvertreterin bzw. zum Stellvertreter bestimmt hat. Je nachdem, welche Berechtigungen Ihnen erteilt wurden, können Sie unter anderem die folgenden Aktionen ausführen:

- Wenn Sie Betrachterin bzw. Betrachter sind, können Sie alle E-Mails lesen. Sie können einzelne E-Mails als gelesen markieren, mit einer Farbe markieren oder drucken
- Wenn Sie Bearbeiterin bzw. Bearbeiter sind, können Sie zusätzlich alle E-Mails im Posteingang als gelesen markieren
- Wenn Sie Verfasserin bzw. Verfasser sind, können Sie zusätzlich Unterordner anlegen, einzelne E-Mails verschieben oder alle E-Mails im Posteingang verschieben
