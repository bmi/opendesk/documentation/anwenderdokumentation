# Chat &amp; Zusammenarbeit starten

## Aufruf des Zusammenarbeitsmoduls

**Chat &amp; Zusammenarbeit** ist ein digitaler Veranstaltungsraum.

Dazu gehören eine Video- und Audiokonferenz-Lösung, eine Chat-Funktion und die Möglichkeit, Bildschirminhalte wie z. B. Präsentationen zu teilen. Es können Dateien ausgetauscht werden. Die gemeinsame Arbeit mit einem Whiteboard (digitale Tafel) und einem Notizbuch ist möglich und es können einfache und erweiterte Abstimmungen durchgeführt werden.

Bitte verwenden Sie einen Chromium-basierten Browser wie z. B. **Microsoft Edge Chromium, Opera oder Google Chrome**. Die Nutzung anderer Internet-Browser wie Mozilla Firefox oder Safari kann zu Funktionalitätseinschränkungen führen. Bitte verwenden Sie einen Laptop oder Computer und keine mobilen Endgeräte auf Basis von iOS sowie Android. Stellen Sie sicher, dass Sie eine stabile Internetverbindung haben. Verwenden Sie am besten ein Headset und keinen Lautsprecher. Stellen Sie sicher, dass in den Browsereinstellungen die richtige Kamera und das richtige Headset ausgewählt sind und die Verwendung dieser zugelassen ist.

Sie finden das Modul ganz unten auf der Startseite des Portals als Teil des Abschnitts **Direkte Kommunikation**.

![Die Modulübersicht von openDesk](media/c138d45ee9b60521456074d02500e943fb5bb678.png "Die Modulübersicht von openDesk")

Öffnen Sie **Chat &amp; Zusammenarbeit** über die entsprechende Schaltfläche.

![Chat &amp; Zusammenarbeit in der Modulübersicht](media/b941189cacf325d185eb4a8eab92546e1bf56657.png "Chat &amp; Zusammenarbeit in der Modulübersicht")
