# Chat

## 1:1-Chats führen

Sie können mit anderen Personen 1:1-Chats führen. Dies ist unabhängig von geplanten Besprechungen möglich. Ebenso können Sie Räume einrichten, in die Sie mehrere Personen zum Gruppenchat einladen.

Um eine Person zu einem 1:1-Chat einzuladen, klicken Sie auf das **Plus** und wählen Sie **Neue Direktnachricht** aus.

![Fenster zum Schreiben einer neuen Direktnachricht](media/6aeb87e76b3b50d2742109216c988e723f052109.PNG "Fenster zum Schreiben einer neuen Direktnachricht")

Es öffnet sich ein neues Fenster. Tragen Sie den **Namen der Person** ein, mit der Sie einen 1:1-Chat starten möchten. Der Name erscheint unter **Vorschläge**. Klicken Sie auf den Namen, um die gewünschte Person auszuwählen. Mit **Los** beginnen Sie den Chat.

![Chat beginnen](media/d846039d869575a0218abc8d224a370cac11af30.jpg "Chat beginnen")

Die von Ihnen eingeladene Person erhält in ihrer Ansicht auf der linken Seite eine **Benachrichtigung** darüber, dass Sie einen 1:1-Chat gestartet haben.

![Benachrichtigung über Chateinladung](media/a6bb48e06929b1ebfb8e226c441f0b3535851a80.png "Benachrichtigung über Chateinladung")

Die eingeladene Person muss auf **Unterhaltung beginnen** klicken, um die Einladung anzunehmen.

![Einladung-zum-Chat-Unterhaltung-beginnen](media/79322d7f6a0fdee3335c34f3b2695eb7329f3429.jpg "Einladung-zum-Chat-Unterhaltung-beginnen")

Nun können Sie zu zweit einen privaten Chat führen.

![Chatfenster](media/16a9991458b5bfe2ede336431aca68a4f372162c.jpg "Chatfenster")

## Gruppenchats erstellen und führen

Es können öffentliche und private Gruppenchats erstellt werden. Hierzu müssen Sie einen sogenannten Raum eröffnen. Im Gegensatz zu den Chats in Besprechungsterminen werden hier Informationen **dauerhaft gespeichert**. Klicken Sie auf das **Plus** rechts neben den Räumen. Wählen Sie nun **Neuer Raum** aus.

![Fenster zum Erstellen eines neuen Raums](media/81f019f8f23d6e2d58d45654b16a20f9a27bc809.PNG "Fenster zum Erstellen eines neuen Raums")

Es erscheint ein neues Fenster, in dem Sie den **Raumnamen** und optional ein Thema eingeben können. Außerdem können Sie entscheiden, ob der Raum öffentlich zugänglich sein soll.

![Raum-erstellen](media/28a01ee4c355e55dbf8b4ae3c1254ed2e92d6ab2.jpg "Raum-erstellen")

Für einen öffentlichen Raum müssen Sie zusätzlich eine Internetadresse festlegen, unter der dieser Raum erreichbar sein soll. Wählen Sie eine Adresse ohne Leerzeichen und Umlaute, wie im Beispiel.

![Öffentlichen-Raum-erstellen](media/e56009a66567fd8e1c8e6a7f352b57fadea50c33.jpg "Öffentlichen-Raum-erstellen")

Klicken Sie auf **Raum erstellen**, sobald Sie die gewünschten Einstellungen vorgenommen haben.

Sie können nun über die **Raum-Info** weitere Teilnehmerinnen und Teilnehmer hinzufügen. Klicken Sie hierzu auf das **Informations-Symbol** oben rechts, um die Raum-Info zu öffnen. Klicken Sie dann auf **Personen**.

![Personen-im-Testraum](media/f64ce32665abb85f47e998920f7f7984641d8172.jpg "Personen-im-Testraum")

Daraufhin werden Ihnen die Teilnehmerinnen und Teilnehmer des Raumes angezeigt. Sie können nun über **In diesen Raum einladen** weitere Teilnehmerinnen und Teilnehmer hinzufügen.

![In-diesen-Raum-einladen](media/272f06cb727495fd6ebb22c6f948338644890f73.jpg "In-diesen-Raum-einladen")
