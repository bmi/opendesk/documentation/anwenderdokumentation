# Überblick

Sie sehen auf der linken Seite Ihren persönlichen Bereich und in der Mitte den Hauptbildschirm, in dem die Veranstaltung stattfindet. Oben rechts können Sie die verschiedenen Module aktivieren.

![Startbildschirm Kollaboration](media/0924e1f7329f0041aa22842b863cc45bc0a5e370.jpg "Startbildschirm Kollaboration")

![Menueleiste-Icons-fuer-die-Videokonferenz](media/094d483a89975851b791b574512c0a40562cd942.jpg "Menueleiste-ViKo")

Von links nach rechts: 

- Breakoutsessions, 
- Meeting-Steuerung, 
- Videokonferenz, 
- Abstimmungen, 
- Whiteboard. 

Die grauen Symbole daneben zeigen 

- den laufenden Anruf oder Videoanruf, 
- Widgets verstecken, 
- Suchen, 
- Threads und 
- die Raum-Informationen.

Wenn Sie am linken Rand auf das Plus-Symbol (+) klicken, eröffnen Sie einen neuen Bereich. Dadurch wird die Organisation erleichtert. Dies funktioniert allerdings nur, wenn Sie einen Account besitzen.

Die aktuell moderierende Person kann auch während des laufenden Meetings die Moderation an Sie oder andere Personen im Meeting abgeben. Sie haben dann die Möglichkeit, Ihren Bildschirm zu teilen und die Moderation zu übernehmen. Manchmal kann es vorkommen, dass Sie beim ersten Mal auf **Fortfahren** klicken müssen, damit es funktioniert.

Sie können während der Veranstaltung die Größe der einzelnen Module anpassen, indem Sie mit der Maus auf den Rahmen der Module klicken und diese ziehen. Dies funktioniert sowohl in die Breite als auch in die Höhe.

Über den Info-Button ganz oben rechts können Sie zusätzlich die Rauminformationen ein- und ausblenden.
