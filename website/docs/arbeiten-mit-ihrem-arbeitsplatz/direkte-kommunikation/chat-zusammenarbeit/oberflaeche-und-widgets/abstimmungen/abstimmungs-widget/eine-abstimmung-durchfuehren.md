# Eine Abstimmung durchführen

Starten Sie eine Abstimmung um diese durchzuführen. Alle vorbereiteten Abstimmungen finden Sie im Bereich **Nicht gestartete Abstimmungen.** Klicken Sie auf den Button **Starten** der Abstimmung, die Sie durchführen möchten. Im nächsten Dialog wird Ihnen eine Vorschau Ihrer Abstimmung angezeigt, in der Sie ein letztes Mal prüfen können, ob alles richtig eingestellt ist.

![Abstimmung-starten-oder-bearbeiten](media/d20cf6bf15e566c34c6019bfc0475c4377c67c6f.jpg "Abstimmung-starten-oder-bearbeiten")

Wenn Sie noch Änderungen vornehmen möchten, klicken Sie auf **Abbrechen** und bearbeiten Sie Ihre Abstimmung, wie weiter oben beschrieben.

Ist alles so, wie es sein soll? Dann klicken Sie in der Vorschau Ihrer Abstimmung ebenfalls auf den Button **Starten** , um die Abstimmung zu aktivieren.

**Hinweis:** Sobald eine Abstimmung gestartet wird, ist es nicht mehr möglich, diese zu bearbeiten oder vor Ablauf der festgelegten Dauer zu beenden.

Nach dem Starten ist die Abstimmung für die eingestellte Dauer aktiv. Sie wird nun allen Teilnehmerinnen und Teilnehmern Ihrer Videokonferenz angezeigt. Alle können den Titel, die Details der Abstimmung und die Beschreibung sehen. Stimmberechtigte Teilnehmerinnen und Teilnehmer sehen darüber hinaus die Abstimmungsoptionen.

![laufende Abstimmung](media/5dfa394ce31a61aabf544a9c6674ed5aa3ecbe68.jpg "laufende Abstimmung")

**Hinweis:** In der Abstimmung sind alle **Teilnehmerinnen und Teilnehmer** Ihrer Besprechung mit den Nutzerrollen **Assistent:in** und **Moderator:in** stimmberechtigt. Um Stimmrechte zu verteilen und zwischen Teilnehmerinnen und Teilnehmern mit und ohne Stimmrecht zu unterscheiden, können Sie über den Button **Raum-Info** oben rechts eine Übersicht aller Personen in dieser Besprechung aufrufen.

Während einer aktiven Abstimmung sind die ausgewählten Antwortoptionen aktiv klickbar und die eingestellte Dauer wird am rechten Rand Ihrer Abstimmung in farbigen Bannern angezeigt und sekündlich heruntergezählt. Die Farben wechseln je nach verbleibender Zeit: **Grün:** es ist noch mehr als eine Minute verbleibende Zeit verfügbar **Gelb:** es ist weniger als 30 Sekunden verbleibende Zeit verfügbar **Rot:** es ist weniger als 10 Sekunden verbleibende Zeit verfügbar

![Abstimmungsergebnisse](media/ab073c5da159838e2a97ab3e3931912e8ccf2d60.jpg "Abstimmungsergebnisse")

Sobald eine stimmberechtigte Person eine Antwort abgegeben hat, sind die Live-Ergebnisse verfügbar. Dafür müssen Sie die Option **Sichtbar** unter **Live-Ergebnisse** aktiviert haben. Durch einen Klick auf den Button **Live-Ergebnis** können diese angezeigt werden.

Die Live-Ergebnisse enthalten ein Diagramm aller Antwortoptionen ( **Ja, Nein** oder **Enthaltung** ) sowie eine Legende und eine Liste der Benutzernamen aller Stimmberechtigten, die an der Abstimmung teilgenommen haben. Nach Ende einer Abstimmung können in den Ergebnissen ungültigen Stimmen auftauchen. Diese kommen zustande, wenn Stimmberechtigte nicht abgestimmt haben. Hat eine stimmberechtigte Person keine Antwort gegeben, wird dies als ungültige Stimme gewertet und im Diagramm entsprechend ausgewiesen.
