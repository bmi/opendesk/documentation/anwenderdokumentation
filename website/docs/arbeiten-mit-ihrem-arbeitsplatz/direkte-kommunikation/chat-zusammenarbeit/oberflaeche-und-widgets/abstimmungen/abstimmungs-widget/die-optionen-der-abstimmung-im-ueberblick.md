# Die Optionen der Abstimmung im Überblick

Nehmen Sie hier in allen Pflichtfeldern die relevanten Eingaben vor, stellen Sie eine passende Dauer für die Abstimmung ein und treffen Sie eine Entscheidung darüber, ob die Namen der Stimmberechtigten in den Ergebnissen auftauchen sollen und ob die Ergebnisse während der Abstimmung sichtbar sein sollen. Wenn alle Angaben erfolgt sind, klicken Sie auf den Button **Speichern** , um Ihre Abstimmung zu sichern. Sie ist nun im Bereich **Nicht gestartete Abstimmungen** verfügbar.

## Titel

Dieses Pflichtfeld gibt Auskunft darüber, wie die Umfrage heißt.

## Beschreibung

Die Beschreibung soll die Stimmberechtigten mit allen nötigen Informationen versorgen, die sie für die Abgabe ihrer Stimme brauchen. Schreiben Sie in dieses Pflichtfeld einen kurzen Text mit den relevanten Daten.

## Frage

Die Frage ist der Kernpunkt Ihrer Abstimmung und daher ebenfalls ein Pflichtfeld. Sie sollte sich aus der Beschreibung ergeben und so formuliert sein, dass sie mit **Ja** oder **Nein** beantwortet werden kann.

## Antworttyp

Wählen Sie hier zwischen zwei Antworttypen: **Ja** oder **Nein** und **Ja** oder **Nein** oder **Enthaltung** . Diese Auswahl ist ebenfalls ein Pflichtfeld.

**Hinweis:** Diese Pflichtfelder sind standardmäßig leer und haben auch keinen Standardwert. Erst wenn Sie hier Einträge vornehmen und Entscheidungen treffen, wird die Schaltfläche **Speichern** aktiv.

## Dauer in Minuten

Hier können Sie die Zeit in Minuten festlegen, die den Stimmberechtigten für die Abgabe ihrer Stimme zur Verfügung steht. Diese Option ist standardmäßig auf 1 Minute eingestellt und kann bei Bedarf angepasst werden.

## Abstimmungstyp

Es stehen Ihnen zwei Abstimmungstypen zur Verfügung: **Offene Abstimmung:** In dieser Abstimmung zählen nur die Ergebnisse, es werden keine Namen der Stimmberechtigten erfasst. **Namentliche Abstimmung:** Bei dieser Variante werden die Stimmberechtigten mit ihren Benutzernamen in den Auswertungen ((Live-)Ergebnisse und PDF- Dokumentation) aufgeführt. So bleibt das Ergebnis nachvollziehbar.

## Live-Ergebnisse

Mit dieser Option können Sie festlegen, ob die Stimmberechtigten während der Abstimmung sehen können, wie die bisher abgegebenen Stimmen verteilt sind. **Hinweis:** Bitte bedenken Sie, dass die Live-Ergebnisse die Antwort einzelner Teilnehmerinnen und Teilnehmer beeinflussen können.
