# Eine Abstimmung bearbeiten oder löschen

Vor dem Starten einer Abstimmung ist es möglich, die jeweilige Abstimmung zu bearbeiten oder zu löschen.

Zum Bearbeiten einer Abstimmung klicken Sie auf das Drei-Punkte-Menü in der oberen rechten Ecke der vorbereiteten Abstimmung. Es öffnet sich der Dialog **Abstimmung bearbeiten** und es stehen die zuvor beschriebenen Optionen zur Verfügung.

Jede vorbereitete Abstimmung kann über das Drei-Punkte-Menü in der oberen rechten Ecke der vorbereiteten Abstimmung auch gelöscht werden.

**Hinweis:** Eine gelöschte Abstimmung kann nicht wiederhergestellt werden. Sie muss neu angelegt werden.

![Abstimmung-bearbeiten-löschen](media/0c847fc1f86dc881ff962507b013937a70695982.jpg "Abstimmung-bearbeiten-löschen")
