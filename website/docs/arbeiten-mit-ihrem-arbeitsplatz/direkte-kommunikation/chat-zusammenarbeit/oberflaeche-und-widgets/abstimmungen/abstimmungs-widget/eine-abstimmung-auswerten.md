# Eine Abstimmung auswerten

Nach Ablauf der Dauer einer Abstimmung wird diese automatisch in den Bereich **Abgeschlossene Abstimmungen** verschoben. Klicken Sie auf **Ergebnis ansehen** um auf die Ergebnisse zuzugreifen.

![Abstimmung-Ergebnis-Anzeigen](media/ed67ed81118ae4636585f1ecc8b8e53606760eef.jpg "Abstimmung-Ergebnis-Anzeigen")

Darüber hinaus ist nach Durchführung der ersten Abstimmung der Bereich **Dokumentation** für die erstellende Person von Abstimmungen verfügbar. Klicken Sie auf PDF Dokumenation generieren und laden Sie anschließend die Datei herunter. Es kann sein, dass die Dokumentation der Ergebnisse im PDF-Format nur den Moderatorinnen und Moderatoren zur Verfügung steht.

![Ergebnis-PDF-Dokumentation](media/f45d0b52b65e503ad9c0ddc9ba2f4ce108c60bb7.jpg "Ergebnis-PDF-Dokumentation")

Die PDF-Dokumentation kann für alle Abstimmungstypen abgerufen werden. Je nachdem, ob eine offene- oder eine namentliche Abstimmung durchgeführt wurde, bietet die PDF-Dokumentation einen anderen Detailgrad:
- **Offene Abstimmungen** werden mit einer übersichtlichen Ergebnistabelle ausgewertet, die alle Informationen und Optionen der Umfrage enthält.

**Namentliche Abstimmungen** werden in der PDF-Dokumentation zusätzlich zu den Informationen der offenen Abstimmung mit einer vollständigen **Liste aller Stimmberechtigten inklusive gewählter Antwortoption** ausgewiesen.  **Hinweis:** Im Falle mehrerer durchgeführter Abstimmungen werden **alle Abstimmungen in *einer* PDF-Datei** ausgewiesen. Dabei beginnt jede Abstimmung auf einer neuen Seite, um die Übersicht zu erhalten. Zum aktuellen Zeitpunkt ist es nicht möglich, pro Abstimmung eine eigene Datei zu erhalten.

Wählen Sie über die Schaltfläche **Mehr Einstellungen** den Zeitraum (in Wochen), für den der Download verfügbar ist. Oder deaktivieren Sie die Download-Möglichkeit unmittelbar.

![PDF-Verfügbarkeit-in-Wochen-einstellen](media/7b36b0f7351d4e0ae39c70c826597bf08a1ac7de.jpg "PDF-Verfügbarkeit-in-Wochen-einstellen")
