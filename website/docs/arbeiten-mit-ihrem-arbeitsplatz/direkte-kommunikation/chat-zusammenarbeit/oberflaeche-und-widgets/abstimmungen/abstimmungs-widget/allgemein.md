# Allgemein

Beim Erstellen eines Termins stehen Ihnen mehrere Widgets zur Verfügung. Für das Durchführen einer Abstimmung ist mindestens das Widget **Abstimmungen** auszuwählen. Optional könnten Sie zum Beispiel eine **Videokonferenz** in Ihrem Termin aktivieren. Wenn das Widget **Abstimmungsgruppen** ausgewählt wird, müssen Gruppen angelegt werden, um eine Abstimmung mit Gruppen durchführen zu können.

![Widgets auswählen bei Besprechungsplanung](media/647df2f6e20d89fce9721c68892ebc255767f57f.jpg "Widgets auswählen bei Besprechungsplanung")

Nach Betreten des Termins stehen Ihnen über die Widgets-Auswahl oben rechts die aktivierten Widgets zur Verfügung. Nach einem Klick auf **Zeige Abstimmungen** öffnet sich das Widget Abstimmungen.

![Schaltfläche-Zeige-Abstimmungen](media/5851ad1d4934ec3c7442d1eb0cda193bb1ef4155.jpg "Schaltfläche-Zeige-Abstimmungen")

Von dort aus kann das Abstimmungs-Widget auch in die rechte Seitenleiste gelegt werden. Dazu reicht es aus, über die 3 Punkte in der oberen rechten Ecke des Widgets die Option **Nicht mehr anheften** zu wählen.

**Hinweis:** Die Funktion Abstimmungsgruppen-Widget des Abstimmungs-Widgets können Sie vollumfänglich nutzen. Unabhängig davon, ob das Abstimmungs-Widget im Zentrum eines Termins mit Videokonferenz oder in der rechten Seitenleiste verwendet wird.

Wenn Sie das Abstimmungs-Widget zum ersten Mal öffnen, enthält es noch keine Abstimmung und weist Sie entsprechend mit einer Meldung darauf hin. Am unteren Rand des Widgets können Sie durch einen Klick auf den Button **+ Neue Abstimmung** eine neue Abstimmung erstellen.

![Neue-Abstimmung-erstellen](media/7e3df5526f293b5e3889373e9cb557ecf00acb75.jpg "Neue-Abstimmung-erstellen")

Im nächsten Dialog können Sie alle für die Abstimmung wichtigen Informationen in die **Eingabefelder** ( **Titel** , **Beschreibung** , **Frage** , usw.) einrichten. Pflichtfelder sind mit einem Sternchen gekennzeichnet. Erst wenn alle Pflichtfelder ausgefüllt sind, wird der Button **Speichern** aktiv. Der Button **Abbrechen** ist immer aktiv und ein Klick darauf (oder auf das graue Kreuz **(X)** in der oberen rechten Ecke) schließt den Dialog **Neue Abstimmung erstellen**.

![Abstimmung-Details-eingeben](media/0fea7eb9cd375c66fe5fae8109e80f87b76152e3.jpg "Abstimmung-Details-eingeben")
