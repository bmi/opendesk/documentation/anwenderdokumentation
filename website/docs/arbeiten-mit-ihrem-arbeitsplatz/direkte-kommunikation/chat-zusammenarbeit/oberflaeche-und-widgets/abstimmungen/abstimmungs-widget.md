# Abstimmungs-Widget
- [Allgemein](abstimmungs-widget/allgemein.md)
- [Die Optionen der Abstimmung im Überblick](abstimmungs-widget/die-optionen-der-abstimmung-im-ueberblick.md)
- [Eine Abstimmung bearbeiten oder löschen](abstimmungs-widget/eine-abstimmung-bearbeiten-oder-loeschen.md)
- [Eine Abstimmung durchführen](abstimmungs-widget/eine-abstimmung-durchfuehren.md)
- [Eine Abstimmung auswerten](abstimmungs-widget/eine-abstimmung-auswerten.md)
