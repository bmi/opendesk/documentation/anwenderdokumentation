# Spaces

## Bessere Organisation für 1:1-Chats und Räume

Chat-Gruppen und Räume können in sogenannten Spaces thematisch zusammengefasst werden. Zum Anlegen und Anzeigen der Spaces benutzen Sie das senkrechte Panel am linken Rand des Programmfensters. Dort gelangen Sie jeweils in die verschiedenen Spaces. Auf dem kleinen Pfeil links neben **Startseite** können Sie das senkrechte Panel ausklappen oder verbergen.

![](media/bee1144cb683d99d1a7e9472a4127cb6667f4857.png "")

![](media/69a37b337659982ad1853ff81b379b26b22c0df2.png "")

## Neuen Space erstellen

Um einen neuen Space zu erstellen, klicken Sie auf **+ Neuen Space erstellen**.

![Neuen-Space-erstellen](media/638d9a8871e5d73346540200185de2102399d2b3.jpg "Neuen-Space-erstellen")

Beim Erstellen eines neuen Spaces können Sie zwischen einem öffentlichen und einem privaten Space wählen. Der öffentliche Space steht allen Nutzerinnen und Nutzern des Zusammenarbeitsmoduls ohne zusätzliche Einladung zur Verfügung. Sie können die Zugriffsrechte und damit das Umwandeln von öffentlichem zu privatem Space (oder umgekehrt) auch im Nachhinein noch anpassen.

![Privater-Space](media/79f8179a0030d45c5758d12a60aa049f6a995e10.jpg "Privater-Space")
