# Bildschirm individualisieren

Sie können das Erscheinungsbild dieses Modul individualisieren. Klicken Sie in der linken Seitenleiste ganz oben auf Ihr Namenskürzel oder Ihr Profilbild um das **Benutzermenü** zu öffnen. Klicken Sie auf **Alle Einstellungen**.

![Benutzermenue-Alle-Einstellungen](media/188ec0ad8a868b1742eb4717a1cedf2a56a9f06e.jpg "Benutzermenue-Alle-Einstellungen")

Es öffnet sich das Fenster mit allen Einstellungen. Navigieren Sie auf der linken Seite zu **Erscheinungsbild**. Sie können hier beispielsweise zwischen einem hellen und einem dunklen Design wählen, den Kontrast erhöhen oder die Schriftgröße ändern.

![Einstellungen-Erscheinungsbild](media/f6c2d47866521e4c4110d3a3995336b2a2a1afae.jpg "Einstellungen-Erscheinungsbild")

Ihre Einstellungen werden automatisch gespeichert. Sie können das Fenster jederzeit schließen.
