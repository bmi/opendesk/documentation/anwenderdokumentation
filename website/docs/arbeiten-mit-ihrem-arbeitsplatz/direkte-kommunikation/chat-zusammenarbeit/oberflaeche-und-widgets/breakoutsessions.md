# Breakoutsessions

## Was sind Breakoutsessions?

Für das Arbeiten in Kleingruppen gibt es die Funktion **Breakoutsessions**, die während einer Videokonferenz zur Verfügung steht. Moderatorinnen und Moderatoren können hier Gruppenräume einrichten und Sie zu einer Gruppe hinzufügen. Sie erhalten dann links oben auf Ihrem Bildschirm automatisch eine Einladung zur Gruppe.

![Benachrichtigung über eine neue Gruppeneinladung](media/2edcf71d210cc32346c262a12a09749d7446df6a.jpg "Benachrichtigung über eine neue Gruppeneinladung")

Klicken Sie auf den Gruppennamen, der mit einem roten Ausrufezeichen markiert ist. Sie werden gefragt, ob Sie der Gruppe beitreten möchten. Wählen Sie **Annehmen**.

![Gruppeneinladung annehmen](media/c8c6ff7eec2e812f5c8909bfd4dad858c4065f02.jpg "Gruppeneinladung annehmen")

Sie sind nun im Gruppenraum. Dieser sieht genauso aus wie ein normaler Konferenzraum. Er hat jedoch einen separaten Namen und ist nur für eingeladene Teilnehmerinnen und Teilnehmer zugänglich.

In diesem Raum können die gleichen Funktionen genutzt werden wie in anderen Räumen, soweit Moderatorinnen und Moderatoren sie zugelassen haben. Sie können die Ergebnisse Ihrer gemeinsamen Arbeit über die entsprechenden Funktionen in den Modulen exportieren.

![Funktionsleiste Exportieren](media/602ae5f8586f2e9ddd640c00a7e1d8f75098964b.png "Funktionsleiste Exportieren")

## Sessions erstellen und verwalten

Um Breakoutsessions zu verwalten, müssen Sie sich in einer laufenden Videokonferenz befinden. Ein Klick oben rechts auf das Symbol für Breakoutsessions öffnet ein Fenster, in dem Sie nach bereits geplanten Breakoutsessions innerhalb eines vorgegebenen Zeitraumes suchen können. Um selbst eine Breakoutsession zu organisieren, klicken Sie auf **+ Breakoutsession planen**.

![Breakoutsession-planen](media/739348b3d142238ff77a16c0434ff92bd0a616c5.jpg "Breakoutsession-planen")

Nun können Sie den Zeitrahmen und die Anzahl der Gruppen festlegen. Sie können Teilnehmerinnen und Teilnehmer automatisch auf die Gruppen verteilen lassen oder dies selbst vornehmen. Auch können Sie Namen für die Gruppen eingeben. Klicken Sie auf **Breakoutsession erstellen**, sobald Sie alle gewünschten Einstellungen vorgenommen haben.

![Breakoutsession-erstellen](media/4b71f856aac6c4a59e9d478ddd894b884e4ccd90.jpg "Breakoutsession-erstellen")

**Hinweis:** Breakoutsessions sind nur möglich, wenn sich genügend Teilnehmerinnen und Teilnehmer in der Videokonferenz befinden, um die gewünschte Anzahl an Gruppen zu bilden.
