# Abstimmungen

Mit dem Abstimmungs-Widget haben Sie die Möglichkeit, vielseitige Abstimmungen in vielfältigen Situationen direkt in einer Besprechung durchzuführen. Sie können es nutzen, um personenbezogene Abstimmungen umzusetzen. Zusammen mit dem Abstimmungsgruppen-Widget können Sie es außerdem verwenden, um Gruppenzugehörigkeiten in der Abstimmung und den Ergebnissen sichtbar zu machen.

## Abstimmungs-Widget

- [Allgemein](abstimmungen/abstimmungs-widget/allgemein.md)
- [Die Optionen der Abstimmung im Überblick](abstimmungen/abstimmungs-widget/die-optionen-der-abstimmung-im-ueberblick.md)
- [Eine Abstimmung bearbeiten oder löschen](abstimmungen/abstimmungs-widget/eine-abstimmung-bearbeiten-oder-loeschen.md)
- [Eine Abstimmung durchführen](abstimmungen/abstimmungs-widget/eine-abstimmung-durchfuehren.md)
- [Eine Abstimmung auswerten](abstimmungen/abstimmungs-widget/eine-abstimmung-auswerten.md)

### Hier geht es weiter mit:

- [Abstimmungs-Widget](abstimmungen/abstimmungs-widget.md)
