# Schlüssel einrichten

Sie benötigen eine Verschlüsselung, um geschützt mit anderen Personen kommunizieren zu können.

Gehen Sie zunächst zu dem Videokonferenztool. Klicken Sie oben links auf Ihr Icon und dann auf den Menüpunkt **Sicherheit**.

![Menü-Übersicht Collaboration](media/295c7caf0e00933bd5d7db90ec5a1d37621fea79.jpg "Menü-Übersicht Collaboration")

Scrollen Sie im sich öffnenden Fenster zum Abschnitt Verschlüsselung und klicken Sie dort auf **Schlüsselsicherung einrichten**.

![Schlüsselsicherung einrichten](media/44602f19bbd9b2d45c988cf5a6e35f0bcce942ec.jpg "Schlüsselsicherung einrichten")

Sie können sich nun entscheiden zwischen einem Schlüssel oder einer Sicherheitsphrase mit optionalem Schlüssel:

![Schlüsselsicherung-einrichten-Auswahl-Schlüssel-oder-Phrase](media/a6faf95061a9a11a3d0b4305ce2dda3ffb25d637.jpg "Schlüsselsicherung-einrichten-Auswahl-Schlüssel-oder-Phrase")

## Sicherheitsschlüssel generieren

Sie müssen den Schlüssel zunächst herunterladen oder in die Zwischenablage kopieren, bevor Sie fortfahren. Sichern Sie ihn sich gut ab.

![Sicherungsschlüssel-herunterladen-oder-kopieren](media/c7028b8074a25e929949387369041f06f5921ed2.jpg "Sicherungsschlüssel-herunterladen-oder-kopieren")

## Sicherheitsphrase setzen

Setzen Sie eine Sicherungsphrase. Auch bei der Sicherheitsphrase können Sie zusätzlich einen Schlüssel sichern.

![Sicherheitsphrase-eingeben](media/1eaecef33fbc0e3e94393c3820ab863c8fc2e5b9.png "Sicherheitsphrase-eingeben")

Zum Abschluss werden Sie aufgefordert, über einen **Single Sign-on** die Einrichtung der Verschlüsselung zu **bestätigen**.

![Phrase-bestätigen](media/bef975c80bea415c4c748dae4062231fd6c7656d.jpg "Phrase-bestätigen")

![SSO-zur-Bestätigung](media/06460d1bd020e7723b5e92d33fbf57e4183116bf.jpg "SSO-zur-Bestätigung")
