# Whiteboard

Im Modul **Chat &amp; Zusammenarbeit** steht Ihnen ein virtuelles **Whiteboard** zur Verfügung. Darauf können Sie geometrische Formen und Text einfügen sowie frei zeichnen. Sie können Ihre Inhalte auf einzelnen Folien anordnen und diese später der Reihe nach als Präsentation darstellen. Die Inhalte können von allen Teilnehmerinnen und Teilnehmern einer Besprechung gemeinsam bearbeitet werden. Dazu muss das Whiteboard-Widget allerdings bei der Einrichtung der Besprechung ausgewählt werden.

## Das Whiteboard aktivieren

Wenn Sie selbst die Moderatorin bzw. der Moderator einer Besprechung sind, in der Sie das Whiteboard verwenden möchten, müssen Sie das Whiteboard-Widget bei der Einrichtung aktivieren. Wie Sie eine Besprechung einrichten, erfahren Sie unter **Ablauf einer Konferenz – Eine Veranstaltung einrichten**.

Im Fenster **Besprechung planen** finden Sie unter **Widgets** alle Widgets aufgeführt, die aktuell für die Besprechung ausgewählt sind. Wenn das Whiteboard hier nicht auftaucht, klicken Sie in den freien Bereich rechts neben den ausgewählten Widgets. Ein Menü öffnet sich. Wählen Sie hier **Whiteboard** aus und schließen Sie die Einrichtung Ihrer Besprechung ab.

![Beim planen einer Besprechung das Whiteboard-Widget auswählen](media/9d3abfad71c9ed6f2dd0b8e6e9b4952bcc7a3802.png "Whiteboard-Widget auswählen")

## Einführung und Ansicht

Bei der ersten Verwendung des Whiteboards erscheint ein Willkommensfenster. Mit Klick auf **Weiter** erhalten Sie eine kurze **Einführung** in die Funktionen. Sie können diese Einführung überspringen, indem Sie auf die Schaltfläche **Schließen (X)** oben rechts klicken. Später können Sie die Einführung jederzeit wieder aufrufen. Klicken Sie dazu auf die Schaltfläche **Hilfe (?)** unten rechts und wählen Sie **Einführung zurücksetzen**.

Je nach Bedarf können Sie Whiteboard und Videokonferenz in der Ansicht minimieren, indem Sie auf die Schaltfläche **Minimieren (Minus)** oben rechts im jeweiligen Fenster klicken.

![Whiteboard während einer Videokonferenz](media/f953b3b9227fc5f2fa5cc1803c25b8fed7834fb0.png "Whiteboard während einer Videokonferenz")

Während der Arbeit auf dem Whiteboard ist es empfehlenswert, das Widget mit der **Schaltfläche Maximieren (zwei diagonale Pfeile)** oben rechts in der Ansicht zu maximieren. Zusätzlich können Sie die Unterhaltungs-Leiste (Chat) auf der rechten Seite mit der Schaltfläche **Schließen (X)** oben rechts ausblenden sowie die Seitenleiste links mit gedrückter Maustaste ganz nach außen ziehen und so ausblenden. So können Sie sich voll auf das Whiteboard konzentrieren und werden nicht von anderen Inhalten abgelenkt.

![Elemente zur besseren Ansicht des Whiteboards ausblenden](media/64903ecc2956c011b7e05a4c9a02ca97672cdc45.png "Elemente ausblenden")

Sie können das Whiteboard auch ausblenden, wenn Sie es gerade nicht benötigen und sich z. B. ganz auf die Videokonferenz konzentrieren möchten. Klicken Sie dazu auf die Schaltfläche **Verberge Whiteboard** oben rechts in der Werkzeugleiste der Besprechung. Über dieselbe Schaltfläche lässt sich das Whiteboard jederzeit wieder einblenden (**Zeige Whiteboard**).

![Die Schaltfläche &quot;Verberge Whiteboard&quot;](media/3a8a2a36e1622fcab46c9734a81137723c40d1fe.png "Verberge Whiteboard")

## Werkzeuge

Unten am Whiteboard finden Sie eine Werkzeugleiste. Hier stehen Ihnen die folgenden Werkzeuge zur Verfügung:

- **Auswählen**: Wählen Sie Elemente (z. B. Formen oder Text) auf der aktuellen Folie aus, um diese zu bearbeiten (siehe unten).
- **Rechteck**: Fügen Sie ein Rechteck ein.
- **Ellipse**: Fügen Sie eine Ellipse ein.
- **Dreieck**: Fügen Sie ein Dreieck ein.
- **Linie**: Fügen Sie eine Linie ein.
- **Text**: Fügen Sie ein Textfeld ein.
- **Stift**: Zeichnen Sie frei, indem Sie die Maus bei gedrückter Maustaste wie einen Stift über das Whiteboard bewegen.
- **Rückgängig**: Machen Sie Ihre letzten Arbeitsschritte rückgängig.
- **Wiederholen**: Stellen Sie rückgängig gemachte Arbeitsschritte wieder her.

![Werkzeugleiste des Whiteboards](media/55866683825fd8f00423174807496f1f8f028e46.png "Werkzeugleiste des Whiteboards")

## Elemente einfügen und bearbeiten

Um geometrische Formen oder Textfelder einzufügen, wählen Sie zunächst das entsprechende Werkzeug aus. Klicken Sie dann in einen leeren Bereich des Whiteboards, halten Sie die linke Maustaste gedrückt und ziehen Sie das Element auf die gewünschte Größe.

Nachdem Sie ein **Textfeld** erzeugt haben, können Sie sofort den gewünschten Text darin eingeben. Die Schriftgröße wird automatisch an die Größe des Textfeldes angepasst, während Sie tippen.

Sie können Position und Größe von Elementen später jederzeit ändern. Wählen Sie dazu das Werkzeug **Auswählen** und klicken Sie auf das gewünschte Element. Halten Sie zum **Verschieben** die linke Maustaste gedrückt und bewegen Sie das Element an die gewünschte Stelle.

Ausgewählte Elemente werden eingerahmt dargestellt. Um die **Größe** eines Elementes zu ändern, klicken Sie auf eine Ecke des Rahmens. Halten Sie die linke Maustaste gedrückt und ziehen Sie das Element auf die gewünschte Größe.

![Objekte auf dem Whiteboard skalieren](media/7bbf6efba9d62f746cf404e8b84f46698f4cb61c.png "Objektskalierung")

## Weitere Funktionen für Elemente

Wenn Sie ein neues Element einfügen oder ein vorhandenes Element auswählen, erscheint oberhalb oder unterhalb des Elementes eine weitere kleine Werkzeugleiste mit den folgenden Funktionen:

- **Wähle eine Farbe**: Mit Klick auf diese Schaltfläche öffnet sich eine Farbauswahl. Hier können Sie die gewünschte Farbe für das Element auswählen. Für Textfelder steht diese Funktion nicht zur Verfügung.
- **Element duplizieren**: Diese Schaltfläche erzeugt eine Kopie des ausgewählten Elementes.
- **Element löschen**: Mit Klick auf diese Schaltfläche entfernen Sie das ausgewählte Element vom Whiteboard.

![Werkzeugleiste für Elemente inklusive Farbauswahl](media/357eb9a87e182187ba8152a61b47cbb139115d91.png "Werkzeugleiste für Elemente")

## Folien hinzufügen und anordnen

Sie können auf dem Whiteboard mehrere Folien anlegen, die sich beliebig anordnen und später als Präsentation wiedergeben lassen. Klicken Sie dafür zunächst auf die Schaltfläche **Folienübersicht öffnen** oben links. Sie können die Folienübersicht mit derselben Schaltfläche jederzeit wieder schließen ( **Folienübersicht schließen** )

![Die Schaltfläche &quot;Folienübersicht öffnen&quot;](media/c59b419648337a61ec6731124f62510217f7953c.png "Folienübersicht öffnen")

In der Folienübersicht werden alle aktuell vorhandenen Folien untereinander dargestellt und nach der eingestellten Reihenfolge durchnummeriert.

Klicken Sie oben auf die Schaltfläche **Folie hinzufügen** , um eine neue Folie anzulegen. Diese wird unterhalb der letzten Folie eingefügt und automatisch zur Bearbeitung geöffnet.

![Die Folienübersicht mit der Schaltfläche &quot;Folie hinzufügen&quot;](media/9f01de0baf7d133c90a69b59fb561ee069d06782.png "Die Folienübersicht")

Die **Reihenfolge** der Folien können Sie bequem per **Drag-and-Drop** ändern (z. B. wenn Sie nachträglich ein Deckblatt hinzufügen und an den Anfang verschieben möchten). Klicken Sie in der Folienübersicht auf eine Folie, halten Sie die linke Maustaste gedrückt und schieben Sie die Folie nach oben oder unten an die gewünschte Position. Lassen Sie nun die Maustaste wieder los.

![Eine Folie in der Folienübersicht verschieben](media/bcf6cc2b9a7655a33813b726755395d807a5d582.png "Folie verschieben")

## Präsentation starten

Wenn Sie mit den Inhalten und der Folienreihenfolge zufrieden sind, können Sie diese als **Bildschirmpräsentation** wiedergeben. Als Moderatorin oder Moderator können Sie eine solche Präsentation auch für andere Teilnehmerinnen und Teilnehmer vorbereiten und die Bearbeitung sperren, wenn Sie während der Besprechung Inhalte veranschaulichen möchten, ohne dass gemeinsam daran gearbeitet wird.

Klicken Sie auf die Schaltfläche **Präsentation starten** oben rechts.

![Die Schaltfläche &quot;Präsentation starten&quot;](media/fb0d28ec2aa278b7eaa6742b8faf33c9d33291b4.png "Präsentation starten")

Während der Präsentation steht Ihnen eine kleine senkrechte Werkzeugleiste mit den folgenden Funktionen zur Verfügung:

- **Präsentation beenden**: Hiermit können Sie die Präsentation jederzeit wieder beenden.
- **Vorherige Folie** und **Nächste Folie**: Blättern Sie zwischen den Folien des Whiteboards vor und zurück. Alle Nutzerinnen und Nutzer können selbstständig in der Präsentation blättern.
- **Bearbeitung freigeben** und **Bearbeitung sperren**: Standardmäßig ist die Bearbeitung von Folien während der Präsentation gesperrt. Sie können die Folien jedoch zur Bearbeitung freigeben und bei Bedarf später erneut sperren.

## Inhalte importieren und exportieren

Inhalte, die auf dem Whiteboard bearbeitet wurden, können jederzeit im PDF- oder NWB-Format exportiert werden. So können Sie Ihre gemeinsame Arbeit sichern und später wieder darauf zurückgreifen. Zuvor exportierte NWB-Dateien können Sie auch ins Whiteboard importieren, z. B. um diese in einer anderen Besprechung erneut zu verwenden oder weiter zu bearbeiten.

Um Ihre Whiteboard-Inhalte zu exportieren, klicken Sie auf die Schaltfläche **Einstellungen** oben links und wählen Sie **Exportieren**.

![Die Punkte &quot;Importieren&quot; und &quot;Exportieren&quot; in den Whiteboard-Einstellungen](media/c0bbd328f0b444b8b2c4eea89a71c6b27ce7fbdc.png "Whiteboard-Einstellungen")

Öffnen Sie im Fenster **Inhalt exportieren** das Dropdown-Menü, um das gewünschte Dateiformat auszuwählen. Klicken Sie dann auf **Herunterladen**.

Je nachdem, welchen Internetbrowser Sie verwenden, kann der nächste Schritt unterschiedlich aussehen. Möglicherweise werden Sie zunächst gefragt, ob Sie die Datei direkt öffnen oder einen Speicherort dafür auswählen möchten (**Speichern unter**). Andere Browser öffnen sofort den Datei-Explorer, in dem Sie zum gewünschten Speicherort auf Ihrer Festplatte navigieren können, oder speichern die Datai automatisch in einem Standardordner (meist der Ordner **Downloads**). Schlagen Sie bei Schwierigkeiten in der Anleitung Ihres Browsers nach.

![Inhalte aus dem Whiteboard exportieren](media/6196cfe8188a4289285c30256c6d1cd84ac8f0bd.png "Inhalte exportieren \(Whiteboard\)")

Um zuvor exportierte Whiteboard-Inhalte zu importieren, klicken Sie auf die Schaltfläche **Einstellungen** oben links und wählen Sie **Importieren**. Der Datei-Explorer öffnet sich. Navigieren Sie zum Speicherort der gewünschten Datei und importieren Sie diese mit einem Doppelklick. Sie können die Datei auch mit einem einfachen Klick auswählen und dann auf **Öffnen** klicken.

![Whiteboard-Datei importieren](media/27d25ff678863484762dd51c3c7c42ab1a3d1bb5.png "Whiteboard-Datei importieren")

## Weitere Funktionen

Über die Schaltfläche **Einstellungen** können Sie neben dem Import und Export von Dateien noch auf folgende Optionen zugreifen:

- **Raster**: Schalten Sie das Raster auf dem Whiteboard aus oder wieder ein.
- **Entwicklerwerkzeuge**: Blenden Sie am rechten Bildschirmrand eine Entwickleransicht ein. Diese Funktion ist für fortgeschrittene Nutzerinnen und Nutzer gedacht. Je nachdem, welche Berechtigungen Sie haben, wird Ihnen die Funktion möglicherweise nicht angezeigt.

![Whiteboard-Einstellungen](media/c0bbd328f0b444b8b2c4eea89a71c6b27ce7fbdc.png "Whiteboard-Einstellungen")

Oben rechts auf dem Whiteboard finden Sie die Schaltfläche **Zeiger der Mitwirkenden anzeigen** bzw. **ausblenden**. Hier können Sie auswählen, ob Sie die Mauszeiger anderer Nutzerinnen und Nutzer während der Bearbeitung auf dem Whiteboard sehen möchten.

Neben dieser Schaltfläche sehen Sie ein kleines Bild oder Kürzel für die ersten fünf Nutzerinnen und Nutzer, die aktuell auf dem Whiteboard aktiv sind. Wenn mehr als fünf Nutzerinnen und Nutzer an der Besprechung teilnehmen und Sie die übrigen Personen anzeigen lassen möchten, klicken Sie auf die Zahl neben den angezeigten Personen.

![Nutzerinnen und Nutzer des Whiteboards sowie die Schaltfläche &quot;Zeiger der Mitwirkenden anzeigen&quot;](media/69305419b42606ad66931bd7bc7f8f1df822991f.png "Nutzerinnen und Nutzer des Whiteboards")
