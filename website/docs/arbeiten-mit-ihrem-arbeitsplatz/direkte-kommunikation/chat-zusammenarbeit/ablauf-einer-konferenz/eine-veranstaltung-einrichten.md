# Eine Veranstaltung einrichten

## Planen im Terminplaner

Klicken Sie auf das Modul **Chat &amp; Zusammenarbeit** ,um zu dem **Terminplaner** zu gelangen.

![Modulübersicht auf der Startseite](media/c138d45ee9b60521456074d02500e943fb5bb678.png "Modulübersicht auf der Startseite")

![Direkte Kommunikation in der Modulübersicht](media/b941189cacf325d185eb4a8eab92546e1bf56657.png "Direkte Kommunikation in der Modulübersicht")

Danach gelangen Sie zu einer Übersicht mit Ihren vorhandenen Besprechungen. Diese finden Sie unter dem Punkt **Räume**. Um eine neue Besprechung zu planen, klicken Sie auf den Terminplaner.

![Benutzeroberfläche Kollaboration](media/7c10c3948a1d17293b3765355b45083318170b39.png "Benutzeroberfläche Kollaboration")

Klicken Sie auf **Besprechung planen**.

![Eingabefenster Besprechung planen](media/d8b9cf8d7b23ca65331e543ff3e3fbeaa9a65f90.png "Eingabefenster Besprechung planen")

In diesem Fenster erstellen Sie die Veranstaltung. Wählen Sie einen Namen, Start- und Endzeit und welche Widgets in der Konferenz aktiv sein sollen.

Klicken Sie auf **Besprechung erstellen**, um die Konferenz zu erstellen.

Möchten Sie, dass ein Termin regelmäßig in einem bestimmten zeitlichen Intervall stattfindet, können Sie einen Serientermin einrichten. Der Termin wird wie ein regulärer Einzeltermin erstellt, mit der Ausnahme, dass Sie das Dropdown-Menü **Die Besprechung wiederholen** nutzen und die gewünschten Parameter einstellen.

![Serientermin einstellen](media/d3baccc0876fff13a05e587c1bcb467377b5ef20.png "Serientermin einstellen")

Nun erscheint die Veranstaltung unter **Räume**.

![Einladung zum Meeting 3.2b](media/65f564025a31dfd9ddee4f25cae069a70e391baf.png "Einladung zum Meeting 3.2b")

Im **Terminplaner** sehen Sie alle **Besprechungen**, die für den ausgewählten Tag geplant sind, auch die erstellte **Besprechung** ist hier zu sehen.

![Geplante Besprechungen im Kalender ](media/19eaa59e23b0d6635323180b6b2437897744f0ae.png "Geplante Besprechungen im Kalender")

## Einstellungen anpassen

Klicken Sie in Ihrem erstellten **Raum** auf das **Drei-Punkte-Menü**, um in die Raumoptionen zu gelangen.

![Zu den Raumoptionen gelangen](media/a1c444f1c54575aa4f4ef093bfacbaf79e8d7715.png "Zu den Raumoptionen gelangen")

In den Raumoptionen wählen Sie **Einstellungen**.

![Menüleiste der Raumoptionen](media/1e833a815f7481abbe0c1bafd9b5fd0cc729d3fc.png "Menüleiste der Raumoptionen")

## In eine Veranstaltung einladen

Nachdem die Veranstaltung erstellt wurde, können die Gäste eingeladen werden. Es gibt verschiedene Varianten um Teilnehmerinnen und Teilnehmer einzuladen.

![Startseite des Terminplaners ](media/2ce767098ab09459828ffa30b5f3ed8bc288d051.PNG "Startseite des Terminplaners")

Sobald die Besprechung geplant ist, können Sie vor Beginn der Besprechung Teilnehmerinnen und Teilnehmer über den **Terminplaner** hinzufügen.

![Das Teilen-Menü](media/4b3ebd3ec10b6cb3f75aee12d4f532e5794b388a.PNG "Das Teilen-Menü")

Sie können zum Einladen auch das **Raumoptionen-Menü** verwenden. Das **Raumoptionen-Menü** öffnet sich durch einen Klick auf das Symbol mit drei Punkten direkt neben dem Namen der Besprechung. Wenn Sie auf **Einladen** klicken, öffnet sich ein Fenster, indem Sie Teilnehmerinnen und Teilnehmer suchen und einladen können oder klicken Sie auf **Raumlink kopieren**, um den Link in der Zwischenablage zu speichern und anschließend an Gäste weiterzuleiten.

![Das Kontext-Menü](media/00d479223ea940bee51e0729d1d75f8d8965484a.png "Das Kontext-Menü")

Über die **Raum-Info** können Teilnehmerinnen und Teilnehmer jederzeit zu einer Besprechung hinzugefügt werden. Die **Raum-Info** öffnen Sie, nachdem Sie den Raum betreten haben, mit einem Klick auf das Icon rechts oben.

![Icon zum Öffnen der Raum-Info](media/def5556d8d40e9c93609a3a159cc510a52af5fca.png "Icon zum Öffnen der Raum-Info")

Anschließend öffnen Sie mit einem Klick auf **Personen** eine Übersicht über die anwesenden Gäste.

![Personenübersicht in der Raum-Info](media/b2862def1877511166c057e34b2c0f700c2c2f4d.png "Personenübersicht in der Raum-Info")

Mit einem weiteren Klick auf **In diesem Raum einladen** öffnet sich ein neues Fenster. Dort können Sie entweder Teilnehmerinnen und Teilnehmer über den Nutzernamen hinzufügen oder mit einem Klick auf **Teile diesen Raum** einen **Raum-Link** weiterleiten, mit dem Gäste beitreten können.

![Fenster zum Einladen von Teilnehmerinnen und Teilnehmern](media/18ffe0da52c6fc1a071a32bced4201083a4fc16e.png "Fenster zum Einladen von Teilnehmerinnen und Teilnehmern")
