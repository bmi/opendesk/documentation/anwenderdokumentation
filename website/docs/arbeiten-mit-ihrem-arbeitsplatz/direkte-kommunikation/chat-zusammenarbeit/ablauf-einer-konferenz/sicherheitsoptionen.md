# Sicherheitsoptionen

In diesem Abschnitt werden die Einstellungen der Sicherheitsoptionen beschrieben. Dazu gehören die **Zutrittseinstellungen** und die **Verschlüsselung** eines Konferenzraums.

Die **Sicherheitsoptionen** einer Videokonferenz dürfen nur von der Moderatorin oder dem Moderator eingestellt werden. Die Einstellungen finden Sie nach dem Erstellen eines Videokonferenzraums oder innerhalb des Konferenzraums, daher ist es ratsam, sich rechtzeitig vor Beginn der Konferenz einzuloggen.

## Lobbymodus

Der **Lobbymodus** funktioniert wie ein Warteraum für Personen, die an der Videokonferenz teilnehmen möchten. Die Moderatorin oder der Moderator behält hierbei den Überblick über die Anwesenden und hat somit eine Art "Hausrecht". Wenn der Lobbymodus aktiviert ist, können nur Personen in den Raum gelassen werden, die von der Moderatorin oder vom Moderator freigegeben wurden.

Ein Passwort für Teilnehmerinnen und Teilnehmer ist nicht erforderlich. Es kann jedoch eingeschaltet werden.

**Achtung**: Wenn die Moderatorin bzw. der Moderator die Videokonferenz verlässt, kann niemand mehr in die Konferenz eintreten, auch nicht die Moderatorin bzw. der Moderator selbst.

Es gibt zwei Möglichkeiten den Lobbymodus zu aktivieren.

Die erste Möglichkeit erlaubt es, die Sicherheitsoptionen vor dem Betreten des Konferenzraums einzustellen. Dies erfolgt über die **Raumoptionen (Drei-Punkte-Menü)** des Videokonferenzraums. Klicken Sie auf **Einstellungen**. Ein Popup-Fenster öffnet sich und auf der linken Seite finden Sie die Option **Sicherheit**.

![Raumoptionen](media/d63ae7abd7308f95b5e36cb5b0e4921b5a4ac530.png "Raumoptionen")

Hier können Sie die Sicherheitseinsellungen des Videokonferenzraumes vornehmen. Um den Lobbymodus zu aktivieren, wählen Sie **Beitrittsanfragen** und schließen Sie das Fenster, indem Sie auf das **×** oben rechts klicken.

![Raumeinstellungen](media/ec926a8a215cea1e0fd52be7ef407bfe32447623.png "Raumeinstellungen")

Die zweite Möglichkeit erlaubt es, die Sicherheitsoptionen nach dem Betreten des Konferenzraums einzustellen. Dies erfolgt über die Schaltfläche **Weitere Einstellungen (Drei-Punkte-Menü)** auf der Symbolleiste. Klicken Sie dann auf **Sicherheitsoptionen**.

![Sicherheitsoptionen](media/7b45372a01e5720bc64d23978d71036b2bea51d3.png "Sicherheitsoptionen")

Ein Popup-Fenster öffnet sich. Aktivieren Sie die Schaltfläche **Lobby aktivieren**. Die Schaltfläche wird violett und zeigt damit an, dass der Lobbymodus aktiv ist.

![Lobbymodus aktivieren](media/9187297ad7d82398dc6a54d27a31d560d2e9d5fa.png "Lobbymodus aktivieren")

### Mit Passwort sichern

Klicken Sie auf **Passwort hinzufügen**, wenn ein Passwort festgelegt werden soll. Dieses darf maximal 10 Zeichen lang sein. Klicken Sie dann auf **Hinzufügen**. Um das Popup-Fenster zu verlassen klicken Sie auf das **×** oben rechts.

**Hinweis**: Das Passwort sollte nur privat und nicht öffentlich weitergegeben werden.

![Passwort hinzufügen](media/96304dcae1c9f4967411e3ffce7739f2e2547b91.png "Passwort hinzufügen")

![Passwort festlegen](media/95aa4e406cfd3ac52decc67566617966dfe34b9b.png "Passwort festlegen")

![Passwort verwalten](media/c89b36added007cca881723fb849eae1897bc3f8.png "Passwort verwalten")

Ab diesem Moment müssen Sie darauf warten, bis die Anfrage von der Moderatorin bzw. von dem Moderator angenommen wird oder geben Sie das Passwort ein.

![Warteraum](media/b709099ce9f12c775fc9023686239aaaedba43cd.png "Warteraum")

![Passwort eingeben](media/b3d2fd339cb5aa71b30fc9867c7f751b693a1a59.png "Passwort eingeben")

Die Moderatorin bzw. der Moderator bekommt eine Meldung für jede Teilnehmerin und jeden Teilnehmer, der/die angenommen werden soll. Je nach Einstellung hat die Moderatorin oder der Moderator zwei oder drei Möglichkeiten. Teilnehmerinnen und Teilnehmer in den Konferenzraum zulassen, ablehnen oder mit ihnen chatten. Das letztgenannte ist nur nach der zweiten Einstellmöglichkeit möglich.

![Beitrittsanfrage der Lobbyfunktion (1. Möglichkeit)](media/1d2143cddb9f35308159cc4411db8cc02576488e.png "Beitrittsanfrage der Lobbyfunktion \(1. Möglichkeit\)")

![Beitrittsanfrage der Lobbyfunktion (2. Möglichkeit)](media/414b5d295964479f4da0ec64ce7b0d32d8619ce2.png "Beitrittsanfrage der Lobbyfunktion \(2. Möglichkeit\)")

Wenn die Moderatorin oder der Moderator die Option **Chat** wählt, bekommt die Teilnehmerin oder der Teilnehmer eine Nachricht in der Lobby bevor er/sie hineingelassen wird.

![Chat Lobbymodus Teilnehmerin und Teilnehmersicht](media/d964fc809f9ddcc50451cba080ebd87fe34d4a75.png "Chat Lobbymodus Teilnehmerin und Teilnehmersicht")

![Chat Lobbymodus Moderatorsicht](media/200b863616fc73ba3e244b95b5a2e53118909042.png "Chat Lobbymodus Moderatorsicht")

**Hinweis**: Es gibt eine Meldung pro Teilnehmerin bzw. Teilnehmer. Das bedeutet, dass die Moderatorin bzw. der Moderator jede Person einzeln zulassen muss.

## Verschlüsselung

Es gibt zwei Möglichkeiten, um die Verschlüsselung des Konferenzraums einzustellen. Die erste Möglichkeit erfolgt über die **Raumoptionen (drei-Punkte-Menü)** unter **Sicherheit** (siehe oben). Aktivieren Sie den Schalter.

**Hinweis**: Beachten Sie, dass dieser Schalter nicht aktiviert werden kann, wenn VPN oder verschlüsselte Verbindungen aktiv sind.

![Verschlüsselung durch Raumoptionen](media/6f028df510e0edb4080c6e44894c0c5826ac382d.png "Verschlüsselung durch Raumoptionen")

Um eine Ende-zu-Ende-Verschlüsselung zu aktivieren, klicken Sie auf die Schaltfläche **Weitere Einstellungen (Drei-Punkte-Menü)** auf der Symbolleiste im Konferenzraum. Anschließend wählen Sie **Sicherheitsoptionen** (siehe oben) und aktivieren Sie den Schalter **Ende-zu-Ende-Verschlüsselung aktivieren**.

![Ende-zu-Ende-Verschlüsselung aktivieren](media/ba4be24c2eccf463c048d615f2f6982689e711ef.png "Ende-zu-Ende-Verschlüsselung aktivieren")

Wenn die Ende-zu-Ende-Verschlüsselung aktiviert ist, wird dies durch ein Symbol auf dem Hauptfenster des Konferenzraums bestätigt.

![Verschlüsselung Bestätigung](media/7a0d116c41bfa265ad666b8a5829342e5464a528.png "Verschlüsselung Bestätigung")
