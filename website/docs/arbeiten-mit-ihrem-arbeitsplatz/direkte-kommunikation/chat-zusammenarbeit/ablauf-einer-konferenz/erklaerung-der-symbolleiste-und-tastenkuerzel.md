# Erklärung der Symbolleiste und Tastenkürzel

Hier finden Sie die grundlegenden Funktionen in der Videokonferenz erklärt:

- Stummschalten
- Kamera stoppen
- Bildschirmfreigabe starten/stoppen
- Hand heben
- Anwesende
- Kachelansicht ein-/ausschalten
- Persönliche Einstellungen
- weitere Einstellungen
- Konferenz verlassen

## Symbolleiste

![Symbolleiste der Konferenz ](media/d0a311c777f53864e04784b5428268d12bd2bc4f.png "Symbolleiste der Konferenz ")

Von links nach rechts finden Sie folgende **Schaltflächen unten auf dem Bildschirm**. Die kleinen **Pfeile** an einigen Schaltflächen öffnen jeweils ein **Dropdown-Menü** mit zusätzlichen Optionen.

- **Stummschalten** und **Ton-Einstellungen**: Schalten Sie Ihr Mikrofon ein und aus. Wählen Sie aus, welche Audiogeräte (Mikrofon und Lautsprecher) für die Konferenz verwendet werden sollen.
- **Kamera stoppen** und **Kameraeinstellungen**: Schalten Sie Ihre Kamera ein und aus. Wählen Sie aus, welche Kamera für die Konferenz verwendet werden soll und stellen Sie Hintergründe ein. Sie können den Hintergrund auch unscharf darstellen lassen.
- **Bildschirmfreigabe starten**: Teilen Sie Inhalte auf Ihrem Bildschirm mit den Teilnehmerinnen und Teilnehmern der Konferenz.
- **Hand heben** und **Reaktionen**: Klicken Sie auf die Schaltfläche, um zu signalisieren, dass Sie etwas sagen möchten. Mit dem Pfeil können Sie eine Auswahl von Reaktionen öffnen (Daumen hoch, Klatschen, Lachen, Überrascht, Buhen, Stille), um die Konversation schnell und einfach zu kommentieren.
- **Anwesende**: Zeigt alle Anwesenden in der Konferenz an.
- **Kachelansicht ein-/ausschalten**: Stellt die Videos von Teilnehmerinnen und Teilnehmern klein und kachelförmig angeordnet dar, sodass Sie mehrere Personen auf einmal im Blick haben. Schalten Sie die Kachelansicht aus, um nur die aktuell sprechende Person zu sehen.
- **Weitere Einstellungen (Drei-Punkte-Menü)**: Dieses Menü bietet Direktzugriff auf verschiedene Einstellungen und Funktionen. Klicken Sie auf Ihren Namen, um in Ihr Profil zu gelangen. Öffnen Sie die **Qualitätseinstellungen**, wechseln Sie in den **Vollbildmodus**, navigieren Sie zu den **Sicherheitsoptionen**, schalten Sie die **Rauschunterdrückung** ein oder aus, wählen Sie einen **Hintergrund** aus (Sie können den Hintergrund auch unscharf darstellen lassen) und zeigen Sie eine **Sprecherstatistik** an. Ganz unten im Dropdown-Menü gelangen Sie in die allgemeinen **Einstellungen** und können sich die **Tastenkürzel anzeigen** lassen.
- **Konferenz verlassen**: Klicken Sie auf diese Schaltfläche, wenn Sie die Konferenz verlassen möchten.

## Mouseover

Wenn Sie mit dem Mauszeiger über die Tasten fahren, ohne zu klicken, sehen Sie eine Beschreibung der Funktion.

![Beschreibung der Symbole](media/04dd4c354898bbf3b7c8fa851def0149569cab9d.png "Beschreibung der Symbole")

## Tastenkürzel

Sie können viele Befehle mit Tastaturkurzbefehlen steuern. Um eine Liste der Tastenkürzel einzusehen, klicken Sie unten in der Videokonferenz auf das **Drei-Punkte-Menü** und dann auf **Tastenkürzel anzeigen**.

![Tastenkürzel anzeigen lassen](media/1bfc6848fdbd55107abb9de2440054ace5619437.jpg "Tastenkürzel anzeigen lassen")

Folgende Tastenkürzel gibt es aktuell:

| Funktion                                       | Tastenkürzel |
| ---------------------------------------------- | ------------ |
| Stummschaltung aktivieren oder deaktivieren    | M            |
| Kamera starten oder stoppen                    | V            |
| Video-Miniaturansichten ein- oder ausblenden   | F            |
| Qualitätseinstellungen                         | A            |
| Chat öffnen oder schließen                     | C            |
| Wechsel zwischen Kamera und Bildschirmfreigabe | D            |
| Liste der Anwesenden ein- oder ausblenden      | P            |
| Hand heben                                     | R            |
| Vollbildmodus aktivieren oder deaktivieren     | S            |
| Kachelansicht ein- oder ausschalten            | W            |
| Tastenkombinationen ein- oder ausblenden       | ?            |
| Push-to-Talk (Sprechtaste)                     | Leertaste    |
| Sprechstatistik anzeigen                       | T            |
| Lokales Video fokussieren                      | 0            |
| Video einer anderen Person fokussieren         | 1–9          |
| Daumen hoch senden                             | Alt + T      |
| Klatschen senden                               | Alt + C      |
| Lachen senden                                  | Alt + L      |
| Überrascht senden                              | Alt + O      |
| Buhen senden                                   | Alt + B      |
| Stille senden                                  | Alt + S      |
