# Personen verwalten

Als Moderatorin bzw. Moderator stehen Ihnen einige besondere Funktionen zur Verfügung, mit denen Sie Teilnehmerinnen und Teilnehmer verwalten können.

Um eine Übersicht über alle Personen zu öffnen, klicken Sie auf den Raumnamen und wählen Sie im Untermenü **Personen** aus. Daraufhin können Sie die Teilnehmerinnen und Teilnehmer einzeln auswählen.

![Teilnehmenden Personen](media/1345401fff50b58ee6d62e3f9f7ddd849a39556f.png "Teilnehmenden Personen")

### Hier geht es weiter mit:

- [Personen bannen oder entfernen](personen-verwalten/personen-bannen-oder-entfernen.md)
- [Rollen von Personen ändern](personen-verwalten/rollen-von-personen-aendern.md)
