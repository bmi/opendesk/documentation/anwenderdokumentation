# Einführung

Die Teilnehmerinnen und Teilnehmer erhalten die Einladung zu jeder einzelnen digitalen Veranstaltung von den Moderatorinnen und Moderatoren auf einem gängigen Weg (z. B. per Mail) zugeschickt oder werden direkt in **Chat &amp; Zusammenarbeit** eingeladen. Die E-Mail beinhaltet den Link zum digitalen Veranstaltungsraum.

Nach dem Klick auf den Link zur Veranstaltung geben die Teilnehmerinnen und Teilnehmer ihre Namen an und gelangen in den digitalen Veranstaltungsraum. Hier kann die Veranstaltung verfolgt und mittels verschiedener Funktionen gemeinsam bearbeitet werden. Dazu gehören z. B. die Videokonferenz, der Chat, ein Whiteboard, der Dateiaustausch und Abstimmungen. Verschiedene Funktionen, wie z. B. das Whiteboard, bieten zusätzlich die Möglichkeit, die erarbeiteten Ergebnisse zu sichern.

Teilnehmerinnen und Teilnehmer können jederzeit alle für sich relevanten Inhalte sehen, diese jedoch nur bearbeiten, wenn Moderatorinnen und Moderatoren dies erlauben.
