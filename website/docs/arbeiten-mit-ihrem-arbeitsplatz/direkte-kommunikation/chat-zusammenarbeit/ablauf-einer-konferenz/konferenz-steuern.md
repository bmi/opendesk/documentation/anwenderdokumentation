# Konferenz steuern

## Kamera und Mikrofon ein- und ausschalten

Wenn Sie Ihre Kamera und Ihr Mikrofon aktivieren oder deaktivieren möchten, klicken Sie auf die entsprechenden Buttons unten in der Mitte neben dem Auflege-Button:

![(De)-Aktivieren von Kamera und Mikrofon](media/cc6ab9382a915befe4bf4e9177dd4271ab63a785.png "\(De\)-Aktivieren von Kamera und Mikrofon")

**Achtung**: Die Moderatorin und der Moderator können Ihr Mikrofon und Ihre Kamera ebenfalls deaktivieren. Nur Sie selbst können beides wieder aktivieren.

## Bildschirm-Teilen-Funktion

Sie können Ihren Bildschirm mit den anderen teilen. Verwenden Sie dafür den **Bildschirm freigeben** Button auf der unteren Symbolleiste.

![Bildschirm-Teilen-Funktion](media/1df70e8eddf5e6b081856bbc990f3561209648d0.png "Bildschirm-Teilen-Funktion")

Wählen Sie anschließend aus den folgenden drei Optionen:

- Gesamter Bildschirm, wenn ein gesamter Bildschirm geteilt werden soll. Wenn Sie mehrere Monitore verwenden, werden Sie gefragt welcher davon freigegeben werden soll.
- Nur ein Anwendungsfenster, wenn lediglich eine bestimmte Anwendung geteilt werden soll (z. B. Powerpoint)

Microsoft Edge-Tab (kann bei anderen Browsern anders heißen), wenn nur ein einzelner Tab Ihres Browsers angezeigt werden soll. Sie können in diesem Auswahlfenster unten links über Audio teilen auch einstellen, dass der Ton Ihres Endgerätes übertragen werden soll – das ist hilfreich, wenn Sie z. B. ein Video oder eine Tondatei für alle vorspielen möchten.

![Auswahlfenster für die Bildschirmfreigabe](media/e37f7899d883ef2aff8f12cae8c98bcf07c7d599.jpg "Auswahlfenster für die Bildschirmfreigabe")

## Vollbildmodus

Insbesondere wenn andere Teilnehmerinnen und Teilnehmer ihren Bildschirm teilen, ist es hilfreich, nur die Videokonferenz möglichst groß zu sehen. Sie können den Vollbildmodus über die weiteren Einstellungen aktivieren und über die Escape-Taste wieder deaktivieren. Alternativ kann auch die „S“-Taste verwendet werden.

![Vollbildmodus aktivieren/deaktivieren](media/da85cd3e1959d2dc703d67fa1ab3fab6a4deda32.jpg "Vollbildmodus aktivieren/deaktivieren")

**Achtung**: Sie sehen dann nur noch die Videokonferenz, nicht mehr den digitalen Veranstaltungsraum, das Whiteboard etc.
