# Besprechungsraum

Unterhalb des Textfelds **Nachricht senden** können Sie Textnachrichten mit anderen austauschen.

In der unteren rechten Ecke stehen Ihnen verschiedene Optionen zur Verfügung, darunter Abstimmungen, Emojis und die Möglichkeit, Sprachnachrichten zu versenden sowie Dateien bereitzustellen.

![Besprechungsraum](media/7dc6b196df8100d74156986bb8b6b577059303f4.png "Besprechungsraum")

![Icon Balken](media/def5556d8d40e9c93609a3a159cc510a52af5fca.png "Icon Balken")

- Sie können **Breakoutsessions** einrichten und betreten, um zusätzliche Besprechungsräume für Gruppenarbeit zu schaffen.
- Die **Meeting-Steuerung** erlaubt es Ihnen, die Konfigurationen als Moderatorin oder Moderator durchzuführen.
- Nutzen Sie die **Videokonferenz-Funktion**, um Besprechungen per Video abzuhalten.
- Mit **Abstimmungen** können Sie Umfragen in der Besprechung erstellen.
- Das **Whiteboard** ist ein Tool, mit dem Sie gemeinsam arbeiten und PDF-Dateien präsentieren können.
- In der **Suche** können Sie nach Begriffen suchen.
- **Threads** helfen Ihnen dabei, Ihre Konversationen nach Themen zu organisieren und leicht nachzuverfolgen.
- In der **Raum-Info** finden Sie alle Informationen über einen Raum.
