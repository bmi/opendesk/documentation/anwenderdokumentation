# Meeting-Steuerung und Breakoutsessions

Als **Moderatorin** oder **Moderator** können Sie **Breakoutsessions** einrichten und bestimmte **Steuerungsfunktionen** übernehmen. Mit den Breakoutsessions erhalten Sie **zusätzliche Räume**, in denen Teilnehmerinnen und Teilnehmer der Besprechung in Gruppen arbeiten können.

## Steuerungsfunktionen

- Aktuelle Besprechung **bearbeiten**
- Besprechung **löschen**
- **Zeiten** der Besprechung **anpassen**.

![Besprechung bearbeiten](media/23ef92d08d39c329da19c37360491fede3603f2e.png "Besprechung bearbeiten")
