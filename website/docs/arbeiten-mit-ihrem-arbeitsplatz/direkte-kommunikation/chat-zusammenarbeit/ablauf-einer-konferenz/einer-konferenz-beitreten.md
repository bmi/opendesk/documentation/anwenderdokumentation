# Einer Konferenz beitreten

Wenn Sie nicht die Moderatorin oder der Moderator sind, müssen Sie lediglich der Veranstaltung beitreten.

Im ersten Schritt müssen Sie sich anmelden.

Wenn Sie direkt in das Zusammenarbeitsmodul eingeladen wurden, tauchen am linken Bildschirmrand über **Personen** die Einladungen auf.

![Einladungen der Veranstaltung ](media/d86879edc781b43782f36a05c3ad68685c00c6c6.jpg "Einladungen der Veranstaltung ")

Durch Klick auf die Einladung können Sie diese entweder annehmen oder ablehnen. Nach der Annahme gelangen Sie automatisch in den Raum.

Falls Sie ohne Zugang einen Link zugesendet bekommen haben, verfahren Sie so: Klicken Sie wenige Minuten vor Beginn der Veranstaltung auf den Link in der Einladung um direkt in den digitalen Veranstaltungsraum zu gelangen.

Am besten benutzen Sie ein geeignetes Headset mit Mikrofon und einen Laptop oder PC. Es ist ebenfalls möglich, über Tablet und Smartphone an einer digitalen Veranstaltung teilzunehmen. Nach Klick auf den Einladungslink sehen Sie als erstes einen Startbildschirm:

![Einladung annehmen](media/543140d019ed4ab9802703c244ee01f3c0ee9b03.jpg "Einladung annehmen")

Klicken Sie auf **Beitreten** und geben Sie Ihren Namen ein.

Durch Klick auf **Starten** gelangen Sie in den digitalen Veranstaltungsraum.

![Digitaler Veranstaltungsraum](media/aa06836aa901c351e4ca2c7bac8ee31c5d0cc082.png "Digitaler Veranstaltungsraum")

Wenn die Moderatorin oder der Moderator ein Passwort für die Konferenz vergeben hat, steht es in Ihrer Einladung. Geben Sie es bei Aufforderung ein. Es kann sein, dass Sie auch noch nach der Aktivierung für Mikrofon und Kamera in Ihrem Browser gefragt werden – bestätigen Sie dies.

Wenn Sie der **Konferenz beitreten**, bevor die Moderatorin oder der Moderator anwesend ist, befinden Sie sich zunächst **im Warteraum**. Sie werden automatisch in die Videokonferenz geholt, sobald die Moderatorin oder der Moderator die Veranstaltung beginnt. Sie sehen dann zunächst diesen Bildschirm:

![Warteraum der Konferenz](media/60974ad997e530724a458263d3de9bbcb63f4f7f.jpg "Warteraum der Konferenz")

Falls der Lobbymodus aktiv ist, müssen Sie warten, bis Ihre Anfrage angenommen wurde.

![Beitritt Anfragen](media/54b6153cdd43ac968bb61f6f1d066fd4eb3cf278.jpg "Beitritt Anfragen")
