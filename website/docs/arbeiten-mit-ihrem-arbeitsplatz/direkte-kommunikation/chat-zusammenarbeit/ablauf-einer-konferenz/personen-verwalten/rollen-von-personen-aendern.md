# Rollen von Personen ändern

Klicken Sie auf **Raum-Info** und anschließend auf **Raumeinstellungen** . Es öffnet sich das Menü für **Raumeinstellungen** . Klicken Sie auf **Rollen und Berechtigungen** . Wählen Sie die teilnehmende Person, dessen Rolle Sie ändern möchten oder suchen Sie die Person über **Benutzer im Raum suchen** . Sie können sich im Dropdown Menü zwischen

- **Teilnehmer*in**,
- **Assistent*in***,
- **Moderator*in** oder
- **Selbstdefiniertes Berechtigungslevel** entscheiden.

Durch die Wahl verändern Sie die Rolle der entsprechenden Person.

![Rollen eines Teilnehmers ändern](media/d43f171d193e128fc0f2d53c2748dacf1ab97804.PNG "Rollen eines Teilnehmers ändern")

Klicken Sie anschließend auf **Anwenden**, um die Änderung zu übernehmen.

![Anwenden-Button](media/f7890e30cfdc7e0a5cc5b1c4f23049bbbcb0f68e.PNG "Anwenden-Button")
