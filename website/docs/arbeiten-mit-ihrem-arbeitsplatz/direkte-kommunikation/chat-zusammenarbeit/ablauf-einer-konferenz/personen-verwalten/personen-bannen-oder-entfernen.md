# Personen bannen oder entfernen

Wählen Sie die Personen aus, welche Sie bannen oder entfernen möchten, und scrollen sie nach unten. Wählen Sie dann unter **Adminwerkzeuge** die Funktion **Aus dem Raum entfernen** oder **Bannen** aus.

![Teilnehmerinnen und Teilnehmer bannen oder entfernen](media/7785c1424e3604232ef999e5e83460c35fac54dc.png "Teilnehmerinnen und Teilnehmer bannen oder entfernen")
