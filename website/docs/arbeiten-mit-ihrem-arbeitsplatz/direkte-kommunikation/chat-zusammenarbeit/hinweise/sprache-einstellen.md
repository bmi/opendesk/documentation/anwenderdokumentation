# Sprache einstellen

Sie können die Sprache über **weitere Einstellungen -&gt; Einstellungen** ändern. Die Standardsprache ist Deutsch.

![Sprache ändern](media/9ad8b4a4144ff23e05eabe491c06be1a3ffe553b.png "Sprache ändern")

Klicken Sie im nächsten Menü auf **Mehr** und wählen Sie Ihre Sprache aus:

![Sprache auswählen](media/d6cf1ba826d3588ea52b30141ab51594fbc24638.png "Sprache auswählen")

Diese Sprache ist bei Ihrer nächsten Anmeldung dann voreingestellt.
