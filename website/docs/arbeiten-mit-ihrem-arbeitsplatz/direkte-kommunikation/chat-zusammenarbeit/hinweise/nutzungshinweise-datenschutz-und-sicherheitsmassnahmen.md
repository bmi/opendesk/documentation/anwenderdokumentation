# Nutzungshinweise, Datenschutz und Sicherheitsmaßnahmen

## Nutzungshinweise

Verwenden Sie zur Nutzung des Moduls **Chat &amp; Zusammenarbeit** die neueste Version eines chromiumbasierten Internet-Browsers wie Microsoft Edge, Opera, Chromium oder Google Chrome. Die Nutzung anderer Internet-Browser wie Mozilla Firefox oder Safari, sowie veralteter Versionen, kann zu Funktionalitätseinschränkungen führen. Es werden keine weiteren Plug-Ins, Add-Ons oder Extensions benötigt.

Um an einer Online-Konferenz teilnehmen zu können, wird eine stabile und schnelle Internetanbindung (50Mbit/s), sowie ein Endgerät mit Mikrofon, Lautsprecher oder Kopfhörer und ggf. Kamera empfohlen.

Kollaboration funktioniert auf den verbreiteten mobilen Web-Browsern unter Android ab Version 5.0 und iOS ab Version 12 sowie iPadOS ab Version 13. Für ein optimales Erlebnis nutzen Sie bitte stets die aktuelle Version. Wie oben beschrieben, kann es bei der Nutzung von Safari zu Problemen kommen.

**Hinweis:** Die Apps **Jitsi-Meet** und **Element** sind aufgrund fehlender Funktionen (z. B. Bildschirmfreigabe) für die Moderation und Teilnahme an Meetings nicht geeignet. Um alle Funktionalitäten (z. B. das Freigeben des Bildschirms) nutzen zu können, sollte als Endgerät ein Notebook bzw. PC mit dem Betriebssystem Windows 10, Mac OS oder Linux verwendet werden.

## Datenschutz und Sicherheitsmaßnahmen

Das Modul **Chat &amp; Zusammenarbeit** wird sicher in deutschen Rechenzentren betrieben und entspricht höchsten Datenschutz- und IT-Sicherheitsstandards. Durch die Nutzung von herstellerunabhängiger Open-Source-Software stellt Kollaboration darüber hinaus die digitale Souveränität deutscher Bildungsinstitutionen sicher.
