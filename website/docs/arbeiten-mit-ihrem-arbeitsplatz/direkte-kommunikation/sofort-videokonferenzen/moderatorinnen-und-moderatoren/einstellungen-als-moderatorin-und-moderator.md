# Einstellungen als Moderatorin und Moderator

Als Moderatorin oder Moderator können Sie Einstellungen vornehmen. Wählen Sie dazu das Drei-Punkte-Menü **Weitere Einstellungen.**

![Weitere Einstellungen (ausgeblendet)](media/d6a286012b0a8aefba57a818452414b87fcf92cf.PNG "Weitere Einstellungen (ausgeblendet)")

- Über **Qualitätseinstellungen** regulieren Sie Ihre Übertragungsqualität des Videos
- Bei den **Sicherheitsoptionen** gibt es zwei Optionen, den Zugang zur Konferenz zu schützen - zum einen durch die Vergabe eines Passwortes und zum anderen durch die Aktivierung einer Lobby
- Wenn Sie Ihr Mikrofon einschalten und dann auf **Rauschunterdrückung** klicken, werden Hintergrundgeräusche entfernt
- Mit **Hintergrund auswählen** können Sie einen Hintergrund aussuchen, der hinter Ihnen im Video zu sehen ist

Über **Einstellungen** können Sie Ihre Geräte, wie Mikrofon, Audioausgabe oder die Kamera auswählen. Auch hier können Sie Ihren Anzeigenamen und Hinweistöne einstellen. 

**Hinweis:** Sie haben auch die Möglichkeit direkt über die Symbolleiste **Einstellungen** für Ihre Kamera oder Ihr Mikrofon vorzunehmen. Klicken Sie dafür auf die kleinen Pfeile über den Symbolen:

![Kamera Einstellungen](media/20f41205572d8a2319d58976f4c5c981b68bb6ff.png "Kamera Einstellungen")

Um weitere Einstellungen vorzunehmen können Sie auf **Anwesende** klicken:

![Symbol Anwesende](media/7a8abdc2d616548770c0885d69768eecbc9e0dc3.png "Symbol Anwesende")

1. Auf der rechten Seite haben Sie nun die Möglichkeit alle Teilnehmerinnen und Teilnehmer stumm zu schalten
1. Wenn Sie auf das Icon mit den drei Punkten klicken, können Sie alle Kameras der Teilnehmerinnen und Teilnehmer ausschalten und weitere Moderationsoptionen öffnen

![Moderationsoptionen](media/16d1767cd3fb907598a986d8ed0e17f6bf866001.PNG "Moderationsoptionen")
