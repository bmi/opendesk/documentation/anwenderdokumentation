# Konferenz durchführen

Auf dieser Seite finden Sie eine Anleitung für die Durchführung von Konferenzen.

## Vor der Konferenz

Es ist empfehlenswert, sich als Moderatorin oder Moderator rechtzeitig vor dem Konferenztermin in den Raum einzuloggen, um z. B folgende Dinge vorab zu erledigen:

- Raumpasswort festlegen (siehe [Konferenzraum vorbereiten](konferenzraum-vorbereiten.md))
- eigenes Mikrofon testen
- eine Begrüßungsfolie freigeben (s. u.)
- technische Schwierigkeiten von Teilnehmerinnen und Teilnehmer klären  

**Hinweis:** Teilnehmerinnen und Teilnehmer können den angelegten Konferenzraum erst betreten, wenn eine Moderatorin oder ein Moderator eingeloggt ist. Sobald sich Teilnehmerinnen und Teilnehmer im Raum befinden, sollte man sich als moderierende Person nicht mehr abmelden. Bei der erneuten Anmeldung im Raum, kann es ansonsten zu Problemen mit der Bildschirmfreigabe bzw. mit der Audiofunktion kommen.

Sollen bereits beim Login die Mikrofone aller Teilnehmerinnen und Teilnehmer automatisch stumm geschaltet werden, kann dies folgendermaßen eingestellt werden: 

Gehen Sie auf das **Drei-Punkte-Menü (Weitere Einstellungen)** in der Symbolleiste und dann auf **Einstellungen**.

![Symbolleiste: Weitere Einstellungen (ausgeblendet)](media/4fa884ee75597a50462daa6522d8da836a149e5e.jpg "Symbolleiste: Weitere Einstellungen \(ausgeblendet\)")

Anschließend gehen Sie im geöffneten Fenster auf **Moderation** und klicken Sie die Haken bei **Alle Personen treten stummgeschaltet bei** und bei **Alle Personen treten ohne Video bei** an:

![Fenster: Einstellungen](media/bd828edbd9f1f8257fdae30253ab3902b6881cea.png "Fenster: Einstellungen")

## Zu Beginn einer Konferenz

**Eigenes Mikrofon und ggf. eigene Kamera aktivieren:** Durchgestrichene Kamera und Mikrofon bedeuten, dass diese ausgeschaltet sind. Um diese zu aktivieren müssen Sie auf das zugehörige Symbol klicken:

![Symbolleiste: Mikrofon / Kamera](media/cd4ce850480cddf76f8d74f811ebb26147e7fcaf.jpg "Symbolleiste: Mikrofon / Kamera")

**Hinweise** an alle Teilnehmerinnen und Teilnehmer:

- Bei Nutzung eines nicht geeigneten **Internetbrowsers** (z. B. 	Firefox, Safari) ausloggen und mit kompatiblem Browser (siehe 	Vorraussetzungen und Vorbereitungen) erneut einloggen.
- **Mikrofon deaktivieren** und nur bei Aufforderung aktivieren; bei 	aktivem Mikrofon störende Geräusche (Husten, Räuspern, Tippen auf 	der Tastatur) vermeiden.
- Wortbeiträge über **Meldungen** (Handzeichen-Symbol, s. u.) anzeigen oder den **Chat** benutzen.

Teilnehmerinnen und Teilnehmer können sich selber stumm schalten oder die Kamera beenden. Dazu muss in der Symbolleiste auf das **Mikrofon** und/oder die **Kamera** geklickt werden.  

**Ggf. alle Teilnehmerinnen und Teilnehmer** **stumm schalten**:

- Klicken Sie dazu auf das **Anwesende-Icon** mit den 2 Personen und wählen Sie **Alle stumm schalten**
- Alle Teilnehmerinnen und Teilnehmer werden nun automatisch stumm geschaltet. Alle Teilnehmerinnen und Teilnehmer können im Anschluss jedoch selbstständig wieder das Mikrofon in der Symbolleiste freischalten

![Alle stummschalten](media/c65699fe997eaa885bfe834b6a7e8761ac5fab73.png "Alle stummschalten")

**Kachelansicht einschalten:**

Für eine übersichtliche Ansicht aller Teilnehmerinnen und Teilnehmer, aktivieren Sie die Kachelansicht mit den entsprechenden Kachelsymbol.

![Symbolleiste](media/cd4ce850480cddf76f8d74f811ebb26147e7fcaf.png "Symbolleiste")

Die Teilnehmerinnen und Teilnehmer werden nun in einer Übersicht angezeigt.

![Teilnehmer Kachelansicht](media/30c4e56a6c6a13eca85ac7db9f0c2d9d604941b2.png "Teilnehmer Kachelansicht")

## Während der Konferenz

Während der Konferenz können verschiedene Aktionen ausgeführt werden:

**Bildschirm freigeben**:

- Zunächst das Programm bzw. Dokument, das allen Teilnehmerinnen und Teilnehmern gezeigt werden soll, auf dem eigenen Endgerät öffnen
- Im Anschluss das **Bildschirmfreigabe-Symbol** anklicken

![Symbolleiste: Bildschirm freigeben](media/cd4ce850480cddf76f8d74f811ebb26147e7fcaf.png "Symbolleiste: Bildschirm freigeben")

Wählen Sie aus, ob der gesamte Bildschirm, ein Anwendungsfenster oder ein Browser-Tab für alle angezeigt werden soll. Bestätigen Sie Ihre Auswahl mit **Teilen.**

![Fenster: Auswählen, was Sie teilen wollen](media/26ae7f063a90c00d7ad404f2d609e9750e5028b6.jpg "Fenster: Auswählen, was Sie teilen wollen")

- Den Teilnehmerinnen und Teilnehmern wird im Anschluss der gewählte Bildschirminhalt als Vollbild angezeigt. Ein Verändern des Inhalts durch die Teilnehmerinnen und Teilnehmer ist nicht möglich.
- Die Freigabe kann durch das erneute Anklicken des Freigabe-Symbols bzw. über den Button **Nicht mehr teilen** beendet werden:

![&quot;Nicht mehr teilen&quot; Button](media/3aeb72665896504580de0190132a93cd660f5998.jpg "&quot;Nicht mehr teilen&quot; Button")

**Hinweis:** Die Freigabe eines Anwendungsfensters bzw. eines Browser-Tabs hat den Vorteil, dass Teilnehmerinnen und Teilnehmer nicht den gesamten Bildschirminhalt angezeigt bekommen und weitere Anwendungsfenster (z. B. ein E-Mail Programm) unsichtbar bleiben. Der Vorteil beim Teilen des gesamten Bildschirms ist, dass man zwischen mehreren Anwendungen, die gezeigt werden sollen, hin- und herspringen kann.

**Infos und Aktionen für Teilnehmerinnen und Teilnehmern**:

Über Randsymbole an den **Kacheln der Teilnehmerinnen und Teilnehmern** wird z. B. angezeigt, ob Mikrofon und Kamera (de)aktiviert sind bzw. ob sich jemand meldet. Per Klick auf das Punktesymbol am Rand der Kachel der Teilnehmerinnen und Teilnehmern, können **Aktionen für die gewählte Person** durchgeführt werden.

![Teilnehmerin und Teilnehmer Informationen](media/de79c87476abc403cb72d1857691d31860ad18dc.jpg "Teilnehmerin und Teilnehmer Informationen")

**Folgende Aktionen sind beim 3 Punkte Symbol möglich**:

- Alle Teilnehmerinnen und Teilnehmer mit Ausnahme der gewählten Person stumm schalten.
- Die Kamera aller Teilnehmerinnen und Teilnehmer mit Ausnahme der gewählten Person ausschalten.
- Moderationsrechte an die Person vergeben.
- Gewählte Person aus der Konferenz entfernen.
- Private Nachricht an die gewählte Person senden.
- Lautstärke der gewählten Person verändern.

**Breakoutsessions erstellen**:

Sie können sogenannte Breakoutsessions erstellen, also Gruppenarbeitsräume. Klicken Sie dafür auf die Schaltfläche **Anwesende (3 Punkte Menü).** Hier können Sie nun einen Breakout-Raum erstellen.

Nachdem Sie einen Raum hinzugefügt haben, können Sie den Teilnehmerinnen und Teilnehmern einem Raum zuordnen. Klicken Sie dazu auf die drei Punkte am Benutzernamen und wählen Sie unten den gewünschten Breakout-Raum aus.

Der Breakout-Raum kann auch von einer anderen moderierenden Person eröffnet werden, diese muss dann noch einmal ihre Anmeldedaten hinterlegen.

**Videoqualität einstellen**:

**Hinweis**: Die Videoqualität sollte bei instabilen Verbindungen reduziert werden. Die Änderungen wirken sich nur auf das eigene Gerät aus. Wählen Sie dazu das Punkte-Symbol unten rechts in der Symbolleiste aus und klicken Sie auf **Qualitätseinstellungen**:

**Die Videoqualität verändern**:

Wenn die gewünschte Videoqualität technisch nicht möglich ist, wird nur noch das Audiosignal übertragen - verringeren Sie in diesem Fall die Videoqualität.
