# Breakoutsessions erstellen

Sie können sogenannte Breakoutsessions erstellen, also Gruppenarbeitsräume. Klicken Sie dafür auf **Anwesende**:

![Symbolleiste: Teilnehmer*Innen](media/2089939a05ae8172a294e3127c6ca686e4c84a37.png "Symbolleiste: Teilnehmer*Innen")

Es öffnet sich auf der linken Seite ein Informations-Fenster. Hier können Sie nun einen oder mehrere Breakout-Räume erstellen:

![Fenster: Breakout Raum hinzufügen](media/56a496b0b519339f08aeb3215384156e5bb59598.PNG "Fenster: Breakout Raum hinzufügen")

Nachdem Sie einen Raum hinzugefügt haben, können Sie die Teilnehmerinnen und Teilnehmer dem Raum zuordnen. Klicken Sie dazu auf die **drei Punkte am Benutzernamen** und wählen Sie unten den gewünschten Breakout-Raum aus:

![Breakout Raum](media/0436659bcbb614a68fd6f998620283ef3c1f68ca.png "Breakout Raum")

Wenn Sie oder Teilnehmerinnen und Teilnehmer selbstständig in einen Breakout-Raum gehen möchten, gibt es auch die Möglichkeit eigenständig den Raum zu betreten. Dazu gehen Sie mit der Maus auf den erstellten Breakout-Raum und klicken Sie **Teilnehmen**:

![Breakout-Raum Teilnehmen](media/082b90e7785bea46cb95e977befe81f3158a33af.png "Breakout-Raum Teilnehmen")

Um den Breakout-Raum zu verlassen und in den Hauptraum zurückzukehren klicken Sie auf **Breakout-Raum verlassen**:

![Breakout-Raum verlassen](media/b07d9cb4dd7e6d3900fb709b43ee5f272be57e51.png "Breakout-Raum verlassen")
