# Umfragen

Moderatorinnen und Moderatoren sowie Konferenzteilnehmerinnen und Konferenzteilnehmer können Umfragen erstellen. Öffnen Sie dazu den Chat-Bereich über das **Chat-Symbol:**

![Symbolleiste: Chat](media/49446284ff2dc208c9d77780ccb46bd6d6fdea41.jpg "Symbolleiste: Chat")

Hier können Sie das Register **Umfrage** auswählen und **Fragen mit den entsprechenden Antworten** erstellen. Die Reihenfolge der Antworten kann nachträglich per Drag &amp; Drop geändert werden.

![Umfrage erstellen](media/ea379c7a75e2f77ec682e41d6b5e1baf6cc4a22a.jpg "Umfrage erstellen")
