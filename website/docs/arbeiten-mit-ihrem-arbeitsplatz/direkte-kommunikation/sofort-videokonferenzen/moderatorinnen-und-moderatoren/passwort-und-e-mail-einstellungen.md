# Passwort- und E-Mail-Einstellungen

Hier gibt es 3 weitere Auswahlmöglichkeiten:

1. Passwort vergessen
1. Kontozugang schützen
1. Passwort ändern

## Passwort vergessen

Wenn Sie Ihr Passwort vergessen haben müssen, Sie hier Ihren Benutzernamen eingeben. Sie erhalten anschließend die Benachrichtigung, dass ein Token versendet wurde. Sie bekommen eine E-Mail mit einem neuen Link bzw. Token vom Absender: **no-reply@domainname**.

Folgen Sie dabei den Hinweisen der E-Mail. Sie können nun ein neues Passwort setzen.

**Hinweis:**

- Bitte beachten Sie, dass der Token eine Gültigkeit von 2 Tagen hat und danach erneut angefordert werden muss!
- Die E-Mail von no-reply@domainname erhalten Sie zum erstmaligen Einrichten Ihres Accounts, vor der ersten Anmeldung und bei der aktiven Anforderung eines neuen Passworts mittels **Passwort vergessen**
- Wenn Sie die E-Mail nicht in Ihrem Postkorb finden können, prüfen Sie bitte zusätzlich Ihren Spam-Ordner

## Kontozugang schützen

Wenn Sie Ihre E-Mail-Adresse ändern möchten, geben Sie unter **Kontozugang schützen** Ihren Benutzernamen und Passwort ein. Klicken Sie dann auf **weiter**.

Geben Sie Ihre neue E-Mail-Adresse ein. Klicken Sie auf **Speichern** um die Änderungen zu übernehmen.

## Passwort ändern

Wenn Sie Ihr Passwort ändern möchten, geben Sie unter **Passwort ändern** Ihren Benutzernamen, Ihr altes Passwort und anschließend ein neues Passwort ein.

Klicken Sie auf **Passwort ändern** ganz unten, um Ihre Änderungen zu übernehmen.
