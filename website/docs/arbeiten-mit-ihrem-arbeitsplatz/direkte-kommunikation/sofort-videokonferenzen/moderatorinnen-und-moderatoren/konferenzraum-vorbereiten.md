# Konferenzraum vorbereiten

Wenn Sie eine Videokonferenz vorbereiten möchten, können Sie die folgenden Schritte durchgehen:

## 1. Konferenzraum erstellen

Öffne Sie einen geeigneten Internetbrowser (siehe [Vorraussetzungen und Vorbereitungen](../allgemeine-anleitungen/voraussetzungen-und-vorbereitungen.md)).

Öffnen Sie das Modul **Sofort Videokonferenz** wie unter **Allgemeine Anleitungen – Was ist Sofort Videokonferenz** beschrieben.

- Einen Link zu Ihrem Konferenzraum können Sie erstellen, indem Sie einen Namen des Konferenzraums (zum Beispiel "Konferenzraumname") in das Textfeld vor **Meeting starten** eingeben.
- Der Name ist **frei wählbar** . Vermeiden Sie Namen mit Leerzeichen oder Schrägstrich ("/"). Dies kann zu ungültigen Links führen. Nutzen Sie am besten Raummnamen wie: "Meeting3B" oder "Freitags_Meeting"
- Sie können auch die Eingabe der Raumbezeichnung direkt über das Eingabefeld auf der Startseite eingeben und mit **Raum erstellen** bestätigen.

![](media/949e7c5f31e8dd20cafb28135ca3a70b44168f31.png "")

## 2. Konferenzraum eröffnen

Die Teilnehmerinnen und Teilnehmer sollten den Konferenzraum erst aufsuchen, nachdem Sie diesen bereits betreten haben. Deshalb empfiehlt es sich, den Raum immer kurz vor dem Beginn des Meetings zu öffnen.

## 3. Raum-Passwort vergeben

Damit der Konferenzraum nur einer ausgewählten Gruppe zugänglich ist, sollte ein Raum-Passwort vergeben werden. Dazu können Sie in der Menüleiste auf **Sicherheitsoptionen** klicken.

![](media/b9c0eb5c0ab0ee76b0532e167704d634462a727b.png "")

Anschließend öffnet sich das Sicherheitsoptionen-Fenster, es gibt zwei Möglichkeiten Ihre Konferenz zu schützen:

- Wählen Sie **Lobby aktivieren** . So gelangen Teilnehmerinnen und Teilnehmer in einen Warteraum und können erst nach Ihrer Zustimmung dem Meeting beitreten.
- Sie können über **Passwort hinzufügen** ein Raum-Passwort erstellen. Dieses müssen die Teilnehmerinnen und Teilnehmer eingeben, bevor Sie dem Meeting beitreten können.

**Hinweis**: Das Passwort wird nicht dauerhaft gespeichert und muss beim nächsten Betreten des Raumes erneut gesetzt werden.

![Fenster: Sicherheitsoptionen](media/1d04420a04d269710c78535396bc873725889a2b.png "Fenster: Sicherheitsoptionen")

## 4. Link und Passwort versenden

Wenn Sie die vorherigen Schritte durchgeführt haben, ist Ihr Konferenzraum vorbereitet. Sie können jetzt den Teilnehmerinnen und Teilnehmern Ihren Raumnamen und das von Ihnen festgelegte Passwort senden.