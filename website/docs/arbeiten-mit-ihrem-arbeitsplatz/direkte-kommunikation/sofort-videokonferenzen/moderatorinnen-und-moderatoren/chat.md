# Chat

In einem Konferenzraum kann mit allen Teilnehmerinnen und Teilnehmern gechattet werden. Der private Nachrichtenaustausch zwischen einzelnen Konferenzteilnehmerinnen und Konferenzteilnehmern wird zusätzlich angeboten.

## Öffentlicher Chat

In dem öffentlichen Chat können Sie allen Konferenzteilnehmerinnen und Konferenzteilnehmern Nachrichten senden. Den Chat können Sie über das **Chat-Symbol** öffnen:

![Symbolleiste: Chat öffnen](media/c9992cd58d745ddd7c03e3ac8faf2bf5418cfdf1.png "Symbolleiste: Chat öffnen")

In dem sich anschließend öffnenden Chat-Fenster können Sie jetzt Nachrichten lesen und schreiben:

![Chat-Fenster](media/04c446e9e5676c7874baee456985c72bcb895efc.png "Chat-Fenster")

## Privater Chat

Wenn Sie wollen, dass **nur eine bestimmte Person** Ihre Nachricht bekommt, können Sie diese über **Private Nachrichten** senden. Wenn Sie auf das violette Icon mit den drei Punkten **in der Kachel der Person** klicken, können Sie eine private Nachricht senden:

![Private Nachricht senden](media/331e61d22b1b01808bc79ba32e83d98a52736821.png "Private Nachricht senden")

Die folgenden Nachrichten, die Sie schreiben, werden nun an nur eine Person gesendet. Wenn Sie wieder in den öffentlichen Chat schreiben möchten, klicken Sie auf das weiße **X.**

![Private Nachricht rot gefärbt](media/bc1d212d50aa1d885111885c29e1d8c4f36c1b1c.png "Private Nachricht rot gefärbt")
