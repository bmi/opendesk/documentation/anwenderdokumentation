# Teilnehmerinnen und Teilnehmer einladen

Neue Teilnehmerinnen und Teilnehmer können über einen Link zu einem Konferenzraum eingeladen werden. Hierzu müssen Sie einfach den **Link aus der Adresszeile des Browsers kopieren** und den gewünschten Teilnehmerinnen und Teilnehmern schicken.

![Link kopieren](media/6fe7f27657afd4bf59b2ae38b0f289d4e6b42495.png "Link kopieren")

Teilnehmerinnen und Teilnehmer können auch über die grüne Schaltfläche **In diesen Raum einladen** eingeladen werden:

![](media/421e5aa8f4f9e87d13d9a881c3dbe3d095d9a98e.png "")

Es öffnet sich ein Fenster mit einer Suchmaske, in die Sie den Namen der gewünschten Person eingeben und dann aus der Liste von Vorschlägen auswählen können. Im Beispiel werden keine Vorschläge angezeigt, da der gesuchte Name nicht existiert.

Wiederholen Sie diesen Vorgang, bis Sie alle gewünschten Personen hinzugefügt haben. Klicken Sie abschließend auf **Einladen**.

![](media/deb06907437058d056fdd2379a85f74b1e2b4ba8.png "")

Auch aus der Sitzung heraus können Teilnehmerinnen und Teilnehmer durch die Moderatorin oder den Moderator oder durch andere Konferenzteilnehmerinnen und Konferenzteilnehmer eingeladen werden. Nutzen Sie dazu das Steuerungssymbol **Anwesende.**

![Symbolleiste](media/7820ca11a3f92755fa1333fd036b445a39ba21b4.png "Symbolleiste")

Im rechten Fensterbereich werden die anwesenden Personen eingeblendet. Klicken Sie hier auf **Person einladen.**

![Personen einladen](media/0fe9d432d95eca3e84c3c58e8db9044cc86fb017.jpg "Personen einladen")

Im Anschluss öffnet sich das Fenster **Mehr Leute einladen** . Hier können Sie die Linkadresse der Videokonferenz kopieren oder Einladungen zur Videokonferenz an ausgewählte E-Mail-Accounts verschicken.

![Mehr Leute einladen](media/5b653c84c4a929f3b8d1e9c14997834c0183b6a3.jpg "Mehr Leute einladen")
