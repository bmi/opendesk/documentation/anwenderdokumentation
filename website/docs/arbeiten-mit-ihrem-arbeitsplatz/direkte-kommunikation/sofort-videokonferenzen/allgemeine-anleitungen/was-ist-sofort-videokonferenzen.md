# Was ist Sofort Videokonferenzen?

Ein Videokonferenzsystem besteht aus einem virtuellen Konferenzraum. In diesem Raum können sich mehrere Teilnehmerinnen und Teilnehmer per Audio, Video und Chat austauschen. Eingerichtet wird der Raum von einer Moderatorin oder einem Moderator. Die Moderation bzw. der Moderator sorgt über Einladungen dafür, dass weitere Teilnehmerinnen und Teilmehmer den Raum betreten können.

Sie finden das Modul ganz unten auf der Startseite des Portals als Teil des Abschnitts **Direkte Kommunikation**.

![Die Modulübersicht von OpenDesk](media/c138d45ee9b60521456074d02500e943fb5bb678.png "Die Modulübersicht von OpenDesk")

Öffnen Sie **Sofort Videokonferenzen** über die entsprechende Schaltfläche.

![Sofort Videokonferenzen in der Modulübersicht](media/904fb95c6ec88c187fb8222483b20c1a18f89e96.png "Sofort Videokonferenzen in der Modulübersicht")
