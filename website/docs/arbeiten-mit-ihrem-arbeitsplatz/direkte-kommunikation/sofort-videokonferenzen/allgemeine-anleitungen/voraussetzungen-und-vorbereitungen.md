# Voraussetzungen und Vorbereitungen

Um an einer Online-Konferenz teilnehmen zu können, wird eine stabile und schnelle **Internetanbindung** sowie ein **Endgerät** mit **Mikrofon, Lautsprechern / Kopfhörern** und ggf.einer **Kamera** benötigt. Dieses Endgerät muss zunächst für die Teilnahme vorbereitet werden:

Wenn noch nicht vorhanden, muss ein kompatibler Internet-Browser installiert werden. Geeignete Browser sind **Google Chrome** oder Chromium-basierte Browser wie **Microsoft Edge Chromium, Chromium** oder **Opera**.

**Wichtig**: Die Nutzung anderer Browser (z.B. Mozilla Firefox, Safari) kann zu Fehlern und Einschränkungen bei allen Teilnehmerinnen und Teilnehmern einer Sitzung führen. Auch die Nutzung von Chromium Browsern auf Apple-Geräten kann zu Fehlern führen.

**Hinweis**:

- Nutzung von VPN-Verbindungen: Eine Verbindung während einer laufenden Landes-VPN-Session ist möglich, mit Ausnahme aus dem Hamburger Landesnetz
- Nutzung aus dem Landesnetz: Eine Nutzung aus den Landesnetzen (Hamburg und Schleswig-Holstein) ist möglich. Ggf. müssen Ausnahmen an den Browser-Proxy-Einstellungen vorgenommen werden. Bitte wenden Sie sich dazu an Ihre lokale IT-Stelle oder an Ihre Dienstleister Support-Einheit
