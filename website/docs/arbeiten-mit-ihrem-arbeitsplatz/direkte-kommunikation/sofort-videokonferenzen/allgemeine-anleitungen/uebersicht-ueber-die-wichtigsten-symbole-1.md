# Übersicht über die wichtigsten Symbole

Hier sehen Sie eine Übersicht über alle Symbole, die in dem Konferenzraum vorhanden sind.

Die Symbolleiste ist von links nach rechts folgendermaßen aufgebaut:

![Symbolleiste](media/d85881f5b6ce98ab821cc05fccc6479eba55914d.png "Symbolleiste")

- Mikrofon
- Kamera
- Bildschirmfreigabe
- Chat
- Hand
- Anwesende
- Kachelansicht
- 3-Punkte
- Konferenz verlassen

![Mikrofon ist aus](media/9bce51e6c72ef90a1527a0fa64a8a30936cf7019.png "Mikrofon ist aus")

![Mikrofon ist an](media/fa4b41ffc793e237238186affef9151f725ca60e.png "Mikrofon ist an")

Mit dem **Mikrofonsymbol**, auch als **Stummschalten** genannt, können Sie Ihr Mikrofon ein- und ausschalten. Das können Sie auch vor dem Betreten der Konferenz machen. Wenn das Symbol einen Schrägstrich hat, ist das Mikrofon ausgeschaltet, wenn es keinen hat, ist es eingeschaltet.

**Hinweis**: Das Mikrofon sollte nur auf Aufforderung der Moderatorin oder des Moderators eingeschaltet werden.

![Kamera ist aus](media/62b07dba5672f8442a47ba42e0c66bf6a8b1e20d.png "Kamera ist aus")

![Kamera ist an](media/2cad0a99639210dba98114d3a16c4e5bda8032ae.png "Kamera ist an")

Mit dem **Kamerasymbol**, auch als **Kamera stoppen** genannt, können Sie die Kamera ein- und ausschalten. Es kann auch vor dem Betreten der Konferenz ein- oder ausgeschaltet werden. Wenn das Symbol einen Schrägstrich hat, ist die Kamera ausgeschaltet, wenn es keinen hat, ist sie eingeschaltet.

**Achtung**: Zur Gewährleistung der Systemstabilität empfiehlt es sich, die Videofunktion nur bei Bedarf zu aktivieren.

![Verbinden von Mikrofon und Kamera](media/adc10620db9f184750d91349820e04b9b261ba55.png "Verbinden von Mikrofon und Kamera")

Manchmal dauert es einen kurzen Moment, bis sich das Mikrofon und/oder die Kamera mit dem Browser verbunden hat. In diesem Fall werden die Symbole weiß mit einem sich drehenden Rad.

![Einstellungspfeil](media/649c5a536cfbcd83f3b35fb37985451252dd9ca0.png "Einstellungspfeil")

![Einstellungspfeil aktiviert](media/7c9c66852b9d9ed95a9a520f9ebf7b837ef862a5.png "Einstellungspfeil aktiviert")

Der Pfeil neben dem Mikrofon- und Kamerasymbol dient zur Einstellung des Mikrofons bzw. der Kamera. Er wird weiß, wenn er aktiviert ist.

![Bildschirm nicht mehr teilen](media/3ffdf0cdd107106d395ece7392f91ed3ec2fc873.png "Bildschirm nicht mehr teilen")

![Bildschirm teilen](media/861560541fa50d71fa6f192b472177e1364064f8.png "Bildschirm teilen")

Mit dem **Bildschirmsymbol**, auch als **Bildschirmfreigabe starten** genannt, können Sie Ihren eigenen Bildschirm an alle anderen Teilnehmerinnen und Teilnehmer zeigen. Wenn das Symbol keinen Schrägstrich hat, sind Sie in der Lage, Ihren Bildschirm zu teilen. Wenn das Symbol aber einen Schrägstrich hat, bedeutet das, dass Sie Ihren Bildschirm teilen und dieser dient als Hinweis, um die Bildschirm-Freigabe zu beenden.

![Chat Symbol](media/95357550f9378b28de57c5d64dbdeff92ffd1dc7.png "Chat Symbol")

Mit dem **Chat-Symbol**, auch als **Chat öffnen** genannt, kann das Chat-Fenster geöffnet oder geschlossen werden. Damit können Nachrichten geschickt werden.

Wenn dieses Symbol aktiviert ist, wird es in der Symbolleiste grau markiert.

![Hand-Symbol und Reaktionen](media/e5c347d19c03b544cbc66d173ec1f919ad8cd647.png "Hand-Symbol und Reaktionen")

Mit dem **Handsymbol**, auch als **Hand heben** genannt, können Sie der Moderatorin oder dem Moderator ein Hanzeichen geben. Durch erneutes Klicken auf das Symbol wird die Meldung zurückgenommen.

Mit dem Pfeil daneben können Sie verschiedene Reaktionen auslösen: **Daumen hoch, Klatschen, Lachen, Überrascht sein, Buhrufe** und **Stille**. Alle sechs haben integrierte Geräusche.

![Symbol mit Anzahl der Teilnehmerinnen und Teilnehmer](media/f172cad9ff1962fefaa5484466885dc652e8ddf7.png "Symbol mit Anzahl der Teilnehmerinnen und Teilnehmer")

Mit dem **Teilnehmerinnen und Teilnehmer-Symbol** , auch **Anwesende** genannt, können Sie eine Liste von allen Teilnehmerinnen und Teilnehmern sehen. Um die Liste zu schließen, klicken Sie erneut auf das Symbol. Die Anzahl der Teilnehmerinnen und Teilnehmer wird in einem Kreis auf dem Symbol angezeigt.

![Kachelansicht-Symbol](media/49e002b6ffa9b9d8c3353eb7683952c322708bdc.png "Kachelansicht-Symbol")

Mit dem **Kachelansicht-Symbol**, auch als **Kachelansicht ein-/ausschalten** genannt, können Sie die Teilnehmerinnen und Teilnehmer übersichtlicher anzeigen lassen. Wenn das Kachelansicht-Symbol eingeschaltet ist, sehen Sie alle Teilnehmerinnen und Teilnehmer gleich groß in Ihrem Fenster. Wenn es ausgeschaltet ist, sehen Sie die Person, die gerade redet, am größten auf dem Bildschirm und den Rest der Teilnehmerinnen und Teilnehmer kleiner auf der rechten Seite des Fensters.

![Weitere Einstellung Symbol](media/8d1252d27a97af911079f468e219d6b1aee16f57.png "Weitere Einstellung Symbol")

Mit der **3-Punkte-Symbol**, auch als **Weitere Einstellung** genannt, können Sie weitere Optionen aufrufen. Hier können Sie z. B. die Qualitätseinstellungen sowie die Sicherheitsoptionen verwalten, u. a.

![Symbol zum Auflegen](media/fea25bad52f8a71428c076da0a15a5bcbfdc3c12.png "Symbol zum Auflegen")

Mit der roten Schaltfläche kann die Online-Konferenz verlassen werden.
