# Chat

In einem Konferenzraum kann mit allen Teilnehmerinnen und Teilnehmern gechattet werden. Der private Nachrichtenaustausch zwischen einzelnen Konferenzteilnehmerinnen und Konferenzteilnehmern wird zusätzlich angeboten.

## Öffentlicher Chat

In dem öffentlichen Chat können Sie allen Konferenzteilnehmerinnen und Konferenzteilnehmern Nachrichten senden. Den Chat können Sie über das **Chat-Symbol** öffnen:

![Symbolleiste: Chat öffnen](media/b8b44c768bff3cfae97ab17c414a5b8f5890f403.png "Symbolleiste: Chat öffnen")

In dem sich anschließend öffnenden Chat-Fenster können Sie jetzt Nachrichten lesen und schreiben:

![Chat-Fenster: Nachricht eingeben](media/6564e97b007a4b000f4b605d96a47220347a91e4.png "Chat-Fenster: Nachricht eingeben")

## Privater Chat

Wenn Sie wollen, dass **nur eine bestimmte Person** Ihre Nachricht bekommt, können Sie diese über **Private Nachrichten** senden. Wenn Sie auf das violette Icon mit den drei Punkten **in der Kachel der Person** klicken, können Sie eine private Nachricht senden:

![Einstellungen: Private Nachricht senden](media/84d0e0a00cd22ec0d2169a33dccc2ea0e8a86747.png "Einstellungen: Private Nachricht senden")

Die folgenden Nachrichten, die Sie schreiben, werden nun an nur eine Person gesendet. Wenn Sie wieder in den öffentlichen Chat schreiben möchten, klicken Sie auf das weiße **X**.

![Private Nachricht ist rot](media/bc1d212d50aa1d885111885c29e1d8c4f36c1b1c.png "Private Nachricht ist rot")
