# Bildschirm freigeben

- Öffne Sie als Erstes das Programm bzw. das Dokument, das allen Teilnehmerinnen und Teilnehmern gezeigt werden soll
- Klicken Sie im Anschluss auf das **Bildschirmfreigabe-Symbol**:

![Symbolleiste: Bildschirm freigeben](media/468aa7f235106f9aba0be623d90026ff47630e8f.png "Symbolleiste: Bildschirm freigeben")

Über das Symbol **Bildschirmfreigabe** **ein- und ausblenden** haben Sie verschiedene Möglichkeiten, um Bildschirminhalte allen Konferenzteilnehmern zu zeigen:

1. gesamten Bildschirminhalt
1. einzelne Programmfenster
1. einzelne Tabs des Browsers  

Im Anschluss mit **Teilen** bestätigen. Den anderen Teilnehmerinnen und Teilnehmern wird im Anschluss der gewählte Bildschirminhalt angezeigt.

![Fenster: Auswählen was Sie teilen wollen](media/1bff7c88d5bf59f84ea0fa716d395a85e7e34284.png "Fenster: Auswählen was Sie teilen wollen")

**Hinweis:** Die Freigabe eines Anwendungsfensters bzw. eines Browser-Tabs hat den Vorteil, dass die anderen Teilnehmerinnen und Teilnehmer nicht den gesamten Bildschirminhalt angezeigt bekommen und weitere Anwendungsfenster (z.B. ein E-Mail-Programm) unsichtbar bleiben. Der Vorteil beim Teilen des gesamten Bildschirms ist, dass man zwischen mehreren Anwendungen, die gezeigt werden sollen, hin- und herspringen kann.

## Bildschirmfreigabe beenden

Wenn Sie Ihren Bildschirm teilen, können Sie immer über das Informationsfenster am unteren Bildschirmrand die Bildschirmfreigabe beenden. Klicken Sie dazu **Nicht mehr teilen**.

![Nicht mehr teilen Button](media/4dc3c52a2b457c73093434553b5acedee0ec53d6.png "Nicht mehr teilen Button")

Sollte das Informationsfenster **nicht zu sehen** sein, können Sie auf das Konferenz-Fenster gehen und hier über die Menüleiste die Übertragung beenden:

![Symbolleiste: Bildschirmfreigane beenden](media/1ae22f11a8851099ceb0ab43a0ea564e3586de50.png "Symbolleiste: Bildschirmfreigane beenden")
