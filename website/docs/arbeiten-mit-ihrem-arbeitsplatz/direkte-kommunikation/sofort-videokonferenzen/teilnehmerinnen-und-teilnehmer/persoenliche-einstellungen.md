# Persönliche Einstellungen

Jede Konferenzteilnehmerin und jeder Konferenzteilnehmer kann persönliche Einstellungen vornehmen. Wählen Sie dazu das Steuerungssymbol mit den 3 Punkten **Weitere Einstellungen**.

![Symbolleiste: Weitere Einstellungen](media/36067a156eab5ba602f527b4dcd00cdc577efedc.jpg "Symbolleiste: Weitere Einstellungen")

In der aufgeklappten Leiste können Sie verschiedene Vorgänge bestimmen. Über **Qualitätseinstellungen** regulieren Sie Ihre Übertragungsqualität des Videos. Wenn Sie Ihr Mikrofon einschalten und dann auf **Rauschunterdrückung** klicken, werden Hintergrundgeräusche entfernt. Mit **Hintergrund auswählen** können Sie einen Hintergrund aussuchen, der hinter Ihnen im Video zu sehen ist. Über **Einstellungen** können Sie Ihre Geräte, wie Mikrofon, Audioausgabe oder die Kamera auswählen. Sie können auch Ihren Anzeigenamen und Hinweistöne einstellen.

![Auswahl weitere Einstellungen](media/e95d05b2184fbbb242bcc9fcd41953cf424150b8.png "Auswahl weitere Einstellungen")

**Hinweis:** Sie haben auch die Möglichkeit, direkt über die Symbolleiste Einstellungen für Ihre Kamera oder Ihr Mikrofon vorzunehmen. Klicken Sie dafür auf die Pfeile bei dem Kamera und Mikrofon Symbol.
