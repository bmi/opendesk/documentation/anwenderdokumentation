# Konferenzraum betreten

Starten Sie damit, einen geeigneten Internetbrowser zu öffnen. Kopieren Sie den **zugesandten Link** in die **Adresszeile des Browsers** , um die Konferenzseite zu laden.

**Hinweis:** Wird der zugesandte Link direkt in der E-Mail angeklickt und nicht kopiert, öffnet sich evtl. ein nicht geeigneter Browser (z. B. Firefox oder Safari).

Sie müssen die Verwendung von Mikrofon und ggf. Kamera zulassen. Je nach Browser kann diese Auswahl für die Zukunft gespeichert werden (**Entscheidung merken**).

![Verwendung von Mikrofon und Kamera zulassen](media/e159f5eac5a74dd4d5e26ee3d0f408a82874f7e6.png "Verwendung von Mikrofon und Kamera zulassen")

Bei der Erstanmeldung wird zusätzlich der Name abgefragt, der während einer Konferenz den anderen Teilnehmerinnen und Teilnehmern angezeigt wird. Sie können hier außerdem vorab über die entsprechenden Schaltflächen auswählen, ob Mikrofon und Kamera bei Betreten der Konferenz eingeschaltet sein sollen. Haben Sie alle gewünschten Einstellungen vorgenommen, bestätigen Sie mit Klick auf **Konferenz beitreten**.

![Konferenz beitreten](media/08a6a3d2d3e3113b76588598c71896a55cf3b4eb.png "Konferenz beitreten")

Sollte der Fall eintreten, dass Sie vor der Moderatorin oder dem Moderator eine Konferenz betreten wollen, sehen Sie die folgende Meldung:

![Pop-up: Warten auf den Beginn der Konferenz](media/a721bf8c489c6494b69facf448e5cf13ed788f2a.jpg "Pop-up: Warten auf den Beginn der Konferenz")

Wenn Sie diese Nachricht bekommen, müssen Sie darauf warten, dass die Moderatorin oder der Moderator den Raum betritt.

Geben Sie im Anschluss ggf. das Passwort für den Konferenzraum ein.

![Fenster: Passwort erforderlich](media/e4d6ea2ec06393bf7b09f732a71ba03ab6650b77.jpg "Fenster: Passwort erforderlich")

Sollte die Moderatorin oder der Moderator die **Lobby** aktiviert haben, dann warten Sie bitte, bis diese oder dieser Ihnen erlaubt, der Konferenz beizutreten. Alternativ können Sie auch das Passwort (falls bekannt) eingeben.

![Beitritt zur Konferenz anfragen](media/ad72b97b38039859523e48973f4890de22b35a7e.png "Beitritt zur Konferenz anfragen")

Ihr Name kann im Konferenzraum nachträglich abgeändert werden. Klicken Sie dazu oben rechts in der Ecke auf den eigenen Namen und nehmen Sie die Anpassung vor.

![Namen im Konferenzraum nachträglich ändern](media/50d7f9f30819c579d56eb4babebcdf5882829e42.png "Namen im Konferenzraum nachträglich ändern")

Weitere Tipps, die Sie beachten sollten, wenn Sie einer Konferenz beigetreten sind:

- **Mikrofon deaktivieren** und nur bei Aufforderung aktivieren. So vermeiden Sie störende Geräusche (Husten, Räuspern, Tippen auf der Tastatur)
- Wortbeiträge über **Meldungen** ([Handzeichen-Symbol](../allgemeine-anleitungen/uebersicht-ueber-die-wichtigsten-symbole-1.md)) ankündigen oder den [Chat](chat.md) benutzen
