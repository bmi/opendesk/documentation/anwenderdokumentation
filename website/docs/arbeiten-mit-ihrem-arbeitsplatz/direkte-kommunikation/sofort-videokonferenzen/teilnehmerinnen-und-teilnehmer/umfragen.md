# Umfragen

Moderatorinnen und Moderatoren sowie Konferenzteilnehmerinnen und Konferenzteilnehmer können Umfragen erstellen. Öffnen Sie dazu den Chat-Bereich über das **Chat-Symbol:**

![Symbolleiste: Chat](media/9a7710be806c66097f8c53c64c5abbc79af99ce8.png "Symbolleiste: Chat")

Hier können Sie das Register **Umfrage** auswählen und **Fragen mit den entsprechenden Antworten** erstellen. Die Reihenfolge der Antworten kann nachträglich per Drag &amp; Drop geändert werden.

![Umfrage erstellen](media/466307a770c2fd16adac03d9e141b95736d8a431.png "Umfrage erstellen")
