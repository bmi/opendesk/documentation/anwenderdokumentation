# Teilnehmerinnen und Teilnehmer einladen

Neue Teilnehmerinnen und Teilnehmer können über einen Link zu einem Konferenzraum eingeladen werden. Hierzu müssen Sie einfach den **Link** in die **Adresszeile des Browsers kopieren** und der Teilnehmerin oder dem Teilnehmer schicken.

Auch aus der Sitzung heraus können weitere Teilnehmerinnen und Teilnehmer durch die Moderatorin oder den Moderator oder durch andere Konferenzteilnehmerinnen und Konferenzteilnehmer eingeladen werden. Nutzen Sie dazu das **Steuerungssymbol Anwesende:**

![Symbolleiste. Teilnehmer markiert](media/2089939a05ae8172a294e3127c6ca686e4c84a37.png "Symbolleiste. Teilnehmer markiert")

Im rechten Fensterbereich werden die anwesenden Personen eingeblendet. Klicken Sie hier auf **Person einladen:**

![Fenster: Personen einladen](media/0fe9d432d95eca3e84c3c58e8db9044cc86fb017.jpg "Fenster: Personen einladen")

Im Anschluss öffnet sich das Fenster **Mehr Leute einladen** . Hier können Sie die Linkadresse der Videokonferenz kopieren oder Einladungen zur Videokonferenz an ausgewählte E-Mail-Accounts verschicken.

![Fenster: Mehr Leute einladen](media/5b653c84c4a929f3b8d1e9c14997834c0183b6a3.jpg "Fenster: Mehr Leute einladen")
