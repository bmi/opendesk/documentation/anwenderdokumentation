# Kontozugang schützen

Der Unterpunkt **Mein Passwort-Reset Optionen** gibt Ihnen die Möglichkeit, Ihre Konto-Wiederherstellungsoptionen hinzuzufügen oder zu aktualisieren.

Hierfür müssen Sie erneut Ihren **Benutzernamen** und Ihr **Passwort** eingeben.

![Meine Passwort-Reset Optionen](media/4f5884e1b7c51221ff92e3e568162412bb15d937.png "Meine Passwort-Reset Optionen")

Anschließend erhalten Sie die Möglichkeit, Ihre E-Mail-Adresse zu ändern. Geben Sie die von Ihnen gewünschten und benötigten Daten ein und klicken Sie danach auf **Abschicken.**
