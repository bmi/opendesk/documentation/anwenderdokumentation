# Sprache ändern

An Ihrem Arbeitsplatz können Sie die Sprache innerhalb des Portals ändern. Hierzu wählen Sie das **Hamburger-Menü** aus und wählen **Sprache ändern**.

![Sprache ändern - Hamburger-Menü](media/97cf9bb171e2b2c811b4d960563f6a1533334823.png "Sprache ändern - Hamburger-Menü")

Sie können zwischen **Englisch** oder **Deutsch** wählen.

![Sprachenauswahl](media/0a809983701aec04d228197b59e1908634defa66.png "Sprachenauswahl")
