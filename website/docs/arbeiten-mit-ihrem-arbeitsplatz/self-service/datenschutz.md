# Datenschutz

Während der Nutzung Ihres Arbeitsplatzes werden Daten von Ihnen erfasst und verwendet. Zum Schutz Ihrer Daten gibt es einen Datenschutzhinweis, der sich nach der Datenschutz-Grundverordnung richtet. Sie können Antworten auf Ihre offenen Fragen bekommen und erfahren, welche Gestaltungsmöglichkeiten es im Umgang mit Ihren Daten gibt.

Diese finden Sie unter dem Hamburger-Menü. Rechts öffnet sich eine Seitenleiste.

![Datenschutz](media/4d6df7dcac756294a456e5f834ad07daabdf5a41.png "Datenschutz")
