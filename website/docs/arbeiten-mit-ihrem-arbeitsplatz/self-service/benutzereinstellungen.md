# Benutzereinstellungen

Um zu den Benutzereinstellungen zu gelangen, müssen Sie auf das **Hamburger-Menü** klicken. Dieser befindet sich in der Kopfzeile auf der rechten Seite. Eine Seitenleiste öffnet sich. Klicken Sie auf **Benutzereinstellungen**.

![Benutzereinstellungen](media/33cb522295480969579aa50a95a64ed55c1d38b8.png "Benutzereinstellungen")

In den Benutzereinstellungen haben Sie folgende Einstellungsmöglichkeiten:

- Mein Passwort ändern
- Meine Profildaten bearbeiten
- Meine Passwort-Reset Optionen

![Benutzereinstellungen](media/67c284f284a780141044336660aae4a794ba40e6.png "Benutzereinstellungen")
