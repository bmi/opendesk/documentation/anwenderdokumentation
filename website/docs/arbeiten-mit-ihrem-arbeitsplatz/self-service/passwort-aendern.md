# Passwort ändern

Unter den **Benutzereinstellungen** können Sie über den Unterpunkt **Mein Passwort ändern** ein neues Passwort wählen.

![Passwort ändern](media/9cd4af4512fdab27c86f88b8652824be5fdc9397.png "Passwort ändern")

Geben Sie erst Ihr altes Passwort ein, um im Anschluss ein neues Passwort einzugeben. Wiederholen Sie Ihr neues Passwort und klicken Sie auf **Passwort ändern.**
