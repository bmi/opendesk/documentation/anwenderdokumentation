# Globale Benachrichtigungen

Innerhalb des gesamten Arbeitsbereiches können Sie Benachrichtigungen erhalten. Diese werden am oberen Rand eingeblendet und können über einen Klick auf die Schaltfäche **Schließen** in der rechten oberen Ecke der Benachrichtigungen wieder ausgeblendet werden. Je nach Art der Benachrichtigung muss diese auch über die Schaltfläche **Bestätigen** quittiert werden. Die Farbe und der Inhalt wird durch eine Administratorin oder einen Administrator festgelegt.
