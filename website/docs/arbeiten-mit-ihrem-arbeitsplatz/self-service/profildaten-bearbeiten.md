# Profildaten bearbeiten

Im Hamburger-Menü finden Sie unter **Benutzereinstellungen** den Unterpunkt **Meine Profildaten bearbeiten**.

**Hinweis**: Es ist möglich, dass Sie sich erneut anmelden müssen. Geben Sie erneut Ihren **Benutzernamen** und Ihr **Passwort** ein.

![Profildaten bearbeiten](media/61fc5841fdfb06f43994c1b0531594cf3aaf2848.png "Profildaten bearbeiten")

Daraufhin erhalten Sie die Möglichkeit, Ihre Profildaten zu ändern. Das können beispielsweise Ihr Foto, Ihr Name, Ihre Organisation oder eine Adresse sein.

![Daten bearbeiten](media/fe50a4709d7f97dad69a2ffaac32d89dbd7b87e4.png "Daten bearbeiten")
