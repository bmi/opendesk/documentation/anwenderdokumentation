# Überblick

Im Self-Service-Portal können Sie Ihre **Einstellungen** ändern. Mit einem Klick auf das **Hamburger-Menü** bekommen Sie einen Überblick über die Möglichkeiten der Einstellungsänderungen.

![Überblick Hamburger-Menü](media/210ec8687e888f323038374178ef2c836047cdde.png "Überblick Hamburger-Menü")
