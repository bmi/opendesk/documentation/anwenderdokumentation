# Aktivitäten

Es gibt zwei Möglichkeiten, auf das Modul **Aktivitäten** zuzugreifen. Die erste ist, wenn Sie sich auf der Startseite befinden. Klicken Sie auf das Modul **Aktivitäten**.

![Modul &quot;Aktivitäten&quot; auf der Startseite](media/44e6415411940c9e349d0b101d92ae0a43677c44.png "Modul &quot;Aktivitäten&quot; auf der Startseite")

Die zweite Möglichkeit ist, wenn Sie sich in einem anderen Modul befinden. Klicken Sie auf das **9-Punkte-Symbol** . Dort können Sie das Untermodul **Aktivitäten** auswählen.

![Module des 9-Punkte-Symbols](media/50b0b159ced8ef3b217129b8cd62b7fad4045558.png "Module des 9-Punkte-Symbols")

Alle zuletzt durchgeführten Aktivitäten werden nach Auswahl des Moduls chronologisch aufgelistet.

Die Auflistung der Aktivitäten wird in **Alle Aktivitäten** , **Von Ihnen,** **Von anderen, Favoriten, Dateiänderungen, Sicherheit, Dateifreigaben, Kommentare** und **Antivirus** aufgeteilt. Sie können sehen, welche Dokumente Sie selbst bearbeitet haben und welche freigegebenen Dokumente andere Nutzerinnen und Nutzer bearbeitet oder freigegeben haben.

![Aktivitäten](media/3596cf7ac05e7a91ffa855a65089612f6f623f5b.png "Aktivitäten")

Folgende Aktivitäten werden erfasst

- Von wem wurden Dokumente oder Ordner erstellt?
- Wer hat ein Dokument zu welchem Zeitpunkt verändert?

Wer hat wann welches Dokument gelöscht?  Veränderungen können Sie direkt überprüfen, indem Sie über den Titel des Dokuments das Dokument öffnen.

![Aktivitäten des Dokuments](media/21f761add17655dfe5ca44eeccb9f5890be95055.png "Aktivitäten des Dokuments")

![Aktivitäten vergrößert](media/9275275299063ec7230e321a20ab4198578b94eb.png "Aktivitäten vergrößert")

### Hier geht es weiter mit:

- [Einstellungen](aktivitaeten/einstellungen.md)
- [Aktivitäten filtern](aktivitaeten/aktivitaeten-filtern.md)
