# Persönliche Benachrichtigungseinstellungen

Über **Persönliche Benachrichtigungseinstellungen** können Sie festlegen, zu welchen Aktivitäten Sie Mail- oder Push-Benachrichtigungen erhalten möchten. Klicken Sie hierzu zunächst oben rechts auf Ihr **Profilbild** und dann auf **Einstellungen**.

![Einstellungen](media/729e089d8eb8369289e61d8609088d5b618fe8ef.png "Einstellungen")

Sie gelangen auf eine neue Seite. Navigieren Sie nun im Menü links zum Punkt **Benachrichtigungen**.

![Einstellungen zu Benachrichtigungen](media/0ddaac12457cc70da8cb86bf9e59c16efd3b1db0.png "Einstellungen zu Benachrichtigungen")

## Änderungen in Dateien

Es erfolgt eine Benachrichtigung, wenn Dateien geändert, geteilt oder heruntergeladen werden.

Sie können festlegen, ob Sie die Benachrichtigung als E-Mail oder Push-Nachricht erhalten möchten.

## Änderungen in Kalender, Kontakte und Aufgaben

Es erfolgt eine Benachrichtigung, wenn ein Kalendertermin, eine Aufgabe, ein Kontakt oder ein Adressbucheintrag angelegt oder geändert wurde.

Sie können festlegen, ob Sie die Benachrichtigung als E-Mail oder Push-Nachricht erhalten möchten.

## Andere Aktivitäten

Es erfolgt eine Benachrichtigung über geänderte Gruppenmitgliedschaften, Änderungen des eigenen Passworts oder der eigenen E-Mail-Adresse, Sicherheitsbelange, Kommentare für Dateien, Änderungen an System-Schlagwörtern einer Datei oder dem Fund von Viren.

## Unbearbeitete Benachrichtigungen

Sie können oben auf der Seite wählen, ob und wie oft Sie E-Mail-Erinnerungen für unbearbeitete Benachrichtigungen erhalten möchten.

## Tägliche Aktivitätsübersicht

Ganz unten auf der Seite können Sie aktivieren bzw. deaktivieren, dass Ihnen eine tägliche Aktivitätsübersicht gesendet wird.
