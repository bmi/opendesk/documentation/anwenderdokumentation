# Erste Schritte

Im Modul **Produktivität** wird ein umfangreiches Programm zum gemeinsamen Arbeiten an Dateien zur Verfügung gestellt.

Nutzerinnen und Nutzer erhalten die Möglichkeit, ihre **Dateien an einem zentralen Ort** abzuspeichern und mit weiteren Nutzerinnen und Nutzern zu teilen. Sowohl die **Echtzeit-Zusammenarbeit** mit mehreren Personen als auch die **Einzelbearbeitung** von Dokumenten wird unterstützt.

![Modulübersicht Office](media/0e18df25221fb61a5d316ad2efbfe8462243ae89.png "Modulübersicht Office")

Über das Modul **Produktivität** rufen Sie **Dateien** auf. Sie können im **Dateien-Speicher** Ihre Dateien verwalten (erstellen, speichern, bearbeiten und löschen) und mit anderen Nutzerinnen und Nutzern teilen.

Im **Dateien-Speicher** können Sie **Ordner** , **Textdateien** , **Dokumente** , **Tabellenkalkulationen** , **Präsentationen** und **Diagramme** erstellen.

Zusätzlich können Dateien **importiert** und weiter bearbeitet werden. Es werden eine Vielzahl von **Dateiformaten** bei diesem Import unterstützt.
