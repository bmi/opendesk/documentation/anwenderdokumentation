# Neue Dateien schnell erstellen und speichern

## Neue Dateien schnell erstellen

Die Schaltfläche **Erstelle neue Dateien** ist ein Schnellzugriff, um Dateien schnell zu erstellen, ohne zuvor in den Datenspeicher gehen zu müssen. Diese Schaltfläche finden Sie auf der **Startseite** unter **Produktivität**.

Nach Anklicken der Schaltfläche **Erstelle neue Dateien** öffnet sich ein Menü und Sie erhalten einen Überblick über die Untermodule **Präsentation, Tabelle** und **Dokument**. Wenn Sie mit dem Mauszeiger über die einzelnen Module fahren, werden Ihnen Felder zur Erklärung der Module angezeigt.

![Office](media/b15e0bea2a91f5bb63845ae694ac49e006fecce2.png "Office")

Der Schnellzugriff ermöglicht es, eine neue Datei zu erstellen, indem die entsprechende Schaltfläche angeklickt wird. Ein Dokument, eine Tabelle oder eine Präsentation wird in einem neuen Fenster geöffnet.

**Hinweis**: Von der Startseite aus können **nur** Dokumente, Präsentationen und Tabellen **schnell** erstellt werden. Wenn Sie einen anderen Dateityp wie z. B. ein Diagramm oder eine Textdatei erstellen möchten, muss dies im Datenspeicher geschehen, indem Sie auf das **Plus-Symbol** klicken. Dort können Sie den gewünschten Dateityp auswählen.

## Dateien herunterladen und speichern

Um eine Datei herunterzuladen, navigieren Sie zunächst in den Ordner, in dem sich diese befindet. Klicken Sie rechts vom Dateinamen auf das **Drei-Punkte-Menü** und wählen Sie im Dropdown-Menü **Herunterladen** aus.

![Datei herunterladen](media/1157af81412601596073bf12407fad1994f8a137.png "Datei herunterladen")

Sie können alternativ auch mehrere Dateien gleichzeitig auswählen und herunterladen, indem Sie links neben den Dateinamen die Kästchen der gewünschten Dateien auswählen und danach den oben beschriebenen Weg über das Dropdown-Menü gehen. Um alle Dateien im Ordner auszuwählen, klicken Sie das Kästchen ganz oben an.

![Mehrere Dateien herunterladen](media/c49fa491b2560886ae7526cdb12fee423ee69ddc.png "Mehrere Dateien herunterladen")

Je nachdem, welchen Internetbrowser Sie verwenden, kann der nächste Schritt etwas unterschiedlich aussehen. Möglicherweise werden Sie direkt in einem neuen Fenster gefragt, wo Sie die Datei bzw. die Dateien speichern möchten (Beispielbild aus Mozilla Firefox). Navigieren Sie dann zu dem gewünschten Ordner auf Ihrem Computer.

![Speicherort auswählen](media/3a658427e6d8701997a8498081dc1bdb4735f0a1.png "Speicherort auswählen")

In einem anderen Browser wie Microsoft Edge kann auch zunächst oben rechts ein Dialog erscheinen, der Ihnen Optionen zum Speichern bietet.

![Speicheroptionen](media/3315c8ce79bfe09b5a4a12c2d5975f28c246cbf1.png "Speicheroptionen")

Klicken Sie in diesem Dialog auf **Speichern** , um die Datei sofort im Standardordner (meist der Ordner **Downloads** ) zu speichern. Alternativ können Sie mit Klick auf **Speichern unter** in einem neuen Fenster zum gewünschten Ordner navigieren. Wenn Sie **Speichern unter** gewählt haben, müssen Sie den Vorgang durch erneutes **Speichern** abschließen. Die Datei ist nun im gewünschten Ordner gespeichert.
