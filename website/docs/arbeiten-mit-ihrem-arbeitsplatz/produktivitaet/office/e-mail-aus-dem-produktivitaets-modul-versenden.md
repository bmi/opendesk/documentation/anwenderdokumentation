# E-Mail aus dem Produktivitäts-Modul versenden

Sie können über das Modul **Produktivität** auch E-Mails versenden. Klicken Sie dafür oben rechts neben Ihrem Profilbild auf das **Personensymbol** . Damit öffnen Sie eine **Suchmaske** und können nach Kontakten suchen, an die Sie eine E-Mail versenden möchten. Sobald Sie die gewünschte Person gefunden haben, klicken Sie auf ihren **Namen**.

![E-Mail-Kontakte suchen](media/a1123277d4086f93d77e5d41bb83866a8a298356.png "E-Mail-Kontakte suchen")

Klicken Sie rechts neben dem Namen auf das Dreipunkt-Menü und wählen Sie die zweite Option aus. Es wird jetzt eine leere E-Mail an die von Ihnen erstellte Person erstellt. Vervollständigen Sie die E-Mail und klicken Sie wie gewohnt auf **Senden**.

![E-Mail Dialog](media/neue_email.png "E-Mail Dialog")

Sie haben außerdem die Option mit einem von Ihnen ausgewählten Kontakt zu chatten. Klicken Sie dafür ebenfalls auf das Dreipunkt-Menü und wählen Sie die erste Option aus. Es öffnet sich daraufhin ein weiteres Fenster, in dem Sie chatten können.

![Chat-Kontakt-Dialog](media/nextcloud_talk.png "Chat-Kontakt-Dialog")
