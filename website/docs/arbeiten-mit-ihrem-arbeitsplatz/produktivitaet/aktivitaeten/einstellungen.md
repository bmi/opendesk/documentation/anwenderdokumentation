# Einstellungen

Über **Aktivitätseinstellungen** unten links auf dem Bildschirm können Sie einen **RSS-Feed** für die Aktivitäten aktivieren. Setzen Sie dazu einen Haken bei **RSS-Feed aktivieren** . Daraufhin erscheint ein Textfeld mit dem dazugehörigen Link, den Sie kopieren und mit anderen teilen können.

![Aktivitäten Einstellungen](media/rss_feed.png "Aktivitäten Einstellungen")

## Persönliche Benachrichtigungseinstellungen

Klicken Sie auf **Persönliche Benachrichtigungseinstellungen** , um genauer festzulegen, ob und in welcher Form Sie über verschiedene Aktivitäten benachrichtigt werden möchten.
