# Aktivitäten filtern

Um die Auflistung der Aktivitäten zu filtern, können Sie links in der Spalte verschiedene Filter auswählen:

![Filter für Aktivitäten](media/f4b72440ae501c7916d766678d166d03f4066d44.png "Filter für Aktivitäten")

Im Folgenden werden die Filter kurz beschrieben:

## Eigene Aktivitäten

Um nur die eigenen Aktivitäten zu sehen, wählen Sie den Punkt **Von Ihnen** aus.

## Aktivitäten von anderen Personen

Unter dem Punkt **Von anderen** haben Sie die Möglichkeit, sich nur die Aktivitäten der anderen Personen in geteilten Dokumenten anzeigen zu lassen.

## Favoriten

Die Auswahl von **Favoriten** ermöglicht Ihnen das Anzeigen von Aktivitäten in Dateien, die Sie als Favoriten markiert haben.

## Dateiänderungen

Sie können nach **Dateiänderungen** filtern und sich so eine Liste anzeigen lassen, in der nur Dateien mit Änderungen aufgelistet werden.

## Sicherheit

Sie können nach **Sicherheit** filtern und sich so eine Liste anzeigen lassen, in der nur Sicherheits-Veränderungen aufgelistet werden.

## Dateifreigaben

Aktivitäten können auch nach **Dateifreigaben** gefiltert werden, sodass aufgelistet wird, wer wann welche Dateien mit wem geteilt hat.

## Kalender

Änderungen am Kalender oder an Kalenderterminen werden Ihnen unter dem Punkt **Kalender** angezeigt.

## Aufgaben

Änderungen an Aufgaben werden Ihnen durch einen Klick auf **Aufgaben** angezeigt.

## Kommentare

Unter dem Punkt **Kommentare** werden nur neue und geänderte Kommentare angezeigt.

## Kontakte

Sie können **Kontakte** wählen, um Änderungen an Kontakten und Adressbüchern anzuzeigen.

## Antivirus

Die Aktivitäten des Antiviren-Programms werden Ihnen über die Auswahl **Antivirus** angezeigt.
