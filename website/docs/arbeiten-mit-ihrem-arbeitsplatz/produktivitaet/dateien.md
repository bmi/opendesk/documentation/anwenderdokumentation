# Dateien

- [Überblick](dateien/ueberblick.md)
- [Ordner und Dateien erstellen und hochladen](dateien/ordner-und-dateien-erstellen-und-hochladen.md)
- [Neues *diagrams.net*-Diagramm erstellen](dateien/neues-diagramm-erstellen.md)
- [Vorlagenordner](dateien/vorlagenordner.md)
- [Ordner und Dateien löschen](dateien/ordner-und-dateien-loeschen.md)
- [Ordner und Dateien verwalten](dateien/ordner-und-dateien-verwalten.md)
- [Ordner und Dateien herunterladen](dateien/ordner-und-dateien-herunterladen.md)
- [Ordner und Dateien verschieben oder kopieren](dateien/ordner-und-dateien-verschieben-oder-kopieren.md)
- [Ordner und Dateien teilen](dateien/ordner-und-dateien-teilen.md)
