# Ordner und Dateien verwalten

## Ordner und Dateien favorisieren

Um Ordner und Dateien als Favoriten zu markieren, wählen Sie diese mit der rechten Maustaste aus oder klicken Sie auf das Drei-Punkte-Menü hinter dem Ordner oder der Datei. Ein Dropdown-Menü öffnet sich. Hier können Sie die Datei bzw. den Ordner **Zu den Favoriten hinzufügen**.

Favorisierte Ordner und Dateien erscheinen in der Auflistung aller Ordner und Dateien an oberster Stelle und sind mit einem gelben Stern versehen. Sie finden sie auch bei der Auswahl der Favoriten in der Spalte auf der linken Bildschirmseite.

![Der Punkt &quot;Zu den Favoriten hinzufügen&quot; im Dropdown-Menü](media/dd96621622a37217041b4765e8b66d4bf74c83dd.png "Zu den Favoriten hinzufügen")

## Details

Um zusätzliche Funktionen für Dateien und Ordner abzurufen, klicken Sie mit der rechten Maustaste auf die jeweilige Datei bzw. den jeweiligen Ordner und wählen Sie im Dropdown-Menü **Details** . Sie können auch das Drei-Punkte-Menü rechts neben dem Datei- oder Ordnernamen verwenden, um das Dropdown-Menü zu öffnen. Daraufhin öffnet sich auf der rechten Seite eine Spalte, in der Aktivitäten angesehen und Kommentare verfasst oder gelesen werden können. Hier können Sie auch Dateien und Ordner mit anderen Personen teilen sowie eine Verbindung zum Projekte-Modul (OpenProject) herstellen.

![Spalte rechts neben der Dateiliste mit den Reitern Aktivität, Kommentare, Projekte und Teilen](media/a214692b2cad92275e65876ae3dbb458ce4e9c13.png "Ordnerdetails")

## Aktivität

Lassen Sie sich die Aktivitäten am ausgewählten Ordner oder an der ausgewählten Datei anzeigen, um beispielsweise einen Überblick über die letzten Änderungen zu erhalten.

## Kommentare

Es können auch Kommentare zu den ausgewählten Ordnern und Dateien hinterlassen werden. Das ist besonders sinnvoll, wenn die Ordner und Dateien mit anderen Personen geteilt werden.

## Schlagwörter/Tags

**Schlagwörter** oder **Tags** erleichtern Ihnen die Einordnung und das Wiederauffinden von Dateien und Ordnern. Um Tags hinzuzufügen, öffnen Sie zunächst die Detailansicht, wie oben unter **Details** beschrieben. Alternativ können Sie mit der rechten Maustaste auf die gewünschte Datei bzw. den gewünschten Ordner klicken und im Dropdown-Menü **Datei teilen** bzw. **Ordner teilen** auswählen.

![Die Option &quot;Ordner teilen&quot; im Dropdown-Menü](media/39a804c765faf147933aa0b066fdf845c4c7fa23.png "Ordner teilen")

Öffnen Sie in der Detailansicht das **Drei-Punkte-Menü** neben dem Ordner- oder Dateinamen und klicken Sie auf **Tags**.

![Die Tags-Schaltfläche im Kontextmenü](media/29c351602d1c9ac1ee2c9fbbad097b1cd13651a6.png "Die Tags-Schaltfläche im Kontextmenü")

Ein Textfeld erscheint. Wenn Sie in das Textfeld klicken, wird Ihnen eine Dropdown-Liste mit vorhandenen Tags angezeigt, die Sie einzeln per Mausklick auswählen können. Falls bisher noch keine Tags in Ihrem Wiki verwendet wurden, ist die Liste leer.

Alternativ können Sie selbst einen Begriff in das Textfeld eingeben. Während Sie tippen, werden Ihnen bereits passende Vorschläge aus den vorhandenen Tags angezeigt. Wenn der gewünschte Begriff noch nicht vorhanden ist, können Sie ihn nach erfolgter Eingabe mit der Eingabetaste hinzufügen. Neu hinzugefügte Tags werden künftig in der Dropdown-Liste erscheinen.

![Auswahl und Eingabe von Tags](media/a36a461708381f94465c9845f82b54e7586c117c.png "Auswahl und Eingabe von Tags")

Sie können alle Ordner und Dateien über die entsprechenden Schlagwörter (Tags) filtern. Nutzen Sie dazu das Filtermenü links auf dem Bildschirm. Klicken Sie dort auf **Schlagwörter**. Klicken Sie dann in das Textfeld oben und wählen Sie ein Schlagwort aus der Dropdown-Liste aus. Sie können auch einen Begriff eingeben, um die Liste zu durchsuchen.

![Die Dropdown-Liste mit verfügbaren Tags/Schlagwörtern](media/a05c2a4f1e09260dd196bb0bc98642bb4794a7e2.png "Schlagwortsuche")
