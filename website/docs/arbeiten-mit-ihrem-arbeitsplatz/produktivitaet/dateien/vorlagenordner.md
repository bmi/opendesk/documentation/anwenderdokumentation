# Vorlagenordner

Sie können nicht nur herkömmliche Ordner und Dateien erstellen. Über das Dropdown-Menü zum Anlegen neuer Ordner und Dateien können Sie einen Vorlagenordner anlegen. Alle Dateien, die Sie in diesem Ordner ablegen, werden als Vorlagen genutzt.

Legen Sie nun außerhalb des Vorlagenordners eine neue Datei desselben Dateityps an. Sie erhalten ein Vorschaufenster, in dem die von Ihnen angelegten Vorlagendateien zur Auswahl stehen.
