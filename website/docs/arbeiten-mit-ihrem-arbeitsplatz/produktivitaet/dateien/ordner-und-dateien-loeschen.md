# Ordner und Dateien löschen

Um eine Datei oder einen Ordner zu löschen, klicken Sie mit der rechten Maustaste auf die jeweilige Datei bzw. den jeweiligen Ordner und wählen Sie im Dropdown-Menü **Datei löschen** bzw. **Ordner löschen**.

![Die Option &quot;Datei löschen&quot; im Dropdown-Menü](media/4d3baa532d1ca482799a7005d2913e63615c5c71.png "Datei löschen")

Sollen mehrere Dateien und/oder Ordner gelöscht werden, erfolgt die Auswahl über die Kontrollkästchen vor den Datei- und Ordnernamen. Klicken Sie auch dann mit der rechten Maustaste auf eine der ausgewählten Dateien bzw. einen der ausgewählten Ordner und wählen Sie **Löschen**. Alternativ können Sie auch das Menü **Aktionen** über der Dateiliste verwenden, um das Dropdown-Menü zu öffnen.

![Mehrere Ordner/Dateien löschen](media/7d308e41393ec612732e4f5261484aeea3ef5a19.png "Mehrere Ordner/Dateien löschen")

Die Dateien und Ordner werden nicht endgültig gelöscht, sondern zunächst in den Papierkorb verschoben. Von dort können gelöschte Dateien wiederhergestellt oder endgültig gelöscht werden. Sie finden den Ordner **Gelöschte Dateien** unten in der linken Seitenspalte.

![Der Ordner &quot;Gelöschte Dateien&quot; im linken Menü](media/geloeschte_dateien.png "Gelöschte Dateien")
