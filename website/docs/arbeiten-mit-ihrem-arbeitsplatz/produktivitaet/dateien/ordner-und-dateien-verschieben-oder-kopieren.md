# Ordner und Dateien verschieben oder kopieren

Ordner und Dateien können verschoben oder kopiert werden. Wählen Sie dazu den Ordner oder die Datei mit der rechten Maustaste aus. Klicken Sie dann im angezeigten Menü auf **Verschieben oder kopieren**. Ein Fenster öffnet sich.

Ganz oben im Fenster finden Sie ein Textfeld, in das Sie den Namen des gewünschten Zielordners eingeben können. Weiter unten können Sie auswählen, ob alle Dateien, nur die neuesten Dateien oder nur Ihre Favoriten angezeigt werden sollen. Mit der Schaltfläche **+ Neu** können Sie auch einen neuen Ordner anlegen, in den die gewünschten Inhalte verschoben werden sollen. Wählen Sie aus der Liste unten den gewünschten Zielordner aus. Sie können die Ordner mit Klick auf die entsprechenden Überschriften nach Name, Größe und Änderungsdatum sortieren.

Dateien oder Ordner können auch mit gedrückter linker Maustaste in andere Ordner verschoben werden (Drag-and-drop).

Sie können auch Ordner und Dateien von Ihrem lokalen Dateiexplorer per Drag-and-drop in die Dateiablage kopieren. Diese werden daraufhin im aktuell geöffneten Ordner hochgeladen.
