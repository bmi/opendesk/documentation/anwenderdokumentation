# Ordner und Dateien herunterladen

Ordner und Dateien können heruntergeladen werden. Wählen Sie dazu den Ordner oder die Datei mit der rechten Maustaste aus. Im Kontextmenü finden Sie die Option **Herunterladen**. Je nachdem, welchen Browser Sie verwenden, öffnet sich entweder ein Dateiexplorer oder Sie müssen zunächst auswählen, ob Sie die Datei bzw. den Ordner speichern oder öffnen möchten. Beachten Sie hierzu auch die Anleitung Ihres Browsers.

Ordner werden komprimiert als Zip-Dateien heruntergeladen.

![Herunterladen im Dropdown-Menü](media/b969e7086ff8f53fd00d357315a99420c273504c.png "Herunterladen im Dropdown-Menü")
