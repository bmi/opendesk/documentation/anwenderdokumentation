# Neues *diagrams.net*-Diagramm erstellen

## Einführung

Das Modul *diagrams.net* ist geeignet zum Erstellen von Diagrammen und zum Skizzieren. Es gibt viele verschiedene Integrationen mit anderen Plattformen und Anwendungen, wie zum Beispiel einschließlich Atlassian Confluence Cloud und Jira Cloud. Klicken Sie im Modul Dateien auf **+Neu** und wählen Sie aus dem Dropdown **Neues *diagrams.net* Diagramm**.

## Wozu Diagramme erstellen?

Weder malen Sie mit einem Pinsel, noch übertragen Sie Tabellenkalkulationen, wenn Sie mit *diagrams.net* Diagramme erstellen. Stattdessen platzieren Sie eine Reihe von Diagrammelementen, verbinden Sie diese miteinander, fügen beschreibenden Text hinzu und gestalten die Elemente, um komplexere Informationen visuell zu vermitteln.

## Arten und Beispiele von Diagrammen

Zum Beispiel im Bereich Geschäfts- und Projektmanagement:

- Projektplanung und Kanban-Boards
- Abhängigkeitsdiagramme
- Organisations- und Baumdiagramme
- BPMN-Diagramme für Geschäftsprozesse
- Story Mapping
- Schwimmbahn-Diagramme

Grundrisse im Bereich der Software-Entwicklung:

- UML-Diagramme
- Entity-Relationship-Tabellen für die Datenbankmodellierung
- Mermaid-Diagramme
- Gitflow-Diagramme

C4-Modelle und zugehörige Diagramme IT und Infrastruktur:

- Threat Models
- Netzwerk- und Infrastrukturdiagramme
- Rack-Diagramme
- AWS-Infrastruktur-Diagramme
