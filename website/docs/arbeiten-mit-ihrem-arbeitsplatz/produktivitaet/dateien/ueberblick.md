# Überblick

Das Modul **Dateien** finden Sie auf der Startseite des Portals im Abschnitt **Produktivität**.

![Die Schaltfläche für das Modul &quot;Dateien&quot; auf der Portal-Startseite](media/e9613f5d1b75a51eb30ef8504e59e07e85165da8.png "Dateien auf der Startseite")

Nach Auswählen des Moduls erhalten Sie im großen Hauptbereich einen Überblick über vorhandene Dateien und Ordner. Im Menü auf der linken Seite werden Filtermöglichkeiten der Dateien und Ordner angezeigt.

![Übersicht über die Benutzeroberfläche des Dateien-Moduls](media/d81586530e7d70392df830ed49a5cde0c11348ce.png "Modulübersicht Dateien")

## Dateien-Einstellungen

Zunächst sollten Sie über **Dateien-Einstellungen** die Anfangsdarstellung des Moduls festlegen. Die Dateien-Einstellungen finden Sie im linken unteren Bereich der Seite.

![Schaltfläche Dateien-Einstellungen](media/f08fde00b17f12b3d169da830ae32c2e2d7fee6d.png "Schaltfläche Dateien-Einstellungen")

**Versteckte Dateien anzeigen:** Auch versteckte Dateien (Dateien mit einem führenden Punkt im Dateinamen) werden in der Dateiliste angezeigt.

**Bildvorschauen zuschneiden:** Bei Bildern wird die Miniaturansicht in der Kachel vor dem Dateinamen angepasst.  **Zusätzliche Einstellungen:**

**Umfangreiche Arbeitsbereiche anzeigen:** Oberhalb der Ordner- und Dateiliste können Notizen, Listen oder Links hinzugefügt werden.

**Empfehlungen anzeigen:** Oberhalb der Ordner- und Dateiliste werden die zuletzt genutzten Dateien und Ordner angezeigt, damit Sie schnell wieder darauf zugreifen können.  

**WebDAV:** Hier finden Sie die Adresse für den WebDAV-Zugriff.

![Die Dateien-Einstellungen](media/30c1b88e6fb8a21156ba3effe7ee3040cff525f2.png "Die Dateien-Einstellungen")
