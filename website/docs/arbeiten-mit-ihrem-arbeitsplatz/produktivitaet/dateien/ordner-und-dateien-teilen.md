# Ordner und Dateien teilen

Sie können über die Detailansicht (siehe **Dateien – Ordner und Dateien verwalten – Details** ) Dateien und Ordner mit anderen Nutzerinnen und Nutzern teilen. Alternativ können Sie mit der rechten Maustaste auf die gewünschte Datei bzw. den gewünschten Ordner klicken und **Datei teilen** bzw. **Ordner teilen** auswählen.

Unter **Nach Freigabeempfängern suchen** können Sie nun im Textfeld den Namen oder die E-Mail-Adresse der gewünschten Person eingeben. Während Sie tippen, werden Ihnen Vorschläge angezeigt. Bestätigen Sie Ihre Auswahl mit einem Mausklick oder mit der Eingabetaste.

![Teilen über das Textfeld &quot;Nach Freigabeempfängern suchen&quot;](media/datei_teilen.png "Nach Freigabeempfängern suchen")

Sie können auch einen Link zur Datei bzw. zum Ordner erzeugen, den Sie dann anderen Nutzerinnen und Nutzern auf verschiedenen Wegen (E-Mail, Chat usw.) zukommen lassen können. Klicken Sie dazu neben **Link teilen** auf die Schaltfläche **Neuen Freigabe-Link erstellen** . Der Link wird dann in Ihre Zwischenablage kopiert. Fügen Sie ihn mit der Tastenkombination **Strg+V** an gewünschter Stelle ein.

Nachdem der Link erzeugt wurde, erscheint unterhalb von **Link teilen** der Text **Nur anzeigen** . Klicken Sie auf diesen Text, um ein Dropdown-Menü zu öffnen. Hier können Sie bei Bedarf auswählen, welche Berechtigungen Sie Nutzerinnen und Nutzern mit dem geteilten Link erteilen möchten. Wählen Sie aus, ob die Datei bzw. der Ordner nur angezeigt oder auch hochgeladen und bearbeitet werden darf. Sie können mit dem Punkt **Dateiablage** auch festlegen, dass die Datei nur hochgeladen werden darf. Unter **Benutzerdefinierte Berechtigungen** stehen Ihnen weitere Optionen zur Verfügung. Hier können Sie beispielsweise ein Passwort für den Zugriff auf die Datei bzw. den Ordner festlegen sowie ein Ablaufdatum für den Link einstellen.

![Optionen für das Teilen von Dateien und Ordnern](media/7e149c820c2359cb6c05b13b0a25f97e433a21be.png "Teilen")
