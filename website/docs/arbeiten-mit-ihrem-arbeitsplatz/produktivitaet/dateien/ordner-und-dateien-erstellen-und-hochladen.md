# Ordner und Dateien erstellen und hochladen

Durch einen Klick auf die Schaltfläche **+ Neu** öffnet sich ein Dropdown-Menü mit den Optionen, einen neuen Ordner, eine neue Textdatei, ein neues Dokument, eine neue Tabelle, eine neue Präsentation oder ein neues Diagramm zu erstellen. Außerdem finden sich hier die Optionen, eine Datei hochzuladen, einen Vorlagenordner einzurichten, ein neues *diagrams.net*-Diagramm zu erstellen oder eine Beschreibung zum aktuell geöffneten Ordner hinzuzufügen.

**Hinweis:** Beschreibungen können Sie nur hinzufügen, wenn die Option **Umfangreiche Arbeitsbereiche anzeigen** in den **Dateien-Einstellungen** eingeschaltet ist (siehe **Überblick** ).

![Das Dropdown-Menü für neue Ordner und Dateien](media/1285cadd370dce84fcf1c6c4f56ae1367451e04b.png "Das Dropdown-Menü für neue Ordner und Dateien")

## Datei hochladen

Um eine Datei hochzuladen, klicken Sie auf **Datei hochladen** und wählen dann eine Datei im Dateiexplorer aus, der sich öffnet. Klicken Sie anschließend auf **Öffnen** und die Datei wird hochgeladen. Sie können die Datei auch mit einem Doppelklick direkt hochladen.

Sie können auch Ordner und Dateien direkt von Ihrem lokalen Dateiexplorer per Drag-and-Drop in die Dateiablage ziehen. Diese werden daraufhin im aktuell geöffneten Ordner hochgeladen.

![Datei zum Hochladen auswählen](media/a3c125c9cba968730b71c65c49b552d890ba8826.png "Datei zum Hochladen auswählen")

## Ordner, Datei oder *diagrams.net*-Diagramm erstellen

Um Ordner oder Dateien neu zu erstellen, klicken Sie auf **Neuer Ordner** oder auf die entsprechende Option für den gewünschten Dateityp. Geben Sie daraufhin einen Namen für den Ordner bzw. die Datei ein. Der Ordner bzw. die Datei wird erstellt, sobald Sie auf den Pfeil neben dem Namen klicken oder die Eingabetaste drücken.

Dateien und *diagrams.net*-Diagramme werden nach dem Erstellen automatisch geöffnet.

![Namenseingabe für einen neuen Ordner](media/6326c38f0829e8b8b61b830259c8d3acd15805d8.png "Namenseingabe für einen neuen Ordner")

## Beschreibung hinzufügen

Wählen Sie diese Funktion im Dropdown-Menü aus, um eine Beschreibung zum aktuell geöffneten Ordner hinzuzufügen. Dies kann z. B. hilfreich sein, wenn Sie anderen Nutzerinnen und Nutzern erklären möchten, für welche Inhalte dieser Ordner vorgesehen ist. Oberhalb der Dateiliste öffnet sich ein Textfeld, in dem Sie Ihre Beschreibung eingeben und mithilfe einer Werkzeugleiste mit gängigen Formatierungen versehen können.

**Hinweis:** Beschreibungen können Sie nur hinzufügen, wenn die Option **Umfangreiche Arbeitsbereiche anzeigen** in den **Dateien-Einstellungen** eingeschaltet ist (siehe **Überblick** ). Wenn ein Ordner bereits eine Beschreibung enthält, klicken Sie einfach in das Textfeld, um diese zu bearbeiten.

![Das Textfeld, in dem Sie eine Beschreibung eingeben und formatieren können](media/5cc1f71282740ed447efa1b834360afb09fbe625.png "Beschreibung hinzufügen")
