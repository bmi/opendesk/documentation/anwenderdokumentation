# Übersicht der globalen Projekte

## Übersicht der Projekte

Ihre Arbeit in dem Modul **Projekte** kann in mehreren Projekten organisiert werden, jedes mit einer bestimmten Anzahl von Mitgliedern und deren jeweiligen Rollen in diesem Projekt. Jedes Projekt wiederum kann individuell konfiguriert werden. In Bezug auf die aktivierten Funktionen werden Projekte auch als Module bezeichnet. Diese Unterscheidung zwischen den Projekten bietet Ihnen viel Flexibilität bei der Einrichtung Ihrer Arbeit und der Kontrolle darüber, was die Benutzerinnen und Benutzer in jedem einzelnen Projekt sehen und/oder bearbeiten dürfen.

Um mehr über die Erstellung und Verwaltung von Projekten zu erfahren, besuchen Sie bitte unseren separaten Abschnitt **Projekte verwalten**.
