# Roadmap-Planung - Einführung in Gantt-Diagramme

Hier erhalten Sie erste Einblicke in **Gantt-Diagramme**. Sie erfahren wie es Ihnen möglich ist, über **Gantt-Diagramme** einen Projektplan zu ertellen und zu organisieren.

**Gantt-Diagramme** sind im Modul Projekte als eine Art Balkendiagramm definiert, das alle Aufgaben eines Projekts darstellt. Die Aufgaben werden vertikal aufgelistet, die horizontale Achse befasst sich mit der Zeitangabe. Die Länge der Balken ist an die Dauer der jeweiligen Aufgabe angepasst.

## Was ist ein Gantt-Diagramm?

Mittels **Gantt-Diagrammen** können Sie einen Projektplan erstellen, diesen verwalten und die Informationen mit Ihrem Team teilen. Sie können die für die Umsetzung des Projekts notwendige Arbeitspaketplanung und die erforderlichen Schritte zur Fertigstellung Ihres Projekts visualisieren. Nehmen Sie die Rolle der Projektleitung ein, werden Sie über Verzögerungen informiert und können Maßnahmen einleiten.

Das dynamsiche **Gantt-Diagramm** zeigt die Phasen, Aufgaben und Meilensteine Ihres Projekts, sowie die Beziehungen zwischen ihnen an. Die Elemente **Phase** und **Aufgaben** haben jeweil ein Start- und Enddatum, dadurch sind Sie immer über den akutellen Stand Ihres Projekts informiert. Der **Meilenstein** ist als fester Zeitpunkt definiert und hat demnach das selbe Start- und Enddatum.

## Aktivieren der Gantt-Diagramm-Ansicht

Um die **Gantt-Diagramm-Ansicht** in Projekte zu öffnen, muss das Modul **Arbeitspakte** in den Projekteinstellungen aktiviert sein. Navigieren Sie in Ihrem Projektmenü zum Modul **Arbeitspakete**. Wählen Sie **Gantt-Diagramm-Ansicht** in der **Arbeitspaket-Tabelle** über die **Gantt-Schaltfläche** in der rechten oberen Ecke aus.

![Gantt-Ansicht auswählen](media/b077b1c4d5ec1f95374a45e803beb71a362afe32.PNG "Gantt-Ansicht auswählen")

Das **Gantt-Diagramm** das sich darauffolgend öffnet, gibt Ihnen alle Arbeitspakettypen aus. Es spielt keine Rolle obe es sich dabei um Phasen, Meilensteine odere andere Arbeitspakettypen handelt, es werden alle ausgegeben.

Es zeigt Ihnen die Abhängigkeiten zwischen Arbeitspaketen sowie zusätzliche Informationen an.

![Gantt-Diagramm](media/d1612fcfbc444e99ba7a85b67ca39013a5e80ce1.PNG "Gantt-Diagramm")

## Erstellen eines Projektplans

Zum erstellen eines **Projekplans** öffnen Sie die **Arbeitspaket-Tabelle** und wählen Sie **Gantt-Diagramm** aus. Über **Erstelle neues Arbeitspaket** ist es Ihnen möglich, ein neues Arbeitspaket zu erstellen. Sie können Attribute des Arbeitspakets gleich in der **Arbeitspaket-Tabelle** pflegen.

![Attribute pflegen](media/8e350c20c6a715a28506862d7e8b5a0d9e41e716.PNG "Attribute pflegen")

Wurde Ihr **Arbeitspaket** erstellt, ist es Ihnen möglich auf die entsprechende Zeile des Arbeitspakets zu klicken und dieses im **Gantt-Diagramm** zu erstellen und zu bearbeiten.

Im Diagramm können Sie den Zeitraum auswählen in welchem das Arbeitspaket bearbeitet werden soll. Zusätzlich ist es Ihnen möglich das Paket per Drag and Drop zu verschieben.

![Gantt-Diagramm bearbeiten](media/9cb49efc198801d05da02562b7150e4019a1f45f.PNG "Gantt-Diagramm bearbeiten")

## Projektplan bearbeiten

Es ist Ihnen möglich den Projektplan zu bearbeiten indem Sie in der **Arbeitspakete-Ansicht** Attribute wie Status oder Priorität ändern. Um Start- und / oder Enddatum eines Arbeitspaketes zu ändern, klicken Sie im **Gantt-Diagramm** auf das entsprechende Arbeitspaket und strecken oder verkürzen Sie die Zeitangabe.

Änderungen werden unter der **Aktivität** des Arbeitspakets ergänzt.
