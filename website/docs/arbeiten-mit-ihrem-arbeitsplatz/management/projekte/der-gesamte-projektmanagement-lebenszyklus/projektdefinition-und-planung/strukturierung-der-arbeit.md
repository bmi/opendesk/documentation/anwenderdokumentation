# Strukturierung der Arbeit

Auf dieser Seite erhalten Sie eine Einführung in die **Arbeitspakete**. Im Folgenden wird erklärt, was ein **Arbeitspaket** ist und wie Sie **Arbeitspakete** erstellen und bearbeiten können.

**Was ist ein Arbeitspaket?**

Ein Arbeitspaket in Projekte kann im Grunde alles sein, was Sie in Ihren Projekten benötigen, um den Überblick zu behalten. Es kann z. B. eine Aufgabe, ein Meilenstein oder eine Projektphase sein. Diese verschiedenen **Arten von Arbeitspaketen** werden **Arbeitspaket-Typen** genannt.

**Erstellen eines Arbeitspakets**

Klicken Sie innerhalb des Moduls Arbeitspakete auf die **violette Schaltfläche + Erstellen** , um ein neues Arbeitspaket zu erstellen. Wählen Sie im Dropdown-Menü aus, welche Art von Arbeitspaket Sie erstellen möchten; z. B. eine Aufgabe oder einen Meilenstein. Um ein neues Arbeitspaket zu erstellen, können Sie auch den **violett gedruckten Schriftzug + Neues Arbeitspaket erstellen** wählen.

![Übersicht Arbeitspakete](media/4a32f01877429a2af970ba4fa7959ef425f29681.PNG "Übersicht Arbeitspakete")

Es öffnet sich eine geteilte Bildschirmansicht mit dem Formular für **neue Arbeitspakete** auf der rechten Seite und der Tabelle mit den bereits vorhandenen Arbeitspaketen auf der linken Seite.

In das leere Formular auf der rechten Seite können Sie alle relevanten Informationen für dieses Arbeitspaket eingeben, z. B. den Betreff und eine Beschreibung, ein Fälligkeitsdatum oder ein beliebiges anderes Feld. Die Felder, die Sie ausfüllen können, werden Arbeitspaketattribute genannt. Außerdem können Sie Anhänge mit Kopieren und Einfügen oder mit Drag &amp; Drop hinzufügen.

Klicken Sie auf die violette Schaltfläche **Speichern**, um das Arbeitspaket zu erstellen.

![Formular Neue Aufgabe](media/f67d02fb707ea1906caa26fb5fd552bb38446e9b.PNG "Formular Neue Aufgabe")

Das Arbeitspaket öffnet sich anschließend in der Tabellenansicht:

![Tabellenübersicht Arbeitspakete](media/671fa2fc5fa32418c33b7bda958474bacb35014d.png "Tabellenübersicht Arbeitspakete")

Eine weitere Möglichkeit, ein Arbeitspaket zu erstellen, ist die Erstellung über das **Headermenü**. Die aktivierten **Arbeitspakettypen** werden angezeigt, und Sie können den entsprechenden Arbeitspakettyp für die Erstellung auswählen.

![Arbeitspaket via Headermenü erstellen](media/2d63365db6cdfdf8fce213a53f9e6529414cbc9a.PNG "Arbeitspaket via Headermenü erstellen")

Sobald Sie auf den Typ des Arbeitspakets klicken, den Sie erstellen möchten, öffnet sich die Detailansicht des Arbeitspakets. Wählen Sie das Projekt aus, für das Sie das Arbeitspaket erstellen möchten.

![Arbeitspaket Detailansicht](media/dacb71cf85c425316e3faac808079d21b07dd52d.PNG "Arbeitspaket Detailansicht")

Dann folgen Sie den gleichen Schritten wie oben, um die Attribute Ihres Arbeitspakets auszufüllen und es zu speichern.

## Arbeitspakete öffnen und bearbeiten

Um ein vorhandenes Arbeitspaket aus der Tabelle zu öffnen und zu bearbeiten, wählen Sie das Arbeitspaket in der Tabelle aus und klicken Sie auf das Symbol **Detailansicht öffnen**, um die geteilte Bildschirmansicht zu öffnen. Sie können auch auf den Info-Button im oberen Bereich der Tabellenansicht klicken. Ebenso ist es möglich die Detailansicht per Doppelklick auf das Arbeitspaket zu öffnen.

![Arbeitspakete Tabelle](media/671fa2fc5fa32418c33b7bda958474bacb35014d.png "Arbeitspakete Tabelle")

Wenn Sie in die Liste auf der linken Seite auf ein Arbeitspaket klicken, sehen Sie die entsprechenden Details auf der rechten Seite des geteilten Bildschirms.

Hier kann in ein beliebiges Feld geklickt werden, um Änderungen vorzunehmen. Sie können z. B. den Text der Beschreibung anpassen.

Anschließend klicken Sie auf den Haken unter dem Feld, um die Änderungen zu speichern.

![Detailansicht einer Aufgabe](media/9d252a2995bf66726162d7f6d23dd61cdaaaa541.png "Detailansicht einer Aufgabe")

Um den **Status** zu ändern, klicken Sie in der Detailansicht auf den aktuellen Status und wählen Sie den neuen Status des Arbeitspakets im Dropdown-Menü aus.

## Aktivität in Arbeitspaketen

Um über alle Änderungen eines Arbeitspakets auf dem Laufenden zu bleiben öffnen Sie den **Aktivitäten-Tab** oben in der Detaillansicht.

Dort können Sie alle Änderungen ansehen, die an dem Paket vorgenommen wurden. Sie können am Ende der **Aktivitätenliste** kommentieren und andere Kolleginnen und Kollegen per "@" markieren.

![Kommentare in der Aktivitätenübersicht](media/21023d4a4264193eea4bba60d634e044fd4bfdf2.png "Kommentare in der Aktivitätenübersicht")

Andere Kolleginnen und Kollegen können per "@" vor dem Nutzernamen markiert werden, um Sie über Änderungen zu informieren. Die getaggten Personen erhalten die Meldung im Projekte-Modul.
