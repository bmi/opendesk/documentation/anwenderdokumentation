# Protokolle erstellen und bearbeiten

Die **Protokolle** werden automatisch beim Schließen der Agenda in der Detailansicht der Besprechung erstellt.

Wenn Sie die Agenda schließen, wird diese automatisch in das Besprechungsprotokoll übertragen. Sie können nun mit der Erstellung des Protokolls beginnen. Sie können den Protokoll-Text genau wie in einer Wiki-Seite bearbeiten, den Text formatieren, auf Arbeitspakete verlinken, Dokumente anhängen, Arbeitspaket-Tabellen oder andere Macros verwenden.

Anschließend werden Sie zur Besprechungsansicht weitergeleitet, dort können Sie:

1. Protokolle bearbeiten (Bitte vergessen Sie nicht Ihre Daten zu **speichern**)
1. Änderungsverlauf anzeigen

![Protokoll-bearbeiten](media/8e9fc75f8596d363929278be71672875df495143.jpg "Protokoll-bearbeiten")
