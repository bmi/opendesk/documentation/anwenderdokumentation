# Teilnehmerinnen und Teilnehmer hinzufügen

Sie können **Teilnehmerinnen und Teilnehmer** zu einer Besprechung hinzufügen, während Sie sich im Bearbeitungsmodus befinden. Der Prozess ist derselbe, egal ob Sie eine neue Besprechung erstellen oder eine bestehende bearbeiten. Zusätzlich können Sie nach der Besprechung notieren, wer tatsächlich daran teilgenommen hat (**Anwesend**).

Sie können die Liste aller Projektmitglieder unter dem Bereich **Teilnehmer** einsehen. Diese Liste variiert von Projekt zu Projekt. Wenn Sie den Haken in der Zeile eines Projektmitglieds setzen, wird die ausgewählte Person automatisch benachrichtigt, sobald eine Agenda oder ein Besprechungsprotokoll erstellt wird. Durch das Entfernen des Häkchens können Sie Projektmitglieder aus der Besprechung entfernen. Klicken Sie auf **Speichern**, um Ihre Änderungen zu sichern.

![Teilnehmer-hinzufuegen](media/da5d96ba56a482f97ccf7f2216089cef64d7fc4c.jpg "Teilnehmer-hinzufuegen")
