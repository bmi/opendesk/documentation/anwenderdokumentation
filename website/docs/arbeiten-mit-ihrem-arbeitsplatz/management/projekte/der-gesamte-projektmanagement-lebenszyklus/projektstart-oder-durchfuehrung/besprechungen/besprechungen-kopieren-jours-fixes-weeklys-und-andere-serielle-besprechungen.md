# Besprechungen kopieren: Jours Fixes, Weeklys und andere serielle Besprechungen

Wenn Sie wiederkehrende Besprechungen, wie z.B. ein Jour Fixe haben und den Prozess der Erstellung von Besprechungen und Tagesordnungen vereinfachen möchten, können Sie **eine bestehende Besprechung** kopieren. Wählen Sie dazu eine Besprechung aus und öffnen Sie die Detailansicht.

Klicken Sie auf das **Drei-Punkte-Menü** in der oberen rechten Ecke und wählen Sie **Kopieren**.

![Besprechung-kopieren](media/618c3e650c0cffa38d8eb58245bc54d9566c2956.jpg "Besprechung-kopieren")
