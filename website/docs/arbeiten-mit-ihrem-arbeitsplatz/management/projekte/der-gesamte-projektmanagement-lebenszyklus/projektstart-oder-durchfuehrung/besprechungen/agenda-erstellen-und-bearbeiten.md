# Agenda erstellen und bearbeiten

Nachdem Sie eine Besprechung angelegt haben können Sie eine **Besprechungs-Agenda** ergänzen.

1. Vor Beginn der Besprechung können alle Teilnehmerinnen und Teilnehmer ihre Beiträge zur Agenda hinzufügen, indem sie einfach auf die Schaltfläche **Bearbeiten** klicken. Sie können auch Dateien anhängen und Kommentare speichern.
1. Mit der Symbolleiste können Sie Änderungen am Textformat vornehmen oder Makros bearbeiten und so z. B. Inhaltsverzeichnisse oder Arbeitspaket-Tabellen einfügen.
1. Vergessen Sie nicht Ihre Änderungen zu **speichern**.
1. Alle an der Tagesordnung vorgenommenen Änderungen werden dokumentiert. Wenn Sie auf **Historie** klicken, erhalten Sie einen Überblick über alle Änderungen, einschließlich der Personen, die die Änderungen vorgenommen haben.
1. **Schließen** Sie die Agenda zum Beginn der Besprechung, um weitere Änderungen zu vermeiden und die gleiche Grundlage für alle Teilnehmerinnen und Teilnehmer zu schaffen. Nach dem Schließen der Agenda werden die dort eingetragenen Punkte in das Protokoll übertragen, um die Ergebnisse der Besprechung festzuhalten.

![Agenda](media/bb22aa75eaa9bfa5e79667d4b6a9e3e4362fd387.jpg "Agenda")
