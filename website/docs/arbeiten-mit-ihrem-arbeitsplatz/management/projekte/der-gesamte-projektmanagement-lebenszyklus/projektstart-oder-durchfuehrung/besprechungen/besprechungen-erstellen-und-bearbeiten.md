# Besprechungen erstellen und bearbeiten

Um eine neue Besprechung anzulegen, klicken Sie bitte auf **+ Besprechung** in der oberen rechten Ecke. Geben Sie einen Titel für die Besprechung ein. Wählen Sie die Teilnehmerinnen und Teilnehmer aus der Liste der Projektmitglieder aus. Wenn Sie auf die violette Schaltfläche **Anlegen** klicken, werden Ihre Änderungen gespeichert.

![Meeting-erstellen](media/6f53f58f32e855f84f44b3f7d8b873b3e12cede2.jpg "Meeting-erstellen")

Wenn Sie die Details einer Besprechung ändern möchten, wie z.B. die Zeit oder den Standort, öffnen Sie die Ansicht der Besprechungsdetails durch Klicken auf den Titel in der Übersichtsliste. Klicken Sie dann auf die Schaltfläche **Bearbeiten** (Bleistift-Symbol) neben dem Besprechungsnamen.

![Meeting-bearbeiten](media/a96805f3ee200d654d4dbbde8b044d0ee3e6a040.jpg "Meeting-bearbeiten")

Es öffnet sich die Detailansicht der Besprechung, die Sie nun bearbeiten können. Vergessen Sie nicht, Ihre Änderungen zu **speichern**.
