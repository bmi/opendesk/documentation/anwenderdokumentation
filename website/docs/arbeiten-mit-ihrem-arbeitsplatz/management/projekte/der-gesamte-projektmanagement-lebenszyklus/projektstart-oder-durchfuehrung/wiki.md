# Wiki

In einem Projekt-Wiki können Sie kollaborativ mit anderen Projektmitgliedern einen zentralen Informationsbereich zusammen erstellen und bearbeiten, z.B. für die Projektdokumentation, Richtlinien, Spezifikationen oder andere Projektinformationen.

**Wiki** ist als Modul definiert, mit dem textbasiert Wiki-Seiten verwendet, erstellt und strukturiert werden können. Um das Wiki-Modul nutzen zu können, muss es zuerst in der Projektkonfigurationen aktiviert werden.

## Allgemeine Informationen zum Wiki

Das Projekte-Wiki sowie alle weiteren Textfelder sind mit einem intuitiven WYSIWYG-Editor (Akronym für den Grundgedanken „What You See Is What You Get“ (englisch für „Was du siehst, ist [das], was du bekommst.“)) – ausgestattet, der eine benutzerfreundliche Bearbeitung in Echtzeitdarstellung ermöglicht.

## Grundlegende Formatierung

Der Texteditor in Projekte unterstützt grundlegende Textstile wie fette und kursive Formatierung, Überschriften, Durchstreichen, Inline-Code und Anführungszeichen sowie Inline-Bildbehandlung. Das Einfügen von Inhalten wie Bildern oder Rich-Text wird ebenfalls unterstützt, während nicht unterstütztes Styling vom Editor entfernt wird.

Geben Sie einfach Ihren Text in das Eingabefeld ein, markieren Sie einen Textabschnitt und wählen Sie die Formatierung über die Symbolleiste oben.

![Grundlegende Formatierung](media/2c3fb2fe4467bcc5e01e649bfd61b0d20cf51f9f.jpg "Grundlegende Formatierung")

## Zeilenumbrüche

Anstatt einen neuen Absatz mit Enter zu erstellen, können Sie auch SHIFT+Enter drücken. Dies erstellt einen Zeilenumbruch, ohne einen neuen Absatz zu erstellen

## Links

Um einen Hyperlink zu erstellen, klicken Sie einfach auf das Link-Symbol in der Werkzeugleiste oder markieren Sie den Text, den Sie verlinken möchten und drücken Sie STRG+K. Daraufhin öffnet sich ein Fenster, in dem Sie die Webadresse (auch als Linkziel bekannt) eingeben können, auf die Ihr Link verweisen soll.

## Widgets und neue Zeilen

Der Editor verwendet Widgets, um Block-Elemente wie Bilder, Tabellen und andere Elemente anzuzeigen, die nicht inline sind.

Um ein Widget auszuwählen, klicken Sie es meistens einfach an. Bei einem Tabellenwidget jedoch benutzen Sie den kleinen Auswahlknopf oben links, um die ganze Tabelle zu markieren.

Wenn Sie ein Widget ausgewählt haben, können Sie es entfernen. Sie können eine neue Zeile unterhalb erstellen, indem Sie das Widget auswählen und ENTER oder ↓ (Pfeil nach unten) drücken, oder eine neue Zeile oberhalb durch Drücken von SHIFT+enter oder ↑ (Pfeil nach oben). Dies ist besonders hilfreich, wenn das Widget das erste oder letzte Element auf der Seite ist und Sie eine Zeile darunter oder darüber einfügen möchten.

## Codeblöcke

Da der Editor derzeit keine Unterstützung für Code-Blöcke bietet, kann Projekte Code-Blöcke innerhalb der EditorsInstanz anzeigen, aber nicht bearbeiten.

## Emojis

Beginnend mit Projekte können Sie Emojis in allen Texteditoren benutzen. Geben Sie einen Doppelpunkt und einen Buchstaben, z. B. **:a**, in den Wiki-Editor ein und Sie erhalten eine Vorschlagsliste von Emojis, die Sie verwenden können.

![Emojis](media/16c431db985e2c887ffaf99d444d2e89e59e8c96.jpg "Emojis")

## Automatische Formatierung

CKEditor5 erlaubt bestimmte CommonMark-ähnliche (vereinfachte Auszeichnungssprache) Tastatureingaben:

![Automatische Formatierung](media/29f5ccfd08ff1251994da7dcfe986d80563e4303.png "Automatische Formatierung")

## Bildverarbeitung

In Projekte-Ressourcen, die das Anhängen von Dateien unterstützen, haben Sie die Möglichkeit, Bilder auf einer Seite einzufügen. Dies erreichen Sie, indem Sie

- den Toolbar-Button benutzen,
- ein Bild aus der Zwischenablage einfügen,

oder ein Bild via Drag &amp; Drop in den Editor ziehen.  Der Anhang wird automatisch hochgeladen und als Anhang gespeichert.

## Makros

Ein Makro ist eine unter der Bezeichnung (Makroname) zusammengefasste Folge von Anweisungen oder Deklarationen, um diese (anstelle der Einzelanweisungen, i. d. R. an mehreren Stellen im Programm) mit nur einem einfachen Aufruf ausführen zu können.

Projekte unterstützt Makros auf textuell formatierten Seiten und setzt diese Unterstützung auch im WYSIWYG-Editor fort. Bitte beachten Sie, dass Makros während der Bearbeitung der Seite nicht angezeigt werden, sondern bis zum Speichern von einem Platzhalter vertreten werden.

### Inhaltsverzeichnis

Falls vorhanden, gibt das Inhalts-Makro (TOC) eine Auflistung aller Überschriften auf der aktuellen Seite aus.

### Arbeitspaket-Button

Konfigurieren Sie einen Button oder einen Link, um den Bildschirm zur Erstellung von Arbeitspaketpaketen im aktuellen Projekt aufzurufen. Sie können vorab einen Arbeitspakettyp bestimmen, der im Vorfeld ausgewählt wird, sodass die Weiterleitung zum Arbeitspaket-Erstellungsformular umso leichter ist.

### Wiki-Seite einbeziehen

Fügen Sie den Inhalt einer bestimmten Wiki-Seite in das aktuelle oder ein anderes sichtbares Projekt hinzu.

### Arbeitspaket-Tabelle und Gantt-Diagramm einbetten

Dies ist das flexibelste Makro und bietet umfangreiche Funktionalität im Hinblick auf die Aufnahme dynamischer Arbeitspaket-Tabellen mit allen Möglichkeiten der regulären Arbeitspaket-Tabelle.

Durch Hinzufügen einer eingebetteten Arbeitspaket-Tabelle über die Symbolleiste können Sie die Tabellenansicht in einem Pop-up-Fenster konfigurieren (z. B. Spalten, Gruppierung, Filter und weitere Eigenschaften).

Die dargestellte Seite wird dann die Ergebnisse der Arbeitspaket-Tabelle dynamisch abrufen, wobei die Sichtbarkeit für jede Benutzerin und jeden Benutzer respektiert wird.

Verwenden Sie das Makro, um Ansichten in andere Seiten einzubetten, Berichte aus mehreren Ergebnissen zu erstellen oder um eine Gantt-Diagramm-Ansicht einzubetten.

## Links zu Projekt Ressourcen

Wie bei der Text-Formatierungssyntax können Sie mit den gleichen Verknüpfungen wie bisher auch auf andere Ressourcen in Projekte verlinken. Erstellen Sie Links zu:

![Ressourcen in Projekte verlinken](media/d9c380a894e1cdbb4edd922014f0a08b9db83cad.jpg "Ressourcen in Projekte verlinken")

Sie können diese Links immer mit einer Projektkennung versehen, gefolgt von einem Doppelpunkt, um Ressourcen aus anderen Projekten als der aktuellen Wiki-Seite zu referenzieren:

![Ressourcen referenzieren](media/a1f39d30dccf83fc1f0d316b1e6ee6b6722009c3.png "Ressourcen referenzieren")

Um die Verarbeitung dieser Elemente zu vermeiden, stellen Sie ihnen ein Pausenzeichen wie ! voran. Zum Beispiel !#12, um zu verhindern, dass das Arbeitspaket mit der ID 12 verlinkt wird.

![Pausenzeichen ](media/b029959a4df384384f2b5f97e00474064565f31b.png "Pausenzeichen ")

## Automatische Vervollständigung für Arbeitspakete

Für Arbeitspakete öffnet das Tippen von # eine automatische Vervollständigung für sichtbare Arbeitspakete. Das bedeutet, dass Sie z.B. #3 oder #Markt eintippen können, um eine Liste von Arbeitspaketen angezeigt zu bekommen, die der Beschreibung entsprechen. Dann können Sie entweder weiter tippen oder ein Arbeitspaket aus der Liste auswählen.

### Hier geht es weiter mit:
- [Erstellen und Bearbeiten einer Wiki-Seite](wiki/erstellen-und-bearbeiten-einer-wiki-seite.md)
