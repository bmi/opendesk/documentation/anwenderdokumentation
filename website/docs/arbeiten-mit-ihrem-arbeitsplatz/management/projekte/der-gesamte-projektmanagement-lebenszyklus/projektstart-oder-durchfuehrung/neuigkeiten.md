# Neuigkeiten

Auf der Seite **Neuigkeiten** können Sie die aktuellsten **Neuigkeiten** zu einem Projekt in umgekehrter chronologischer Reihenfolge sehen. Über Neuigkeiten werden allgemeine Themen an alle Teammitglieder kommuniziert.

Bei **Neuigkeiten** handelt es sich um ein Modul, das die Veröffentlichung und Verwendung von Nachrichteneinträgen ermöglicht.

![Modulübersicht](media/1c3e872e35b9008bbe4b06236b24fe0645ef5eb0.jpg "Modulübersicht")

## Neuigkeiten kommentieren

In dem **Neuigkeiten**-Modul, das sich im Projekte-Menü links befindet, sehen Sie alle Neuigkeiten zu einem Projekt. Klicken Sie auf den Titel der **Neuigkeit**.

![Neuigkeiten-Beispiel](media/660fa27b83b36cba58c1e1fe672633560f8d503c.png "Neuigkeiten-Beispiel")

Durch den Klick auf die **Neuigkeit** können Sie weitere Details zu dieser einsehen. Es öffnet sich ein neues Fenster. In diesem befindet sich ein Textfeld, über das es Ihnen möglich ist, die **Neuigkeit** zu kommentieren.

![Neuigkeiten kommentieren](media/599368d18c584fbf259a3327607405f8050a3c90.png "Neuigkeiten kommentieren")

Fügen Sie den von Ihnen gewünschten Text ein und klicken Sie auf **Kommentar hinzufügen**, um Ihre Information oder Meinung zum Thema mit anderen zu teilen.

Ihr Kommentar wird danach mit allen anderen Kommentaren unter dem Thema erscheinen.

## Neuigkeiten bobachten

Sie können sich dazu entscheiden, bestimmte Neuigkeiten zu beobachten. Als Beobachterin oder Beobachter werden Sie per E-Mail darüber informiert, ob eine Neuigkeit sich verändert hat oder kommentiert wurde.

![Neuigkeiten bearbeiten](media/f1ea57b2e3dc2c1eb4e3215be627ee373789accd.png "Neuigkeiten bearbeiten")

Um eine **Neuigkeit** zu beobachten, klicken Sie auf **Beobachten** in der rechten Ecke. Wenn Sie keine Benachrichtigungen zu Neuigkeiten mehr erhalten möchten, klicken Sie erneut auf **Beobachten**, um die Funktion zu deaktivieren.

## Neuigkeiten bearbeiten

Sie können jederzeit nachträglich Änderungen an einer **Neuigkeit** vornehmen. Wählen Sie hierzu den gewünschten Beitrag mit einem Mausklick aus. Klicken Sie auf die **Bearbeiten-Schaltfläche** in der rechten Ecke.

![Neuigkeiten bearbeiten](media/05ab3c614b633ff1bce763310288e7255eb8411d.png "Neuigkeiten bearbeiten")

Ihnen stehen nun mehrere Textfelder zur Verfügung, um Änderungen am Titel, der Zusammenfassung und der Beschreibung vorzunehmen. Den ursprünglichen Text finden Sie am Ende Ihrer Seite. Führen Sie Ihre Änderungen durch und klicken Sie auf **Speichern**, um die Bearbeitung abzuschließen.

![Neuigkeiten bearbeiten](media/b90a14877b4528e1f806d09ae05e1b4604cd646c.png "Neuigkeiten bearbeiten")

## Neuigkeit erstellen

Um eine Neuigkeit zu erstellen, klicken Sie auf **Neuigkeit hinzufügen**. Die Schaltfläche befindet sich in der oberen rechten Ecke.

Dadurch öffnet sich ein neues Fenster. In diesem Fenster ist es Ihnen möglich, den Titel, die Zusammenfassung und die Beschreibung zu bearbeiten. Der Titel und die Zusammenfassung werden in der Projektübersicht zu sehen sein. Sie finden ihn auch auf der Hauptübersicht der Neuigkeiten-Seite. Des Weiteren ist es Ihnen möglich, den Inhalt der Neuigkeit mit den zur Verfügung stehenden Basisformatierungen im Textfeld ansprechend zu gestalten. Es stehen Ihnen Makros, Tabellen und viele weitere unterstützende Funktionen zur Verfügung.

Um die Bearbeitung abzuschließen, klicken Sie auf **Anlegen**.

## Neuigkeit löschen

Öffnen Sie die **Neuigkeit**, die gelöscht werden soll. Klicken Sie dann auf **Löschen** in der rechten oberen Ecke.

## Der Aktuelle-Neuigkeiten-Bereich auf der Projekte-Startseite

Es ist Ihnen möglich, die aktuellsten Neuigkeiten in Ihre persönliche Projektübersichtsseite einzubinden. Hierbei handelt es sich um eine benutzerspezifische Einstellung. Klicken Sie dazu auf die **Plus-Schaltfläche** in der rechten oberen Bildschirmecke. Danach wählen Sie mittels eines Klicks auf eine weitere **Plus-Schaltfläche** die gewünschte Position aus. Abschließend wählen Sie Ihr gewünschtes Widget aus.
