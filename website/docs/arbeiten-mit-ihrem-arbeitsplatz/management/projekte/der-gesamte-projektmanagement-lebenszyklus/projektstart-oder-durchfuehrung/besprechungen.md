# Besprechungen

Mit dem Modul Besprechungen können Sie in Projekte Ihre Projektbesprechungen an einer zentralen Stelle verwalten, gemeinsam mit Ihrem Team eine Besprechungsagenda erstellen und Besprechungsprotokolle anlegen, welche alle zentral in dem Bereich Besprechungen dokumentiert werden.

**Hinweis:** Besprechungen sind in Projekte als Modul definiert, welches die Organisation von Besprechungen ermöglicht. Das Modul muss zunächst in der Projektkonfiguration unter dem Menüpunkt **Module** aktiviert werden, um es im Projektmenü sichtbar zu machen.

Wenn Sie links im Projektmenü **Besprechungen** auswählen, erhalten Sie einen Überblick über alle nach Datum sortierten Projekt-Besprechungen. Durch Klicken auf einen der Besprechungsnamen können Sie weitere Details der Besprechung einsehen.

![Besprechungen_Uebersicht](media/98981a1b6ce57e433a0ea44d9ac6354dd2aafdd9.jpg "Besprechungen_Uebersicht")

### Hier geht es weiter mit:

- [Besprechungen erstellen und bearbeiten](besprechungen/besprechungen-erstellen-und-bearbeiten.md)
- [Teilnehmerinnen und Teilnehmer hinzufügen](besprechungen/teilnehmerinnen-und-teilnehmer-hinzufuegen.md)
- [Agenda erstellen und bearbeiten](besprechungen/agenda-erstellen-und-bearbeiten.md)
- [Protokolle erstellen und bearbeiten](besprechungen/protokolle-erstellen-und-bearbeiten.md)
- [Besprechungen kopieren: Jours Fixes, Weeklys und andere serielle Besprechungen](besprechungen/besprechungen-kopieren-jours-fixes-weeklys-und-andere-serielle-besprechungen.md)
