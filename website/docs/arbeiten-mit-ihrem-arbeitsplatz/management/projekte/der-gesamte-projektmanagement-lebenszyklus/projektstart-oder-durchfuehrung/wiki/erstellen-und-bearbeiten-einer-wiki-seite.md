# Erstellen und Bearbeiten einer Wiki-Seite

In **Projekte** haben Sie die Möglichkeit, gemeinsam mit Ihrem Team **Wiki-Seiten** zu kreieren und zu aktualisieren, um essenzielle Details Ihres Projekts festzuhalten.

## Neue Wiki-Seite erstellen

Um in **Projekte** eine **Wiki-Seite** hinzuzufügen, gehen Sie zum Wiki-Bereich in Ihrem Projektmenü und klicken Sie auf **+ Wiki-Seite** in der Werkzeugleiste rechts.

![+Wiki-Seite](media/faf6159a0259959fe54b91288feec52888ca6318.jpg "+Wiki-Seite")

**Hinweis**: Wenn Sie das Wiki-Modul nicht im Projektmenü sehen, müssen Sie das Modul erst in Ihrer Projektkonfiguration aktivieren.

![In Projektkonfiguration aktivieren](media/1c3e872e35b9008bbe4b06236b24fe0645ef5eb0.jpg "In Projektkonfiguration aktivieren")

Ein Fenster zur Bearbeitung erscheint, in dem Sie den Titel und den Inhalt der **Wiki-Seite** festlegen können.

- Tragen Sie den Namen der Seite ein.
- Verfassen Sie den Inhalt für die Wiki-Seite. Nutzen Sie dabei die Werkzeugleiste oben, um Ihren Text anzupassen.
- Dateien lassen sich einfach per Ziehen &amp; Ablegen hinzufügen. Eine weitere Möglichkeit ist das Kopieren und Einfügen von Dateien ins Textfeld oder das Hinzufügen mittels des Bild-Symbols in der Werkzeugleiste.
- Legen Sie eine übergeordnete **Wiki-Seite** fest.
- Notieren Sie kurz, welche Modifikationen Sie durchgeführt haben.
- Bestätigen Sie Ihre Änderungen oder brechen Sie den Vorgang ab.

![Neue Wiki-Seite erstellen](media/130483bf17299855a0dec681143f101416aa51f4.jpg "Neue Wiki-Seite erstellen")

## Wiki-Seite bearbeiten

Um eine Wiki-Seite zu bearbeiten, klicken Sie auf **Bearbeiten** auf der Wiki-Seite oben rechts.

![Wiki-Seite bearbeiten](media/55e500b84e7e240addc375fd5eb7b38916cc3970.jpg "Wiki-Seite bearbeiten")

Das Bearbeitungsfenster erscheint, und Sie können Modifikationen an der **Wiki-Seite** durchführen, wie zuvor beim Hinzufügen einer neuen Seite beschrieben.

Denken Sie daran, Ihre Änderungen zu **Speichern.**

## Wiki-Seitentitel ändern

Um eine Wiki-Seite umzubenennen, wählen Sie einfach die Wiki-Seite aus, die Sie ändern möchten und klicken Sie anschließend auf **Bearbeiten** oben rechts auf der Seite. Klicken Sie in den Titel und geben Sie einfach den neuen Namen der Seite ein.

Bitte vergessen Sie nicht auf den Button **Speichern** zu klicken, um Ihre Änderungen zu sichern.

## Wiki-Seitenstruktur erstellen

Wenn Sie eine Wiki-Struktur mit verschiedenen Seiten und Unterseiten erstellen möchten, können Sie eine übergeordnete Wiki-Seite anlegen, die dann automatisch im Menü der Wiki-Seite angezeigt wird.

![Wiki-Seitenstruktur](media/5431633bbd6e8e8248de777c993ffed7de694274.jpg "Wiki-Seitenstruktur")

Befinden Sie sich auf einer existierenden **Wiki-Seite** und klicken auf **+ Wiki-Seite** am oberen Rand, so wird die aktuelle Seite automatisch als Hauptseite für die neue festgelegt.

Um die Hauptseite einer **Wiki-Seite** im Nachhinein zu modifizieren, klicken Sie einfach auf den Bearbeiten Button der betreffenden Seite und wählen am Ende des Formulars eine andere Hauptseite aus.

Wählen Sie die Option – Keine Hauptseite –, so erscheint die **Wiki-Seite** als erstes Element in der Wiki-Navigation des Projektmenüs.

Innerhalb des Wiki-Menüs werden die Seiten alphabetisch sortiert dargestellt.

![Wiki-Seitenstruktur](media/80a881e121384af15f5306e01a333877039f7236.jpg "Wiki-Seitenstruktur")

## Wiki-Seite beobachten

Möchten Sie über Modifikationen einer **Wiki-Seite** informiert werden, steht Ihnen die Beobachten-Funktion zur Verfügung. Gehen Sie zur gewünschten Seite und klicken Sie auf die Schaltfläche **Bearbeiten** oben rechts. Diese Einstellung lässt sich jederzeit wieder ändern.

![Beobachten](media/53a59330945784ee8f590ea4db1baddb2a8d4f4b.jpg "Beobachten")

Ist diese Option eingeschaltet, bekommen Sie nach jeder Änderung an der Seite eine Mitteilung (abhängig von Ihren Benachrichtigungspräferenzen), die einen Verweis zu den vorgenommenen Modifikationen beinhaltet.
