# Arbeitspakete erstellen

Arbeitspakete gehören immer zu einem Projekt. Daher müssen Sie zuerst ein Projekt auswählen oder erstellen. Anschließend, navigieren Sie zum Arbeitspakete-Modul in der Projektnavigation.

![Projektnavigation: Arbeitspakete](media/5c1421cfffda1e1ddcabce0f0679c023e8c113b8.png "Projektnavigation: Arbeitspakete")

Es gibt zwei Möglichkeiten, neue Arbeitspakete anzulegen:

- **Aus der Arbeitspaket-Tabelle**: Diese Option bietet Ihnen die Möglichkeit schnell mehrere Arbeitspakete anzulegen.
- **Aus der Splitscreen-Ansicht**: Diese Option bietet Ihnen die Möglichkeit, detaillierte Informationen von Anfang an einzugeben.

## Ein Arbeitspaket in der Tabellenansicht erstellen

Klicken Sie unterhalb der Tabelle auf die Schaltfläche **+Erstelle neues Arbeitspaket**, um neue Arbeitspakete direkt in der Zeile der Tabellenansicht zu erstellen.

![Erstellen neues Arbeitspaket](media/f600e047b7f46cc9dced3b05595c7e19e98fd49a.png "Erstellen neues Arbeitspaket")

Das neue Arbeitspaket wird in einer grünen Zeile angezeigt. Geben Sie das Thema des Arbeitspakets ein, ändern Sie die Eigenschaften wie Typ oder Status direkt in der Tabelle und drücken Sie die Eingabetaste, um die Änderungen zu speichern.

![Thema eingeben](media/c1e46070fd67d93e6b4afa275b18ca7361794dcb.png "Thema eingeben")

Klicken Sie auf die Nummer des Arbeitspakets, um die Informationen zu öffnen.

**Achtung**: Sobald Sie das Arbeitspaket erstellt haben, erhalten Sie eine Benachrichtigung, die schnell verschwindet, um die Einstellungen des neuen Arbeitspakets zu öffnen.

## Ein Arbeitspaket in einem Splitscreen erstellen

Sie können ein Arbeitspaket in der geteilten Bildschirmansicht erstellen, um von Anfang an detaillierte Informationen in das Arbeitspaket einzugeben. Im Arbeitspaket-Modul klicken Sie auf die violette Schaltfläche **+Erstellen** und wählen Sie die Art des Arbeitspakets, das Sie erstellen möchten. Diese sind **Aufgabe**, **Meilenstein** oder **Phase**.

![Arbeitspaket erstellen](media/692f7c52958cd9652178a3d19902b6f78b460ab3.png "Arbeitspaket erstellen")

Die Eigenschaften des neuen Arbeitspakets werden in der geteilten Bildschirmansicht geöffnet. Geben Sie alle Informationen ein, z. B. Thema, Beschreibung, Personen, Schätzungen und Zeiten, Details und Kosten. Sie können auch Dateie anhängen.

Zum Abschluss, klicken Sie auf die Schaltfläche **Speichern**.

![Splitscreen: Neue Aufgabe](media/3dd0f71d85c9a6a7e622b804b04c48a38a850006.png "Splitscreen: Neue Aufgabe")

### Emojis

**Emojis** können in allen Texteditoren eingefügt werden, auch in der Arbeitspaketbeschreibung. Geben Sie einen **Doppelpunkt** und einen **Buchstaben**, z. B. **:a** in den **Texteditor** ein und Sie erhalten eine Vorschlagsliste von Emojis, die Sie verwenden können.

![Emojis](media/6b49c4aca95ccf4ac4bb0069e76f37be2c8ff6d0.png "Emojis")

### Anhänge zu einem Arbeitspaket hinzufügen

Sie können Bilder direkt hinzufügen, z. B. durch kopieren und in die Beschreibung des Arbeitspakets einfügen. Sie können auch die Symbolleiste oberhalb der Arbeitspaketbeschreibung benutzen und auf das Symbol **Bild einfügen** klicken.

![Bild hinzufügen in der Arbeitspaketbeschreibung](media/f34043bdd05953a8e8256ddb5bcfe0b42127d07c.png "Bild hinzufügen in der Arbeitspaketbeschreibung")

Sie können Dateien auch per **Drag &amp; Drop** im unteren Bereich des Arbeitspaketformulars hinzufügen.

![Datei hinzufügen per Drag &amp; Drop](media/781a4b444378f93a179f7d6f4ed1c474bd002036.png "Datei hinzufügen per Drag &amp; Drop")

Eine weitere Möglichkeit besteht darin, eine Datei aus einem Ordner auszuwählen und in das Arbeitspaket hochzuladen. In diesem Fall öffnet sich automatisch ein Fenster. Suchen Sie die Datei, wählen Sie sie aus und klicken Sie auf die Schaltfläche **Öffnen**.

![Dateien hochladen](media/772db208b4cf3c6bf1e0d42e250a87a22ee93622.png "Dateien hochladen")

**Hinweis**: Die Nextcloud-Integration kann auch verwendet werden, um Dateien in die Cloud hochzuladen oder bereits vorhandene Dateien und Ordner mit einem Arbeitspaket zu verknüpfen.
