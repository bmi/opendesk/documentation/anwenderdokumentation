# Projektarchiv

Archivieren Sie Ihr Projekt zu Dokumentationszwecken, um später darauf zurückgreifen zu können.  Um ein Projekt zu archivieren, navigieren Sie zu **Projektkonfiguration** und dem Untermenü **Information**. Dort wählen Sie die Schaltfläche Archivieren, die Sie oben rechts finden:

![Projekte_archivieren](media/025c75b55abaf8ba2eb69ff7c4193211f40d9798.jpg "Projekte_archivieren")

**Hinweis:** Diese Funktion steht den Administratorinnen und Administratoren von Projekten zur Verfügung. Als Administratorin oder Administrator können Sie in den Berechtigungen auch anderen Rollen die Funktion **Projekte archivieren** freigeben.

Ein archiviertes Projekt kann nicht mehr ausgewählt werden. Sie können es in der Übersicht **Alle Projekte** wiederfinden, indem Sie auf Filter klicken und anschließend den **Schieberegler Aktiv** nach links schieben zum Deaktivieren. Sie können über das Dreipunktmenü am Ende der Zeile das Projekt auch wiederherstellen, indem Sie auf **Entarchivieren** klicken.

![Schieberegler-aktiv](media/77e6ea571b46bbf6bf6bcf79f1ded538fbfbe8a8.jpg "Schieberegler-aktiv")

![Projekte-entarchivieren](media/464c39510d17eb891991c36b98739cf1e1a581c5.jpg "Projekte-entarchivieren")
