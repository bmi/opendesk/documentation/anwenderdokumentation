# Zeiterfassung

Anwenderinnen und Anwender können die Zeit oder andere Einheiten die innerhalb eines Projekts aufgewendet wurden, direkt Arbeitspaketen zuordnen. Dadurch können Sie sehen, wieviel Zeit Sie in ein Arbeitspaket investiert haben. Hierfür stehen Ihnen unterschiedliche Möglichkeiten zur Verfügung:
- Zeiterfassung in der Arbeitspaketansicht

Zeiterfassung mittels der **Timer-Schaltfläche**  Zusätzlich zu diesen Möglichkeiten können Sie auch auf **Meine Seite** die Funktion **meine verwendete Zeit** nutzen.

**Hinweis:** Um die Funktionen für die Zeiterfassung zu nutzen, muss das Modul **Zeit und Kosten** in den Projekteinstellungen aktiviert sein.

![Zeit und Kosten aktivieren](media/3ef696f864f5556f0940afd01a0b3db538fc35e1.PNG "Zeit und Kosten aktivieren")

## Zeiterfassung in der Arbeitspaketansicht

Um die für eine bestimmte Projekttätigkeit aufgewendeten Stunden zu erfassen, öffnen Sie die Detailansicht des betreffenden Arbeitspakets.

![Detailansicht öffnen](media/6a9aa35da8dab3dc5d88dac1465668471f11de10.PNG "Detailansicht öffnen")

Klicken Sie auf die **3-Punkte-Schaltfläche** in der rechten oberen Ecke oder nutzen Sie die **Timer-Schaltfläche** mit der **Uhr** um Ihre Arbeitszeit wie mit einer Stoppuhr zu messen.

![Zeit erfassen](media/b96e4132cd523ee7a810d27b8cd24f63838e198a.PNG "Zeit erfassen")

Zusätzlich haben Sie die Möglichkeit Ihre Zeit über die Arbeitspaketansicht zu erfassen. Klicken Sie dazu mit der rechten Maustaste auf die Zeile des Arbeitspakets für das Sie die Zeit erfasssen wollen. Wählen Sie dann **Zeit buchen** aus. Durch dieses Vorgehen gelangen Sie ebenfalls zum Fenster für die Zeiterfassung.

![Zeit erfassen Arbeitspaketansicht](media/52c3a7713bb64e6bfa84933e89f658528feeeee1.PNG "Zeit erfassen Arbeitspaketansicht")

Wählen Sie die Variante in der Arbeitspaketansicht oder die Variante über die **3-Punkte-Schaltfläche** öffent sich ein neues Fenster.

![Zeit buchen](media/d0d50303a6e02f3560625384548e7b22e5a9614a.PNG "Zeit buchen")

In diesem Fenster ist es Ihnen möglich Ihre Zeit zu buchen. Im Feld **Benutzer** finden Sie Ihren eigenen Namen. Für das Feld **Datum** wählen Sie das Datum aus, an dem Sie für das Arbeitspaket gearbeitet haben. Bei **Stunden** wählen Sie die Anzahl der Stunden aus, die Sie aufgebracht haben. Im Feld **Arbeitspaket** ist es Ihnen außerdem nochmals möglich ein anderes Arbeitspaket auszuwählen. Wählen Sie das gewünschte Arbeitspaket aus. Wählen Sie außerdem die **Aktivität** aus die Sie ausgeführt haben, hier können Sie zwischen **Management**, **Spezifikation**, **Entwicklung**, **Testen**, **Unterstützung** und **Andere** wählen. Ist es Ihnen wichtig eine weitere Information zu Ihrere Arbeit an diesem Paket zu erwähnen, können Sie diese unter **Kommentar** eingeben. Abschließend klicken Sie auf **Speichern**.

![Detailansicht gebuchte Zeit](media/83a4c8d8e0523d0ee6f31212fb4689d60f71a73c.PNG "Detailansicht gebuchte Zeit")

Klicken Sie auf die Detailansicht Ihres ausgewählten Arbeitspakets, finden Sie neue Informationen zu den aufgewendeten Stunden für dieses Paket.

## Zeiterfassung mittels der Timer-Schaltfläche

Sie können Ihre Arbeitszeit auch erfassen indem Sie die **Timer-Schaltfläche** nutzen. Diese funktioniert in Echtzeit wie eine Stopuhr. Um dies zu tun öffnen Sie die Detailansicht des gewünscshten Arbeitspakets. Danach klicken Sie auf **Neuen Timer Starten**.

![Neuen Timer starten](media/54c84eddcaa5277ad1585c9568973b7351247dad.PNG "Neuen Timer starten")

Daraufhin beginnt der Timer sofort damit Ihre Arbeitszeit zu messen.

![Timer starten](media/c8e7f5ac14b1ed140c4ae469dcf53017204494fc.PNG "Timer starten")

Klicken Sie erneut auf die **Timer-Schaltfläche** öffnet sich ein neues Fenster. In diesem ist es Ihnen möglich wie zuvor **Datum**, **Stunden**, **Arbeitspaket** und **Aktivität** auszuwählen. Erneut können Sie auch einen **Kommentar** hinzufügen. Klicken Sie auf **Speichern** um Ihre Eingaben zu übernehmen. Wollen Sie die erfasste Zeit nicht übernehmen klicken Sie auf **Abbrechen** oder **Löschen**.

![Zeit erfassen mittels Timer](media/ce6d01cee870cefc08865a9ed50424a4cddae7b0.png "Zeit erfassen mittels Timer")

Wenn Sie den Bereich des Arbeitspakets verlassen, in dem Sie Ihre aktuelle Arbeitszeit messen, können Sie über die Schaltfläche Ihres Benutzerprofils die Zeitmessung beenden oder zum Arbeitspaket zurückkehren.

![Zeiterfassung Benutzerprofil](media/8dcb7345077c20de451d9f00b1619f8dd34ffecf.PNG "Zeiterfassung Benutzerprofil")

## Zeit über Meine Seite erfassen

Sie können im **Meine gebuchte Zeit-Widget** auf **Meiner Seite** Ihre erfasste Zeit und und Ihre Aktivitäten einer Woche betrachten. Bevor Sie dies tun können, müssen Sie das Widget auf **Meiner Seite** über die **Plus-Schaltfläche** hinzufügen.

![Meine gebuchte Zeit-Widget](media/a5ca4d5cb70047d497f4f0d6e1d9f7a0b86025df.PNG "Meine gebuchte Zeit-Widget")

Haben Sie das Widget ausgewählt können Sie die verbuchten Stunden Ihrere Arbeitswoche sehen und über **Zeit buchen**, weitere Zeiten erfassen.

![Erfasste Arbeitszeit](media/48c6ce0087a8db598bae6d7036510fcd32fbc1dc.PNG "Erfasste Arbeitszeit")

## Erfasste Arbeitszeit bearbeiten

Um bereits erfasste Arbeitszeit zu bearbeiten, klicken Sie in der Detailansicht des Arbeitspakets auf **Aufgewendete Zeit**.

![Arbeitszeit bearbeiten](media/9d5eed47f7a0261d79d5225759fe7122680072ad.PNG "Arbeitszeit bearbeiten")

Dadurch gelangen Sie zum Zeiterfassungsbericht. Hier ist es Ihnen möglich alle Zeiteinträge des Arbeitspakets zu sehen. Abhängig davon welche Rechte Sie besitzen, können Sie Arbeitszeiten von anderen Nutzerinnen und Nutzern möglicherweise nicht bearbeiten. Klicken Sie auf die **Bearbeiten-Schaltfläche** (Bleistift) neben einem Zeitantrag in der Liste.

![Liste der Zeiteinträge](media/bfaaa9e796814495c8fa7293359b4184f266e638.PNG "Liste der Zeiteinträge")

Es öffnet sich erneut die Ansicht in der Sie auch Ihre Arbeitszeit erfassen konnten. Hier ist es Ihnen möglich Ihre Änderungen vorzunehmen und zu speichern.

![Zeiteintrag bearbeiten](media/cbb55dbf7950298d6bfa9443f7979b758e463f6c.PNG "Zeiteintrag bearbeiten")

## Zeiten für andere Personen erfassen und bearbeiten

Mit den entsprechenden Rechten ist es Ihnen in Projekte möglich, Arbeitszeit auch für andere Personen zu erfassen und zu bearbeiten.

Um diese Funktion nutzen zu können, müssen Ihen diese Rechte vom Admin gewährt werden.

Wurden Ihnen diese Rechte zugewiesen ist es Ihnen möglich unter Benutzer auch andere Nutzerinnen und Nutzer einzutragen.

Klicken Sie nachdem Sie die gewünschten Änderungen durchgeführt haben auf **Speichern**.

![Zeit für andere Person erfassen und ändern](media/c9073be23d709247e5f0fcf9cfd1ab430e43e85f.PNG "Zeit für andere Person erfassen und ändern")

Wenn Sie sich erneut die Zeiteinträge zu einem Arbeitspaket ansehen, ist es Ihnen möglich zu sehen wer für wen Zeit erfasst hat.

![Zeiteintrag gebucht von](media/c6765fd2c698be2600916f3341ffaf3a2d6f8626.PNG "Zeiteintrag gebucht von")

## Personaleinsatzkosten verfolgen

Um Personaleinsatzkosten verfolgen zu können, muss ein Stundensatz in Ihrem Profil festgelegt werden. Die Personaleinsatzkosten werden dadruch automatisch anhand der gebuchten Stunden und des angegebenen Stundensatzes berechnet.

## Stundensatz für Personalkosten festlegen

Es kann ein Stundensatz festgelegt werden um die Arbeitskosten pro Nutzerin oder Nutzer zu verfolgen. Dafür werden spezielle Systemadministrationsrechte benötigt. Navigieren Sie zu einem Profil indem Sie auf einen Link (Name einer Nutzerin oder eines Nutzers) in einem Arbeitspaket klicken.

![Profil ansehen](media/992d88439c3e4781d669f905bf31e465d115c9b5.PNG "Profil ansehen")

Sie werden auf die Profilseite weitergeleitet. Klicken Sie auf die **Bearbeiten-Schaltfläche** (Bleistift) in der rechten oberen Ecke.

![Profil bearbeiten](media/752d00f2e37b41356cb7a4f8ed5f883267c5295c.PNG "Profil bearbeiten")

Besitzen Sie die entsprechenden Berechtigungen ist es Ihnen möglich unter der Registerkarte **Stundensatz-Historie**, mittels eines Klicks auf **Aktualisieren** (Bleistift) Stundensätze für ein Projekt oder einen bestimmten Projektzeitraum anzugeben.

![Stundensatz bearbeiten](media/3091993bbc118f0d3ffd0da37df96bf66ce93f1b.PNG "Stundensatz bearbeiten")
