# Zeit- und Kostenberichte

Sie können ganz einfach Berichte für Zeit und Kosten erstellen und filtern, sowie diese Berichte nach Ihren Bedürfnissen gruppieren und speichern.

**Hinweis**: Aktivieren Sie bitte zunächst das Modul **Zeit- und Kosten** in den Projekteinstellungen, bevor Sie die Zeiterfassung nutzen.

| Zeit- und Kostenberichte             | Erfahren Sie, wie Sie Zeit- und Kostenberichte in Projekte öffnen können.                 |
| ------------------------------------ | ----------------------------------------------------------------------------------------- |
| Kostenberichte filtern               | Erfahren Sie, wie Sie Kostenauswertungen filtern können.                                  |
| Kostenberichte gruppieren            | Erfahren Sie, wie Sie Zeit- und Kostenberichte nach Ihren Bedürfnissen gruppieren können. |
| Einheiten für die Anzeige auswählen  | Erfahren Sie, wie Sie die Einheiten für Ihren Bericht auswählen können.                   |
| Zeit- und Kostenberichte exportieren | Erfahren Sie, wie Zeit- und Kostenberichte exportiert werden können.                      |

### Hier geht es weiter mit:

- [Zeit- und Kostenberichte](zeit-und-kostenberichte/zeit-und-kostenberichte.md)
- [Kostenberichte filtern](zeit-und-kostenberichte/kostenberichte-filtern.md)
- [Kostenberichte gruppieren](zeit-und-kostenberichte/kostenberichte-gruppieren.md)
- [Einheiten für die Anzeige auswählen](zeit-und-kostenberichte/einheiten-fuer-die-anzeige-auswaehlen.md)
- [Zeit- und Kostenberichte exportieren](zeit-und-kostenberichte/zeit-und-kostenberichte-exportieren.md)
