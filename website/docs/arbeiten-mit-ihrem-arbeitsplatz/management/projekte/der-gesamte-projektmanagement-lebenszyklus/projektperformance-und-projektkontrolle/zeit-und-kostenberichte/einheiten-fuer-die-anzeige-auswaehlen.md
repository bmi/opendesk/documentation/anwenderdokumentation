# Einheiten für die Anzeige auswählen

In der Zeit- und Kostenauswertung können Sie die **Einheiten** auswählen, die Sie anzeigen möchten.

Sie können entweder **Personal** auswählen, wodurch die erfasste Zeit für die Arbeitspakete gemäß den oben genannten Filter- und Gruppierungskriterien angezeigt wird. Abhängig von Ihrem Filter, z.B. wenn Sie nach der zugewiesenen Person filtern, erhalten Sie eine Übersicht ähnlich einem Stundenzettel.

Der **Geldwert** zeigt die Kosten an, die gemäß den obigen Filter- und Gruppierungskriterien erfasst wurden. Dies beinhaltet Arbeitskosten (berechnet nach der erfassten Zeit und dem Stundensatz) sowie gebuchte Stückkosten.
