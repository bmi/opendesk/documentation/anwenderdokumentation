# Zeit- und Kostenberichte

## Auswertungsbericht

Öffnen Sie die Zeit- und Kostenauswertung in Projekte, indem Sie zum Modul **Zeit und Kosten** in der Projektnavigation navigieren.

![Projektnavigation zu Zeit und Kosten](media/9e1e820961073cb7ada86b1a457cc881a8e144d3.jpg "Projektnavigation zu Zeit und Kosten")

**Zeit und Kosten** ist ein Plugin zum Filtern von Kostenberichten für einzelne oder mehrere Benutzerinnen und Benutzer über einzelne oder mehrere Projekte. Das Modul muss zunächst in der Projektkonfiguration unter dem Menüpunkt **Module** aktiviert werden, um es im Projektmenü sichtbar zu machen.

![Modulauswahl der Projektkonfiguration - Zeit und Kosten ausgewählt](media/8f2c0a1f76e81ee81a9afba85aee974a1b1697e4.jpg "Modulauswahl der Projektkonfiguration")
