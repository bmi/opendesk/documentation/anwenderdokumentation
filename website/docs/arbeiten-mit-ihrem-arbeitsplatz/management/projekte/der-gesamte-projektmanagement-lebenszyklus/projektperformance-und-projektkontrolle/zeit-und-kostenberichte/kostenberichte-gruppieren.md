# Kostenberichte gruppieren

Gruppieren Sie Ihre Zeit- und Kostenauswertung. Dazu stehen Ihnen verschiedene Kriterien zur Verfügung, wie z.B. nach Datum, Arbeitspaketen, zugewiesener Person oder einem beliebigen anderen Feld, einschließlich selbstdefinierter Felder. Um Gruppierungskriterien zu den Spalten oder Zeilen des Berichts hinzuzufügen, wählen Sie das Dropdown-Menü auf der rechten Seite, um ein **Gruppierungsfeld** hinzuzufügen:

![Kostenauswertung-Gruppierung](media/035223af00ae8a2f16a67dc47d2f23a0613f412d.jpg "Kostenauswertung-Gruppierung")

Die gewählten Kriterien zur Gruppierung werden dann zu den Spalten oder Zeilen des Berichts hinzugefügt. Klicken Sie auf die Schaltfläche **Anwenden**, um Ihre Änderungen für den Bericht zu speichern. Der Bericht wird dann entsprechend den ausgewählten Kriterien in den Spalten und Zeilen angelegt. Selbstverständlich lässt sich die Reihenfolge der Gruppen in den Spalten oder Zeilen per Drag &amp; Drop ändern.
