# Kostenberichte filtern

Natürlich können Sie die Ansicht der Kostenberichte ändern und an Ihre Bedürfnisse anpassen: Wählen Sie verschiedene Filter aus und wenden diese auf z.B. Arbeitspakete, Autorin oder Autor, Startdatum oder Zielversion an. Durch Hinzufügen eines Projektfilters können mehrere **Projekte** ausgewählt werden. Abhängig von Ihren Rechten im Projekt können auch mehrere Nutzerinnen und Nutzer bei einer Filterung ausgewählt werden. Auf diese Weise können Sie die Zeit- und Kosteneinträge genau nach Ihrem Bedarf filtern, je nachdem, welche Zeitspanne, Arbeit oder welche Person Sie sehen möchten. Die Ergebnisse werden anschließend im Zeit- und Kostenbericht angezeigt.

![Kostenauswertung-Filter](media/04475d8f17814a8184dea1d413081898b28f7671.jpg "Kostenauswertung-Filter")

## Filterauswahl

Sie können folgende Kriterien filtern:

- Aktivität
- Aktualisiert am
- Anfangstermin
- Arbeitspaket
- Autor
- Benutzer
- Budget
- Datum der Buchung
- Endtermin
- Erstellt
- Kategorie
- Logged by
- Priorität
- Projekt
- Status
- Thema
- Typ
- Verantwortlich
- Version

![Filter-Auswahl](media/72371cce812f0cf0e2affd05480581abab482a3a.jpg "Filter-Auswahl")
