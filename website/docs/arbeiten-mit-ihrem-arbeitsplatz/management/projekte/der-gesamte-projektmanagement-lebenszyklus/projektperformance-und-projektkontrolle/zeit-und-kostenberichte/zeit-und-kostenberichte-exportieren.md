# Zeit- und Kostenberichte exportieren

Exportieren Sie die **Zeit- und Kostenauswertungen nach Excel.** Filtern Sie zuerst den Bericht nach Ihren Bedürfnissen. Wählen Sie auch die anzuzeigende Einheit (Arbeit, Geldwert, etc.) aus. Bitte beachten Sie, dass die Einstellungen für **Gruppieren nach** nicht auf die exportierte Datei angewendet werden. Klicken Sie auf die graue Schaltfläche **Exportieren als Excel Spreadsheet**. Der Download startet automatisch.

![Kostenbericht-exportieren](media/e42f7db9c2bc1b5d2a02be01ef1d3592bc4ff388.jpg "Kostenbericht-exportieren")
