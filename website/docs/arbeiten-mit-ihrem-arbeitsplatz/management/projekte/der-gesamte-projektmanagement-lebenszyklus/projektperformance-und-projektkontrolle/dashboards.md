# Dashboards

## Startseite der Anwendung

Auf der Startseite der Anwendung erhalten Sie einen Überblick über wichtige Informationen. Von hier aus können Sie auf alle globalen Module in der linken Navigation zugreifen.

Um zur Startseite der Anwendung zu gelangen, klicken Sie auf das Logo in der Kopfzeile der Anwendung.

![Übersicht über das Dashboard des Projekte-Moduls](media/0ddd706ca8d1d0540ebed302a6d6cd4b050ecedc.PNG "Übersicht über das Dashboard des Projekte-Moduls")

1. Begrüßungstextblock zur Begrüßung Ihrer Teammitglieder, zum Austausch wichtiger Projektinformationen oder anderer Informationen. Sie können den Willkommenstextblock unter **Verwaltung** → **Systemeinstellungen** → **Allgemein** konfigurieren.
1. Im Block **Projekte** wird Ihr aktuelles Projekt angezeigt. Sie können ein neues Projekt erstellen oder alle Projekte anzeigen.
1. Der Block **Neue Funktionen** zeigt Funktionsankündigungen und Entwicklungen der neuesten Versionen im Modul **Projekte** an.
1. Der Block **Benutzer** zeigt die zuletzt registrierten Benutzerinnen und Benutzer der Instanz an. Sie können neue Benutzerinnen und Benutzer mit der grünen Schaltfläche **+ Benutzer** einladen.
1. Der Block **Mein Konto** verlinkt zu wichtigen Kontoeinstellungen wie dem Benutzerprofil, der Seite „Meine Seite“ und dem Bereich „Passwort ändern“.
1. Der Block **Neueste Nachrichten** zeigt die neuesten Nachrichten aus all Ihren Projekten an. Klicken Sie auf den Link der Nachricht, um die Details zu lesen.
1. Der Block **Community** zeigt Links zu wichtigen Community-Informationen an, wie z. B. Versionshinweise, Forum oder die API-Dokumentation.
1. Der Block **Administration** zeigt Links zu wichtigen Systemadministrations-Ressourcen an. Außerdem wird das Sicherheitsabzeichen der Anwendung angezeigt, wenn es aktiviert ist.
1. Unten auf der Seite finden Sie Links zu den Benutzerhandbüchern, dem Glossar, den Shortcuts und dem Community-Forum.
