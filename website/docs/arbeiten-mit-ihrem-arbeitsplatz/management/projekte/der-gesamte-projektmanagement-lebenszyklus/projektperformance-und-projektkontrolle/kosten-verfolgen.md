# Kosten verfolgen

Sie können Stückkosten für ein Arbeitspaket innerhalb eines Projekts erfassen. Dies erleichtert den Überblick über bestimmte Einheiten, die in einem Projekt ausgegeben werden, z. B. Reisekosten oder Material.

## Kosten über ein Arbeitspaket buchen

Zuerst müssen Sie in den Projekteinstellungen das Modul **Zeit und Kosten** und das Modul **Budget** aktivieren, um Stückkosten erfassen zu können. Dies erfolgt in der **Projektkonfiguration** unter **Module**.

![Module aktivieren](media/ac709e1392256046653a1e300ff08f4691700c0d.png "Module aktivieren")

Wenn diese Optionen bereits aktiviert sind, wählen Sie ein Projekt aus und gehen Sie dann in das Modul **Arbeitspakete**. Öffnen Sie die Detailansicht eines Arbeitspakets. Dies geschieht durch Klicken auf die Nummer oder durch Klicken auf **Detailansicht öffnen** im Kontextmenü.

![Detailansicht öffnen](media/ecb189c49c029c9ec6425ba5474c46ed61fc42bc.png "Detailansicht öffnen")

Wählen Sie dann ein Budget aus dem Dropdown-Menü der Arbeitspaketdetails aus und verknüpfen Sie es mit dem ausgewählten Arbeitspaket.

![Budget Dropdown-Menü](media/db6f88d58fb242e1d799860c164c48219db7583b.png "Budget Dropdown-Menü")

Sie können auch über das Kontextmenü **Stückkosten buchen**. Damit können Sie die Kosten für das ausgewählte Arbeitspaket erfassen.

![Stückkosten buchen](media/4b1a6c2dbe1dc2b8e9bb7c7f05063c5d9fc42d2f.png "Stückkosten buchen")

Eine neue Seite wird geladen, auf der Sie die Stückkosten erfassen können. Hier können Sie folgende Informationen bearbeiten:
- **Arbeitspaket-ID**: Als Standard wird die ID des Arbeitspakets angezeigt, für das Sie die Option **Stückkosten buchen** gewählt haben. Sie können diese Nummer ändern. In diesem Fall werden die Stückkosten für das jeweilige Arbeitspaket erfasst.
- Das **Datum**, für das die Stückkosten gebucht werden.
- Sie können einen **Benutzer** (Projektmitglied) aus dem Dropdown-Menü auswählen, für den Sie die Stückkosten erfassen.
- Das Feld **Kostentyp** enthält vordefinierte Kostenarten, die von den **Administratorinnen** und **Administratoren** festgelegt werden können z. B. Beratungstage oder Lizenzen. Die Einheiten und die entsprechenden Kosten pro Einheitenkategorie werden für jede dieser Kostentypen separat definiert.
- **Stücke**: Sie können die Anzahl der zu erfassenden Einheiten eingeben.
- Die **Kosten** der Stücke werden nach Eingabe der Stückzahl automatisch berechnet. Sie können die Summe der berechneten Kosten manuell ändern, wenn z. B. ein Rabatt angeboten wird oder eine Sondervereinbarung getroffen wurde. Um die Kosten manuell anzupassen, klicken Sie auf das **Bleistiftsymbol** links neben der Kostensumme.
- Im **Kommentar** feld können Sie weitere Details zur Beschreibung der gebuchten Stückkosten eingeben.

Bitte speichern Sie Ihre Stückkostenbuchung.  **Hinweis**: Administratorinnen und Administratoren haben bestimmte Zugriffsrechte, um neue Kostenarten zu erstellen und Kosten für bestimmte Einheiten zu definieren.

![Formular Stückkosten buchen](media/06f16c4c657356802c1eba952c74641388233460.png "Formular Stückkosten buchen")

Die **gebuchte Einheiten** sowie die Summe der **Gesamtkosten** werden in der Detailansicht der Arbeitspakete angezeigt.

![Kosten Detailansicht](media/930bc0c60225b37e23c63f310a83b63aea8e7f50.png "Kosten Detailansicht")

## Gebuchte Kosten beabreiten

Um die gebuchten Einheiten für ein Arbeitspaket anzuzeigen und zu bearbeiten, rufen Sie die Detailansicht des Arbeitspakets auf. Hier wird die Gesamtsumme der gebuchten Einheitskosten angezeigt. Klicken Sie auf der Anzahl der gebuchte Einheiten innerhalb eines Arbeitspaketes, um die Details anzuzeigen.

![Gebuchte Einheiten](media/03cfb69257fcaf257ac1e004f0006c89616c5efa.png "Gebuchte Einheiten")

Es öffnet sich ein neues Fenster. Die detaillierten Einträge werden in einem Kostenbericht angezeigt. Klicken Sie auf das **Bleistiftsymbol** neben dem Kosteneintrag, um ihn zu bearbeiten.

![Kostenauswertung bearbeiten](media/dc37d1d857b07a0bc31f65ad3f40af2e997cccdd.png "Kostenauswertung bearbeiten")

Dadurch wird die Detailansicht des Kosteneintrags geöffnet, und Sie können Ihre Änderungen auf dieselbe Weise vornehmen, wie bei der [Buchung von Kosten über ein Arbeitspaket](kosten-verfolgen.md#c8250).

In bestimmten Fällen kann es vorkommen, dass Sie die Kosten manuell ändern möchten, so dass sie von den automatisch berechneten Gesamt- und Stückkosten abweichen. In diesem Fall klicken Sie auf das **Bleistiftsymbol**, um die Kosten manuell einzugeben.

![Kosten manuell ändern](media/034274419f9ff49e1f1946c2bb2d196144a8f8f9.png "Kosten manuell ändern")

**Hinweis**: Stellen Sie sicher, dass Sie Ihre geänderten Eingaben speichern.

## Gebuchte Kosten löschen

Wenn Sie die erfassten Kosten löschen möchten, klicken Sie auf das **Mülleimersymbol** neben einem Kosteneintrag im Kostenbericht.

![Kostenbericht löschen](media/0bb8b674c3a354bc8087ac24f2416ac196899066.png "Kostenbericht löschen")
