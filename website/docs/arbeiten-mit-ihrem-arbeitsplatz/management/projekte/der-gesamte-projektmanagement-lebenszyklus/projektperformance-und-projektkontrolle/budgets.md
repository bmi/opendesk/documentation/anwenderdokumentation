# Budgets

In **Projekte** können Sie ein Projektbudget erstellen und verwalten, um Ihre verfügbaren und ausgegebenen Kosten in einem Projekt im Auge zu behalten.

Sie können sowohl die geplanten Stückkosten als auch die Arbeitskosten für das Projekt hinzufügen.

Anschließend ordnen Sie einem Budget Arbeitspakete zu. Wenn Sie Zeit oder Kosten für dieses Arbeitspaket erfassen, werden die Kosten auf dieses Budget gebucht und zeigen den Prozentsatz an, der für ein Projektbudget ausgegeben wurde.

## Ein Projektbudget erstellen

Um ein Budget in Ihrem Projekt erstellen zu können, aktivieren Sie zunächst das Modul **Budgets** in den Projekteinstellungen.

Navigieren Sie dann im Projektmenü links zum Punkt **Budgets** und klicken Sie auf die Schaltfläche **Neues Budget anlegen (+ Budget)** oben rechts auf der Seite.

![Budgets](media/d2b0c5dd9652d5e3a572e4c8c5bc9b138b80e25a.jpg "Budgets")

In der Detailansicht können Sie die Details für Ihr Projektbudget eingeben sowie geplante Stück- und Arbeitskosten hinzufügen.

1. Geben Sie einen Betreff für Ihr Budget ein, damit Sie es leicht identifizieren können.
1. Geben Sie eine detaillierte Beschreibung ein, um weitere Informationen zu Ihrem Budget hinzuzufügen, z. B. Budgetverantwortlicher, Status und mehr.
1. Laden Sie weitere Dateien in Ihre Budgets hoch. Dies geht per Drag &amp; Drop oder durch Klicken auf das Feld und Auswahl einer Datei aus dem Explorer.
1. Geben Sie ein festes Datum ein. Dieses Datum ist die Basis für das Budget, um die geplanten Kosten auf Grundlage der Kostenarten oder des konfigurierten Stundensatzes im Profil der Benutzerin bzw. des Benutzers zu berechnen. Die Stundensätze können für verschiedene Datumsbereiche konfiguriert werden, daher müssen Sie ein festes Datum für ein Budget festlegen, für das die Kosten berechnet werden.

![Neues Budget](media/42e7a771e8159b18c7f54cfcbfd0cb0ee4b90e31.jpg "Neues Budget")

## Geplante Stückkosten hinzufügen

Sie können geplante Stückkosten zu einem Budget in Ihrem Projekt hinzufügen. Diese Stückkosten müssen zunächst in der Systemverwaltung konfiguriert werden.

1. Geben Sie die Stückzahl der Kostenart ein, die Sie zu Ihren Projektbudgets hinzufügen möchten.
1. Wählen Sie die Kostenart, die Sie für Ihr Budget planen möchten, aus der Dropdown-Liste aus. Der Name des Eintrags wird automatisch entsprechend der Konfiguration der Kostenarten in Ihrer Systemverwaltung festgelegt.
1. Fügen Sie einen Kommentar hinzu, um die Stückkosten genauer zu beschreiben.
1. Die geplanten Kosten für diese Kostenart werden automatisch berechnet, je nachdem, welche Stückkosten für diese Kostenart eingestellt sind. Der Kostensatz wird von dem festen Datum abgeleitet, das Sie für Ihr Budget konfiguriert haben. Sie können auf das **Bearbeitungssymbol (Bleistift)** klicken, wenn Sie die berechneten Kosten für diese Kostenart manuell überschreiben möchten.
1. Klicken Sie auf das **Löschsymbol** , wenn Sie die geplanten Stückkosten löschen möchten.
1. Das **+-Symbol** fügt eine neue Stückkostenart für dieses Budget hinzu.

## Geplante Arbeitskosten hinzufügen

Sie können auch geplante Arbeitskosten zu einem Budget hinzufügen.

1. Legen Sie die Stunden fest, die für eine Benutzerin bzw. einen Benutzer in diesem Budget geplant werden.
1. Fügen Sie eine Benutzerin bzw. einen Benutzer aus der Dropdown-Liste hinzu.
1. Sie können bei Bedarf einen Kommentar zu den geplanten Arbeitskosten hinzufügen.
1. Der Gesamtbetrag der geplanten Kosten wird auf Grundlage der eingegebenen Stunden und des für diese Benutzerin bzw. diesen Benutzer im Benutzerprofil konfigurierten Stundensatzes berechnet. Sie können die berechneten geplanten Arbeitskosten manuell überschreiben, indem Sie auf das **Bearbeitungssymbol (Bleistift)** neben dem berechneten Betrag klicken. Die Kosten werden dann auf Grundlage des Stundensatzes berechnet, der zum festgelegten Datum für Ihr Budget gilt.
1. Mit dem **Löschsymbol** können Sie die geplanten Arbeitskosten aus dem Budget entfernen.
1. Fügen Sie mit dem **+-Symbol** weitere geplante Arbeitskosten für verschiedene Benutzerinnen bzw. Benutzer zu Ihrem Budget hinzu.
1. Speichern und übermitteln Sie Ihre Änderungen, indem Sie auf die **violette Schaltfläche** klicken.

![Geplante Personaleinsatzkosten](media/48d11ab141bb297d7348f72bf8083056174bccb1.jpg "Geplante Personaleinsatzkosten")

## Einem Budget ein Arbeitspaket zuordnen

Mit Arbeitspaketen können Sie Zeit und Kosten auf ein Budget buchen. Um ein Arbeitspaket zu einem Projektbudget hinzuzufügen, navigieren Sie zur **Detailansicht** des jeweiligen Arbeitspakets.

Wählen Sie im Bereich **Kosten** das Budget aus, dem Sie dieses Arbeitspaket zuordnen möchten. In der Dropdown-Liste werden alle Budgets angezeigt, die in Ihrem Projekt angelegt wurden.

Nun werden alle Zeiten und Kosten, die auf dieses Arbeitspaket gebucht werden, auch auf das entsprechende Budget gebucht.

![Arbeitspaket zuordnen](media/92c7dde0743077ab9c7c5b65cfe01876a2e4c247.jpg "Arbeitspaket zuordnen")

## Details anzeigen und Budget aktualisieren

Sie können die Details eines Budgets einsehen und Änderungen an Ihrem Budget vornehmen, indem Sie es in der Liste der Budgets auswählen.

Klicken Sie auf die Bezeichnung des gewünschten Budgets, um die Detailansicht zu öffnen.

Sie erhalten einen Überblick über die geplanten, ausgegebenen und verfügbaren Kosten für Ihr Budget. Außerdem wird der Gesamtfortschritt des Budgets angezeigt, d. h. der Gesamtanteil, der bereits ausgegeben wurde. Sie können hier auch den festen Satz einsehen, aus dem die Stück- und Arbeitskosten berechnet werden.

1. Aktualisieren Sie das Budget und nehmen Sie Änderungen vor, z. B. bei den geplanten Stück- oder Arbeitskosten.
1. Kopieren Sie das Budget, um auf Grundlage der Einstellungen dieses Budgets ein neues Budget zu erstellen.
1. Löschen Sie das Budget.
1. In den Budgetdetails sehen Sie alle geplanten Stückkosten.
1. Hier sehen Sie die diesem Budget zugeordneten Arbeitspakete, für die bereits Ist-Stückkosten gebucht wurden.
1. Hier finden Sie die geplanten Arbeitskosten für dieses Budget.
1. Die Ist-Arbeitskosten listen alle dem Budget zugeordneten Arbeitspakete auf, auf die bereits Zeit gebucht wurde.

![Budget-Liste](media/6aabb1f9083e015a13396299440296e8106c551c.jpg "Budget-Liste")

![Budgets](media/368464cb4e48bf3db4940891e3c91f0929602408.jpg "Budgets")

**Hinweis:** Grundlage für die Berechnung der Kosten sind die Kostenarten und der Stundensatz, die im Benutzerprofil eingestellt wurden.

## Projektübergreifende Budgets

Budgets sind derzeit auf ein einzelnes Projekt beschränkt. Sie können nicht über mehrere Projekte hinweg geteilt werden. Das bedeutet, dass Sie für die verschiedenen Haupt- und Unterprojekte ein separates Budget einrichten müssen. Sie können jedoch Kostenberichte verwenden, um den Zeit- und Kostenaufwand für mehrere Projekte zu analysieren. Einzelheiten hierzu finden Sie in der Anleitung zu Zeit- und Kostenberichten.
