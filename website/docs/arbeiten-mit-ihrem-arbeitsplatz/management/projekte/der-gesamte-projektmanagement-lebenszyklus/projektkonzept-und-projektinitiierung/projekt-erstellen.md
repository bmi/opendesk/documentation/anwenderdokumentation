# Projekt erstellen

Sie können ein neues Projekt erstellen, indem Sie auf die violette Schaltfläche namens **+Projekt** klicken. Diese befindet sich direkt auf dem Startbildschirm des Systems im Bereich **Projekte**.

![Neues Projekt erstellen](media/abaf62ae7afdb175bec0ad2e1accb6432a41351b.png "Neues Projekt erstellen")

Sie können auch ein neues Projekt erstellen, indem Sie auf das violette **Pluszeichen (+)** in der oberen Menüleiste klicken.

Ein Menü wird geöffnet. Klicken Sie dann auf **+Projekt**.

![Pluszeichen-Menü](media/62b0064af6f26242e175e333d322575f7665fdac.png "Pluszeichen-Menü")

Eine neue Seite öffnet sich.

![Neues Projekt einstellen](media/7aec3b07f7a7de79d07fc078b0b956fcec38d5cb.png "Neues Projekt einstellen")

Bei der Erstellung eines neuen Projekts können Sie ein von Grund auf neues Projekt erstellen oder ein Projekt aus einer **Vorlage** erstellen. Im letzteren Fall wählen Sie die Vorlage aus dem Dropdown-Menü aus.

Geben Sie Ihrem Projekt einen Namen.

Sie können auswählen, ob Ihr Projekt ein **Unterprojekt** eines anderen Projekts sein soll. In diesem Fall wählen Sie das Projekt aus dem Dropdown-Menü aus.

Unter **Erweiterte Einstellungen** haben Sie die Möglichkeit, Ihrem Projekt eine Beschreibung hinzuzufügen. Des Weiteren können Sie bestimmen, ob es öffentlich zugänglich ist und welchen Status es hat, sowie eine Beschreibung des Status des Projekts eingeben.

![Erweiterte Einstellungen](media/68dcce2565d8aef6680fe2364c99989868319362.png "Erweiterte Einstellungen")

Wenn Sie Ihre Projekteinstellungen abgeschlossen haben, klicken Sie auf die hellviolette Schaltfläche **Speichern**.

Wenn Sie erfolgreich ein neues Projekt erstellt haben, werden Sie automatisch zur Projektübersichtsseite weitergeleitet.

![Übersichtseite](media/2cb61a6f7c7e19c0b17093213545d651af1508e3.png "Übersichtseite")

**Hinweis**: Wenn Sie ein neues Projekt erstellen, werden Sie standardmäßig als Projektadministrator festgelegt, unabhängig davon, ob Sie ein Projekt kopiert, eine Vorlage verwendet oder ein Projekt von Grund auf neu erstellt haben.
