# Mitglieder hinzufügen

Zur Ansicht und Bearbeitung eines Projekts müssen Sie Mitglied eines Projekts sein. Aus diesem Grund ist das Hinzufügen von Teammitgliedern zu einem Projekt erforderlich. Ein **Mitglied** wird als Projektmitglied innerhalb eines Projekts definiert. Projektmitglieder können im Modul **Mitglieder** des Projektmenüs hinzugefügt werden.

**Hinweis**: Sind Sie kein Projektmitglied, erscheint das Projekt nicht in der Projektauswahl oder in der Projektliste.

## Mitglieder ansehen

Wählen Sie im Projektmenü das Modul **Mitglieder**, um eine Liste aller **Projektmitglieder** und ihrer **Rollen** im Projekt anzuzeigen.

Einen Überblick über die aktuellen Mitglieder dieses Projekts finden Sie in der Mitgliederliste oder im Mitglieder-Widget auf der Übersichtsseite.

![Übersichtseite eines Projektes](media/3d8b3db96e861b4b2c0d2624d488c89b91de2bcf.png "Übersichtseite eines Projektes")

## Bestehende Konten hinzufügen

Wählen Sie zuerst das Projekt aus, zu dem Sie Mitglieder hinzufügen möchten, um bestehende Konten oder Gruppen zu einem Projekt hinzuzufügen. Dies können Sie tun, indem Sie das Modul **Mitglieder** auf der linken Seite des Projektmenüs auswählen. Eine andere Möglichkeit ist, auf **+Mitglied** im Mitglieder-Widget auf der Projektübersichtsseite zu klicken.

![Mitglieder hinzufügen](media/eb30f555bb2d0c611a346993b8f61bb714724fdd.png "Mitglieder hinzufügen")

Klicken Sie auf die violette Schaltfläche **+Mitglied** oben rechts. Tippen Sie den Namen der Person oder der Gruppe ein, die Sie zum Projekt hinzufügen möchten. Es können mehrere Mitglieder gleichzeitig ausgewählt und hinzugefügt werden. Den neuen Mitgliedern eine Rolle zuweisen (**Mitglied**, **Leser** oder **Projekt-Admin**) und auf die hellviolette Schaltfläche **Hinzufügen** klicken.

**Hinweis**: Klicken Sie auf den Namen des neuen Mitglieds oder drücken Sie die Eingabetaste, bevor Sie auf die Schaltfläche **Hinzufügen** klicken.

## Neue Mitglieder einladen

Sie können auch Mitglieder zu einem Projekt einladen, die noch kein Konto haben. Dafür gibt es drei Möglichkeiten:

- Über das Mitgliedermodul einladen
- Aus der Kopfleiste einladen
- Mitglieder aus einem Arbeitspaket einladen

### Einladung von Personen im Mitglieder-Modul

Folgen Sie den Schritten wie im **Bereich Bestehenden Konten hinzufügen** (s. o.). Anstatt eines beliebigen Namens geben Sie die E-Mail-Adresse des neuen Mitglieds ein. Findet Projekte kein bestehendes Konto, wird automatisch die Information **Einladung an... senden** gesetzt. Drücken Sie die **Eingabetaste** oder wählen Sie den Text **Einladung an... senden**. Weisen Sie dem neuen Mitglied eine Rolle zu und klicken Sie auf die violette Schaltfläche **Hinzufügen**.

Der Benutzer wird per E-Mail eine Einladung mit einem **Link** erhalten, um einen Account für Projekte zu erstellen.

![Neue Mitglieder einladen](media/6f6531b943094f5aeb1778d88eba775678379f4d.png "Neue Mitglieder einladen")

### Einladung von Personen aus der Kopfleiste

In der immer vorhandenen Kopfzeile finden Sie eine violettes Plus-Symbol. Diese Schaltfläche ermöglicht es Ihnen, Projekte und Arbeitspakete zu erstellen sowie Benutzer einzuladen, wo auch immer Sie sich in der Anwendung befinden.

![Mitglieder einladen durch Kopfleiste](media/d8b82f1aeeea9b820297146c7c762846b8702e87.png "Mitglieder einladen durch Kopfleiste")

Sobald Sie auf **Nutzer einladen** geklickt haben, erscheint ein Pop-up-Fenster, in dem Sie einstellen können, zu welchem Projekt Sie die neuen Mitglieder hinzufügen und welche Rollen sie erhalten. Klicken Sie auf **Weiter**, wenn Sie alle Daten eingegeben haben. Bevor die Einladung versendet wird, sehen Sie die Angaben zur Kontrolle. Wenn alles in Ordnung ist, klicken Sie auf **Einladung senden**. Eine Bestätigung wird angezeigt. Klicken Sie auf **Fortfahren**. Sie werden sofort zur Mitgliederliste weitergeleitet.

**Achtung**: Wenn Sie das Popup-Fenster im letzten Schritt schließen, werden die Daten **nicht** gespeichert und die Einladung wird **nicht** versendet.

![Pop-Up Fenster](media/3483bf8d79ba5a30532239a0e2d4a290af8e9c7e.png "Pop-Up Fenster")

![Nutzer einladen](media/f0bd79abdc5fcd09d951ad90f83016c1b77d9b3f.png "Nutzer einladen")

![Einladung senden](media/6466bcda4b71d7e72125bc00405f68e60bbfedf6.png "Einladung senden")

![Bestätigung zur Einladung](media/c6a481987cbecb16cb069055e74fb08d90566706.png "Bestätigung zur Einladung")

### Mitglieder aus einem Arbeitspaket einladen

Sie können Mitglieder einladen, indem Sie in der Arbeitspaket-Tabelle arbeiten. Durch Klicken auf die Spalte **Zugewiesen an** öffnet sich ein Dropdown-Menü mit der Option **Einladen**. Damit können Sie wie im Abschnitt [Einladung von Personen aus der Kopfleiste](mitglieder-hinzufuegen.md#c5415) (s. o.) vorgehen und z. B. eine Person einladen, die noch keinen Zugang zu Projekte hat. Das Gleiche gilt für die Buchhaltung oder benutzerdefinierte Felder für Ihr Projekt.

Sie können jetzt mit Ihrem Team in Projekte zusammenarbeiten. Neu eingeladene Personen werden mit einem Briefsymbol neben ihrem Namen angezeigt.

**Achtung**: Das Entfernen eines Mitglieds, das die Einladung noch nicht angenommen hat, aus einem Projekt führt zur Löschung des Kontos, die nicht rückgängig gemacht werden kann.

![Einladen aus Arbeitspaket](media/8c15eef1e184cd7454f521bbd26f4d4c34c4aa82.png "Einladen aus Arbeitspaket")

## Verhalten von Gruppen als Projektmitglieder

Gruppen haben die folgenden Auswirkungen auf eine Projektmitgliederliste und verhalten sich etwas anders als einzelne Konten:

- Der Gruppenname wird in der Projektmitgliederliste in einer eigenen Zeile angezeigt
- Die Gruppenmitglieder können nicht einzeln aus der Mitgliederliste entfernt werden (kein Löschsymbol)
- Das Hinzufügen einer Gruppe, deren Mitglieder sich bereits in einer Projektmitgliedsliste befinden, fügt die Rolle der Gruppe zu deren Projektrollen hinzu
- Ein Projektmitglied, das Mitglied einer Gruppe ist, kann einzeln weitere Rollen erhalten
- Die Gruppenrolle kann für einzelne Gruppenmitglieder nicht geändert werden
