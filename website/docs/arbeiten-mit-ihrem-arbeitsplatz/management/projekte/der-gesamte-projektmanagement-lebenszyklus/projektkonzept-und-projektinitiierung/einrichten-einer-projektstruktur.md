# Einrichten einer Projektstruktur

Projekte haben eine klare Struktur, diese umfasst Unterprojekte und übergeordnete Projekte. Ein Projekt kann zum Beispiel Organisationseinheiten eines Unternehmens abbilden, um die Vorgänge der verschiendenen Organisationseinheiten von einander zu trennen. Zum Beispiel:

- **Unternehmen (übergeordnetes Projekt)**
  - Marketing (Unterprojekt)
  - Vertrieb
  - HR
  - IT
  - ...

Die Projekte können auch für Teams verwendet werden, die an einem gemeinsamen Thema arbeiten. Zum Beispiel:

- **Einführung eines neuen Produkts**
  - Design
  - Entwicklung
  - ...

Ein Projekt kann sich auch auf einzelne Produkte oder Kunden beziehen. Zum Beispiel:

- **Produkt A**
  - Kunde A
  - Kunde B
  - Kunde C

Projekte werden in diesem Modul verwendet, um die relevanten Module und Plugins zu strukturieren. Der Screenshot unten zeigt ein Entwicklungsprojekt.

![Projektstruktur](media/d9dc09a87e808229025cfc66ffdb8137e015023c.png "Projektstruktur")

**Hinweis**: Sie müssen Mitglied eines Projekts sein, um das Projekt zu sehen und in einem Projekt arbeiten zu können.
