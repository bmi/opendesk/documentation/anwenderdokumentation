# Projekt-Einstellungen

In den erweiterten Einstellungen der Projektkonfiguration haben Sie die Möglichkeit, zusätzliche Konfigurationsoptionen festzulegen. Um zur Projektkonfiguration zu gelangen, gehen Sie wie folgt vor: Wählen Sie zunächst das gewünschte Projekt aus, klicken Sie dann auf **Projektkonfiguration** und anschließend auf **Informationen**.

Dort können Sie festlegen, ob dieses Projekt als übergeordnetes Projekt betrachtet werden soll, indem Sie die Option **Unterprojekt von** aktivieren. Auf diese Weise können Sie die Struktur Ihrer Projekthierarchie nach Ihren Wünschen anpassen.

Es ist wichtig, eine ausführliche Beschreibung für Ihr Projekt bereitzustellen, und Sie können auch die Standard-Projektkennung einsehen, die in der URL angezeigt wird.

Bitte beachten Sie, dass das Ändern der Projektkennung eines bereits bearbeiteten Projekts erhebliche Auswirkungen auf die Zusammenarbeit haben kann und daher nicht empfohlen wird. Zum Beispiel kann es dazu führen, dass Repositories nicht ordnungsgemäß geladen werden und Deep Links nicht mehr funktionieren, da sich die Projekt-URL ändert, wenn die Projektkennung geändert wird.

Falls gewünscht, können Sie Ihr Projekt auch auf **öffentlich** setzen, was bedeutet, dass darauf ohne Anmeldung bei Projekten zugegriffen werden kann. Vergessen Sie nicht, Ihre Änderungen durch Klicken auf den violetten **Speichern** Button zu sichern. Sie können auch die Autovervollständigung verwenden, um die Projektattribute schnell und einfach auszufüllen.

![Neues Projekt erstellen](media/477a893949c1d55dd3a8c15f9562dbdd77ccf2ec.jpg "Neues Projekt erstellen")

## Projekthierarchie ändern

Um die Hierarchie des Projekts zu ändern, navigieren Sie zu den **Projektkonfiguration**, genauer gesagt zu **Informationen**, und passen Sie die Angaben im **Unterprojekt von** Feld an.

Anschließend betätigen Sie den violetten **Speichern** Button, um Ihre Anpassungen zu sichern.

![Unterprojekt von](media/a21a8549bc31322e2d5a6d7dc3a6283a8f780677.jpg "Unterprojekt von")

## Ein Projekt auf öffentlich setzen

Möchten Sie Ihr Projekt öffentlich zugänglich machen, markieren Sie einfach das Kontrollkästchen neben **Öffentlich** in den **Projektkonfiguration** unter Informationen.

Die Einstellung **Öffentlich** ermöglicht allen Benutzern innerhalb Ihrer Projekte-Instanz den Zugriff auf das Projekt.

(Wenn Ihre Instanz **ohne Authentifizierung zugänglich** ist, wird durch diese Option das Projekt auch für die breite Öffentlichkeit sichtbar, unabhängig von registrierten Benutzerinnen und Benutzern.)

## Projekt kopieren

Um ein bereits existierendes Projekt zu duplizieren, navigieren Sie zu den **Projektkonfiguration** und klicken Sie oben rechts in den **Projektkonfiguration** auf die Option **Kopieren**.

![Kopieren](media/8d7d22931fdef58d65e77814945a9ec7a3561610.jpg "Kopieren")

Benennen Sie das neue Projekt, bestimmen Sie, welche Module und Konfigurationseinstellungen Sie duplizieren möchten, und entscheiden Sie, ob Sie die Projektmitglieder per E-Mail über den Kopiervorgang informieren möchten oder nicht. Zudem haben Sie die Möglichkeit, vorhandene Boards (außer dem Subprojekt-Board) sowie die **Projektübersicht** in Ihr neues Projekt zu übernehmen.

![Kopieren](media/153bd21eab2d0600e1a07812e06d481708722292.jpg "Kopieren")

**Hinweis**: dass **Budgets** nicht kopiert werden können. Daher sollten Sie diese zuvor aus der Arbeitspaket-Tabelle entfernen. Alternativ können Sie die Budgets auch im Budget-Modul löschen, was gleichzeitig deren Entfernung aus den Arbeitspaketen bewirkt.

Für weitere Anpassungen stehen Ihnen die **erweiterten Einstellungen** zur Verfügung. Hier haben Sie die Möglichkeit, verschiedene Aspekte festzulegen, darunter die URL (Kennung) des Projekts, seine Sichtbarkeit und seinen Status. Zusätzlich können Sie Werte für benutzerdefinierte Felder definieren (im Screenshot nicht dargestellt).

![Projekt Kopieren](media/aabce3ee6a118e2e36c0dce296afb5844f6edac5.png "Projekt Kopieren")

## Projekt archivieren

Wenn Sie ein Projekt archivieren möchten, begeben Sie sich zu den **Projektkonfiguration** und betätigen Sie **Archivieren**.

**Hinweis:** Diese Option steht immer für Mitglieder der Instanz- und Projektadministration zur Verfügung. Unter Umständen kann sie auch für bestimmte Rollen aktiviert werden, indem die Berechtigung zur Projektarchivierung über die **Einstellungen für Rollen und Berechtigungen** in den Administrations-Einstellungen aktiviert wird.

![Archivieren](media/05b5b570563f661ce307cb8b32a4090c42d874e0.jpg "Archivieren")

Nachdem dies getan ist, wird das Projekt nicht mehr in der Liste der auswählbaren Projekte angezeigt. Es bleibt jedoch weiterhin im **Alle Projekte anzeigen** Dashboard verfügbar, sofern Sie den **Aktiv** Filter auf **aus** setzen, indem Sie den Schieberegler nach links verschieben. Hier haben Sie die Möglichkeit, die Archivierung rückgängig zu machen, indem Sie auf die drei Punkte am rechten Ende einer Zeile klicken und **Entarchivieren** auswählen.

![Aktiv](media/187282835f50c13a017dbdaa5b09d22bd67beff9.jpg "Aktiv")

## Ein Projekt löschen

Falls Sie ein Projekt löschen möchten, begeben Sie sich bitte zur **Projektkonfiguration** und betätigen Sie **Löschen**. Dies befindet sich oben rechts auf der Seite.

![Löschen](media/54d901a522c0133af391cb3e1b1df8a4638ebd14.jpg "Löschen")

Es besteht auch die Möglichkeit, ein Projekt über die Projektübersicht zu entfernen.

**Hinweis**: Das Löschen von Projekten ist ausschließlich Systemadministratoren vorbehalten.
