# Projektstart oder -durchführung

Verwalten Sie alle Projektaktivitäten der Arbeitspakete wie Aufgaben, Ergebnisse, Risiken, Funktionen, Bugs und Change-Requests. Verwenden Sie agile Boards mit Ihren Teams, dokumentieren Sie Besprechungen und teilen Sie Nachrichten.

## Über das Modul Projekte

Projekte ist eine kostenfreie Open-Source-Software für klassisches und agiles Projektmanagement, die Ihr Team während eines gesamten Projektverlaufs unterstützt. Die Anwendung ist in mehr als 30 Sprachen verfügbar.

## Die ersten Schritte

Bevor Sie mit **Projekte** beginnen, befolgen Sie bitte die folgenden Schritte:

1. Melden Sie sich mit Ihren Zugangsdaten an.
1. Legen Sie ein neues Projekt an.
1. Laden Sie Mitglieder ein, um gemeinsam an dem Projekt zu arbeiten.
1. Erstellen Sie neue Arbeitspakete, um die Aufgaben im Projekt zu strukturieren.
1. Richten Sie einen Projektplan ein, um die zeitliche Abfolge und Ressourcen für die Arbeitspakete zu planen.

### Hier geht es weiter mit:

- [Arbeitspakete erstellen](projektstart-oder-durchfuehrung/arbeitspakete-erstellen.md)
- [Boards](projektstart-oder-durchfuehrung/boards.md)
- [Besprechungen](projektstart-oder-durchfuehrung/besprechungen.md)
- [Neuigkeiten](projektstart-oder-durchfuehrung/neuigkeiten.md)
- [Wiki](projektstart-oder-durchfuehrung/wiki.md)
