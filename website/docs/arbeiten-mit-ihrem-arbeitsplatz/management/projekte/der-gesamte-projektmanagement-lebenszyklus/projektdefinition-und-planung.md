# Projektdefinition und -planung

Erstellen Sie eine Projektübersicht mit detaillierteren Informationen, richten Sie Ihren Projektplan ein, strukturieren Sie Ihre Arbeit, erstellen Sie eine Roadmap.

### Hier geht es weiter mit:

- [Übersicht der globalen Projekte](projektdefinition-und-planung/uebersicht-der-globalen-projekte.md)
- [Strukturierung der Arbeit](projektdefinition-und-planung/strukturierung-der-arbeit.md)
- [Roadmap-Planung - Einführung in Gantt-Diagramme](projektdefinition-und-planung/roadmap-planung-einfuehrung-in-gantt-diagramme.md)
