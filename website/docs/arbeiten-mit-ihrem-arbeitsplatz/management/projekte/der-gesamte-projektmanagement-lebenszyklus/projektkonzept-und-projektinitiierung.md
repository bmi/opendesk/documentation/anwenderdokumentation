# Projektkonzept und Projektinitiierung

| Funktionen                 | Dokumentation für                                                          |
| -------------------------- | -------------------------------------------------------------------------- |
| Erstellen eines Projekts   | Erstellen und Einrichten eines neuen Projekts                              |
| Projektstruktur einrichten | Erstellen Sie eine Hierarchie, um Ihre Arbeit in Projekte zu strukturieren |
| Projekteinstellungen       | Erstellen von ersten Ideen, Aufgaben und Meilensteinen                     |
| Mitglieder hinzufügen      | Teameinladungen verschicken, um gemeinsam an Projekten zu arbeiten         |

### Hier geht es weiter mit:

- [Projekt erstellen](projektkonzept-und-projektinitiierung/projekt-erstellen.md)
- [Einrichten einer Projektstruktur](projektkonzept-und-projektinitiierung/einrichten-einer-projektstruktur.md)
- [Projekt-Einstellungen](projektkonzept-und-projektinitiierung/projekt-einstellungen.md)
- [Mitglieder hinzufügen](projektkonzept-und-projektinitiierung/mitglieder-hinzufuegen.md)
