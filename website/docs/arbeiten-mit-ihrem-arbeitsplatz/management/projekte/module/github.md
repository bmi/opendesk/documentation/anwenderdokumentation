# GitHub

## Github-Integration

**Projekte** stellt eine Integration mit GitHub Pull Requests (Arbeitsablauf) bereit, um Ihnen die Möglichkeit zu bieten die Softwareentwicklung eng mit der Planung zu verknüpfen. Sie können einen Pull Request in GitHub erstellen und ihn mit einem Arbeitspaket integrieren.

## Überblick

Die Arbeitspakete zeigen die Informationen direkt aus GitHub in einem separaten Tab an.

![Github Überblick](media/9a2fa5e52d7bbbe9d6cf0c91eb4229426dd99fea.png "Github Überblick")

Unter **GITHUB** können Pull Requests zu einem Arbeitspaket hinzugefügt werden. Sie können dann die vernüpften Pull Requests inklusive Status (**Offen** oder **Gemerged**) sowie den Stand (**Warteschlange** oder **erfolgreich**) einsehen. Ein Arbeitspaket kann mit mehreren Pull Requests verknüpft sein.

Außerdem können Sie innerhalb von Arbeitspaketen Branches erstellen und daraus entsprechende Pull Requests erstellen.

![Branches in Github](media/dbbe1ce8d5d990a950478d853d0ef369a49c1e5c.png "Branches in Github")

Sie können sich Aktivitäten zu Pull Requests auch im Tab **Aktivitäten** anzeigen lassen. Pull Requests können
- first referenced (usually when opened)
- merged

closed  sein.

![Aktivitäten in Github](media/41ee5e898b307e864a938e6ce9a1ea84984dbd65.png "Aktivitäten in Github")

## Einen Pull Request erstellen

Pull Requests basieren auf Branches, deshalb muss zuerst ein neuer Branch erstellt werden. Klicken Sie dazu auf **GitHub** in Ihrer Arbeitspaket-Detailansicht. Wählen Sie **Git snippets**, um das Menü zu erweitern. Kopieren Sie zuerst den Branch-Namen, indem Sie auf das Kopieren-Symbol klicken.

![Branch erstellen](media/b40bdbf98d9eb47b9ce6617fd9a0d4b65fcf764e.png "Branch erstellen")

Anschließend öffnen Sie Ihr Software-Programm oder eine Konsole. Dort erstellen Sie Ihren Branch. Alle Informationen zu dem neuen Branch können Sie aus Ihrem Arbeitspaket in **Projekte** entnehmen.

Mit dem neu erstellten Branch können Sie in einem von Ihnen ausgesuchten Software-Programm mit der Entwicklung beginnen.

Sobald Sie mit den Änderungen zufrieden sind, erstellen Sie einen Commit (Meldung über Änderungen des Entwicklers). Die Schaltfläche **Git-Snippet** erstellt automatisch eine Commit-Nachricht für Sie, welche Sie aber ändern können, wenn diese Ihnen nicht gefällt. Die Commit-Nachricht basiert auf der URL des Pull Requests.

![Commit-Nachricht Github](media/121b355e7eb3da6e02a9338c0158c54446f314f6.png "Commit-Nachricht Github")

Sie haben außerdem die Möglichkeit über **Git-Snippet** die Option **Branch mit leerem Commit erstellen** zu wählen. So hat man die Möglichkeit einen Branch zu öffnen und mit einem Befehl einen leeren Commit zu erstellen. Hierbei wird der Branch direkt mit dem Arbeitspaket verknüpft.

![Erstellen eines leeren Branch](media/397a80e16ed78e654af13e9b6b1470ac0e9f7583.png "Erstellen eines leeren Branch")

Haben Sie den Pull Request in Ihrem Software-Programm erstellt, so erscheint dieser unter **Aktivität**.

![Erstellung Pull Request](media/1a58ef12640a01c33e44d4aec6e29048a21becec.png "Erstellung Pull Request")

## Konfiguration

Für eine funktionierende Integration müssen **Projekte** und **GitHub** konfiguriert werden.

## Projekte

Erstellen Sie in **Projekte** zuerst einen Benutzer. Es ist notwendig, dass dieser Benutzer zu jedem Projekt hinzugefügt wird und eine Rolle erhält, die es ihm gewährt, Arbeitspakete zu sehen und zu ändern. Stellen Sie sicher, dass die Rolle vorher erstellt wurde. Klicken Sie dafür rechts oben auf Ihr Profilbild und anschließend auf **Administration**. Dort können Sie nun eine neue Rolle erstellen. Die zugewiesene Rolle sollte über die Berechtigungen **Arbeitspakete anzeigen** und **Notizen hinzufügen** verfügen.

Um einen neuen Benutzer zu erstellen, klicken Sie rechts oben auf Ihr Profilbild und anschließend auf **Administration**. Sie werden nun auf die Administrationsseite weitergeleitet. Dort klicken Sie auf **Benutzer und Berechtigungen**.

![Administration Projekte](media/0ed8ebcea1403c26b56e88d2dd775a883d871170.png "Administration Projekte")

![Benutzer und Berechtigungen](media/738ab292220b32f722d8fa1ec5065c24d5b6cb79.png "Benutzer und Berechtigungen")

Sie befinden sich nun auf der Seite mit der Überschrift **Benutzer.** Klicken Sie auf **+Benutzer,** um einen neuen Benutzer zu erstellen.

![Neuen Benutzer erstellen](media/86aa134b5b78b9a0a0d3b04a08f50a44edf9cc3b.png "Neuen Benutzer erstellen")

![Neuer Benutzer](media/3a8a5c245553192631b70591ce6840c0d7202a73.png "Neuer Benutzer")

Klicken Sie unten auf **Anlegen** oder **Anlegen und Weiter**, um den Benutzer zu speichern.

![Anlegen eines Benutzers](media/274921dfe9ffe4bb4ac2eb575abebb641986b032.png "Anlegen eines Benutzers")

Nachdem Sie den Benutzer erstellt haben, müssen Sie für diesen einen API-Token in **Projekte** generieren, um den Benutzer später auf der GitHub-Seite zu integrieren. Hierzu folgen Sie diesen Schritten:

1. Melden Sie sich als der neu erstellte Benutzer an.
1. Navigieren Sie zu **Mein Konto** (klicken Sie auf Ihr Profilbild in der oberen rechten Ecke).
1. Gehen Sie zu **Zugriffstoken**.
1. Klicken Sie auf **Generieren** in der Zeile für die API.

Kopieren Sie den generierten Schlüssel. Anschließend können Sie die erforderliche Webhook-Konfiguration in GitHub vornehmen.  Zusätzlich müssen Sie unter **Projektkonfigurationen** und unter **Module** **GitHub** aktivieren, damit alle Informationen, die von GitHub übertragen werden, in den Arbeitspaketen angezeigt werden.

![Modulübersicht, Github aktivieren](media/ea404ae4b1fcc5197793d9b478400b155be3163b.png "Modulübersicht, Github aktivieren")

## Github

In **GitHub** müssen Sie für die Verbindung mit **Projekte** in jedem einzelnen Repository einen Webhook konfigurieren.

Im Webhook müssen zwei Konfigurationsschritte ausgeführt werden. Der **Content Type** sollte auf **application/json** festgelegt sein, und die **Payload URL** muss auf den GitHub-Webhook-Endpunkt Ihres Projekte-Servers verweisen (/webhooks/github)

**Hinweis**: Für die Ereignisse, die durch den Webhook ausgelöst werden sollen, wählen Sie bitte **Alles senden** (**Send me everything**).

Jetzt benötigen Sie den zuvor kopierten API-Schlüssel. Fügen Sie ihn als einfachen GET-Parameter mit dem Namen **key** an die Payload-URL an. Die endgültige URL sollte in etwa so aussehen:

**https://myopenproject.com/webhooks/github?key=42**

Nun ist die Integration auf **Github** und **Projekte** eingerichtet, und Sie können damit arbeiten.