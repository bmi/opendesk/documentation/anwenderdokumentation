# Arbeitspakete

**Arbeitspakete** sind Teile in einem Projekt. Ein Arbeitspaket enthält wichtige Informationen. Arbeitspakete können Projektmitgliedern zur Bearbeitung zugewiesen werden.

Alle Arbeitspakete haben einen **Typ**, eine **ID** und einen **Betreff**. Sie können verschiedene zusätzliche Attribute haben, zum Beispiel **Status**, **Zugewiesen**, **Priorität** oder **Fälligkeitsdatum**.

Für Arbeitspakete stehen drei verschiedene **Typen** zur Auswahl: **Aufgaben**, **Meilensteine** und **Phasen**.

Sprints und andere Backlogs, die Sie im Modul **Backlogs** anlegen, erscheinen ebenfalls im Arbeitspakete-Modul.

### Hier geht es weiter mit:
- [Das Arbeitspakete-Modul aktivieren und aufrufen](arbeitspakete/das-arbeitspakete-modul-aktivieren-und-aufrufen.md)
- [Arbeitspaket-Ansichten](arbeitspakete/arbeitspaket-ansichten.md)
- [Arbeitspakete erstellen](arbeitspakete/arbeitspakete-erstellen.md)
- [Arbeitspakete bearbeiten](arbeitspakete/arbeitspakete-bearbeiten.md)
- [Daten und Dauer festlegen und ändern](arbeitspakete/daten-und-dauer-festlegen-und-aendern.md)
- [Arbeitspakete kopieren, verschieben, löschen](arbeitspakete/arbeitspakete-kopieren-verschieben-loeschen.md)
- [Arbeitspaket-Tabelle konfigurieren](arbeitspakete/arbeitspaket-tabelle-konfigurieren.md)
- [Arbeitspaket-Beziehungen und Hierarchien](arbeitspakete/arbeitspaket-beziehungen-und-hierarchien.md)
