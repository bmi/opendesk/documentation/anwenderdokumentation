# Arbeitspaket-Beziehungen und Hierarchien

Es besteht die Möglichkeit Arbeitspaket-Beziehungen sowie -Hierarchien zu erstellen.

Beziehungen umfassen dabei funktionale oder zeitliche Verknüpfungen, wie beispielsweise **folgt** oder **Vorgänger von**, **blockiert durch**, **Teil von**, usw. Hierarchien stellen eine hierarchische Beziehung dar, wobei es eine übergeordnete und untergeordnete Struktur von Arbeitspaketen gibt.

## Arbeitspaket-Beziehungen

Arbeitspaketbeziehungen signalisieren, dass die betreffenden Arbeitspakete ein gemeinsames Thema behandeln oder von Statusabhängigkeiten betroffen sind. Um eine Verbindung zwischen zwei Arbeitspaketen herzustellen, folgen Sie diesen Schritten:

1. Wählen Sie ein Arbeitspaket aus und öffnen Sie den Reiter für Beziehungen
1. Klicken Sie auf **+ Neue Beziehung erstellen**
1. Wählen Sie die Art der Beziehung aus dem Dropdown-Menü
1. Geben Sie die ID oder den Namen des Arbeitspakets ein, zu dem die Beziehung hergestellt werden soll, und wählen Sie einen Eintrag aus dem Dropdown-Menü
1. Drücken Sie die Eingabetaste

![Arbeitspaket-Beziehung erstellen](media/50f3791022da02d4597187250f60f077ad276f3c.png "Arbeitspaket-Beziehung erstellen")

Sie haben die Möglichkeit, eine der folgenden Beziehungen aus dem Dropdown-Menü auszuwählen:

- **Verwandt mit:** Diese Option erstellt einen Link von Arbeitspaket A zu Arbeitspaket B. Dadurch können die Projektmitglieder die Verbindung sofort erkennen, auch wenn die Arbeitspakete nicht in derselben Hierarchie stehen.
- **Dupliziert / Dupliziert durch:** Diese Option gibt an, dass Arbeitspaket B Arbeitspaket A auf irgendeine Weise dupliziert, zum Beispiel, wenn beide dieselbe Aufgabe behandeln. Dies ist nützlich, wenn ein Arbeitspaket sowohl Teil eines geschlossenen als auch eines öffentlichen Projekts sein muss. Die Verbindung ist in diesem Fall nur semantisch, und Änderungen in Arbeitspaket A müssen manuell in Arbeitspaket B angepasst werden. Beachten Sie, dass einige Statusänderungen automatisch für das duplizierte Arbeitspaket übernommen werden.
- **Blockiert / Blockiert durch:** Diese Option legt Statusänderungsbeschränkungen zwischen zwei Arbeitspaketen fest. Wenn Arbeitspaket A Arbeitspaket B blockiert, kann der Status von B nicht auf "geschlossen" oder "gelöst" gesetzt werden, solange A nicht geschlossen ist.
- **Vorgänger / Folgt:** Diese Option definiert eine chronologische Beziehung zwischen zwei Arbeitspaketen. Zum Beispiel muss das Startdatum von Arbeitspaket B mindestens einen Tag nach dem Enddatum von A liegen, wenn A vor B geplant ist.
- **Enthält / Teil von:** Diese Option definiert, ob Arbeitspaket A Teil von Arbeitspaket B ist oder es beinhaltet. Dies kann zum Beispiel verwendet werden, um ein Roll-out-Arbeitspaket und die dazugehörigen Arbeitspakete ohne hierarchische Beziehungen anzuzeigen.
- **Benötigt / Benötigt von:** Diese Option definiert, ob Arbeitspaket A das Arbeitspaket B benötigt oder von ihm benötigt wird. Es hat keinen zusätzlichen Effekt.

Der ausgewählte Beziehungsstatus wird automatisch im Arbeitspaket angezeigt, das Sie für die Beziehung auswählen. Wenn Sie z.B. im aktuellen Arbeitspaket A die Option **Verwandt mit** auswählen und Arbeitspaket B angeben, wird in Arbeitspaket B automatisch angezeigt, dass es mit A **verwandt ist**.

![Auswahl der Arbeitspaket-Beziehungen](media/5b1a859c85d5548b0e124449794cc0af1023b72f.png "Auswahl der Arbeitspaket-Beziehungen")

## Zugehöriges Arbeitspaket hinzufügen

Wenn bei einem Arbeitspaket die Beziehung **Vorgänger/Folgt** ausgewählt wurde, dann ist das die einzige Beziehung innerhalb dieses Moduls, welches die Daten von Arbeitspaketen einschränken oder beeinflussen kann.

Arbeitspakete, bei denen **Vorgänger/Folgt** ausgewählt wurde, müssen nicht immer aufeinander folgen, es können auch Arbeitspakete dazwischen liegen. Diese hängen also nicht miteinander zusammen und können unabhängig voneinander bearbeitet werden.

Möchte man das Anfangs- bzw. Enddatum eines Arbeitspakets ändern, muss man darauf achten, dass das Datum des Anfangstickets zuerst bearbeitet, damit man die nachfolgenden Tickets anpassen kann. Hierbei wird das Nachfolge-Ticket so in die Zukunft verschoben, dass er am Tag nach den neuen Enddatums des Vorgängertickets beginnt.

## Beziehungen in der Arbeitspaket-Tabelle anzeigen

In der Enterprise On-Premises Edition oder der Enterprise Cloud Edition können Sie als Nutzer:in Beziehungen als Spalten in der Arbeitspaket-Tabelle anzeigen lassen.

Diese Funktion ist hilfreich, wenn Sie einen Überblick über bestimmte Arten von Beziehungen zwischen Arbeitspaketen erhalten möchten, beispielsweise, um zu sehen, welche Arbeitspakete andere Arbeitspakete blockieren.

Um Beziehungs-Spalten hinzuzufügen, gehen Sie zur oberen rechten Ecke der Arbeitspaket-Tabelle und klicken Sie auf das Dreipunkt-Menü. Wählen Sie **Spalten einfügen** und geben Sie den Namen der Beziehung in die Suchleiste ein. Wählen Sie dann die gewünschte Beziehung aus und klicken Sie auf **Anwenden**.

Die hinzugefügte Spalte zeigt alle Beziehungen dieser Art für jedes Arbeitspaket an, sofern sie gesetzt wurde (z.B. **blockiert durch**).

Klicken Sie auf die in der Beziehungs-Spalte angezeigte Nummer, um das entsprechende Arbeitspaket zu öffnen.

## Arbeitspakethierarchie

Die Organisation von Arbeitspaketen in einer Hierarchie ermöglicht es, ein umfangreiches Arbeitspaket in mehrere kleinere Aufgaben zu unterteilen. Dies impliziert das Vorhandensein eines übergeordneten Arbeitspakets, das mindestens ein untergeordnetes Arbeitspaket enthält.

## Ein untergeordnetes Arbeitspaket hinzufügen

Es gibt drei Möglichkeiten, ein untergeordnetes Arbeitspaket festzulegen:

Durch das Hinzufügen oder Erstellen eines untergeordneten Arbeitspakets im Tab **Beziehungen** in der Detailansicht eines Arbeitspakets.

- Durch Rechtsklick auf ein Arbeitspaket in der Arbeitspaket-Tabelle und Auswahl von **Neue Unteraufgabe**.
- Durch Rechtsklick auf ein Arbeitspaket in der Arbeitspaket-Tabelle und Auswahl von **In Hierarchie einrücken**.

## Hinzufügen oder Erstellen eines untergeordneten Arbeitspaketes im Beziehungen-Reiter in der Detailansicht eines Arbeitspakets

Um ein untergeordnetes Arbeitspaket zu erstellen, öffnen Sie ein Arbeitspaket und wechseln Sie zum Reiter "Beziehungen". Klicken Sie auf **+ Neue Unteraufgabe erstellen**, um ein neues untergeordnetes Arbeitspaket zu erstellen. Alternativ dazu können Sie auch ein bereits bestehendes untergeordnetes Arbeitspaket neu zuordnen, indem Sie die Option **+ Unteraufgabe zuweisen** nutzen.

![Untergeordnete Aufgabe erstellen](media/66daed94513c6420562259b3a3d5bf6715db8b24.png "Untergeordnete Aufgabe erstellen")

Geben Sie den Namen des entsprechenden Arbeitspakets ein und speichern Sie es als neues untergeordnetes Arbeitspaket, indem Sie auf Enter klicken. Falls gewünscht, können Sie auch Änderungen am Arbeitspaket vornehmen, indem Sie auf die ID des Arbeitspakets klicken.

![Neue untergeordnete Aufgabe hinzufügen](media/0fde12dbc713aff859e4ff15c49b65fb0e2749a5.png "Neue untergeordnete Aufgabe hinzufügen")

## Übergeordnetes Arbeitspaket ändern oder löschen

Sie können ein übergeordnetes Arbeitspaket bearbeiten oder entfernen. Öffnen Sie dazu das betreffende Arbeitspaket und klicken Sie in der oberen Detailansicht auf **Übergeordnetes Arbeitspaket ändern (Stift-Symbol)** oder **Übergeordnetes Arbeitspaket entfernen (X)**.

![Übergeordnete Arbeitspakete ändern oder löschen](media/c1ff60c45a6b0e69ced95f047c6a76c66f0ad7cb.png "Übergeordnete Arbeitspakete ändern oder löschen")

## Arbeitspaket-Hierarchien anzeigen

Nachdem Sie ein untergeordnetes Arbeitspaket hinzugefügt haben, wird dies im Reiter **Beziehungen** aufgeführt. Die übergeordneten Arbeitspakete können Sie sehen, indem Sie in der oberen Detailansicht, überhalb des Arbeitspakettitels schauen.

![Untergeordnetes Arbeitspaket](media/6a1df46975ea23ab57ca373eb5feffa130f47f83.png "Untergeordnetes Arbeitspaket")

![Übergeordnetes Arbeitspaket](media/39463ee837edb808bbf886c35c5692c0ec0706fb.png "Übergeordnetes Arbeitspaket")

Hierarchien können ebenfalls in der Arbeitspaket-Tabelle angezeigt werden. Gehen Sie aus der Detailansicht raus, zurück in die Arbeitspaket-Tabelle und klicken Sie das Symbol neben **Thema.** Dort können Sie die Hierarchie aktivieren oder deaktivieren.

![Arbeitspaket-Tabelle_ Thema aktivieren oder deaktivieren](media/007d3798eaa126177108baed62d1b2a4201fa663.png "Arbeitspaket-Tabelle_ Thema aktivieren oder deaktivieren")

Um Informationen zu übergeordneten Arbeitspaketen anzuzeigen, können Sie eine entsprechende Spalte hinzufügen:

1. Öffnen Sie die Konfiguration der Arbeitspaket-Tabelle, indem Sie auf das Zahnrad in der oberen rechten Ecke klicken
1. Wählen Sie im Dropdown-Menü die Option **Übergeordnetes Arbeitspaket** aus und bestätigen Sie mit **Anwenden**  
1. Jetzt wird eine Spalte mit Informationen zu übergeordneten Beziehungen in der Arbeitspaket-Tabelle angezeigt und Sie können diese einsehen.

![Konfiguration der Arbeitspaket-Tabelle](media/0be73a2bbac748b70baa40d99d1aed761306c878.png "Konfiguration der Arbeitspaket-Tabelle")

![Konfiguration der Arbeitspaket-Tabelle](media/da7e86c06ecdfb13e12424054422b7ec700349d8.png "Konfiguration der Arbeitspaket-Tabelle")
