# Arbeitspaket-Tabelle konfigurieren

Sie können im Modul **Projekte** die Ansicht der Arbeitspaket-Tabelle nach Ihren Bedürfnissen konfigurieren. Sie können die Kopfzeile der Tabelle anpassen, indem Sie Spalten hinzufügen oder löschen, Arbeitspakete filtern, gruppieren oder nach bestimmten Kriterien sortieren. Es besteht außerdem die Option, zwischen einer flachen Listenansicht, einer Hierarchieansicht und einer gruppierten Ansicht zu wechseln.

Die konfigurierte Ansicht kann gespeichert werden, um sie direkt im Projektmenü verfügbar zu machen. In der Arbeitspaket-Ansicht sehen Sie die Gesamtheit aller vorgenommenen Änderungen an der Standardliste, wie beispielsweise angewendete Filter.

Um die Konfiguration der Arbeitspaket-Tabelle vorzunehmen, öffnen Sie bitte die **Einstellungen**, indem Sie auf das Drei-Punkte-Menü oben rechts in der Arbeitspaket-Tabelle klicken. Wählen Sie dann **Ansicht konfigurieren** aus.

![Arbeitspaket-Tabellen konfigurieren](media/795c9977df572f1e39b4977547a4221fc4a1476b.png "Arbeitspaket-Tabellen konfigurieren")

## Entfernen oder Hinzufügen von Spalten in der Arbeitspaket-Tabelle

Ändern Sie die Darstellung Ihrer Arbeitspaket-Tabelle und passen Sie verschiedene Attribute in der Liste an. Hierfür können Sie Spalten hinzufügen oder entfernen. Führen Sie folgende Schritte aus:

1. Klicken Sie oben rechts auf **Ansicht konfigurieren** in der Arbeitspaket-Tabelle.
1. Verwenden Sie die Suchleiste, um das gewünschte Attribut zu finden, das Sie in der Tabelle anzeigen möchten und wählen Sie es aus.
1. Fügen Sie Spalten hinzu, indem Sie den Namen des Attributs in die Suchleiste eingeben und es auswählen.
1. Entfernen Sie Spalten, indem Sie auf das **Entfernen-Symbol (x)** klicken.
1. Nutzen Sie Drag-and-Drop, um die Reihenfolge der Attribute in der Tabelle nach Ihren Wünschen neu anzuordnen.

![Spalten hinzufügen in der Arbeitspaket-Tabelle ](media/fef1cea69564a7412f89ac970512424b598fa36f.png "Spalten hinzufügen in der Arbeitspaket-Tabelle ")

Klicken Sie anschließend auf **Anwenden**, um Ihre Anpassungen zu speichern.

## Arbeitspakete filtern

Um die Arbeitspakete in der Tabelle zu filtern, klicken Sie auf **Filter** oben in der Arbeitspaketansicht. Die Zahl neben dem Filter-Button gibt an, wie viele Filterkriterien angewendet wurden.

Um ein Filterkriterium hinzuzufügen, können Sie ein Attribut aus dem Dropdown-Menü neben **+ Filter hinzufügen** auswählen. Alternativ können Sie auch einen Begriff in die Suchleiste eingeben, um nach einem Kriterium zu suchen und es auszuwählen.

Es besteht die Möglichkeit eine beliebige Anzahl von Filterkriterien hinzuzufügen. Des Weiteren können Sie nach selbstdefinierten Feldern filtern, sofern dies in der Administration entsprechend konfiguriert wurde.

![Einen Filter setzen](media/0e1c9e0f5a9463d32e1fce5012ef8c6208ef642e.png "Einen Filter setzen")

## Filter Operatoren

Unterschiedliche Attribute bieten vielfältige Filterkriterien. Bei den meisten Auswahlattributen, wie beispielsweise **Zugewiesen an**, stehen die folgenden Filtermöglichkeiten zur Verfügung:

- **ist (ODER)**: Listet alle Arbeitspakete auf, die einem der eingegebenen Werte entsprechen.
- **ist nicht**: Listet alle Arbeitspakete auf, die keinem der eingegebenen Werte entsprechen.
- **ist nicht leer**: Listet alle Arbeitspakete auf, für die das Attribut einen Wert hat.
- **ist leer**: Listet alle Arbeitspakete auf, für die das Attribut keinen Wert hat. 

 Multi-Select-Attribute bieten zusätzlich die Option:

- **ist (UND)**: Listet alle Arbeitspakete auf, die allen der eingegebenen Werte entsprechen.

Andere Attribute wie **Status** können weitere Kriterien wie **offen** oder **geschlossen** aufweisen. Erforderliche Attribute bieten in der Regel nur zwei Optionen: **ist (ODER)** und **ist nicht**, da sie nicht leer bleiben können.

## Nach Text filtern

Um nach spezifischen Textelementen im Titel, der Beschreibung oder den Kommentaren eines Arbeitspakets zu suchen, geben Sie ein gewünschtes Wort im Bereich **Nach Text filtern** ein.

![Nach einem Text filtern](media/801b4085a597ab68e6750945d2b7664e0570d537.png "Nach einem Text filtern")

## Nach untergeordneten Arbeitspaketen filtern

Sie können nach untergeordneten Arbeitspaketen filtern und diese anzeigen lassen. Beispielsweise können Sie alle Arbeitspakete, die zu einer bestimmten Zeit in der Bearbeitung zu Ihrem Projekt gehörten, einsehen. Verwenden Sie hierbei bitte den Filter **untergeordneten Arbeitspaket**.

## Arbeitspakete von einem bestimmten Projekt oder Unterprojekt einbeziehen

Sie haben die Möglichkeit, Arbeitspakete aus mehreren Projekten anzuzeigen. Um Arbeitspakete einzubeziehen, klicken Sie auf **Enthaltene Projekte**. Hier können Sie die relevanten Projekte und Unterprojekte auswählen oder abwählen. Wenn Sie alle Unterprojekte mit einbeziehen möchten, setzen Sie einen Haken bei **Alle Unterprojekte einbeziehen** ganz unten im Dropdown-Menü.

![Option: Alle Projekte miteinbeziehen](media/3d05f3b38445899b1059f8ae2455a7cb196259ec.png "Option: Alle Projekte miteinbeziehen")

## Arbeitspakete nach Arbeitspaket-ID filtern

Möchten Sie eine Ansicht für bestimmte Arbeitspakete erstellen, können Sie die Tabelle nach der **Arbeitspaket-ID filtern**. Hierfür wählen Sie bitte im Dropdown-Menü neben +Filter hinzufügen **ID** aus und geben Sie dazu die ID oder den Namen der gewünschten Arbeitspakete ein. Sie können außerdem noch einen Filter-Operator dazu auswählen, um den Filter noch spezifischer zu gestalten.

## Filtern nach zugewiesenen Mitgliedern oder Gruppen

Es stehen verschiedene Filteroptionen zur Verfügung, um nach der zugewiesenen Person eines Arbeitspakets zu filtern. Sie können einen der folgenden Filter auswählen:

- **Zugewiesen an**: Filtert Arbeitspakete, bei denen die angegebene Person oder Gruppe als beauftragt festgelegt ist.
- **Zugewiesen an Person oder deren Gruppe**:
    - Wenn für eine einzelne Person gefiltert wird: Arbeitspakete, die dieser Person zugewiesen sind, sowie alle Gruppen, zu denen sie gehört.
    - Wenn für eine Gruppe gefiltert wird: Arbeitspakete, die dieser Gruppe zugewiesen sind, sowie allen Personen innerhalb der Gruppe.
- **Gruppe der zugewiesenen Person**: Filtert Arbeitspakete, die einer Person aus dieser Gruppe zugewiesen sind.
- **Rolle der zugewiesenen Person**: Filtert Arbeitspakete, die Personen mit der angegebenen Projektrolle zugewiesen sind.

![Nach zugewiesener Person filtern](media/b5a3e47b898df4cb5057b34980a6059facc0b587.png "Nach zugewiesener Person filtern")

## Nach Dateinamen und Inhalt des Anhangs filtern

Durch Verwendung eines erweiterten Filters haben Sie die Möglichkeit, eine Volltextsuche durchzuführen. Hierbei können Sie nach Dateinamen oder dem Inhalt von angehängten Dokumenten in Arbeitspaketen filtern.

Nutzen Sie die Filteroptionen **Inhalt des Anhangs** oder **Dateiname des Anhangs** , um angehängte Dokumente in der Arbeitspaket-Tabelle zu durchsuchen und zu filtern.

Für den Dateinamen und den Inhalt können Sie die Filteroptionen **enthält** und **enthält nicht** verwenden, um ausgewählte Schlüsselwörter und Textabschnitte zu unterscheiden. Geben Sie hierfür den entsprechenden Text in das benachbarte Feld ein.

![Nach angehängten Dateien filtern](media/e22334d33601a4e0e9cbdbf831d580c35ad0d114.png "Nach angehängten Dateien filtern")

## Arbeitspaket-Tabelle sortieren

**Automatische Sortierung der Arbeitspaket-Tabelle**

Die Standard-Sortierreihenfolge für die Arbeitspaket-Tabelle ist nach der Arbeitspaket-ID voreingestellt.

Um die Arbeitspaketlistenansicht zu sortieren, klicken Sie auf das Dreipunkte-Menü und öffnen **Ansicht konfigurieren.** Wählen Sie in der Konfiguration den Reiter **Sortieren nach**. Sie können aufsteigend oder absteigend nach bis zu drei Attbritbute sortieren. Klicken Sie anschließend auf **Anwenden**, um die Änderungen zu speichern.

![Arbeitstabelle sortieren](media/c4864d31be8017605182a566e1e02eda2682d868.png "Arbeitstabelle sortieren")

**Hinweis**: Falls Sie den Hierarchie-Modus aktiviert haben, werden alle gefilterten Tabellenergebnisse um ihre übergeordneten Elemente erweitert. Hierarchien können je nach Bedarf erweitert oder eingeklappt werden.

## Manuelle Sortierung der Arbeitspaket-Tabelle

Sie haben die Möglichkeit, die Tabelle der Arbeitspakete manuell zu sortieren. Sie verwenden dafür das Symbol mit den sechs Punkten auf der linken Seite jedes Arbeitspakets und können es per **Drag-and-Drop** verschieben.

## Einfache Liste, Hierarchie-Modus und gruppierte Ansicht der Arbeitspaket-Tabelle

Es stehen drei verschiedene Möglichkeiten zur Verfügung, um Ergebnisse in der Arbeitspaket-Tabelle anzuzeigen:

- **Eine einfache Liste**: Diese enthält alle Arbeitspakete in einer Liste, unabhängig von ihren Beziehungen zu untergeordneten oder übergeordneten Arbeitspaketen.
- **Hierarchie**: Diese Ansicht zeigt die gefilterten Ergebnisse innerhalb der Eltern-Kind-Beziehungen an.
- **Gruppieren nach**: Hierbei wird die Arbeitspaket-Tabelle nach einem bestimmten Attribut gruppiert.

Um die gewünschte Anzeigeoption auszuwählen, klicken Sie auf **Ansicht konfigurieren** und wechseln Sie zum Reiter **Anzeigeeinstellungen**. Wählen Sie die bevorzugte Darstellungsform der Arbeitspaket-Tabelle aus und klicken Sie anschließend auf **Anwenden**.

![Anzeigeeinstellungen anpassen](media/ebc01754e514297cf143314bd773f61128282c99.png "Anzeigeeinstellungen anpassen")

## Summierungen in der Arbeitspaket-Tabelle anzeigen

Um die Summen der relevanten Arbeitspaketattribute anzuzeigen, klicken Sie auf **Ansicht konfigirieren** und öffnen Sie den Reiter **Anzeigeeinstellungen** (wie im obigen Screenshot dargestellt). Setzen Sie ein Häkchen neben **Summen anzeigen**, um die Summen der geschätzten Zeit, der verbleibenden Stunden der Arbeitspaket-Tabelle anzuzeigen. Wenn Sie die Arbeitspaket-Tabelle gruppieren, werden die Summen für jede Gruppe entsprechend angezeigt.

## Ansichten der Arbeitspaket-Tabellen speichern

Nachdem Sie Ihre Arbeitspaket-Tabelle konfiguriert haben, können Sie die Ansichten speichern. Folgende Schritte sind hierbei zu beachten:

1. Klicken Sie auf das **Drei-Punkte-Menü** oben rechts in der Arbeitspaket-Tabelle, um die Einstellungen zu öffnen.
1. Wählen Sie **Speichern unter** aus.
1. Geben Sie einen Namen für Ihre gespeicherte Ansicht ein.
1. Setzen Sie ein Häkchen bei **Öffentlich**, wenn Sie diese Arbeitspaket-Ansicht auch für andere Mitglieder dieses Projekts zugänglich machen möchten.
1. Setzen Sie ein Häkchen bei **Favorisiert**, wenn Sie diese Arbeitspaketansicht als Menüpunkt unter Favoriten anzeigen möchten.
1. Klicken Sie auf **Speichern**, um Ihre Änderungen zu speichern.

![Arbeitstabelle speichern](media/a10d09ab4d4502843b9c6f4e1b42810dd04de8f3.png "Arbeitstabelle speichern")

**Hinweis**: Wenn Listen zusammengeklappt sind, können diese Zusammenklappungen nicht in eine gespeicherte Ansicht übernommen werden und werden daher beim Wiederherstellen der Ansicht automatisch ausgeklappt angezeigt.

## Änderungen der Arbeitspaket-Tabellen-Ansichten speichern

Nehmen Sie erneut Änderungen einer gespeicherten Ansicht vor, müssen diese noch einmal gespeichert werden. Klicken Sie auf das **Disketten-Symbol** neben **Arbeitspakete** (der Titel dieser Seite) um eine aktuell verwendete Ansicht anzuwenden.

Um eine komplett neue Arbeitspaketansicht zu speichern, klicken Sie erneut auf das Drei-Punkte-Menü und wählen Sie **Speichern unter.**

![Speichern Arbeitstabellen-Ansicht](media/8ecc56aba297cdf696c0dd34a8def13cce8f6292.png "Speichern Arbeitstabellen-Ansicht")
