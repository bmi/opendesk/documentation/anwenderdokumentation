# Daten und Dauer festlegen und ändern

## Start- und Enddatum festlegen

Das Start- und Enddatum eines Arbeitspakets kann geändert werden. Öffnen Sie dazu die Detailansicht oder die Vollbildansicht des gewünschten Arbeitspakets und klicken Sie unter dem Abschnitt **DETAILS** in das Feld neben **Datum**. Nun öffnet sich die Kalenderansicht und Sie können das Datum Ihres Arbeitspakets ändern.

**Hinweis**: Sie können die Kalenderansicht auch in der Tabellen-Ansicht einsehen und ändern.

![Datum ändern in Arbeitspaketen](media/38f2ad00f5c5e42ebcebf17540100c02f2469d2c.png "Datum ändern in Arbeitspaketen")

Innerhalb der Kalenderansicht haben Sie die Möglichkeit den Schalter für die **Manuelle Planung** und **Nur Werktage** zu aktivieren. Bei der Manuellen Planung werden Termine nicht aus den Beziehungen der Arbeitspakete abgeleitet bzw. werden diese dabei ignoriert. Wenn der Schalter **Nur Werktage** aktiviert ist, dann werden nur die Tage Montag bis Freitag angezeigt. Für die Festlegung des Datums haben Sie Eingabefelder für den Anfangstermin, Endtermin und die Dauer des Arbeitspakets. Außerdem haben Sie in der Minikalender-Ansicht eine monatliche Übersicht. Klicken Sie auf **Speichern**, um das Datum und die Dauer des Arbeitspakets festzuhalten.

## Erweiterte Funktionen

In der Kalenderansicht haben Sie die Möglichkeit, Start- und Enddatum in umgekehrter Reihenfolge zu wählen. Wenn Sie zunächst ein Startdatum auswählen und dann den Zeiger rückwärts bewegen, um ein früheres Datum zu wählen, wird das zuerst ausgewählte Datum als Enddatum betrachtet, während das danach ausgewählte Datum als Startdatum gilt.

Wenn das Start- und Enddatum bereits eingestellt wurde, kann man auch ausschließlich das Enddatum ändern. Klicken Sie dazu auf das Feld **Endtermin** und wählen Sie ein neues Datum aus. Wenn das neue Datum nicht vor dem Startdatum liegt, wird das Enddatum entsprechend angepasst. Liegt das gewählte Datum vor dem Startdatum, wird das ursprüngliche Startdatum durch das neue Datum ersetzt. Durch einen weiteren Klick auf ein späteres Datum wird dann das neue Enddatum festgelegt.

Es ist ebenfalls möglich, nur eines der beiden Daten festzulegen. Klicken Sie dazu auf das Datum, welches Sie festlegen möchten (standardmäßig ist das Startdatum ausgewählt, aber Sie können dies durch einen Klick auf das Enddatum ändern) und wählen Sie dann ein Datum aus. Klicken Sie nun auf **Speichern**, ohne ein zweites Datum festzulegen. Falls beide Daten voreingestellt sind, können Sie einfach eines davon löschen. Speichern Sie anschließend Ihre Datumsänderung.

**Hinweis**: Die Vorgänger-/Nachfolger-Beziehung kann die Daten von Arbeitspaketen einschränken oder beeinflussen. Weitere Informationen finden Sie unter **Arbeitspaket-Beziehungen und Hierarchien**.

## Eintägige Planung

Sie können das Datum eines Arbeitspakets so einstellen, dass es am selben Tag gestartet und beendet wird. Klicken Sie dazu bei **Anfangstermin** und **Endtermin** auf das gleiche Datum. Im Eingabefeld **Dauer** steht dann automatisch **1 Tag**.

![Eintägige Planung eines Arbeitspakets](media/dc0d387567b64d2982d4c835bf8e550fc7cc107b.png "Eintägige Planung eines Arbeitspakets")

## Informations- und Warnmeldungen

Innerhalb der Datumseinstellungen gibt es Warnmeldungen, die Ihnen bei der Änderung von Terminen für Arbeitspakete wichtige Informationen liefern. Warnmeldungen werden immer in violett oder gelb überhalb der Kalenderansicht angezeigt.

Es gibt insgesamt vier mögliche Banner:

![Blaue Warnmeldung](media/9dbaab1768f984aa665e12da1acd6c38bc545153.png "Blaue Warnmeldung")

![Gelbe Warnmeldung](media/2d861a7484dd2bf2d3ed152be581d6c93a645276.png "Gelbe Warnmeldung")

## Automatisch geplant

Diese Warnmeldung wird eingeblendet, wenn die Daten des aktuellen Arbeitspakets aus bestehenden Beziehungen abgeleitet werden und daher nicht editierbar sind. Dies gilt insbesondere für übergeordnete Arbeitspakete, bei denen die Start- und Enddaten von den frühesten beziehungsweise spätesten untergeordneten Arbeitspaketen abgeleitet werden.

Trotz dieser Einschränkung besteht die Möglichkeit, spezifische Start- und Enddaten für solche Arbeitspakete manuell einzugeben. Dies wird durch die Aktivierung des Schalters der manuellen Terminplanung ermöglicht, wodurch die Beziehungen, die normalerweise die Daten beeinflussen, ignoriert werden.

## Die verfügbaren Start- und Endtermine sind durch Beziehungen begrenzt

Diese Warnmeldung wird eingeblendet, wenn bestimmte Zeitspannen aufgrund von Beschränkungen durch **Folgt/Vorgänger von** -Beziehungen nicht verfügbar sind. Ein Beispiel hierfür ist, dass Arbeitspakete, die einem anderen Arbeitspaket folgen, nicht vor dem Enddatum des vorherigen Arbeitspakets geplant werden können.

## Manuelle Terminplanung aktiviert, alle Beziehungen wurden ignoriert

Diese Warnmeldung wird sichtbar, wenn Sie die manuelle Planung von Arbeitspaketen aktivieren, die **Folgt/Vorgänger von** - oder über- oder untergeordnete Arbeitspaket-Beziehungen haben. Durch die Aktivierung dieser manuellen Planung werden diese Beziehungen außer Kraft gesetzt, und die zeitliche Festlegung dieses Arbeitspakets ist nicht mehr durch sie beschränkt. Das bedeutet ebenfalls, dass Änderungen an einem manuell geplanten Arbeitspaket keine Auswirkungen mehr auf andere Arbeitspakete haben, selbst wenn bestehende Beziehungen vorhanden sind.

Diese Warnmeldung erscheint nicht, wenn die manuelle Terminplanung für Arbeitspakete ohne Beziehungen aktiviert wird.

## Das Ändern dieser Daten wirkt sich auf die Termine verwandter Arbeitspakete aus

Diese Warnmeldung wird in Arbeitspaketen eingeblendet, deren Start- und Enddatum Auswirkungen auf andere verbundene Arbeitspakete haben. Zum Beispiel bewirkt eine Änderung des Enddatums eine Anpassung des Startdatums eines anderen (nachfolgenden) Arbeitspakets und/oder des Enddatums eines übergeordneten Arbeitspakets.

## Beziehungen anzeigen

Die Info- und Warnmeldungen sind mit einem Button **Beziehungen anzeigen** ausgestattet. Durch einen Klick darauf öffnet sich ein neuer Reiter, der Arbeitspakete mit direkten Verbindungen zum aktuellen Arbeitspaket in der Gantt-Ansicht (im Hierarchie-Modus) zeigt.

![Infomeldung Button Beziehungen anzeigen](media/0b3e7491d7da4fef6edbbc5a97197781e5b55764.png "Infomeldung Button Beziehungen anzeigen")
