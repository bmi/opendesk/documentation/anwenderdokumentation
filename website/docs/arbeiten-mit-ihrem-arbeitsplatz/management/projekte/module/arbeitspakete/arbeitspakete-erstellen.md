# Arbeitspakete erstellen

Es gibt zwei Möglichkeiten, Arbeitspakete zu erstellen:

- Arbeitspakete in der **Tabellenansicht** erstellen: Dieser Weg erlaubt es Ihnen, viele Arbeitspakete in kurzer Zeit zu erstellen.

Arbeitspakete in der **Detailansicht** erstellen: Auf diesem Weg können Sie von Anfang an detaillierte Informationen angeben.  Arbeitspakete gehören immer zu einem **Projekt**. Daher müssen Sie zunächst das zugehörige Projekt auswählen.

Klicken Sie danach auf das Modul **Arbeitspakete** in der Projektnavigation.

![Projektnavigation](media/c4d7f0cd10a73b0b50ab376e0c88a7c683c8c835.jpg "Projektnavigation")

## Arbeitspakete in der Tabellenansicht erstellen

Um neue Arbeitspakete direkt in der Tabellenansicht erstellen zu können, klicken Sie unten auf die Schaltfläche **+ Erstelle neues Arbeitspaket**.

![Tabellenansicht](media/86623c555a5c5db8435b5148629f818ca182b7ab.jpg "Tabellenansicht")

Das neue Arbeitspaket erscheint in einer neuen, grün gefärbten Zeile. Geben Sie eine **Bezeichnung** für das Arbeitspaket ein und ändern Sie die Attribute **Typ**, **Status**, **Zugewiesen an** und **Priorität** direkt in der Zeile. Bestätigen Sie ihre Änderungen mit der **Eingabetaste**.

![Arbeitspaket in Zeile](media/afa35bd74e8455861f6877e97bf24ca89a264a6a.jpg "Arbeitspaket in Zeile")

## Arbeitspakete in der Detailansicht erstellen

Um von Anfang an detaillierte Informationen für das Arbeitspaket anzugeben, können Sie ein Arbeitspaket in der Detailansicht erstellen. Klicken Sie oben im Modul Arbeitspakete auf die Schaltfläche **Neues Arbeitspaket (** **+ Erstellen)** und wählen Sie den gewünschten **Arbeitspaket-Typ** (Aufgabe, Meilenstein oder Phase) aus.

![Headermenü Arbeitspaket erstellen](media/782b5c46ac5cd52d863be7569c5839825349e38d.jpg "Headermenü Arbeitspaket erstellen")

Der Bildschirm wird geteilt und das neue Arbeitspaket öffnet sich in der **Detailansicht** auf der rechten Seite. Hier können Sie alle gewünschten Angaben machen und Einstellungen vornehmen. Einzelheiten dazu finden Sie unter **Arbeitspakete bearbeiten – Bearbeitung in der Detail- und Vollbildansicht** . Beachten Sie, dass die verschiedenen Reiter der Detail- bzw. Vollbildansicht erst bei der späteren Bearbeitung zugänglich sind. Die Optionen beim Erstellen neuer Arbeitspakete entsprechen denen des Reiters **Übersicht**.

Klicken Sie abschließend auf die Schaltfläche **Speichern**.

![Arbeitspaket in der Splitscreen-Ansicht](media/ceb8ea7038a3ec1db93e2ff337f701f992eeae76.jpg "Arbeitspaket in der Splitscreen-Ansicht")
