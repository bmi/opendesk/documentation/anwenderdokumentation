# Arbeitspakete kopieren, verschieben, löschen

Sie können mit der rechten Maustaste in eine Arbeitspaket-Tabelle klicken, um in Bearbeitungsoptionen der jeweiligen Arbeitspakete zu gelangen.

Sie können:

- **In ein anderes Projekt verschieben**
- **Kopieren**
- **Link in Zwischenablage kopieren**
- **In anderes Projekt kopieren**
- **Löschen**

![Bearbeitungsoptionen von Arbeitspaketen](media/1d202af37e122160c2bfd23399ca9742c4c06e42.png "Bearbeitungsoptionen von Arbeitspaketen")

Sie haben auch Zugriff auf diese Funktionen, indem Sie auf das **Drei-Punkte-Menü** ganz rechts eines Arbeitspakets verwenden, um das Menü aufzurufen oder Sie klicken in die Detailansicht des entsprechenden Arbeitspakets, indem Sie die Zeile doppelklicken und klicken dort auf das **Drei-Punkte-Menü**.

## Ein Arbeitspaket kopieren

Beim Kopieren eines Arbeitspakets wird ein Arbeitspaketformular angezeigt, das die voreingestellten Werte des ursprünglichen Arbeitspakets enthält. Diese Funktion ermöglicht das einfache Erstellen und Anpassen neuer Arbeitspakete auf der Grundlage bereits bestehender Arbeitspakete.

## Link in Zwischenablage kopieren

Mit dieser Option wird ein kurzer Link zum Arbeitspaket in die Zwischenablage kopiert. Das ermöglicht Ihnen eine schnelle und einfache Einfügung an anderer Stelle. Diese Funktion ist besonders praktisch, wenn Sie zügig Verknüpfungen zu mehreren Arbeitspaketen kopieren möchten, ohne jedes Paket einzeln in der Detailansicht öffnen zu müssen.

## Ein Arbeitspaket in ein anderes Projekt verschieben

Die Funktion **In anderes Projekt verschieben** ermöglicht das Verschieben eines Arbeitspakets in ein anderes Projekt. Wählen Sie das gewünschte Zielprojekt aus und verschieben Sie es.

**Bitte beachten Sie**: Sie können ein Arbeitspaket nur in Projekte verschieben, wenn Sie im gewünschten Projekt Mitglied sind und Sie die Berechtigung haben, diese Funktion auszuführen.

## Ein Arbeitspaket löschen

Die Berechtigung zum Löschen von Arbeitspaketen ist an bestimmte Rollen gekoppelt. Die Berechtigungen können von Projekt zu Projekt unterschiedlich sein und bestimmt der Projektinhaber. Wenden Sie sich an diesen, wenn Sie die Berechtigungen für das Löschen von Arbeitspaketen nicht haben.
