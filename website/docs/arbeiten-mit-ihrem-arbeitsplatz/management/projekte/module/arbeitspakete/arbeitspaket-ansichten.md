# Arbeitspaket-Ansichten

Wenn Sie das Modul Arbeitspakete öffnen, gelangen Sie in die **Tabellenansicht**. Diese bietet Ihnen eine Übersicht über die vorhandenen Arbeitspakete. Sie können die Tabelle auf verschiedene Arten sortieren und filtern. Dazu stehen Ihnen das **Menü** auf der linken Seite sowie **Dropdown-Menüs in den Tabellenüberschriften** zur Verfügung.

Neben der Tabellensicht gibt es noch weitere **Ansichtsmodi**: Die **Kartenansicht**, die **Gantt-Ansicht**, die **Detailansicht** und die **Vollbildansicht**. Jede Ansicht bietet ihre eigenen Vorteile.

### Hier geht es weiter mit:

- [Ansichtsmodi](arbeitspaket-ansichten/ansichtsmodi.md)
- [Die Tabellenansicht im Überblick](arbeitspaket-ansichten/die-tabellenansicht-im-ueberblick.md)
- [Die Tabellenansicht anpassen](arbeitspaket-ansichten/die-tabellenansicht-anpassen.md)
- [Sortieren und Filtern über das Menü](arbeitspaket-ansichten/sortieren-ueber-das-menue.md)
- [Sortieren und Filtern in der Tabellen- und Kartenansicht](arbeitspaket-ansichten/sortieren-und-filtern-ueber-tabellenueberschriften.md)
