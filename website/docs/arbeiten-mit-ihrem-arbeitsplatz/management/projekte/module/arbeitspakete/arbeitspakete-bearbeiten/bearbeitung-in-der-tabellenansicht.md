# Bearbeitung in der Tabellenansicht

Suchen Sie zunächst das gewünschte Arbeitspaket in der Tabellenansicht. Klicken Sie nun in der Zeile des Arbeitspakets auf das Attribut, das Sie ändern möchten.

Unter **Thema** können Sie die Bezeichnung des Arbeitspakets ändern. Speichern Sie Ihre Eingaben mit der **Eingabetaste** oder mit einem Mausklick in einen anderen Bereich der Tabelle. Bei **Typ**, **Status**, **Zugewiesen an** und **Priorität** öffnet sich jeweils ein Dropdown-Menü, aus dem Sie die gewünschte Option auswählen können.

![Ein Arbeitspaket in der Tabellenansicht bearbeiten (Beispiel: Dropdown-Menü Status)](media/8d34cfc012c1ddc4b8616eea30299d4f9b748beb.png "Ein Arbeitspaket in der Tabellenansicht bearbeiten")
