# Bearbeitung in der Detail- und Vollbildansicht

Das Erstellen und Bearbeiten von Arbeitspaketen in der Detail- und Vollbildansicht funktioniert grundsätzlich gleich. Wie sich die Ansichten im Layout unterscheiden und wie Sie die Detail- bzw. Vollbildansicht öffnen, erfahren Sie unter **Ansichtsmodi**.

Beim Bearbeiten vorhandener Arbeitspakete stehen Ihnen einige Zusatzfunktionen zur Verfügung, die beim Erstellen nicht vorhanden sind.

## Reiter

Die Detail- und Vollbildansicht für vorhandene Arbeitspakete ist in mehrere Reiter eingeteilt, die Sie oben auswählen können:

- **Übersicht**: Hier können Sie die wichtigsten Angaben zum Arbeitspaket einsehen und bearbeiten. In der Vollbildansicht finden Sie diesen Abschnitt in der linken Bildschirmhälfte.
- **Aktivität**: Hier können Sie die letzten Änderungen am Arbeitspaket einsehen und Kommentare hinzufügen.
- **Dateien**: Hier können Sie Dateianhänge einsehen, löschen und hinzufügen. Die Zahl neben der Überschrift **Dateien** gibt an, wie viele Dateien aktuell an das Arbeitspaket angehängt sind.
- **Beziehungen**: Hier können Sie zur besseren Organisation Beziehungen zwischen unterschiedlichen Arbeitspaketen herstellen. Die Zahl neben der Überschrift **Beziehungen** gibt an, wie viele Beziehungen aktuell für das Arbeitspaket eingerichtet sind.
- **Beobachter**: Hier können Sie einsehen, wer das ausgewählte Arbeitspaket beobachtet, sowie Beobachterinnen und Beobachter hinzufügen oder entfernen. Die Zahl neben der Überschrift **Beobachter** gibt an, wie viele Personen das Arbeitspaket aktuell beobachten.
- **Github**: Hier können Sie das Arbeitspaket in Github einbinden.
- **Meetings**: Hier können Sie das Arbeitspaket mit Besprechungen verknüpfen, in denen Sie es zur Diskussion stellen möchten.  Je nach Bildschirmgröße sind womöglich nicht alle Reiter sichtbar. Mit den **pfeilförmigen Schaltflächen** an den Rändern der Navigationsleiste können Sie nach rechts und links blättern, um weitere Reiter einzublenden.

Beim Erstellen von neuen Arbeitspaketen sind die Reiter noch nicht verfügbar.

![Ein Arbeitspaket in der Detailansicht bearbeiten](media/2958e32d9703fad15f13e2d7b7f56448b3e57577.png "Arbeitspaket bearbeiten \(Detailansicht\)")

## Oberer Bereich

Im oberen Bereich direkt unter der Navigationsleiste finden Sie folgende Funktionen, unabhängig davon, welcher Reiter gerade ausgewählt ist:

- **Übergeordnetes Arbeitspaket festlegen**: Mit dieser Funktion können Sie eine Hierarchie zwischen zwei Arbeitspaketen herstellen und das aktuell geöffnete Arbeitspaket einem anderen unterordnen. Klicken Sie auf die Schaltfläche und geben Sie einen Suchbegriff ein, um ein Arbeitspaket zu finden, dem Sie das aktuell geöffnete Arbeitspaket unterordnen möchten.
- **Typ**: Hier steht entweder **Aufgabe**, **Meilenstein** oder **Phase**, je nachdem, welcher Typ beim Erstellen des Arbeitspakets ausgewählt wurde. Sie können per Mausklick ein Dropdown-Menü öffnen, um einen anderen Typ auszuwählen.
- **Thema**: Hier finden Sie die Bezeichnung, die beim Erstellen des Arbeitspakets eingegeben wurde. Klicken Sie in das Textfeld, um eine neue Bezeichnung einzugeben. Dieses Feld darf nicht leer bleiben.

![Oberer Bereich der Detailansicht](media/2958e32d9703fad15f13e2d7b7f56448b3e57577.png "Oberer Bereich Detailansicht")
