# Arbeitspakete bearbeiten

Sie können vorhandene Arbeitspakete auf folgenden Wegen bearbeiten:

- In der **Tabellenansicht** können Sie die wichtigsten Parameter unmittelbar ändern.

In der **Detailansicht** und der **Vollbildansicht** stehen Ihnen sämtliche Optionen zur Verfügung. Alle Änderungen, die Sie an Arbeitspaketen vornehmen, werden im Reiter **Aktivität** in der Detail- und Vollbildansicht dokumentiert.

### Hier geht es weiter mit:

- [Bearbeitung in der Tabellenansicht](arbeitspakete-bearbeiten/bearbeitung-in-der-tabellenansicht.md)
- [Bearbeitung in der Detail- und Vollbildansicht](arbeitspakete-bearbeiten/bearbeitung-in-der-detail-und-vollbildansicht.md)
