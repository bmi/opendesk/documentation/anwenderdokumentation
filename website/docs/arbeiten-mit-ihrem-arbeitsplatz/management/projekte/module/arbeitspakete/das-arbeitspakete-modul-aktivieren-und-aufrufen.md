# Das Arbeitspakete-Modul aktivieren und aufrufen

Um das Modul **Arbeitspakete** nutzen zu können, muss es zunächst in der Projektkonfiguration aktiviert werden. Navigieren Sie dazu im Hauptmenü links zum Punkt **Projektkonfiguration** ganz unten.

![Der Punkt &quot;Projektkonfiguration&quot; im Hauptmenü](media/be3626caf0ad0c17b6c834da28f6b17fd7c9dda2.png "Projektkonfiguration im Hauptmenü")

Navigieren Sie nun im Menü links unter **Projektkonfiguration** zum Punkt **Module**. Dort können Sie Module hinzufügen, indem Sie einen Haken neben den Modulnamen setzen. Setzen Sie einen Haken bei **Arbeitspakete** und klicken Sie auf **Speichern**.

![Auswählen von Modulen in der Projektkonfiguration](media/1c3e872e35b9008bbe4b06236b24fe0645ef5eb0.jpg "Auswählen von Modulen in der Projektkonfiguration")

Ab sofort können Sie das Arbeitspakete-Modul über den entsprechenden Punkt im Hauptmenü links auf der Seite auswählen.

![Das Modul &quot;Arbeitspakete&quot; im Hauptmenü](media/e2f47179558e166e4e7e1ce6f7fd2879bbe3ce13.png "Arbeitspakete im Hauptmenü")
