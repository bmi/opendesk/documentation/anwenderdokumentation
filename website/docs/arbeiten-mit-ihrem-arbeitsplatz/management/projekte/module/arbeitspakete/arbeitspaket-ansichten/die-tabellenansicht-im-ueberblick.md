# Die Tabellenansicht im Überblick

Die Tabellenansicht bietet Ihnen einen Überblick über die vorhandenen Arbeitspakete. Welche Arbeitspakete angezeigt und wie die Arbeitspakete sortiert werden, hängt davon ab, was im **Menü** auf der linken Seite ausgewählt wurde (siehe **Sortieren und filtern über das Menü**). Die Auswahl erscheint als **Überschrift** über der Tabelle.

Rechts oben finden Sie eine Leiste mit **Schaltflächen** und **Dropdown-Menüs**, die auch in der Kartenansicht verfügbar sind (siehe **Ansichtsmodi**):

-   **Neues Arbeitspaket (+ Erstellen)**: Erstellen Sie hiermit ein neues Arbeitspaket.
-   **Enthaltene Projekte**: Arbeitspakete sind immer einem Projekt zugeordnet. Wählen Sie hier aus, welche Projekte bei der Auflistung von Arbeitspaketen einbezogen werden sollen.
-   **Filter**: Filtern Sie hier die Liste der Arbeitspakete nach Suchbegriffen oder Attributen.
-   **Dropdown-Menü für Ansichtsmodi (Standard: Tabelle)**: Wechseln Sie hier zwischen Tabellen-, Karten- und Gantt-Ansicht (siehe **Ansichtsmodi**).
-   **Detailansicht öffnen**: Öffnen Sie die Detailansicht des ausgewählten Arbeitspakets (siehe **Ansichtsmodi** und **Bearbeitung in der Detailansicht**).
-   **Zen-Modus aktivieren**: Verwenden Sie das Modul in einem Vollbildmodus, in dem das Menü links sowie sämtliche Steuerelemente Ihres Browsers ausgeblendet werden. So können Sie sich voll auf die Bearbeitung konzentrieren.

**Weitere Aktionen (Drei-Punkte-Menü)**: Öffnen Sie ein Dropdown-Menü mit weiteren Optionen  Unter dieser Leiste befindet sich die eigentliche **Tabelle**, in der die Arbeitspakete aufgelistet werden.

![Die Tabellenansicht im Überblick](media/911ff3bf8b90ebc2e76f0fdc2b99dd5068d7dc90.png "Die Tabellenansicht im Überblick")

## Tabellenspalten

Die einzelnen Spalten der Tabellenansicht geben Auskunft über folgende Attribute der Arbeitspakete:

-   **ID**: Hier finden Sie die fortlaufende Nummer, die allen Arbeitspaketen automatisch zugewiesen wird.
-   **Thema**: Hier finden Sie die Bezeichnung der Arbeitspakete.
-   **Typ**: Sie können für Arbeispakete zwischen den Typen **Aufgabe**, **Meilenstein** und **Phase** wählen.
-   **Status**: Hier wird angegeben, ob Arbeitspakete **Neu**, **In Bearbeitung**, **Erledigt**, **Gestoppt** oder **Zurückgewiesen** sind (siehe auch **Backlogs – Arbeiten mit Backlogs**).
-   **Zugewiesen an**: Hier wird angegeben, welcher Person die Arbeitspakete zur Bearbeitung zugewiesen wurden.
-   **Aktualisiert am**: Hier finden Sie das letzte Datum, an dem die Arbeitspakete bearbeitet wurden.
