# Die Tabellenansicht anpassen

In der Tabellenansicht können Sie einzelne Spalten ausblenden oder einfügen sowie die Spalten neu anordnen. Entfernen Sie zur besseren Übersicht Spalten, die Sie nicht benötigen, z. B. wenn Sie die Arbeitspakete nie nach dem entsprechenden Attribut sortieren. Fügen Sie andererseits weitere Attribute hinzu, die Sie direkt in der Tabellenübersicht einsehen oder nach denen Sie sortieren möchten.

Um eine Tabellenspalte zu entfernen, klicken Sie auf die **Tabellenüberschrift** des gewünschten Attributs. Ein **Dropdown-Menü** öffnet sich. Wählen Sie **Spalte ausblenden**.

![Dropdown-Menü für eine Tabellenspalte](media/8b5b95ebcee12671fe669d1752ee5c2cc7822eee.png "Dropdown-Menü Tabellenspalte")

Um eine oder mehrere neue Spalten hinzuzufügen oder die Spalten neu anzuordnen, öffnen Sie das Dropdown-Menü in einer beliebigen Tabellenspalte und wählen Sie **Spalten einfügen**. Alternativ können Sie rechts oberhalb der Tabelle auf die Schaltfläche **Weitere Aktionen** klicken und den ersten Punkt **Ansicht konfigurieren** auswählen.

![Das Menü &quot;Weitere Aktionen&quot;](media/e614ca817c913e79934499152c792e5b3682aaf5.png "Weitere Aktionen")

Das Fenster **Konfiguration der Arbeitspaket-Tabelle** öffnet sich. Der Reiter **Spalten** ist hier bereits vorausgewählt.

Unterhalb der Reiter sind alle aktuell vorhandenen Spalten als kleine Kästchen nebeneinander dargestellt. Auch hier können Sie bei Bedarf **Spalten entfernen**, indem Sie auf das **Schließen-Symbol** rechts im Kästchen klicken.

Sie können die **Reihenfolge** der Spalten anpassen, indem Sie die Kästchen der einzelnen Attribute per **Drag-and-Drop** an die gewünschte Position verschieben.

Um neue Spalten einzufügen, klicken Sie in das Suchfeld **Spalte hinzufügen**. Ein Dropdown-Menü öffnet sich. Wählen Sie das gewünschte Attribut, für das Sie eine Spalte hinzufügen möchten, aus der Liste aus. Sie können auch die Attributbezeichnung von Hand in das Suchfeld eingeben. Während der Eingabe werden Ihnen Vorschläge angezeigt. Bestätigen Sie Ihre Auswahl mit der **Eingabetaste**.

![Spalten der Tabellenansicht konfigurieren](media/4e8e514dd7a044b6ef3b375cd7681cdb41845c8b.png "Spalten der Tabellenansicht konfigurieren")

Klicken Sie abschließend auf **Anwenden**. Neue Spalten werden der Tabelle auf der rechten Seite hinzugefügt. Sofern Sie die Reihenfolge der Spalten geändert haben, werden diese Änderungen ebenfalls übernommen.
