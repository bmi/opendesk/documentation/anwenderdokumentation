# Sortieren und Filtern über das Menü

Das Menü auf der linken Seite bietet Ihnen eine Reihe von nützlichen Voreinstellungen für die Sortierung der Tabellenansicht. Die einzelnen Optionen sind den Gliederungsabschnitten **Favorit**, **Standard** und **Öffentlich** zugeordnet. Sie können jeden dieser Abschnitte einzeln ein- und ausblenden, indem Sie auf die Überschrift klicken.

![Menü für Arbeitspakete](media/c7287331691bb87b10ef0778530adff44d21496c.png "Menü für Arbeitspakete")

## Favorit

Hier finden Sie Aufgabenpakete, die Sie als Favoriten markiert haben.

## Standard

- **Alle offenen**: Dies ist die Standardeinstellung, die beim Öffnen des Moduls automatisch ausgewählt ist. In dieser Ansicht werden alle offenen Arbeitspakete (d. h. Arbeitspakete, die noch nicht als abgeschlossen markiert wurden) angezeigt. Die Sortierung erfolgt dabei aufsteigend nach der **ID** der Arbeitspakete.
- **Letzte Änderungen**: In dieser Ansicht werden alle Arbeitspakete angezeigt, offene und geschlossene gleichermaßen. Die Sortierung erfolgt dabei absteigend nach dem **Datum der letzten Änderung**.
- **Kürzlich erstellt**: In dieser Ansicht werden alle offenen Arbeitspakete angezeigt. Die Sortierung erfolgt dabei nach dem **Erstelldatum** , wobei das neueste Arbeitspaket ganz oben auf der Liste erscheint.
- **Gantt-Diagramm**: In dieser Ansicht werden alle offenen Arbeitspakete angezeigt und auf der rechten Seite in einem Gantt-Diagramm dargestellt. Die Sortierung erfolgt dabei nach dem **Erstelldatum** , wobei das älteste Arbeitspaket ganz oben auf der Liste erscheint.
- **Überfällig**: In dieser Ansicht werden alle offenen Arbeitspakete angezeigt, deren geplantes Abschlussdatum in der Vergangenheit liegt. Die Sortierung erfolgt dabei nach dem **geplanten Abschlussdatum** , wobei das jüngste überfällige Abschlussdatum ganz oben auf der Liste erscheint.
- **Zusammenfassung**: Über diesen Punkt öffnen Sie eine Seite mit Tabellen, die Ihnen einen Überblick über die Anzahl an Arbeitspaketen mit bestimmten Attributen bieten. Es gibt eigene Tabellen für die Attribute **Typ**, **Priorität**, **Zugewiesen an**, **Verantwortlich**, **Autor**, **Version** und **Kategorie**.
- **Erstellt von mir**: In dieser Ansicht werden alle Arbeitspakete angezeigt, die Sie selbst erstellt haben. Die Sortierung erfolgt dabei absteigend nach dem **Datum der letzten Änderung**.
- **Mir zugewiesen**: In dieser Ansicht werden alle Arbeitspakete angezeigt, die Ihnen aktuell zur Bearbeitung zugewiesen sind. Die Sortierung erfolgt dabei absteigend nach dem **Datum der letzten Änderung**.
- **Geteilt mit Benutzern**: In dieser Ansicht werden alle Arbeitspakete angezeigt, die mit anderen Nutzerinnen und Nutzern geteilt wurden. Die Sortierung erfolgt dabei absteigend nach dem **Datum der letzten Änderung**.

## Öffentlich

Hier können Sie bei Bedarf ausschließlich **Aufgaben** oder **Meilensteine** anzeigen lassen.
