# Ansichtsmodi

## Tabellen- und Kartenansicht

Über ein **Dropdown-Menü** in der oberen Leiste können Sie zwischen der Tabellen-, Karten- und Gantt-Ansicht umschalten. Diese drei Ansichtsmodi bieten dieselben Steuerelemente in der oberen Leiste (siehe **Die Tabellenansicht im Überblick**).

![Dropdown-Menü für Ansichtsmodi](media/244b13c15c62b75b95a07e5eefa9341bdfd2c5e1.png "Dropdown-Menü für Ansichtsmodi")

Normalerweise ist die **Tabellenansicht** vorausgewählt. Nähere Informationen zur Tabellenansicht finden Sie unter **Die Tabellenansicht im Überblick** und **Die Tabellenansicht anpassen**.

Die **Kartenansicht** stellt jedes einzelne Arbeitspaket als Karte dar. Auf den Karten finden Sie die wichtigsten Angaben zum Arbeitspaket auf einen Blick.

![Die Kartenansicht](media/dc65f3b490f3af437d4a06598ef574df8ef878f0.png "Die Kartenansicht")

Klicken Sie auf die kleine Schaltfläche **Detailansicht öffnen (i)** oben rechts auf der Karte, um zur Detailansicht zu gelangen.

## Gantt-Ansicht

In dieser Ansicht werden alle aktuell sichtbaren Arbeitspakete in einem Gantt-Diagramm dargestellt. Eine genauere Erklärung dazu finden Sie unter **Module – Gantt-Diagramm**.

![Die Gantt-Ansicht](media/2e1feda350a4a4edc718d8552b62305295a88ece.png "Gantt-Ansicht")

## Detailansicht

Die Detailansicht wird in der rechten Bildschirmhälfte dargestellt. Darin können Sie die Details von Arbeitspaketen einsehen und bearbeiten. Die Tabellen- oder Kartenansicht bleibt in der linken Bildschirmhälfte sichtbar, sodass Sie jederzeit andere Arbeitspakete für die Detailansicht auswählen können.

![Geteilter Bildschirm: Detailansicht rechts, Tabellenansicht links](media/2b17ea10b900c33f0ae9a05bc9281173dadd705f.png "Die Detailansicht")

Um in die Detailansicht zu gelangen, öffnen Sie zunächst das Kontextmenü für das gewünschte Arbeitspaket. Dies ist auf folgenden Wegen möglich:

- Klicken Sie mit der rechten Maustaste in die Zeile (Tabellansicht) bzw. Karte (Kartenansicht) des gewünschten Arbeitspaketes.

Klicken Sie auf die Schaltfläche **Kontextmenü öffnen (Drei-Punkte-Menü)** ganz rechts in der Zeile.  Wählen Sie nun den Punkt **Detailansicht öffnen** ganz oben im Menü.

Sie können die Detailansicht auch über die Schaltfläche **Detailansicht öffnen (i)** in der Leiste ganz oben öffnen. Dazu muss das gewünschte Arbeitspaket ausgewählt sein.

![Kontextmenü öffnen (Drei-Punkte-Menü)](media/ec05dee238672e45c508b8077445a5f1e66c78f1.png "Kontextmenü öffnen")

![Detailansicht öffnen über das Kontextmenü](media/6492ada66987fc5d0e138c88435a3207ac641373.png "Detailansicht öffnen")

![Die Schaltfläche &quot;Detailansicht öffnen&quot;](media/e7ab92384634877fd008db769480ed8b64d11fd8.png "Detailansicht öffnen \(Schaltfläche\)")

## Vollbildansicht

In der Vollbildansicht stehen Ihnen dieselben Funktionen zur Verfügung wie in der Detailansicht. Sie liefert den besten Überblick über alle Details einzelner Arbeitspakete, bietet dafür aber keinen Direktzugriff auf die Tabellen- oder Kartenansicht, um schnell zwischen Arbeitspaketen zu wechseln.

Die Hauptfunktionen, die in der Detailansicht unter dem Reiter **Übersicht** zu finden sind, werden in der Vollbildansicht auf der linken Bildschirmhälfte dargestellt. Auf der rechten Seite können Sie zwischen den übrigen Reitern navigieren, wobei hier der Reiter **Aktivität** vorausgewählt ist.

![Arbeitspaket bearbeiten (Vollbildansicht)](media/f9a9937a8e90fafe7a9a901b123520f2e4347f72.png "Arbeitspaket bearbeiten \(Vollbildansicht\)")

Um die Vollbildansicht zu öffnen, stehen Ihnen folgende Wege zur Verfügung:

- Ein **Doppelklick** auf die Zeile des gewünschten Arbeitspakets in der Tabellenansicht
- Ein **Doppelklick** auf die Karte des gewünschten Arbeitspakets in der Kartenansicht
- Der Punkt **Vollbildansicht öffnen** im **Kontextmenü** der Tabellen- und Kartenansicht (siehe oben)
- Die kleine Schaltfläche **Vollbildansicht anzeigen** oben rechts in der Detailansicht

![Die Schaltfläche &quot;Vollbildansicht anzeigen&quot; in der Detailansicht](media/0ec291b5222938407dbc435dbd531d62049b7c91.png "Vollbildansicht anzeigen")

Um die Vollbildansicht zu verlassen und zurück zur Tabellen- bzw. Kartenansicht zu gelangen, verwenden Sie die **Zurück** -Schaltfläche Ihres Browsers oder klicken Sie auf die Schaltfläche **Zurück** links oben in der Vollbildansicht.

![Vollbildansicht über die Schaltfläche &quot;Zurück&quot; verlassen](media/ffe9fd7d71d1e2708218b75a40e3b55407115d03.png "Vollbildansicht verlassen")
