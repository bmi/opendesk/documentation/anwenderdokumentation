# Sortieren und Filtern in der Tabellen- und Kartenansicht

## Tabellenansicht sortieren

Sie können die Arbeitspakete in der Tabellenansicht auf- und absteigend nach allen Attributen sortieren, für die es eine Tabellenspalte gibt. Klicken Sie dazu auf die **Tabellenüberschrift** des gewünschten Attributs. Ein **Dropdown-Menü** öffnet sich. Wählen Sie **Sortiere absteigend** bzw. **aufsteigend**.

![Sortieren über das Dropdown-Menü in der Tabellenansicht](media/8b5b95ebcee12671fe669d1752ee5c2cc7822eee.png "Dropdown-Menü Tabellenansicht")

## Arbeitspakete filtern

Über die Schaltfläche **Filter** in der oberen Leiste können Sie individuelle Einstellungen vornehmen, um Arbeitspakete genau nach Ihren Wünschen zu filtern. Die Voreinstellung in diesem Bereich hängt davon ab, welche Option aus dem Menü auf der linken Seite ausgewählt wurde (siehe **Sortieren und filtern über das Menü**). Standardmäßig ist die Option **Alle offenen** ausgewählt, d. h. es wird nach offenen Arbeitspaketen gefiltert und abgeschlossene Arbeitspakete werden nicht angezeigt. Am einfachsten ist es, den Menüpunkt auszuwählen, der Ihren Vorstellungen am nächsten kommt, und die Filtereinstellungen danach weiter zu verfeinern.

Oben im Filterbereich finden Sie ein **Textfeld**. Geben Sie hier einen **Suchbegriff** ein, nach dem gefiltert werden soll. Dabei werden sowohl die Bezeichnungen als auch die Beschreibungen von Arbeitspaketen durchsucht.

![Der Filter-Bereich](media/597ad48fd0009129c67057a2a35556c985e332a5.png "Der Filter-Bereich")

Weiter unten können Sie beliebig viele Filter einstellen, die nicht auf einem Suchbegriff, sondern auf Attributen basieren. Vorhandene Filter können Sie über das **Löschen-Symbol** auf der rechten Seite entfernen.

Klicken Sie auf das **Dropdown-Menü** bei **Filter hinzufügen** und wählen Sie das gewünschte Attribut aus der Liste aus. Einige Attribute (z. B. Anfangs- und Endtermin) bieten Ihnen weitere Einstell- und Eingabemöglichkeiten. Sie können in manchen Fällen auch negative Filter auswählen, beispielsweise indem Sie beim Attribut **Status** über den Listenpunkt **ist nicht** nach Arbeitspaketen filtern, die einen bestimmten Status **nicht** haben.

![Beispiel eines negativen Filters](media/844a19018375227188bc938e81d20a62001c07b8.png "Negativer Filter")

Sie können beliebig viele Filter hinzufügen, um die angezeigten Arbeitspakete möglichst genau einzugrenzen. Ein Anwendungsfall könnte z. B. der Folgende sein: Sie möchten sich einen Überblick über noch nicht erledigte Arbeitspakete verschaffen, die besonders wichtig und in Kürze fällig sind. So können Sie Ressourcen gezielt auf diese Arbeitspakete fokussieren, damit sie rechtzeitig abgeschlossen werden. Filtern Sie also nach Arbeitspaketen mit hoher Priorität, die nicht erledigt sind und deren Endtermin in naher Zukunft (z. B. in 5 Tagen) liegt.

![Beispiel für mehrteiliges Filtern](media/0c9ce1f05737140c3b7b1c1b388373e6f544a4c6.png "Mehrteiliges Filtern")
