# Neuigkeiten

Auf der Seite **Neuigkeiten** können Sie die aktuellsten **Neuigkeiten** zu einem Projekt in umgekehrter chronologischer Reihenfolge sehen. Über Neuigkeiten werden allgemeine Themen an alle Teammitglieder kommuniziert.

Bei **Neuigkeiten** handelt es sich um ein Modul, das die Veröffentlichung und Verwendung von Nachrichteneinträgen ermöglicht.

![Neuigkeiten-Menü](media/c82c79ed96d0fad6024e59144f11af830f2e5574.PNG "Neuigkeiten-Menü")

## Neuigkeiten kommentieren

In dem **Neuigkeiten** -Modul, das sich im Projekte-Menü links befindet, sehen Sie alle Neuigkeiten zu einem Projekt. Klicken Sie auf den Titel der **Neuigkeit**.

![Neuigkeit](media/a8be176d94a66a4d8e72c1c4973ba2cead8824dc.PNG "Neuigkeit")

Durch den Klick auf die **Neuigkeit** können Sie weitere Details zu dieser einsehen. Es öffnet sich ein neues Fenster. In diesem befindet sich ein Textfeld, über das es Ihnen möglich ist, die **Neuigkeit** zu kommentieren.

![Neuigkeit kommentieren](media/af9c46fcf8bb53261555c18ae00697f8883c6df2.PNG "Neuigkeit kommentieren")

Fügen Sie den von Ihnen gewünschten Text ein und klicken Sie auf **Kommentar hinzufügen,** um Ihre Information oder Meinung zum Thema mit anderen zu teilen.

Ihr Kommentar wird danach mit allen anderen Kommentaren unter dem Thema erscheinen.

## Neuigkeiten beobachten

Sie können sich dazu entscheiden, bestimmte Neuigkeiten zu beobachten. Als Beobachterin oder Beobachter werden Sie per E-Mail darüber informiert, ob eine Neuigkeit sich verändert hat oder kommentiert wurde.

![Beobachter werden](media/0c7f3038c69b1c652dde1f71221857f0589abac6.PNG "Beobachter werden")

Um eine **Neuigkeit** zu beobachten, klicken Sie auf **Beobachten** in der rechten Ecke. Wenn Sie keine Benachrichtigungen zu Neuigkeiten mehr erhalten möchten, klicken Sie erneut auf **Beobachten,** um die Funktion zu deaktivieren.

## Neuigkeiten bearbeiten

Sie können jederzeit nachträglich Änderungen an einer **Neuigkeit** vornehmen. Wählen Sie hierzu den gewünschten Beitrag mit einem Mausklick aus. Klicken Sie auf die **Bearbeiten-Schaltfläche** in der rechten Ecke.

![Neuigkeit bearbeiten](media/8e0e69a3f60a4341df822385f7802984a020b466.PNG "Neuigkeit bearbeiten")

Ihnen stehen nun mehrere Textfelder zur Verfügung, um Änderungen am Titel, der Zusammenfassung und der Beschreibung vorzunehmen. Den ursprünglichen Text finden Sie am Ende Ihrer Seite. Führen Sie Ihre Änderungen durch und klicken Sie auf **Speichern,** um die Bearbeitung abzuschließen.

![Bearbeitung einer Neuigkeit](media/f9e3329359319a8ea8c7974307797205e528a4f4.PNG "Bearbeitung einer Neuigkeit")

## Neuigkeit erstellen

Um eine Neuigkeit zu erstellen, klicken Sie auf **Neuigkeit hinzufügen**. Die Schaltfläche befindet sich in der oberen rechten Ecke.

Dadurch öffnet sich ein neues Fenster. In diesem Fenster ist es Ihnen möglich, den Titel, die Zusammenfassung und die Beschreibung zu bearbeiten. Der Titel und die Zusammenfassung werden in der Projektübersicht zu sehen sein. Sie finden ihn auch auf der Hauptübersicht der Neuigkeiten-Seite. Des Weiteren ist es Ihnen möglich, den Inhalt der Neuigkeit mit den zur Verfügung stehenden Basisformatierungen im Textfeld ansprechend zu gestalten. Es stehen Ihnen Makros, Tabellen und viele weitere unterstützende Funktionen zur Verfügung.

Um die Bearbeitung abzuschließen, klicken Sie auf **Anlegen**.

## Neuigkeit löschen

Öffnen Sie die **Neuigkeit**, die gelöscht werden soll. Klicken Sie dann auf **Löschen** in der rechten oberen Ecke.

## Der Aktuelle-Neuigkeiten-Bereich auf der Projektübersichtsseite

Es ist Ihnen möglich, die aktuellsten Neuigkeiten in Ihre persönliche Projektübersichtsseite einzubinden. Hierbei handelt es sich um eine benutzerspezifische Einstellung. Klicken Sie dazu auf die **Plus-Schaltfläche** in der rechten oberen Bildschirmecke. Danach wählen Sie mittels eines Klicks auf eine weitere **Plus-Schaltfläche** die gewünschte Position aus. Abschließend wählen Sie Ihr gewünschtes Widget aus.
