# Definition of Done einstellen

Sie können für Backlogs in Ihrem Projekt die Definition of Done einstellen. Das heißt, dass Aufgaben mit einem bestimmten Status automatisch als abgeschlossen gelten. Navigieren Sie dazu im Hauptmenü links zum Punkt **Projektkonfiguration** ganz unten.

![Projektkonfiguration im Hauptmenü](media/be3626caf0ad0c17b6c834da28f6b17fd7c9dda2.png "Projektkonfiguration im Hauptmenü")

Wählen Sie im Menü links den Punkt **Backlogs** aus. Dort finden Sie nun auf der linken Seite unter **Arbeitspaket-Status** jeden Status aufgelistet, den Aufgaben haben können. Rechts daneben können Sie unter **Arbeitspaket ist abgeschlossen, wenn** einen Haken bei jedem Status setzen, der als Definition of Done gelten soll. Aufgaben, denen ein ausgewählter Status zugewiesen wird, gelten fortan automatisch als abgeschlossen.

Neben der Überschrift **Definition of Done** haben Sie mit den Funktionen **Alle auswählen** und **Alle abwählen** die Möglichkeit, alle Haken auf einmal zu setzen oder zu entfernen.

Klicken Sie abschließend auf **Speichern**.

![Definition of Done einstellen](media/3002409ec313f2b1e3f88334eec21a25a29fd325.png "Definition of Done einstellen")
