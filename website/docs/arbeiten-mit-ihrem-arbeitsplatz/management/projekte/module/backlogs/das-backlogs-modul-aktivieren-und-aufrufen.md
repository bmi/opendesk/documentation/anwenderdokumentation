# Das Backlogs-Modul aktivieren und aufrufen

Agile Projektteams, die nach der Scrum-Methode arbeiten, haben mit dem Backlogs-Modul ein nützliches Werkzeug zur Verfügung. Hier können Sie Sprint- und Produkt-Backlogs anlegen und verwalten, Burn-Down-Charts erstellen und vieles mehr.

Um Backlogs nutzen zu können, muss das entsprechende Modul zunächst in der Projektkonfiguration aktiviert werden. Navigieren Sie dazu im Hauptmenü links zum Punkt **Projektkonfiguration** ganz unten.

![Der Punkt &quot;Projektkonfiguration&quot; im Hauptmenü](media/be3626caf0ad0c17b6c834da28f6b17fd7c9dda2.png "Projektkonfiguration im Hauptmenü")

Navigieren Sie nun im Menü links unter **Projektkonfiguration** zum Punkt **Module**. Dort können Sie Module hinzufügen, indem Sie einen Haken neben den Modulnamen setzen. Setzen Sie einen Haken bei **Backlogs** und klicken Sie auf **Speichern**.

![Auswählen von Modulen in der Projektkonfiguration](media/1c3e872e35b9008bbe4b06236b24fe0645ef5eb0.jpg "Auswählen von Modulen in der Projektkonfiguration")

Ab sofort können Sie das Backlogs-Modul über den entsprechenden Punkt im Hauptmenü links auf der Seite auswählen.

![Backlogs im Hauptmenü](media/d927929456ec4c25903619563848f4af23114d2d.png "Backlogs im Hauptmenü")
