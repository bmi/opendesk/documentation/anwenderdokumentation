# Das Taskboard

Im Backlogs-Modul steht Ihnen ein virtuelles Taskboard zur Visualisierung Ihrer Sprints zur Verfügung. Dort können Sie sich schnell einen Überblick über aktuell ausgewählte Aufgaben verschaffen und diese verwalten. Das Taskboard ist ausschließlich für Sprints verfügbar, also für Versionen, die der linken Spalte in der Backlogs-Übersicht zugeordnet sind.

Sie können die Taskboard-Ansicht für einen Sprint direkt aus der Backlogs-Übersicht heraus aufrufen. Öffnen Sie dazu das **Kontextmenü** rechts im Kasten des gewünschten Sprints (siehe auch **Funktionen der Backlogs-Übersicht**) und wählen Sie **Taskboard**.

Das Taskboard besteht aus folgenden Spalten:

- **Story**: Hier sehen Sie die übergeordneten Aufgaben, die Sie in der Backlogs-Übersicht angelegt haben. Auf dem Taskboard können Sie für jede dieser sogenannten Storys Unteraufgaben anlegen (**Schaltfläche mit Pluszeichen (+)** , siehe unten) und auf die verbleibenden Spalten verteilen. Die obere Zeile ist dabei Hindernissen vorbehalten (siehe unten).
- **Neu**: Hier werden alle Unteraufgaben einsortiert, die vom Product Owner für den aktuellen Sprint ausgewählt wurden. Neu erstellte Aufgaben erscheinen automatisch in dieser Spalte. Diese Aufgaben werden im Zuge der Scrum-Umsetzung auf die anderen Spalten verteilt.
- **Zu planen**: Sortieren Sie hier die Aufgaben ein, die als nächstes geplant werden sollen.
- **Geplant**: Sortieren Sie hier die Aufgaben ein, die geplant sind. In der Regel wurden diese bereits einer Bearbeiterin bzw. einem Bearbeiter zugewiesen.
- **In Bearbeitung**: Sortieren Sie hier die Aufgaben ein, die aktuell bearbeitet werden.
- **Erledigt**: Wenn eine Aufgabe fertig bearbeitet wurde, wird sie von der Spalte **In Bearbeitung** hierher verschoben.
- **Gestoppt**: Manchmal kann es vorkommen, dass Aufgaben vorzeitig abgebrochen oder pausiert werden, z. B. weil ein Hindernis aufgetreten ist. Für solche Aufgaben ist diese Spalte vorgesehen.
- **Zurückgewiesen**: Sortieren Sie hier Aufgaben ein, die zurückgewiesen wurden, da sie z. B. aktuell nicht umsetzbar oder finanzierbar sind.

![Die Taskboard-Ansicht mit einigen Beispielaufgaben](media/a47ce4e65fcd15cdfdc2fa707d211b205fd4bdf6.png "Die Taskboard-Ansicht")

## Neue Aufgaben auf dem Taskboard hinzufügen

Zwischen den Spalten **Storys** und **Neu** befindet sich eine **Schaltfläche mit einem Pluszeichen (+)**. Klicken Sie auf diese Schaltfläche, um eine neue Unteraufgabe hinzuzufügen. Ein kleines Fenster öffnet sich. Machen Sie hier alle gewünschten Angaben zur neuen Aufgabe.

- **Subject**: Geben Sie hier eine kurze, aber möglichst aussagekräftige Bezeichnung für die Aufgabe ein.
- **Assigned to**: Öffnen Sie das Kontextmenü, um eine Nutzerin bzw. einen Nutzer für die Bearbeitung der Aufgabe auszuwählen. Sobald die Aufgabe einer Person zugewiesen wurde, ändert sich die Farbe der Aufgabe. Jede Person erhält dabei eine eigene Farbe.
- **Remaining Hours**: Geben Sie hier eine Schätzung dazu an, wie viele Stunden für die Bearbeitung der Aufgabe benötigt werden.  Klicken Sie abschließend auf **OK**. Die neue Aufgabe erscheint nun in der Spalte **Neu**.

![Neue Aufgabe auf dem Taskboard hinzufügen](media/2ad339534d11d3d0d5a169a3f1bc413602dd40e2.png "Neue Aufgabe \(Taskboard\)")

## Aufgaben auf dem Taskboard bearbeiten

Klicken Sie auf eine Aufgabe auf dem Taskboard, um diese zu bearbeiten. Ein kleines Fenster öffnet sich. Nehmen Sie hier alle gewünschten Änderungen vor. Die Funktionsweise ist genauso wie oben für neue Aufgaben beschrieben.

![Eine Aufgabe auf dem Taskboard bearbeiten](media/115fc3f984993a05741edc45e9a2b94a75e07f93.png "Aufgabe bearbeiten \(Taskboard\)")

## Aufgaben auf dem Taskboard verschieben

Sie können Aufgaben ganz einfach per Drag-and-Drop zwischen den Zeilen und Spalten des Taskboards verschieben. Aufgaben können auf diesem Wege also auch einer anderen Story zugewiesen werden. Davon ausgenommen ist die obere Zeile, die Sprint-Hindernissen vorbehalten ist (siehe unten).

Klicken Sie auf eine Aufgabe, halten Sie die linke Maustaste gedrückt und verschieben Sie die Aufgabe an die gewünschte Stelle. Lassen Sie nun die Maustaste los.

![Aufgabe auf dem Taskboard verschieben](media/c9d8b2a1cc9bd4ade1062c750db8fe2f38be8b65.png "Aufgabe verschieben \(Taskboard\)")

## Sprint-Hindernisse

Die obere Tabellenzeile des Taskboards ist für Sprint-Hindernisse vorgesehen. Diese Hindernisse werden dem Scrum-Master zugewiesen, der sich um deren Beseitigung kümmert. Hindernisse werden genauso angelegt und bearbeitet wie andere Aufgaben, allerdings müssen Sie das zusätzliche Feld **IDs der geblockten Arbeitspakete** ausfüllen. Geben Sie hier die Identifikationsnummer von mindestens einer Aufgabe an, die vom Hindernis betroffen ist. Sie können auch mehrere Identifikationsnummern angeben und diese jeweils mit einem Komma voneinander trennen. Die Nummern der einzelnen Aufgaben finden Sie oben im Kästchen der Aufgabe auf dem Taskboard.

Hindernisse können nicht in andere Zeilen des Taskboards verschoben werden. Auch können keine Aufgaben aus anderen Zeilen in die Hinderniszeile verschoben werden.

![Bearbeitung einer Hindernisaufgabe, daneben die geblockten Aufgaben und weitere Hindernisse auf dem Taskboard](media/bd40faa8e5b2a87ea03b4f540118f6ad56fbcb95.png "Hindernis-Aufgabe")
