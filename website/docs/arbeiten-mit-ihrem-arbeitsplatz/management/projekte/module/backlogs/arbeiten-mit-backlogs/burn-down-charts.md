# Burndown-Charts

In einem Burndown-Chart können Sie den verbleibenden Aufwand der Aufgaben in Ihrem Sprint im Verhältnis zur verbleibenden Zeit darstellen. So erhalten Sie einen schnellen Überblick über Ihre Fortschritte und können rechtzeitig nachjustieren, falls Ihr Zeitplan zu knapp bemessen ist oder aber genügend Pufferzeit vorhanden ist, um zusätzliche Aufgaben in den Sprint aufzunehmen. Burndown-Charts sind ausschließlich für Sprints verfügbar, also für Versionen, die der linken Spalte in der Backlogs-Übersicht zugeordnet sind.

Sie können das Burndown-Chart für einen Sprint direkt aus der Backlogs-Übersicht heraus aufrufen. Öffnen Sie dazu das **Kontextmenü** rechts im Kasten des gewünschten Sprints (siehe auch [Funktionen der Backlogs-Übersicht](../funktionen-der-backlogs-uebersicht.md)) und wählen Sie **Burndown-Chart**.

![Beispiel für ein Burndown-Chart](media/2e0c0a8a6a7dbf34a89e0a7263625f79ea9ed4e5.png "Burndown-Chart")

## Aufbau von Burndown-Charts

Das Burndown-Chart besteht aus zwei Achsen. Die senkrechte Achse repräsentiert die Storypunkte, die auf aktuell verbleibende Aufgaben verteilt sind. Die waagerechte Achse repräsentiert die verbleibende Zeit. Jede senkrechte Linie im Raster des Diagramms repräsentiert ein festes Zeitintervall. Die Größe der Intervalle hängt davon ab, wie lang der Gesamtzeitraum für den Sprint oder das Backlog ist.

## Burndown-Charts interpretieren

Im Raster von Burndown-Charts werden zwei farbige Linien dargestellt.

- **Storypunkte (gelbe Linie)**: Diese Linie schätzt auf Grundlage des bisherigen Bearbeitungsfortschritts und der verbleibenden Storypoints ein, wie viel Zeit benötigt wird, um die offenen Aufgaben im Sprint zu bearbeiten. Die Linie berührt die waagerechte Achse bei dem Tag, an dem die Aufgaben nach aktueller Schätzung abgeschlossen sein werden.

- **Storypunkte (ideal) (violette Linie)**: Diese Linie stellt einen idealen Verlauf des Sprints dar. Dabei wird davon ausgegangen, dass genau die richtige Menge Storypoints vergeben ist, um bei gleichmäßiger Abarbeitung zum Enddatum des Sprints alle Aufgaben erledigt zu haben.  Ihr Ziel sollte sein, sich mit der gelben Linie möglichst an die violette Linie anzunähern. Verläuft die gelbe Linie oberhalb der violetten Linie, dann ist der aktuell vorgesehene Arbeitsaufwand voraussichtlich zu hoch und Ihr Team kann nicht alle Aufgaben zum Enddatum erledigen. Verläuft die gelbe Linie hingegen deutlich unterhalb der violetten Linie, dann bestehen womöglich noch Kapazitäten, um weitere Aufgaben in den Sprint aufzunehmen.

## Chart-Optionen

Unterhalb des Diagramms finden Sie den Abschnitt **Chart-Optionen**. Hier können Sie die Linien **Storypunkte** und **Storypunkte (ideal)** bei Bedarf einzeln ausblenden, indem Sie per Mausklick den Haken beim entsprechenden Punkt entfernen.
