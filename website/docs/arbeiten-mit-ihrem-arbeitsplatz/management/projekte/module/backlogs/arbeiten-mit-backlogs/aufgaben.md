# Aufgaben anlegen und bearbeiten

## Neue Aufgaben anlegen

Sie können direkt in der Backlogs-Übersicht neue Aufgaben zu Ihren Backlogs (z. B. zu einem Sprint) hinzufügen. Diese Aufgaben werden dann in der Backlogs-Übersicht aufgeführt, erscheinen aber auch als sogenannte Storys im **Taskboard**, wo Sie ihnen weitere Unteraufgaben zuweisen können.

Klicken Sie auf den kleinen Pfeil rechts im Kasten des jeweiligen Backlogs, um ein Kontextmenü zu öffnen. Wählen Sie dann den Punkt **Neue Aufgabe**.

![&quot;Neue Aufgabe&quot; im Kontextmenü für Backlogs](media/73a6cdc45ad1dbc91ec10eae5611d669590bc8bc.png "Neue Aufgabe")

Im Kasten des Backlogs wird eine neue Zeile für die Aufgabe angelegt. Hier können Sie einige Angaben zur Aufgabe machen, die Sie später jederzeit ändern können.

Das **linke Kontextmenü** in dieser Zeile können Sie beim Anlegen neuer Aufgaben ignorieren. Hier ist bereits ein Aufgabentyp voreingestellt.

Weiter rechts finden Sie ein weiteres **Kontextmenü**, in dem der Punkt **Neu** vorausgewählt ist. Hier können Sie den Status der Aufgabe einstellen. In aller Regel werden Sie bei neu angelegten Aufgaben den Status **Neu** beibehalten wollen. Sie können aber auch per Mausklick das Kontextmenü öffnen und als Status alternativ **In Bearbeitung**, **Erledigt**, **Gestoppt** oder **Zurückgewiesen** auswählen.

Geben Sie im größeren Textfeld links eine kurze, aber möglichst aussagekräftige **Bezeichnung** für die Aufgabe ein. Im rechten Textfeld können Sie sogenannte **Storypoints** vergeben (siehe unten).

Bestätigen Sie Ihre Eingaben mit der **Eingabetaste**. Die Aufgabe ist nun gespeichert. Sie können das Anlegen von Aufgaben auch mit der Taste **Escape** abbrechen.

![Angaben zu einer neuen Aufgabe machen](media/3f610d8e09e06a8e3927d073d3e582bb948fe492.png "Angaben zu einer neuen Aufgabe machen")

## Storypoints

Storypoints sind Zahlenwerte, die dazu dienen, den ungefähren Arbeitsaufwand einzelner Aufgaben im Verhältnis zueinander einzuschätzen. Die Verwendung von Storypoints ist optional. Sie können beim Anlegen neuer Aufgaben Storypoints vergeben und den Wert später jederzeit anpassen. Die Summe der Storypoints aller Aufgaben in einem Backlog wird automatisch berechnet und erscheint als Ziffer ganz rechts in der oberen Spalte des Backlogs.

Welche genauen Werte Sie wählen, ist Ihnen überlassen. Entscheidend ist, dass Sie in all Ihren Backlogs mit derselben Größenordnung arbeiten, damit die Arbeitsaufwände vergleichbar bleiben. Sie behalten so den Überblick darüber, welchen Anteil einzelne Aufgaben am Gesamtaufwand haben. Auch können Sie mithilfe von Storypoints den Aufwand verschiedener Backlogs in der Backlogs-Übersicht leicht vergleichen.

## Aufgaben bearbeiten

Die Aufgabenbezeichnung sowie den Zahlenwert der Storypoints können Sie jederzeit ändern, indem Sie auf das entsprechende Textfeld klicken.

![Aufgaben bearbeiten in der Backlogs-Übersicht](media/8572958f1b27f4bc9cb52381ab528d07c3d827e5.png "Aufgaben bearbeiten in der Backlogs-Übersicht")
