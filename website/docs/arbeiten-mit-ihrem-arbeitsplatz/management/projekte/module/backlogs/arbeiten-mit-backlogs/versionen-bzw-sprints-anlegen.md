# Versionen bzw. Sprints anlegen

Sprints und andere Arten von Backlogs werden im Backlogs-Menü als **Versionen** bezeichnet. Um mit der Benutzung von Backlogs beginnen zu können, muss mindestens eine Version vorhanden sein. Auch Sprints im Scrum-Sinne müssen als Versionen angelegt werden. Öffnen Sie zum Anlegen einer Version das Backlogs-Modul und klicken Sie auf die Schaltfläche **Neue Version (+ Version)** oben rechts.

![Die Schaltfläche &quot;Neue Version&quot; im Backlogs-Modul](media/7d5cf24e26b4a2718e2ebb0dd9843e1007017d83.png "Neue Version")

Sie gelangen auf die Seite **Neue Version**. Hier können Sie alle gewünschten Angaben zu Ihrer Version machen.

- **Name**: Geben Sie einen aussagekräftigen Namen für die Version ein (Pflichtfeld). Wenn Sie Sprints anlegen, sollten Sie diese zur besseren Übersicht chronologisch durchnummerieren.
- **Beschreibung**: Beschreiben Sie die Version bei Bedarf genauer.
- **Status**: Wählen Sie über das Kontextmenü zwischen den Optionen **offen**, **gesperrt** und **geschlossen**.
- **Wiki-Seite**: Falls es eine [Wiki-Seite](../../wiki.md) gibt, die zu Ihrem Projekt gehört und mit der Version verknüpft werden soll, können Sie diese im Kontextmenü auswählen.
- **Anfangstermin**: Klicken Sie in das Textfeld. Ein Kalender öffnet sich. Sie können den gewünschten Anfangstermin im Kalender auswählen oder von Hand eintippen. Wenn Sie den Termin von Hand eintippen, müssen Sie das Format Jahr-Monat-Tag verwenden (z. B. 2023-11-30 für den 30. November 2023).
- **Endtermin**: Gehen Sie beim Endtermin dabei genauso vor wie beim Anfangstermin.
- **Gemeinsame Verwendung**: Wählen Sie über das Kontextmenü zwischen den Optionen **Nicht gemeinsam verwenden**, **Mit Unterprojekten**, **Mit Projekthierarchie**, **Mit Projektbaum** und **Mit allen Projekten**. Je nach Einstellung wird Ihre Version entweder nur für das aktuelle Projekt verfügbar sein oder für eine größere Zahl von Projekten. Treffen Sie hier die passende Auswahl, je nachdem, ob Sie ein Backlog für Detailaufgaben in einem Unterprojekt anlegen möchten oder es um übergeordnete Aufgaben geht, die einen größeren Personenkreis betreffen.

**Spalte im Backlog**: Wählen Sie über das Kontextmenü aus, in welcher Spalte die Version erscheinen wird. Wählen Sie **links** für Sprints. Damit stehen Ihnen später zusätzliche Funktionen zur Verfügung. Produkt-/Bug-Backlogs, Wunschlisten und Ähnliches sollten **rechts** eingeordnet werden.  Klicken Sie auf **Anlegen**, sobald Sie alle gewünschten Angaben gemacht haben.

![Die Seite &quot;Neue Version&quot;](media/82d5f6c69933bfc318ff9ae121d3070d9572943c.png "Neue Version anlegen")

Nach dem Anlegen einer Version gelangen Sie in die Backlogs-Übersicht. Im Beispiel gibt es in jeder der beiden Spalten je zwei Versionen, die jedoch noch leer sind.

![Die zwei Spalten in der Backlogs-Übersicht](media/ee94c4f1a10dfce6c507db8ce1b3d8573dc9c6d6.png "Backlogs-Übersicht")

**Hinweis:** Backlog-Versionen und dazugehörige Aufgaben werden auch im Modul [Roadmap](../../roadmap.md) angezeigt.
