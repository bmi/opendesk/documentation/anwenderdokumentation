# Funktionen der Backlogs-Übersicht

Wenn Sie das Modul Backlogs öffnen, gelangen Sie in die Backlogs-Übersicht. Von hier aus können Sie direkt auf alle wichtigen Funktionen zugreifen.

**Hinweis:** Bei der ersten Benutzung ist das Modul noch leer. [Legen Sie eine neue Version](arbeiten-mit-backlogs/versionen-bzw-sprints-anlegen.md) an, um mit der Benutzung zu beginnen.

## Aufbau der Backlogs-Übersicht

Ganz oben neben der Überschrift **Backlogs** finden Sie die Schaltfläche [Neue Version (+ Version)](arbeiten-mit-backlogs/versionen-bzw-sprints-anlegen.md), mit der Sie Backlogs bzw. Sprints anlegen können. Ihre Backlogs werden in der Übersicht in zwei **Spalten** aufgeteilt. Beim Anlegen neuer Versionen können Sie auswählen, in welcher Spalte Ihr Backlog erscheinen soll.

Für Sprints sollten Sie die linke Spalte verwenden, in der Ihnen passende Zusatzfunktionen zur Verfügung stehen. Diese Spalte wird chronologisch sortiert, da Sprints stets aufeinander folgen.

Ordnen Sie andere Arten von Backlogs (z. B. Bug-Backlogs oder Wunschlisten mit optionalen Aufgaben) der rechten Spalte zu. Hier erfolgt die Sortierung alphabetisch.

Jedes Backlog wird in einem eigenen Kasten dargestellt, der eine Tabelle mit den einzelnen Aufgaben enthält.

![Die Backlogs-Übersicht](media/da3dff0d20cff03c374026a649249690dfd2d30b.png "Die Backlogs-Übersicht")

## Schaltflächen und Menüs

In der oberen Tabellenzeile finden Sie links und rechts je eine pfeilförmige Schaltfläche. Mit der **linken Schaltfläche** können Sie zur besseren Übersicht die Aufgaben des Backlogs ausblenden.

![Aufgaben eines Backlogs in der Übersicht ausblenden](media/322188dc6caa6d08f1dcc61b6f10d4a52583a178.png "Aufgaben ausblenden")

Die **rechte Schaltfläche** öffnet ein **Kontextmenü** mit verschiedenen Optionen. In der linken Spalte der Backlogs-Übersicht, die für Sprints vorgesehen ist, stehen Ihnen dabei drei Zusatzfunktionen zur Verfügung.

- **Neue Aufgabe**: Fügen Sie dem Backlog eine [neue Aufgabe](arbeiten-mit-backlogs/aufgaben.md) hinzu.
- **Stories/Aufgaben**: Öffnen Sie die [Arbeitspakettabelle](../arbeitspakete.md) mit einer Detailansicht der Aufgaben in diesem Backlog.
- **Taskboard** (nur Sprints): Öffnet das [Taskboard](arbeiten-mit-backlogs/das-taskboard.md). Auf dem Taskboard können Sie Aufgaben auf übersichtliche und intuitive Weise verwalten.
- **Burndown-Chart** (nur Sprints): Erzeugt ein [Burndown-Chart](arbeiten-mit-backlogs/burn-down-charts.md) für das Backlog. In einem solchen Diagramm können Sie den verbleibenden Aufwand der Aufgaben in Ihrem Backlog im Verhältnis zur verbleibenden Zeit darstellen.
- **Wiki** (nur Sprints): Falls es eine [Wiki-Seite](../wiki.md) gibt, die zu Ihrem Projekt gehört und mit dem Backlog verknüpft ist, können Sie diese hier aufrufen.
- **Eigenschaften**: Ändern Sie die Angaben, die Sie beim [Anlegen der Version](arbeiten-mit-backlogs/versionen-bzw-sprints-anlegen.md) vorgenommen haben.

![Kontextmenü links](media/5f4dfc392b9b4dd0bb3c5138f777567d8f95f997.png "Kontextmenü links")

![Kontextmenü links](media/fe7c7f15c542000919246a4e17eb9d5b1d3f9920.png "Kontextmenü links")

## Die obere Tabellenzeile

In der oberen Tabellenzeile finden Sie von links nach rechts den **Namen** des Backlogs, das **Anfangs- und Enddatum** im Format Jahr-Monat-Tag sowie die **Summe der Storypoints**, die allen Aufgaben im Backlog zugeordnet wurden. Näheres zu Storypoints erfahren Sie unter [Aufgaben](arbeiten-mit-backlogs/aufgaben.md).

Klicken Sie auf das Anfangs- oder Enddatum, um dieses zu ändern. Ein Kalender öffnet sich. Sie können den gewünschten Anfangstermin im Kalender auswählen oder von Hand eintippen. Wenn Sie den Termin von Hand eintippen, müssen Sie das Format Jahr-Monat-Tag verwenden (z. B. 2023-11-30 für den 30. November 2023). Bestätigen Sie Ihre Auswahl mit der **Eingabetaste**.

![Anfangs- und Enddatum des Backlogs ändern](media/21fd71a15fbc6c5d4e3522481517f5fba691ec21.png "Anfangs- und Enddatum des Backlogs ändern")

## Tabellenzeilen für Aufgaben

Jede weitere Zeile der Tabelle repräsentiert je eine Aufgabe. Hier finden Sie von links nach rechts die **Aufgabennummer** (diese wird automatisch fortlaufend vergeben), den **Aufgabentyp**, die **Aufgabenbezeichnung**, den **Status** sowie die **Storypoints**, die der Aufgabe zugewiesen wurden. Näheres zu Storypoints finden Sie unter [Aufgaben](arbeiten-mit-backlogs/aufgaben.md).

Die Aufgabenbezeichnung sowie den Zahlenwert der Storypoints können Sie jederzeit ändern, indem Sie auf das entsprechende Textfeld klicken. Machen Sie dann die gewünschten Eingaben und bestätigen Sie diese mit der **Eingabetaste**.

![Aufgaben in der Backlogs-Übersicht bearbeiten](media/8572958f1b27f4bc9cb52381ab528d07c3d827e5.png "Aufgaben in der Backlogs-Übersicht bearbeiten")

## Aufgaben verschieben

Sie können Aufgaben in der Backlogs-Übersicht ganz einfach per Drag-and-Drop verschieben. Dabei können Sie die Aufgaben einerseits innerhalb eines Sprints bzw. Backlogs nach oben oder unten verschieben, um sie neu zu priorisieren. Sie haben andererseits aber auch die Möglichkeit, Aufgaben zwischen verschiedenen Sprints bzw. Backlogs zu verschieben. Klicken Sie dazu auf eine Aufgabe, halten Sie die linke Maustaste gedrückt und verschieben Sie die Aufgabe an die gewünschte Stelle. Lassen Sie nun die Maustaste los.

![Eine Aufgabe in der Backlogs-Übersicht verschieben](media/f4ca360e360e07aad9d749899c09d5569bb97d4d.png "Aufgabe verschieben \(Übersicht\)")
