# Aktivitäten

Erstellen Sie sich eine Übersicht aller aktuellen Aktivitäten innerhalb eines Projektes. Dazu gehören Änderungen zu:
- Arbeitspaketen (neue Arbeitspakete, neue Kommentare, Statusänderungen, Zugewiesen an, Datum, benutzerdefinierte Felder…)
- Projekt-Attributen (Name, Beschreibung, benutzerdefinierte Felder...)

andere Modulen (News, Budget, Wiki-Bearbeitungen, Forum-Nachrichten, gebuchte Zeiten…)  Bitte beachten Sie: Damit Sie die Projektaktivität angezeigt bekommen, muss zunächst das Modul **Aktivität** aktiviert werden. Navigieren Sie in den **Projektkonfigurationen** zu **Module**. Setzen Sie ein Häkchen bei **Aktivität** und klicken Sie abschließend auf **Speichern**.

![Projektkonfiguration-für-Aktivitäten-aktivieren](media/315cb5b795a8c329b32fc14e358550235a5affba.jpg "Projektkonfiguration-für-Aktivitäten-aktivieren")

## Projektaktivität anzeigen

![Aktivität am Arbeitspaket](media/b5546eac4e09e6c38992752239226a82b7fc7ef1.jpg "Aktivität am Arbeitspaket")

## Projektaktivität filtern

Im Seitenmenü der Aktivitätsliste finden Sie verschiedene Filter. Sie können diese einzeln anwenden oder kombinieren um sich die entsprechenden Änderungen anzeigen zu lassen. Klicken Sie abschließend auf die Schaltfläche **Anwenden**. Zur Auswahl stehen:

- Arbeitspakete
- Aufgewendete Zeit
- Besprechungen
- Dokumente
- Foren
- Neuigkeiten
- Wiki
- Änderungssätze

Projekt-Attribute  Bitte beachten Sie, dass nur aktivierte Module (diese finden Sie in den **Projektkonfigurationen**) gefiltert werden können. Nicht aktivierte Module sind in der Filterauswahl nicht sichtbar.

![Aktivitäten-Filter-Auswahl](media/5e052ac7427cca5dc37f28ab33fddb775b0cd634.jpg "Aktivitäten-Filter-Auswahl")

## Wie weit können Projektaktivitäten zurückverfolgt werden?

Die Anzeige für die Aktivitäten eines Projekts ist nicht begrenzt. Sie können also alle Aktivitäten seit Projektbeginn einsehen. Als Adminstratorin oder Administrator können Sie einstellen, wie viele vergangene Tage angezeigt werden sollen. Es ist empfehlenswert, ein niedriges Niveau (etwa 7 Tage) zu wählen, um lange Ladezeiten zu vermeiden.

**Hinweis:** Die Liste der Projektaktivitäten ist nummeriert. Nutzen Sie **Zurück** oder **Weiter** am Ende der Seite, um zwischen den Seiten zu navigieren.

![Aktivität-Blättern](media/71f7b265c1c1667c456291f7523ef5ce5216d494.jpg "Aktivität-Blättern")

## Arbeitspaket-Aktivität

Klicken Sie auf die ID eines Arbeitspakets, so teilt sich der Bildschirm. Auf der rechten Seite werden Ihnen die Aktivitäten des Arbeitspakets aufgezeigt. Sie sehen dort neben den Aktivitäten auch Nachrichten und andere Änderungen aufgelistet. So können Sie und andere Nutzerinnen und Nutzer leicht nachverfolgen, wer wann welche Änderungen am Arbeitspaket vorgenommen hat. Sie können im Textfeld auch Kommentare verfassen und mit anderen Nutzerinnen und Nutzern Informationen teilen. Nutzen Sie das @-Symbol um Teammitglieder direkt zu makieren. Diese werden dann entsprechend benachrichtigt.

![Aktivität im Arbeitspaket](media/1fb37a1ffc1af28e4bb399d58e5a997253a228e8.jpg "Aktivität im Arbeitspaket")
