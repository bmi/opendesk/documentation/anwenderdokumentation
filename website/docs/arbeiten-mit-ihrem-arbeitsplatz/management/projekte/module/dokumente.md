# Dokumente

## Dokumente

Dieses Modul erlaubt das direkte Hinzufügen von Dokumenten zu Ihrem Projekt unter dem Menüpunkt **Dokumente** im Projektbereich.

## Neues Dokument dem Projekt hinzufügen

Um ein Dokument hochzuladen, gehen Sie zum Projektmenü und wählen Sie die Option **Neues Dokument** aus. Sie können die Kategorie für das Dokument auswählen, die Sie zuvor in den Einstellungen der Projektverwaltung festgelegt haben.

Geben Sie dem Dokument einen Namen und fügen Sie eine kurze Beschreibung hinzu. Nachdem Sie die Datei hochgeladen haben, vergessen Sie nicht, auf **Speichern** zu klicken.

![Neues Dokument hinzufügen](media/b6f2e3edda133770d9c322b07351bde0ab4fa4d4.jpg "Neues Dokument hinzufügen")

Die hochgeladenen Dokumente können von allen Projektmitgliedern mit den entsprechenden Zugriffsrechten eingesehen werden.

**Hinweis**: Es gibt keine Versionierung von Dokumenten. Es handelt sich lediglich um ein Hochladen von Dokumenten in das jeweilige Projekt.

## Projektdokument bearbeiten oder löschen

Sie haben die Möglichkeit, Dokumente nach Belieben zu bearbeiten oder zu entfernen. Dazu navigieren Sie zur Übersicht der Dokumente und wählen das entsprechende Dokument aus. Durch die Auswahl der Optionen **Bearbeiten** oder **Löschen** können Sie entweder die Dokumentdatei und die dazugehörigen Informationen anpassen oder die Datei dauerhaft entfernen. Bei Bedarf können Sie die Datei zu einem späteren Zeitpunkt erneut hinzufügen.

![Projektdokument bearbeiten oder löschen](media/497d41675d382d8782100019bebd569f2fd02333.jpg "Projektdokument bearbeiten oder löschen")
