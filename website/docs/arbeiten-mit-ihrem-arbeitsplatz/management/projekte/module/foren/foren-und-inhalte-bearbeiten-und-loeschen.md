# Foren und Inhalte bearbeiten und löschen

## Foren bearbeiten und löschen

Um Foren zu bearbeiten oder zu löschen, öffnen Sie die Forenliste mit einem Klick auf **Foren** im Hauptmenü links. Für jedes Forum stehen Ihnen ganz rechts in der Liste die Schaltflächen **Bearbeiten (Stiftsymbol)** und **Löschen (Papierkorbsymbol)** zur Verfügung.

![Bearbeiten und Löschen in der Forenliste](media/085e3f04a534c3266997de08be5c4d207e48f190.png "Bearbeiten und Löschen in der Forenliste")

Wenn Sie **Bearbeiten** wählen, können Sie den Namen und die Beschreibung des gewünschten Forums anpassen, wie unter **Neues Forum anlegen** beschrieben.

Wenn Sie **Löschen** wählen, erscheint ein Dialog mit der Frage **Sind Sie sicher?**. Bestätigen Sie mit Klick auf **OK**, dass Sie das Forum löschen möchten.

## Themen und Antworten bearbeiten und löschen

Um Themen zu bearbeiten oder zu löschen, verwenden Sie die Schaltflächen **Bearbeiten** und **Löschen** ganz oben im Thema.

Um Antworten zu bearbeiten oder zu löschen, verwenden Sie die kleinen Schaltflächen **Bearbeiten (Stiftsymbol)** und **Löschen (Papierkorbsymbol)** rechts neben der Antwort.

![Die Schaltflächen &quot;Bearbeiten&quot; und &quot;Löschen&quot; für Themen und Antworten](media/be0591a0e511775cdf7668736350ab58137490ab.png "Bearbeiten und Löschen von Inhalten")

Wenn Sie **Bearbeiten** wählen, gelangen Sie in den Editor und können Ihre Inhalte bearbeiten, wie unter **Neues Forum anlegen** beschrieben.

Wenn Sie **Löschen** wählen, erscheint ein Dialog mit der Frage **Sind Sie sicher?**. Bestätigen Sie mit Klick auf **OK**, dass Sie das Forum löschen möchten.
