# Das Foren-Modul aktivieren und aufrufen

Foren bieten Ihnen die Möglichkeit, sich mit Ihrem Team in übersichtlicher Weise über Themen auszutauschen.

Um Foren nutzen zu können, muss das entsprechende Modul zunächst in der Projektkonfiguration aktiviert werden. Navigieren Sie dazu im Hauptmenü links zum Punkt **Projektkonfiguration** ganz unten.

![Der Punkt &quot;Projektkonfiguration&quot; im Hauptmenü](media/be3626caf0ad0c17b6c834da28f6b17fd7c9dda2.png "Projektkonfiguration im Hauptmenü")

Navigieren Sie nun im Menü links unter **Projektkonfiguration** zum Punkt **Module**. Dort können Sie Module hinzufügen, indem Sie einen Haken neben den Modulnamen setzen. Setzen Sie einen Haken bei **Foren** und klicken Sie auf **Speichern**.

![Auswählen von Modulen in der Projektkonfiguration](media/1c3e872e35b9008bbe4b06236b24fe0645ef5eb0.jpg "Auswählen von Modulen in der Projektkonfiguration")

Ab sofort können Sie das Foren-Modul über den entsprechenden Punkt im Hauptmenü links auf der Seite auswählen. Sie gelangen dann direkt auf die Forenliste.

**Hinweis:** Bei der ersten Benutzung ist die Forenliste noch leer.

![Der Punkt &quot;Foren&quot; im Hauptmenü](media/8e794102de3c96e1169425303ac2c5f36b36095a.png "Foren im Hauptmenü")
