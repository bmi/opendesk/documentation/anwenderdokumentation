# Neue Themen eröffnen

Um ein neues Thema in einem Forum zu eröffnen, wählen Sie zunächst das gewünschte Forum aus der Forenliste aus (siehe **Foren öffnen und sortieren**). Klicken Sie dann auf die Schaltfläche **Neues Thema (+ Nachricht)** oben rechts.

![Die Schaltfläche &quot;Neues Thema&quot; im Beispielforum](media/84932026b6a76c3b7b63ec774cd8365e4335d8d8.png "Schaltfläche &quot;Neues Thema&quot;")

Sie gelangen in einen Editor. Hier stehen Ihnen eine Reihe von Textfeldern und Optionen zur Verfügung. Die mit einem Asterisk (*) markierten Textfelder **Thema** und **Nachrichteninhalt** müssen ausgefüllt werden, damit Sie Ihr Thema eröffnen können.

- **Thema**: Geben Sie hier einen aussagekräftigen Titel für Ihr Thema ein. Dieser wird später auf der Themenliste des Forums angezeigt.
- **Angepinnt**: Mit dieser Funktion können Sie besonders wichtige Themen ganz oben auf der Themenliste des Forums anheften. Andere Sortierungen werden dann für dieses Thema ignoriert. Dies ist z. B. hilfreich bei Themen, in denen Sie Regeln für das Forum aufstellen oder andere Informationen bekanntgeben, die jederzeit relevant sind. Klicken Sie auf das Kästchen rechts, um die Funktion zu aktivieren.
- **Gesperrt**: Mit dieser Funktion können Sie verhindern, dass andere Nutzerinnen und Nutzer Kommentare zu Ihrem Thema hinzufügen. Dies ist z. B. hilfreich bei Themen, die lediglich der Bekanntgabe von Informationen dienen und bei denen keine Diskussion vorgesehen ist. Nutzen Sie die Funktion nachträglich bei Themen, die abgeschlossen sind und nicht weiter diskutiert werden sollen (siehe **Foren und Inhalte bearbeiten**). Klicken Sie auf das Kästchen rechts, um die Funktion zu aktivieren.
- **Nachrichteninhalt**: Geben Sie hier den eigentlichen Inhalt Ihres Themas ein. Geben Sie z. B. Informationen bekannt, die für andere Nutzerinnen und Nutzer relevant sind, oder stellen Sie eine Frage zur Diskussion. Hierbei stehen Ihnen über eine Werkzeugleiste verschiedene Funktionen zur Formatierung und Einbettung von Inhalten zur Verfügung, wie sie auch in der Textverarbeitung zum Einsatz kommen. Näheres dazu finden Sie unter **Inhalte formatieren und einbetten**.
- **Anhänge**: Hier können Sie Dateien eines beliebigen Typs hochladen, die für Ihr Thema relevant sind, z. B. Bilder, die Ihre Inhalte veranschaulichen. Eine kurze Anleitung dazu finden Sie weiter unten auf dieser Seite.  Klicken Sie abschließend auf **Anlegen**. Ihr neues Thema wird daraufhin geöffnet und erscheint fortan in der Themenliste des Forums.

![Der Themeneditor im Überblick](media/d0d3d6d28d2524a7cb346dfc8b59583db82272ca.png "Der Themeneditor")

## Anhänge

Wenn Sie Dateien an Ihr Thema anhängen möchten, steht Ihnen der Abschnitt **Anhänge** unten im Editor zur Verfügung. Hier können Sie zwischen folgenden Vorgehensweisen wählen:

- Öffnen Sie Ihren Dateibrowser in einem separaten Fenster. Navigieren Sie zum Speicherort der gewünschten Datei und ziehen Sie diese per Drag-and-Drop in den gestrichelten Kasten mit der Beschriftung **Dateien hier ablegen oder anklicken, um Dateien anzuhängen**.

Klicken Sie auf den gestrichelten Kasten mit der Beschriftung **Dateien hier ablegen oder anklicken, um Dateien anzuhängen** oder auf die Schaltfläche **Dateien anhängen**. Der Dateibrowser öffnet sich. Navigieren Sie zum Speicherort der gewünschten Datei und öffnen Sie diese mit einem Doppelklick. Alternativ können Sie die Datei auch mit einem einfachen Mausklick auswählen und dann auf **Öffnen** klicken.  Dateien, die Sie auf diese Weise hinzugefügt haben, werden im Abschnitt **Anhänge** aufgelistet. Wenn Sie den Mauszeiger auf die Höhe eines Anhanges bewegen, können Sie diesen mit einem Mausklick öffnen. Außerdem erscheint am rechten Rand die Schaltfläche **Löschen (Papierkorbsymbol)**, mit der Sie den Anhang wieder entfernen können.

![Der Abschnitt &quot;Anhänge&quot; mit einem Beispielanhang sowie der Löschen-Schaltfläche](media/a436a5ac0553d0d646a13cd9ea586644db59e47c.png "Anhänge")
