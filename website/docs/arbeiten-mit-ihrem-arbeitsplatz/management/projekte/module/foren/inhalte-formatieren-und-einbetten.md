# Inhalte formatieren und einbetten

## Übersicht

Wenn Sie Themen und Kommentare hinzufügen oder bearbeiten, stehen Ihnen verschiedene Möglichkeiten zur Verfügung, Inhalte zu formatieren und einzubetten. Über dem Textfeld **Nachrichteninhalt** finden Sie eine Werkzeugleiste, die Ihnen folgende Optionen bietet:

- **Absatz**: Wählen Sie aus Formatvorlagen für Überschriften oder Absätze. Eine kurze Anleitung dazu finden Sie weiter unten.
- **Fett**: Markieren Sie Teile Ihres Textes fett
- **Kursiv**: Markieren Sie Teile Ihres Textes kursiv
- **Durchgestrichen**: Streichen Sie Teile Ihres Textes durch
- **Code**: Fügen Sie Code in Ihr Thema ein. Dies ist eine Funktion für erfahrene Nutzerinnen und Nutzer.
- **Code-Snippet einfügen**: Fügen Sie ein Code-Snippet ein. Hier können Sie in einem neuen Fenster neben dem eigentlichen Code auch die Sprache für die Syntaxhervorhebung eingeben. Dies ist eine Funktion für erfahrene Nutzerinnen und Nutzer.
- **Link**: Fügen Sie Links ein, über die Nutzerinnen und Nutzer auf eine andere Internetseite gelangen können.
- **Aufzählungsliste**: Erstellen Sie eine Liste mit Aufzählungszeichen. Verwenden Sie dabei jeweils die Eingabetaste, um in die nächste Zeile zu gelangen. Wenn Sie alle Punkte Ihrer Aufzählungsliste eingegeben haben und zu normalem Fließtext zurückkehren möchten, drücken Sie zweimal die Eingabetaste.
- **Nummerierte Liste**: Erstellen Sie eine nummerierte Liste. Gehen Sie dabei genauso vor wie bei Aufzählungslisten.
- **Aufgabenliste**: Erstellen Sie eine Liste mit Aufgaben, die nach Erledigung abgehakt werden können. Jeder Punkt auf der Liste erhält ein Kästchen, in dem Sie später per Mausklick einen Haken setzen können. Gehen Sie beim Anlegen der Liste genauso vor wie bei Aufzählungslisten.
- **Bild einfügen**: Fügen Sie Bilder ein. Eine kurze Anleitung dazu finden Sie weiter unten auf dieser Seite.
- **Blockzitat**: Heben Sie einen bestimmten Textabschnitt als Blockzitat hervor. Wenn Sie zu normalem Fließtext zurückkehren möchten, drücken Sie zweimal die Eingabetaste.
- **Tabelle einfügen**: Fügen Sie eine Tabelle ein.
- **Makros**: Makros sind automatisierte Funktionsabläufe, die Ihnen die Arbeit erheblich erleichtern können. Beispielsweise können Sie mithilfe eines Makros automatisch ein Inhaltsverzeichnis für Ihre Seite anlegen, statt von Hand eine entsprechende Übersicht mit Verweisen zu jeder einzelnen Überschrift zu erstellen.
- **Rückgängig** und **Wiederherstellen**: Machen Sie Ihre letzten Arbeitsschritte rückgängig oder stellen Sie Arbeitsschritte wieder her, die Sie rückgängig gemacht haben.
- **Hilfe zur Textformatierung**: Diese Schaltfläche führt auf eine englischsprachige Internetseite mit Hinweisen zur Textformatierung. In der Regel werden Sie diese Funktion nicht benötigen, wenn Sie sich an den Hinweisen in dieser Anleitung orientieren.
- **Vorschau-Modus ein/aus**: Wechseln Sie in einen Vorschaumodus, um zu sehen, wie Ihr Thema oder Kommentar nach der Veröffentlichung aussehen wird. In dieser Ansicht können Sie keine Änderungen vornehmen. Um Ihren Inhalt weiter zu bearbeiten, klicken Sie erneut auf die Schaltfläche.
- **Wechseln zu reinem Markdown-Text**: Ihre Formatierungen sind im Hintergrund als sogenannter Markdown-Text hinterlegt. Dabei handelt es sich um eine Art vereinfachten Quellcode. Sie können sich den Markdown-Text mit dieser Funktion anzeigen lassen und ihn direkt bearbeiten. Dies ist eine Funktion für erfahrene Nutzerinnen und Nutzer.

![Wergzeuge zum Formatieren und Einbetten von Inhalten](media/d0d3d6d28d2524a7cb346dfc8b59583db82272ca.png "Wergzeuge zum Formatieren und Einbetten von Inhalten")

## Absatz

Mit Klick auf **Absatz** können Sie ein Kontextmenü öffnen, das Ihnen die Auswahl zwischen verschiedenen Formatvorlagen bietet. Neben einfachen Absätzen können Sie hier Überschriften mit verschiedenen Hierarchie-Ebenen auswählen, um Ihren Text zu gliedern. Überschrift 1 ist hierbei die höchste Hierarchie-Ebene, verwenden Sie diese Formatvorlage also für die Hauptüberschrift Ihres Themas.

![Kontextmenü Absatz](media/afea8cca740b9cc26090831e6ae69cb2569c7683.png "Kontextmenü Absatz")

## Links einfügen

Sie können Links in Ihren Text einfügen. Nutzerinnen und Nutzer können auf einen solchen Link klicken, um direkt auf eine gewünschte Internetseite zu gelangen.

Navigieren Sie hierfür zunächst zu der gewünschten Internetseite. Wählen Sie dann die URL in der Adresszeile Ihres Internetbrowsers aus und drücken Sie die Tastenkombination **Strg + C**. Sie haben nun folgende Möglichkeiten:

- Sie können die Adresse der Internetseite direkt in den Nachrichteninhalt einfügen. Klicken Sie dazu in die gewünschte Stelle Ihres Textes und dann auf die Schaltfläche **Link** in der Werkzeugleiste.

Sie können ein Wort oder einen längeren Textabschnitt auswählen, auf den Nutzerinnen und Nutzer klicken sollen, um zur gewünschten Internetseite zu gelangen. Dies ist meist übersichtlicher. Markieren Sie dazu den gewünschten Textabschnitt bei gedrückter Maustaste und kllcken Sie dann auf die Schaltfläche **Link** in der Werkzeugleiste.  Ein Textfeld mit der Beschriftung **Link-Adresse** erscheint. Klicken Sie in dieses Textfeld und fügen Sie den Link mit der Tastenkombination **Strg + V** ein. Drücken Sie dann die Eingabetaste oder klicken Sie auf die Schaltfläche **Speichern (grüner Haken)** neben dem Textfeld, um den Link einzufügen.

![Einen Link einfügen](media/eddf1fa41070a17e73492f0b37677f254b71dae4.png "Einen Link einfügen")

## Bilder einfügen

Sie können Bilder einfügen, die auf Ihrem Computer gespeichert sind. Klicken Sie dazu auf die Schaltfläche **Bild einfügen** in der Werkzeugleiste. Daraufhin öffnet sich der Dateibrowser. Navigieren Sie zum Speicherort des gewünschten Bildes und öffnen Sie das Bild mit einem Doppelklick. Alternativ können Sie das Bild auch mit einem einfachen Klick auswählen und dann auf **Öffnen** klicken.

Ihr Bild wird nun in das Textfeld eingefügt. Daraufhin stehen Ihnen folgende Optionen zur Verfügung, auf die Sie über den Rahmen bzw. eine kleine Werkzeugleiste zugreifen können:

- **Bild vergrößern/verkleinern**: Bewegen Sie den Mauszeiger auf eines der kleinen Quadrate in den Bildecken. Halten Sie die linke Maustaste gedrückt. Bewegen Sie nun Ihre Maus nach oben, um das Bild zu vergrößern, oder nach unten, um das Bild zu verkleinern. Das Seitenverhältnis des Bildes bleibt dabei erhalten.
- **Tabellenüberschrift aktivieren**: Mit dieser Schaltfläche öffnen Sie ein Textfeld unter dem Bild. Dort können Sie eine Bildunterschrift eingeben.
- **Alternativtext ändern**: Mit dieser Schaltfläche öffnen Sie ein Textfeld, in das Sie einen Alternativtext für das Bild eingeben können. Beschreiben Sie im Alternativtext kurz und verständlich, was im Bild zu sehen ist. Alternativtexte sind nicht sichtbar, werden aber für die Auffindbarkeit in Suchmaschinen sowie für die Lesbarkeit mit Screenreadern zur Barrierefreiheit benötigt. Wenn Sie mit der Eingabe fertig sind, klicken Sie auf die Schaltfläche **Speichern (grüner Haken)**.
- **Text in Zeile**: Hiermit stellen Sie ein, dass sich Text und Bild dieselbe Zeile teilen können. Es kann also eine Zeile Text direkt neben Ihrem Bild erscheinen. Dies kann hilfreich sein, wenn Bilder möglichst nahtlos in den Textfluss eingebunden werden sollen.
- **Zentriertes Bild**: Hiermit stellen Sie ein, dass Text und Bild ausschließlich übereinander erscheinen können. Am oberen und unteren Rand des Bildes finden Sie daraufhin die Schaltflächen **Absatz vor Block einfügen** und **Absatz nach Block einfügen**. Nutzen Sie diese, um oberhalb oder unterhalb des Bildes neue Textabsätze hinzuzufügen.
- **Bild in Originalgröße ändern**: Wenn Sie das Bild zuvor vergrößert oder verkleinert haben, können Sie mit dieser Schaltfläche die Originalgröße des Bildes wiederherstellen.

![Ein eingefügtes Bild mit Rahmen und Werkzeugleiste](media/82b5f22cb4abd95261aa37442cfd084fc5d6cb61.png "Bild einfügen")

![Ein zentriertes Bild mit entsprechenden Schaltflächen und Beispielabsätzen](media/06c3716410923200c577c873616f4071307385c3.png "Zentriertes Bild")
