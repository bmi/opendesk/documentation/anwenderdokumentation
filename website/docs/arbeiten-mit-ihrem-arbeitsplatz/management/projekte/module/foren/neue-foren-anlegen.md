# Neue Foren anlegen

Wenn Sie das Foren-Modul gerade erst aktiviert haben, sind noch keine Foren vorhanden. Erstellen Sie nun Ihr erstes Forum, um das Modul zu nutzen. Klicken Sie dazu auf die Schaltfläche **Neues Forum** oben rechts. Sie können später jederzeit weitere Foren hinzufügen

![Die Schaltfläche &quot;Neues Forum&quot;](media/4b823a4d4b708247ff6b1f2262d1ce645cf29f46.png "Neues Forum")

Sie gelangen auf eine neue Seite. Geben Sie in den entsprechenden Textfeldern einen aussagekräftigen Namen sowie eine Beschreibung für Ihr Forum ein. Klicken Sie danach auf **Anlegen**.

![Name und Beschreibung eines neuen Forums eingeben](media/e935069f84c29d5208187aac0854159818d9d404.png "Neues Forum anlegen")

Ihr neues Forum ist nun angelegt und erscheint in der Forenliste. Von dort aus können Sie jederzeit darauf zugreifen.

![Das neue Forum in der Forenübersicht](media/085e3f04a534c3266997de08be5c4d207e48f190.png "Forenübersicht")
