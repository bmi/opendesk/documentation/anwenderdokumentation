# Foren öffnen und sortieren

Um ein Forum zu öffnen, klicken Sie in der Forenliste auf den Namen des gewünschten Forums.

![Ein Forum durch Klick auf den Namen öffnen](media/3b80f3ab9328dcbc3b5e49bcd7cd8d0b29daef37.png "Forum öffnen")

Oben links sehen Sie nun den Namen und die Beschreibung des Forums. Über die Schaltflächen oben rechts können Sie ein **Neues Thema (+ Nachricht)** hinzufügen oder das Forum **Beobachten**.

Weiter unten finden Sie eine Liste der Themen, die aktuell im ausgewählten Forum vorhanden sind. Hier sehen Sie auf den ersten Blick, von wem und zu welchem Zeitpunkt die Themen erstellt wurden und wie viele Antworten es gibt. Mit einem Mausklick auf die entsprechende Überschrift können Sie die Themen nach Erstelldatum (**Erstellt**) oder Anzahl der **Antworten** sortieren. Wenn Sie die Überschrift **Letzter Forenbeitrag** auswählen, ist das Datum der letzten Antwort auf ein Thema ausschlaggebend.

![Übersicht eines Beispielforums](media/84932026b6a76c3b7b63ec774cd8365e4335d8d8.png "Beispielforum")
