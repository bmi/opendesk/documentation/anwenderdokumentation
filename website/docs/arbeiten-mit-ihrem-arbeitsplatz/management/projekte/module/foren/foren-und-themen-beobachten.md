# Foren und Themen beobachten

Sie können ganze Foren oder einzelne Themen beobachten, um jederzeit über neue Beiträge auf dem Laufenden zu bleiben. Foren und Themen, die Sie selbst angelegt haben, beobachten Sie automatisch. Sie können diese Einstellung aber jederzeit ändern.

Öffnen Sie das gewünschte Forum bzw. Thema. Klicken Sie oben rechts auf die Schaltfläche **Beobachten**, um das Forum bzw. Thema zu beobachten, oder aber auf **Nicht beobachten**, um eine bereits aktivierte Beobachtung aufzuheben.

![Die Schaltfläche &quot;Beobachten&quot;](media/1e9680a4b3b5be0a99947c4ff2e9714fe52b471c.png "Beobachten")

![Die Schaltfläche &quot;Nicht beobachten&quot;](media/7485c5001c00bac258a8cce82d7313cd9f048600.png "Nicht beobachten")

Sie erhalten E-Mail-Benachrichtigungen über neue Inhalte in beobachteten Foren und Themen. Um Ihre E-Mail-Benachrichtigungen einzusehen, wechseln Sie in das E-Mail-Modul. Näheres dazu finden Sie in der Anleitung zum E-Mail-Modul.

Öffnen Sie die Benachrichtigungen in der E-Mail-Liste genauso wie jede andere E-Mail. In der E-Mail finden Sie den Inhalt des neuen Themas oder der neuen Antwort sowie einen Direktlink dorthin.

![Eine E-Mail-Benachrichtigung zu einem neuen Thema in einem beobachteten Forum](media/2d22fcb7897de50a088fa849bd53277b1d7f4073.png "E-Mail-Benachrichtigung")
