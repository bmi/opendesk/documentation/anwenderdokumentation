# Auf ein Thema antworten

Um auf ein Thema zu antworten, öffnen Sie zunächst das gewünschte Thema, indem Sie in der Themenübersicht auf den Titel klicken.

![Ein Thema in der Themenübersicht auswählen und öffnen](media/414249fe7e524578a7661734fe81296d90c1b051.png "Thema öffnen")

Scrollen Sie nun im Thema ganz nach unten. Dort finden Sie einen Editor, der genauso funktioniert wie beim Eröffnen eines Themas (Näheres dazu finden Sie unter **Neue Themen eröffnen**). Im Textfeld **Thema** ist hier bereits ein Betreff vorausgefüllt, den Sie jedoch nach Belieben anpassen können.

Geben Sie Ihre Antwort ein, nehmen Sie gewünschte Formatierungen vor und fügen Sie bei Bedarf Links, Bilder usw. ein. Klicken Sie abschließend auf die Schaltfläche **OK**, um Ihre Antwort zu speichern.

![Der Editor beim Antworten auf ein Thema](media/4884ab1db1f58179ee673fec63e58bd079c21666.png "Auf ein Thema antworten")

## Zitieren

Sie können andere Beiträge zitieren, um in Ihrer Antwort direkt darauf Bezug zu nehmen. Wenn Sie den Ursprungstext des Themas zitieren möchten, klicken Sie ganz oben im Thema auf die Schaltfläche **Zitieren**. Wollen Sie hingegen eine Antwort auf das Thema zitieren, klicken Sie auf die kleinere Schaltfläche **Zitieren (Sprechblasen-Symbol)** rechts neben der Antwort.

![Die Zitieren-Schaltflächen](media/b1a5436f26bd5d6fd97254b857e0fe3862fec719.png "Die Zitieren-Schaltflächen")

Scrollen Sie nun im Thema ganz nach unten. Im Textfeld **Nachrichteninhalt** wurde der gewünschte Beitrag als Blockzitat eingefügt. Bei Bedarf können Sie das Zitat beliebig kürzen.

**Hinweis:** Fügen Sie Zitate immer zu Beginn ein. Wenn Sie zuvor bereits Eingaben im Textfeld **Nachrichteninhalt** gemacht haben, werden diese durch das Zitat ersetzt.

![Beispiel für ein Zitat im Editor](media/3a1a163630a26b0f56c86a33cb4314a5748e093a.png "Beispielzitat")
