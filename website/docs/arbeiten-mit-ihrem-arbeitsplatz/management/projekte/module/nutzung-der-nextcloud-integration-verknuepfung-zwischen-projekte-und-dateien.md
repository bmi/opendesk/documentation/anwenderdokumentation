# Nutzung der Nextcloud-Integration: Verknüpfung zwischen Projekte und Dateien

## Überblick

Sie können das Dateien-Modul als integrierten Datei-Speicher in Projekte verwenden. Auf diese Weise können Sie:

- Dateien und Ordner im Dateien-Modul mit Arbeitspaketen in Projekte verknüpfen
- In der Registerkarte Dateien der Detailansicht von Arbeitspaketen sich Dateien und Ordner anzeigen lassen, diese öffnen und herunterladen

Alle mit einer Datei verknüpften Arbeitspakete anzeigen lassen  **Wichtiger Hinweis**: Um das Dateien-Modul als Datei-Speicher in Ihrem Projekt verwenden zu können, müssen Sie zunächst beide Module einmal miteinander verknüpfen.

## Projekte mit Dateien verknüpfen

So verbinden Sie Projekte mit dem Dateien-Modul:

- Öffnen Sie ein beliebiges Arbeitspaket und lassen Sie sich die Details auf der rechten Bildschirmseite anzeigen. (Klick auf die ID des Arbeitspakets)
- Gehen Sie zum Dateien-Reiter und klicken Sie unter der Nextcloud-Überschrift auf **Nextcloud Login**.
- Im nächsten Bildschirm können Sie sich mit Ihrem Konto verbinden. Klicken Sie unten auf **Anmelden** und geben Sie Ihre Anmeldedaten erneut ein (manchmal werden Sie dazu auch nicht aufgefordert, sondern direkt weitergeleitet. Das ist auch in Ordnung). Sie werden außerdem eine Sicherheitswarnung sehen, aber da Sie tatsächlich versuchen, die beiden Konten zu verbinden, können Sie diese gefahrlos ignorieren.
- Sie werden nun zurück zu Projekte geleitet, wo Sie außerdem gebeten werden, Lese- und Schreibzugriff auf Ihr Projekte-Konto zu gewähren. Dies ist notwendig, damit die Integration funktioniert. Klicken Sie auf **Autorisieren**.
- Der einmalige Prozess, um Ihre beiden Konten zu verbinden, ist abgeschlossen. Sie werden nun zum ursprünglichen Arbeitspaket zurückgeleitet, wo Sie alle Dateien, die bereits verlinkt sind, anzeigen und öffnen oder mit der Verknüpfung von neuen Dateien beginnen können.

**Hinweis:** Um die Verbindung zwischen Projekte und Dateien zu trennen, navigieren Sie in Dateien zu **Einstellungen** und dann zu **Projekte**. Dort klicken Sie auf **Von Projekte** trennen.

![Arbeitspaket-Details-Nextcloud-Login](media/9c4b1418b5e5ae1099b34fcf03f4a72acccf8d06.jpg "Arbeitspaket-Details-Nextcloud-Login")

![Nextcloud-Anmeldung](media/899c86b50f22d0041f3270875a0ae60c72b98da9.jpg "Nextcloud-Anmeldung")

## Dateien mit Projekte verknüpfen

ACHTUNG: Dieser Inhalte sollte nach Review auch oben unter Dateien einmal kopiert und abgelegt werden.  Auch in Ihren Dateien-Modul müssen Sie einmal die Verknüpfung zu Projekte aktivieren:

1. Klicken Sie bei einer beliebigen Datei auf das **Drei-Punkte-Menü**, um die **Detailansicht** auf der rechten Bildschirmhälfte zu öffnen.
1. Navigieren Sie zum Reiter **Projekte** und klicken dort auf **Mit Projekte** verbinden.
1. Im nächsten Schritt werden Sie dazu aufgefordert, Nextcloud dazu zu autorisieren. Klicken Sie auf **Autorisieren**.
1. Das Dateien-Modul ist jetzt erfolgreich mit Projekte verbunden.

![Dateien-mit-Projekte-verbinden](media/42b1a0eeb2a3b5bf61004610f08ad947fd96ee3e.jpg "Dateien-mit-Projekte-verbinden")

![autorisiere-dateien-projekte](media/de66aaaaff06caa3e0a069389ef77eadee0dd935.jpg "autorisiere-dateien-projekte")

![Erfolgreich-mit-projekte-verbunden.jpg](media/30ce62226a74845a3ae140118c1af4065e13babf.jpg "Erfolgreich-mit-projekte-verbunden.jpg")

Zurück in Ihrem Arbeitspaket sehen Sie im Dateien-Reiter nun auch die Dateien, die aus dem Dateien-Modul verknüpft worden sind. Fahren Sie mit der Maus über die Dateien (Mouseover), werden Ihnen die **Optionen** eingeblendet:

- Datei herunterladen
- Datei am Speicherort öffnen
- Datei-Link entfernen

![Mouseover-Dateien-im-Arbeitspaket](media/fefc9ddaea0da65ef69923bd4ac4ac2c4031c4b0.jpg "Mouseover-Dateien-im-Arbeitspaket")

## Dateien und Ordner mit einem Arbeitspaket verlinken

Sie haben zwei Möglichkeiten um Dateien aus Ihrem Dateien-Modul mit dem aktuellen Arbeitspaket zu verknüpfen:

1. Wählen Sie eine Datei von Ihrem Computer aus, laden Sie sie in das Dateien-Modul hoch und verknüpfen Sie sie mit diesem Arbeitspaket

Wählen Sie eine bereits existierende Datei aus Ihrem Dateien-Modul aus  **Neue Dateien hochladen und verlinken**

Wenn Sie eine Datei hochladen möchten, die Sie nicht im Dateien-Modul gespeichert haben, klicken Sie (in der Detailsansicht Ihres Arbeitspakets) auf **Dateien hochladen**. Im nächsten Fenster wählen Sie die gewünschte Datei aus und bestätigen Ihre Auswahl mit **Öffnen**. Anschließend öffnet sich in Projekte ein Dialogfenster um den Speicherort für die neue Datei in Dateien festzulegen. Im oberen Bereich können Sie über den Pfad zum gewünschten Speicherort navigieren (auch: Breadcrumb-Navigation). Bestätigen Sie den richtigen Speicherplatz mit **Ort wählen**. Die gewünschte Datei ist nun erstens im Dateien-Speicher hochgeladen und zweitens mit dem aktuellen Arbeitspaket verknüpft.

**Verlinkung entfernen**

Um die Verlinkung eines Ordners oder einer Datei zum Arbeitspaket zu entfernen, bewegen Sie den Mauszeiger in der Liste der verknüpften Dateien darauf und klicken Sie auf **Datei-Link Entfernen**.

![Speicherort-Auswahl](media/8b490732059bd1d95a93b975130b474709fc7cf0.jpg "Speicherort-Auswahl")

## Arbeitspakete aus dem Dateien-Modul heraus verlinken

Öffnen Sie Dateien und navigieren Sie zu der Datei oder dem Ordner, die Sie mit einem Arbeitspaket verlinken möchten. Klicken Sie auf das **Drei-Punkte-Menü**, um die **Detailsansicht** zu öffnen. In der nun offenen Seitenleiste rechts navigieren Sie zum Reiter **Projekte**.

Hier verknüpfen Sie nun alle gewünschten Arbeitspakete mit der Datei (oder dem Ordner). Im unteren Bereich können Sie bereits verlinkte Arbeitspakete aufgelistet sehen. Ist nichts verlinkt, ist die Liste entsprechend leer. Nutzen Sie die Suchleiste um das gewünschte Arbeitspaket zu finden. Sie können nach Wörtern aus dem Arbeitspaket-Titel oder auch nach der Arbeitspaket-ID suchen. Durch **Klick** auf das gewünschte Arbeitspaket wird dieses automatisch verlinkt und unten aufgelistet.

**Verlinkung löschen**

Wenn Sie die Verknüpfung zum Arbeitspaket entfernen wollen, so klicken Sie in der Detailansicht und das **Link-Entfernen** -Symbol beim Arbeitspaket. Im zweiten Schritt werden Sie aufgefordert, dies zu bestätigen. Klicken Sie auf **Verknüpfung aufheben**.

**Hinweis**: Das Aufheben der Verknüpfung einer Datei oder eines Ordners entfernt einfach nur die Verbindung mit diesem Arbeitspaket. Die ursprüngliche Datei (oder Ordner) wird nicht gelöscht oder in irgendeiner Weise beeinträchtigt.

![Verknüpfung-zum-Arbeitspaket-löschen](media/ff80edf091071cfbcb9adf6a513db22190783699.jpg "Verknüpfung-zum-Arbeitspaket-löschen")

## Fehlerbehebung

**Fehlende Berechtigung, um diese Datei zu sehen**

Wenn Sie die mit einem Arbeitspaket verknüpften Dateien nicht öffnen können, müssen die Berechtigungen Ihres Kontos angepasst werden. Bitte wenden Sie sich dafür an Ihre zuständige Administratorin oder Administrator.

**Benutzerin oder Benutzer nicht in Nextcloud eingeloggt**

In der Liste der verknüpften Dateien eines Arbeitspakets kann es vorkommen, dass Sie zu einem erneuten Log-In aufgefordert werden (etwa, weil Sie sich ausgeloggt hatten oder ausgeloggt wurden): **Einloggen in Nextcloud**. Folgen Sie einfach den Anweisungen zum Log-In um wieder über alle Funktionen dieser Verknüpfung verfügen zu können.

**Verbindungsfehler**

Wenn Sie die Worte **Keine Nextcloud-Verbindung** sehen, wenden Sie sich bitte an Ihre zuständige Administratorin oder zuständigen Administrator. Sie bzw. er kann das Problem der fehlerhaften Verbindung erkennen und beheben.

**Fehler beim Abrufen der Datei**

In seltenen Fällen ist es möglich, dass die Integration nicht alle Details aller verknüpften Dateien abrufen kann. Eine einfache Aktualisierung der Seite sollte das Problem lösen. Sollte der Fehler weiterhin bestehen, kontaktieren Sie bitte die zuständige Administratorin oder den zuständigen Administrator.
