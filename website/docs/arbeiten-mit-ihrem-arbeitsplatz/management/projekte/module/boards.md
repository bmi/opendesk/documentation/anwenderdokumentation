# Boards

## Boards für agiles Projektmanagement

Agile Boards sind wertvolle Werkzeuge für das agile Projektmanagement, das Methoden wie Scrum oder Kanban einsetzt. Diese Boards können in vielfältiger Weise genutzt werden, um alle wichtigen Aspekte Ihrer Projekte zu organisieren und im Blick zu behalten. Dazu gehören Aufgaben, Bugs, Informationen, Features in Entwicklung, Risiken, Ideen und vieles mehr.

Die Struktur der Agile Boards besteht aus einer Tabelle und den zugeordneten Karten. Innerhalb dieser Boards haben Sie die Wahl zwischen einem Basis-Board und verschiedenen Action-Boards.

## Agile Boards

Die Boards sind eng mit allen anderen Funktionalitäten für das Projektmanagement in Projekte verknüpft, beispielsweise mit Arbeitspaketen oder Gantt-Diagrammen. Dies erleichtert es Teilnehmerinnen und Teilnehmern erheblich, die Boards in ihre täglichen Projektmanagement-Routinen zu integrieren und einen schnellen Überblick über wichtige Themen in ihrem Projekt zu erhalten.

![](media/2a00dc7d7ff631043d3f2041d1aabea062b9707b.jpg "")

## Neues Board erstellen

Sie haben die Möglichkeit, in einem Projekt so viele Agile Boards zu erstellen, wie erforderlich, und diese nach Ihren Wünschen anzupassen. Ihr erster Schritt besteht darin, eine neue Board-Ansicht zu generieren.

Falls Sie dies noch nicht durchgeführt haben, aktivieren Sie bitte das **Board-Modul** in Ihrem Projekt. Bitte überprüfen Sie die Einstellungen im Bereich **Rollen und Berechtigungen** innerhalb der Systemverwaltung.

Klicken Sie anschließend auf die violette Schaltfläche **+Board,** um eine frische Board-Ansicht zu erstellen.

![Board erstellen](media/7413edc0b49cad4567a175699a223789928f0bae.jpg "Board erstellen")

## Basis-Board

Teilnehmerinnen und Teilnehmer können Listen nach ihren Vorlieben erstellen und diese individuell benennen. Darüber hinaus ist es möglich, Arbeitspakete in diesen Listen zu platzieren. Wichtig ist zu beachten, dass beim Verschieben von Arbeitspaketen zwischen den Listen **keine Veränderungen an den Arbeitspaketen selbst** vorgenommen werden. Dies ermöglicht es Ihnen, flexible Boards für verschiedenste Aktivitäten zu erstellen, die Sie verfolgen möchten, wie beispielsweise das Management von Ideen.

![Basis Board](media/57d24518cc090428283bba46f7294363ebf39ac8.jpg "Basis Board")

## Geben Sie dem Board einen Titel

Wählen Sie einen informativen Namen für Ihr Board aus, um anderen Teammitgliedern deutlich zu machen, was Ihr Ziel ist.

![Geben Sie dem Board einen Titel](media/c38b5cbe09464ab4e4e5b75d88efe4e601874e18.jpg "Geben Sie dem Board einen Titel")

## Spalten in einem Board ergänzen

In Projekte sind **Spalten** vielseitig einsetzbar und können in der Regel einen **Status-Workflow, Zuweisungen**, eine **Version** oder **alles**, was Sie in Ihrem Projekt verfolgen möchten, repräsentieren. Sie haben die Freiheit, so viele Spalten zu einem Board hinzuzufügen, wie Sie benötigen.

**Spalten im Aktionsboard**: Die verfügbaren Spalten variieren je nach **Art des gewählten Boards**. Es ist wichtig zu beachten, dass beim Verschieben einer Karte zwischen den Spalten das entsprechende Attribut (z.B. der Status) automatisch aktualisiert wird.

**Spalten im Einfachen Board**: Hier haben Sie die Möglichkeit, jede Art von Spalte zu erstellen und diese nach Ihren individuellen Anforderungen zu benennen. Es ist jedoch wichtig zu bedenken, dass beim Verschieben von Karten zwischen den Spalten die Attribute nicht automatisch aktualisiert werden.

![Spalten in einem Board ergänzen](media/9a1e1378a8c8370a59ecd8e7ad2b7c1be9623e0a.jpg "Spalten in einem Board ergänzen")

Um eine neue Spalte in Ihrem Board zu erstellen, klicken Sie auf **+  Liste zum Board hinzufügen.**

![ Liste hinzufügen](media/facf6d467ce9ddb8595c9cea58c2fffe618e762b.jpg " Liste hinzufügen")

**Spalten im Einfachen Board**: Geben Sie der Spalte einen beliebigen, aussagekräftigen Namen. **Spalten im Aktions-Board**: Der Name der Spalte hängt von der Art des Aktionsboards ab, die Sie gewählt haben, z.B. **Neu**, **In Bearbeitung**, etc. für Aktionsboard.

![Spalte hinzufügen](media/3709ff2446cb23b85873baea1aba0c2c63e677fb.jpg "Spalte hinzufügen")

## Spalten entfernen

Um Spalten zu entfernen, klicken Sie auf die drei Punkte neben dem Titel einer Liste und wählen **Liste löschen** aus.

![Spalten entfernen](media/e3d4d0b562a8ef6d914ce5de2d81a41ccbf68965.jpg "Spalten entfernen")

## Fügen Sie Aufgaben zu einer Spalte hinzu

Sie haben die Möglichkeit, Karten zu einer Liste hinzuzufügen. Eine Karte repräsentiert ein **Arbeitspaket** in Projekte. Diese Karte kann verschiedene Arten von Arbeit innerhalb eines Projekts darstellen, wie beispielsweise eine Aufgabe, ein Bug, ein Feature oder ein Risiko.

![Fügen Sie Aufgaben zu einer Spalte hinzu](media/aa6007e219f81dc060a05bdeffa4694387b71725.jpg "Fügen Sie Aufgaben zu einer Spalte hinzu")

Um eine Karte hinzuzufügen, klicken Sie einfach auf das Symbol + unterhalb des Listen-Titels. Sie haben die Option, entweder eine neue Karte zu erstellen oder ein bereits vorhandenes Arbeitspaket als Karte Ihrer Liste hinzuzufügen.

![Erstellen Sie eine neue Karte](media/b0002e8541be54d2286194a12c31c73e6b3a30aa.jpg "Erstellen Sie eine neue Karte")

Zum **Hinzufügen einer neuen Karte** geben Sie einen Titel ein und betätigen die Enter-Taste. Um **bestehende Karten hinzuzufügen**, geben Sie entweder den vorhandenen Titel oder die ID ein und drücken Sie ebenfalls Enter.

![Neue Karte hinzufügen](media/3934c44b6d64dcb823d03d52dd5729707ba8e948.jpg "Neue Karte hinzufügen")

## Aufgaben bearbeiten

Sie können Karten auf folgende Weisen aktualisieren:

Sie können Karten innerhalb einer Liste oder in eine andere Liste per **Drag and Drop verschieben**. Beachten Sie bitte, dass das Verschieben von Karten in eine andere Spalte in einem Aktionsboard deren Attribute, wie zum Beispiel den Status, aktualisiert.

![Bewegen Sie Karten per Drag and Drop](media/13680df41c1012bf6c08c0b9d92176758c32368d.jpg "Bewegen Sie Karten per Drag and Drop")

Es ist möglich, den Status eines Arbeitspakets nicht nur im Statusboard, sondern auch **direkt in der Karte zu aktualisieren.**

![Statusboard aktualisieren](media/62279260582d2f9b5fcddd9e9ae4aaefdab4a476.jpg "Statusboard aktualisieren")

Durch einen **Doppelklick auf eine Karte** gelangen Sie zur **Vollbildansicht** des Arbeitspakets. Um zur Kartenansicht zurückzukehren, verwenden Sie den **Pfeil** am oberen Rand.

![Vollbildansicht](media/5027fb077485c7fddda5e398ef8a3b0c39574083.jpg "Vollbildansicht")

Wenn Sie auf **Detailansicht öffnen klicken**, symbolisiert durch das violette **i**, öffnet sich die **geteilte Bildschirmansicht** des Arbeitspakets. Sie können diese Ansicht schließen, indem Sie auf das **x** in der oberen rechten Ecke klicken.

![ Detailansicht öffnen](media/f7c4bb7b4a63cae7cd054f4d10c497abc146c044.jpg " Detailansicht öffnen")

## Aufgaben löschen

Um eine Karte aus einem **Basis-Board** zu entfernen, bewegen Sie den Mauszeiger über die Karte und drücken Sie auf das **X**.

![Karte entfernen](media/3ac9ba59713e2b63ca165d8291233d1914194c33.jpg "Karte entfernen")

Karten, die sich in einem **Aktions-Board** befinden, werden automatisch aus der Spalte entfernt, sobald das entsprechende Attribut, wie beispielsweise der Status, geändert wird.

Wenn Sie eine Karte entfernen, bedeutet das nicht, dass das Arbeitspaket gelöscht wird. Sie haben die Möglichkeit, es erneut einer Spalte hinzuzufügen oder über das Arbeitspaket-Modul darauf zuzugreifen.

## Boards verwalten

Um **neue Boards zu erstellen, bestehende Boards zu öffnen** oder Boards **zu entfernen**, gehen Sie bitte zum Hauptmenüpunkt Boards.

![Neues Board erstellen](media/abbe4acad795da7f2a278713cebd1dd019959a00.jpg "Neues Board erstellen")
