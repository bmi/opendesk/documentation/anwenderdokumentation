# Projektübersichtsseite

**Die Überblicksseite des Projekts** stellt ein Kontrollzentrum dar, in dem Schlüsselinformationen zu Ihren Projekten präsentiert werden. Diese Seite dient als Anzeigetafel für Teammitglieder, um Projektupdates, Teamzusammensetzung, Projektbeschreibungen, Berichte über Arbeitspakete und den allgemeinen Projektstatus einzusehen.

## Was ist die Projektübersicht?

Die Projektübersicht ist eine spezifische Seite des Dashboards, die essentielle Daten eines ausgewählten Projekts zur Verfügung stellt und als Informationsknotenpunkt für das gesamte Team dient.

Die Einbindung von Informationen erfolgt über Widgets, die flexibel im Dashboard platziert und in Größe sowie Reihenfolge angepasst werden können.

Nebenbei können Sie durch Text-Widgets eigene Inhalte wie Texte, Links und andere Informationen hinzufügen.

Um zur Projektübersicht zu gelangen, wählen Sie im linken Menü den Punkt **Übersicht** aus.

![Projektübersichtsseite](media/22197a85613d0cf17dc148d93be8da4e8ee07705.jpg "Projektübersichtsseite")

## Hinzufügen eines Widgets zur Projektübersicht

Wählen Sie die Art des Widgets, die Sie hinzufügen möchten.

Suchen Sie aus einer Liste das Widget aus, das am besten zu Ihrem Bedarf passt.

![Widget hinzufügen](media/a9dc88e06465d5f23c1e20959c6eba3bcada6a30.jpg "Widget hinzufügen")

Wählen Sie den Ort, an dem das neue Widget platziert werden soll.

Um ein Widget hinzuzufügen, klicken Sie auf das Plus-Symbol (+) in der oberen rechten Ecke der Seite.

Sie haben die Möglichkeit, verschiedene Widgets Ihrer Projektübersicht hinzuzufügen.

![Projektübersichtsseite](media/93e413bc08232fa9bb14d44a05e138d423d51580.jpg "Projektübersichtsseite")

## Kalender-Widget

Das Kalender-Widget zeigt auf einen Blick die aktuellen Arbeitspakete, die zum gegenwärtigen Datum bearbeitet werden, wobei bis zu 100 Arbeitspakete dargestellt werden können.

![Kalender Widget](media/b55eea566e844bb0c264ca9c3d7a98ec9d324f43.jpg "Kalender Widget")

## Benutzerdefiniertes Textwidget

Im benutzerdefinierten Textwidget können Sie diverse Informationen für Ihr Team hinterlegen, wie Verweise auf wichtige Projektressourcen, Details zu Arbeitspaketen, Filtereinstellungen und technische Spezifikationen.

Auch das Hinzufügen von Dateien, die in der Projektübersicht ersichtlich sein sollen, ist hier möglich.

![Selbstdefinierter Text-Widget](media/de7b82c0ef0d9089d3d1b17644d19d1886b9f1fc.jpg "Selbstdefinierter Text-Widget")

## Widget für Projektmitglieder

Fügen Sie ein Widget hinzu, das die Mitglieder Ihres Projekts und ihre jeweiligen Rollen auf der Projektübersichtsseite anzeigt. Dies beinhaltet sowohl Gruppen als auch individuelle Benutzerprofile.

![Mitglieder](media/71e135f344aa16e48645d0f4e9b9d46b8d1815e6.jpg "Mitglieder")

Projektmitglieder können durch klicken auf das violette **+ Mitglied** Schaltfläche in der unteren linken Ecke hinzugefügt werden.

Die Schaltfläche **Alle Mitglieder anzeigen** liefert eine Übersicht aller Personen, die dem Projekt hinzugefügt wurden, einschließlich Einzelpersonen und Gruppen.

## Nachrichten-Widget

Das Nachrichten-Widget zeigt die aktuellsten Mitteilungen bezüglich des Projekts auf der Projektübersichtsseite an.

![Neuigkeiten](media/35d7df1d9205dccd5ffad47e91141026c84eee70.jpg "Neuigkeiten")

## Projektbeschreibung

Das Projektbeschreibungs-Widget integriert die Projektbeschreibung in Ihre Projektübersicht.

Änderungen an der Projektbeschreibung können über die Projekteinstellungen vorgenommen werden.

## Projektdetails

Das Widget für Projektdetails präsentiert alle spezifischen Felder eines Projekts wie den Projektinhaber, das Fälligkeitsdatum, die Projektnummer und andere individualisierte Felder.

Diese benutzerdefinierten Felder sind in den Projekteinstellungen konfigurierbar. Als Systemadministrator haben Sie die Option, neue Felder zu definieren.

![Projektdetails](media/690ef4e837b8b8c362b599c01872ad078bfc565a.jpg "Projektdetails")

Neue Felder für Projekte können in der Systemadministration erstellt werden.

## Projektstatus-Widget

In der Projektübersichtsseite können Sie den Projektstatus festlegen und eine detaillierte Beschreibung abgeben. Der Projektstatus ist ein Widget, welches Sie Ihrer Übersicht beifügen können. Mit dem Projektstatus-Widget können Sie den Status Ihres Projekts einsehen und bewerten, ob es sich auf dem richtigen Weg befindet, Verzögerungen aufweist oder Gefahren ausgesetzt ist.

Wählen Sie aus der Dropdown-Liste den passenden Status für Ihr Projekt aus:

- PLANMÄSSIG (grün)
- GEFÄHRDET (orange)
- UNPLANMÄSSIG (rot)
- NICHT GESETZT (grau)
- NICHT GESTARTET (helblau)
- ABGESCHLOSSEN (dunkelgrün)
- EINGESTELLT (dunkelblau)

![Projektstatus](media/9318a76e32194f92fcb1f1cd241c15c30fef2058.jpg "Projektstatus")

Überarbeiten Sie die Projektstatusbeschreibung und integrieren Sie essentielle Details wie die Leitung des Projekts, wichtige Meilensteine, sowie relevante Links und aktuelle Statusinformationen.

![Projektstatus](media/c06dd50818173a237704d1534c0fdcecd4766d6a.jpg "Projektstatus")

## Widget für Verbuchte Zeit

Das **Verbuchte Zeit** Widget zeigt die im Projekt in den letzten sieben Tagen aufgewendete Zeit an.

Die eingetragenen Zeiten sind mit dem zugehörigen Arbeitspaket verbunden und können modifiziert oder entfernt werden. Für eine umfassende Übersicht aller erfassten Zeiten und Kosten, wechseln Sie zum Modul **Zeit und Kosten**, um eine Kostenauswertung durchzuführen.

## Widget für Unterprojekte

Das Widget für Unterprojekte zeigt in der Projektübersicht alle zugehörigen Unterprojekte des aktuellen Projekts auf. Mittels des bereitgestellten Links können Sie direkt zu einem spezifischen Unterprojekt wechseln.

![Unterprojekte](media/d38ff2f5e0ee81388fffc57f0ec9962b6e02b141.jpg "Unterprojekte")

Das Widget stellt lediglich Links zu den Unterprojekten der ersten Ebene bereit, also zu denen, die direkt unter dem Hauptprojekt liegen, und nicht zu Unterprojekten von Unterprojekten.

Um Änderungen in der Projekthierarchie vorzunehmen, navigieren Sie bitte zu den Projekteinstellungen.

## Widget für Arbeitspaket-Übersicht

Das Widget für die Übersicht der Arbeitspakete präsentiert sämtliche Arbeitspakete eines Projekts, die durch spezifische Kriterien charakterisiert sind.

![Widget für Arbeitspaket-Übersicht](media/1acfd4b06a91fc2da23d8819a2e8e1243c8145ba.jpg "Widget für Arbeitspaket-Übersicht")

Das Diagramm lässt sich anhand der folgenden Kriterien erstellen:

- Kategorie
- Typ
- Status
- Priorität
- Autor
- Zugewiesen an

![Arbeitspakete nach Kriterien](media/29be5a5bd4915ebfb68e513aa609393df2a02278.jpg "Arbeitspakete nach Kriterien")

Das Widget zeigt eine Auflistung aller offenen und abgeschlossenen Arbeitspakete, sortiert nach Kriterien.

## Widget für Arbeitspaket-Tabellen

Das Arbeitspaket-Tabellen-Widget ermöglicht es Ihnen, eine Tabelle mit Arbeitspaketen auf Ihrer Projektübersichtsseite darzustellen. Diese Tabelle lässt sich durch die **Konfigurationseinstellungen** filtern, ordnen und gruppieren, sodass beispielsweise nur Arbeitspakete mit hoher Priorität angezeigt werden.

![Widget für Arbeitspaket-Tabellen](media/70453f29af5808b7e6b819db0a18347c21e12fd1.jpg "Widget für Arbeitspaket-Tabellen")

## Größe und Position von Widgets bearbeiten

Um ein Widget zu verschieben, klicken Sie auf das sechspunktige Symbol in der oberen linken Ecke des Widgets, halten die Maustaste gedrückt und bewegen es an den gewünschten Ort.

Um die Größe eines Widgets anzupassen, klicken Sie auf den gepunkteten Bereich in der unteren rechten Ecke des Widgets, halten die Maustaste gedrückt und ziehen das Widget entweder nach rechts oder links, um es zu vergrößern oder zu verkleinern.

## Widget von der Projektübersicht entfernen

Um ein Widget von der Projektübersicht zu entfernen, klicken Sie auf das Drei-Punkte-Menü in der oberen rechten Ecke des Widgets und wählen Sie die Option **Widget entfernen**.

![Widget von der Projektübersicht entfernen](media/b75b369a5af367c763d1f4179b8f214fbd7ee7d9.jpg "Widget von der Projektübersicht entfernen")
