# Gantt-Diagramm

Die **Gantt-Diagramme** in Projekte zeigen Ihnen Ihre Arbeitspakete auf einer Zeitleiste. Sie können gemeinsam Projektpläne erstellen und steuern. Allen Teammitgliedern ist es möglich, die Projektzeitleiste einzusehen und somit immer auf dem aktuellsten Stand zu sein und diese Informationen mit den Stakeholdern zu teilen. Es ist Ihnen möglich, Start- und Enddaten hinzuzufügen und diese auch anzupassen sowie diese per Drap-and-Drop in ein **Gantt-Diagramm** zu überführen. Zusätzlich können Sie Abhängigkeiten, Vorgänger und Beobachter hinzufügen.

## Gantt-Diagramm aktivieren

Ein **Gantt-Diagramm** kann in jeder Arbeitspakettabelle aktiviert werden um die Arbeitspakete auf einer Zeitleiste abzubilden. Um ein **Gantt-Diagramm** zu aktivieren, wählen Sie die **Gantt-Schaltfläche** in der Arbeitspaket-Tabelle in der rechten oberen Ecke Ihres Bildschirms aus.

![Gantt-Schaltfläche](media/1038189c15d95890484147647b847c4c05bd1518.PNG "Gantt-Schaltfläche")

## Ein neues Element in einem Gantt-Diagramm erstellen

Um ein Arbeitspaket einem **Gantt-Diagramm** hinzuzufügen, klicken Sie auf **Erstelle neues Arbeitspaket**. Die Schaltfläche befindet sich am Ende der Arbeitspaket-Tabelle.

Sie können ein neues Arbeitspaket erstellen und die Attribute für **Typ**, **Status** und auch die anderen Eingabefelder wählen.

Im **Gantt-Diagramm** können Sie per Drag-and-Drop das Start- und Enddatum Ihres Arbeitspakets wählen.

Um die Reihenfolge von Objekten im **Gantt-Diagramm** zu ändern, ziehen Sie das entsprechende Objekt per Drag-and-Drop an die gewünschte Stelle.

![Erstelle Arbeitspaket](media/74530c948d1d042ebc3bbe2a2848ff9a9805f7b5.PNG "Erstelle Arbeitspaket")

## Änderungen an der Zeitleiste eines Arbeitspakets

Um die Länge eines Arbeitspakets auf der Zeitleiste zu verändern, öffnen Sie die **Gantt-Diagramm-Ansicht**. Wählen Sie die Zeile Ihres gewünschten Arbeitspakets aus und verweilen Sie mit dem Mauszeiger über dem Arbeitspaket in der Zeitleiste. Es werden kleine Pfeile am Anfangs- und Endzeitpunkt des Arbeitspakets erscheinen. Durch ein Ziehen an diesen Pfeilen ist es Ihnen möglich, die Dauer des Arbeitspakets zu verändern. Wollen Sie die Position des Arbeitspakets verändern (Verschieben des Arbeitspakets in die Zukunft oder in die Vergangenheit), ziehen Sie es per Drag-and-Drop an die gewünschte Stelle.

**Hinweis:** Im Gantt-Diagramm werden die Tage, an denen nicht gearbeitet wird, mit einer dunkleren Hintergrundfarbe dargestellt. Arbeitspakete können nicht an diesen Tagen beginnen oder enden. Um dies zu ändern, müssen Sie Änderungen in den Einstellungen der Arbeitstage vornehmen.

## Beziehungen von Arbeitspaketen im Gantt-Diagramm

Abhängigkeiten von Arbeitspaketen zueinander können im **Gantt-Diagramm** verfolgt werden. Auf diese Weise ist es Ihnen möglich, zu verfolgen welche Pakete zuerst und welche zuletzt bearbeitet werden müssen. Eine Abhängigkeit können Sie einem Arbeitspaket hinzufügen, indem Sie mit der rechten Maustaste auf ein Arbeitspaket im **Gantt-Diagramm** klicken.

Es öffnet sich ein Menü, in diesem können Sie auswählen ob Sie einen **Vorgänger** oder einen **Nachfolger** hinzufügen möchten. Wählen Sie aus, zu welchem Element Sie eine Vorgänger- oder Nachfolgerverbindung herstellen möchten. Die Beziehung zwischen den Arbeitspaketen wird als violette Linie im **Gantt-Diagramm** dargestellt.

![Beziehung herstellen](media/778c0d58bf663dffd86c66496067133311ae315c.PNG "Beziehung herstellen")

Eigenschaften, die auftreten wenn Arbeitspakte einen **Vorgänger** oder **Nachfolger** aufweisen:

- Nachfolger können nicht verschoben werden, wenn dieser dadurch früher als das Enddatum des Vorgängers starten würde
- Kommt es zu dem Fall, dass ein Enddatum eines Vorgängers hinter das Startdatum eines Nachfolgers gelangt, wird das Startdatum des Nachfolgers automatisch in die Zukunft verschoben

Existiert eine Lücke zwischen Voränger und Nachfolger, können beide vorwärts oder rückwärts bewegt werden, ohne den anderen zu beeinträchtigen, so lange sich die Daten nicht überschneiden.

**Hinweis:** Es werden nur die Beziehungen Vorgänger/ Nachfolger, sowie Eltern/ Kind in Gantt-Diagrammen angezeigt.

## Farben und Linien in Gantt-Diagrammen verstehen

- Die **violette Linie** - verbindet zwei Arbeitspakete (Vogänger und Nachfolger)
- Die **vertikale, rot gestrichelte Linie** - zeigt das aktuelle Datum an
- Die **schwarze Klammer** - zeigt die Dauer ab dem frühesten untergeordneten Arbeitspakets bis zum Enddatum des letzten untergeordneten Arbeitspakets an
- Die **rote Klammer** - hat dieselbe Bedeutung wie die schwarze Klammer, mit einem Unterschied: Die Klammer färbt sich rot, wenn die von den Kindern abgeleiteten Termine vor oder nach den manuell geplanten Terminen liegen. Die Klammern sind schwarz, wenn die abgeleiteten Termine innerhalb der festgelegten Termine liegen
- Das **Diamantsymbol** - steht für einen Meilenstein
- Der **Balken** - steht für Arbeitspakete (Phasen, Aufgaben)

## Konfigurationen des Gantt-Diagramms

Um in die Einstellungen eines **Gantt-Diagramms** zu gelangen, klicken Sie auf das **3-Punkte-Menü** oben rechts im Arbeitspaketmodul. Wählen Sie **Ansicht konfigurieren** aus.

![Ansicht konfigurieren](media/2b61383e6b473314de10cb4a3c231edc6a578358.PNG "Ansicht konfigurieren")

Wählen Sie danach in dem sich öffnenden Fenster den Reiter **Gantt-Diagramm** aus. Hier ist es Ihnen möglich, die Zoomstufe anzupassen. Sie können wählen zwischen:

- **Auto-Zoom**
- **Tage**
- **Wochen**
- **Monate**
- **Quartale**
- **Jahre**

Durch die Auswahl der Zoomstufe haben Sie Einfluss darauf, in welcher Größe das **Gantt-Diagramm** auf Ihrem Bildschirm dargestellt wird. Wählen Sie **Auto-Zoom**, so wird die Größe ausgewählt, die am besten zu Ihrer Bildschirmgröße passt.

Zusätzlich ist es Ihnen möglich, Konfigurationen an den Beschriftungen Ihres **Gantt-Diagramms** vorzunehmen. Über **Links**, **Rechts** und **Rechts außen**, können Sie 3 zusätzliche Beschriftungen hinzufügen. Wählen Sie, welche zusätzlichen Informationen Sie im **Gantt-Diagramm** abbilden möchten. Dies kann besonders nützlich sein, wenn Sie vorhaben ein **Gantt-Diagramm** auszudrucken.

Klicken Sie auf **Anwenden** um Ihre Konfigurationen zu speichern.

![Konfigurieren des Gantt-Diagramms](media/584ac7b43746ccdc823ad8ad7cc215f8c84452b6.PNG "Konfigurieren des Gantt-Diagramms")

## Daten exportieren

Um Daten aus Ihrem Gantt-Diagramm zu exportieren, stehen Ihnen mehrere Möglichkeiten zur Verfügung:

- Export über Arbeitspaket-Ansicht
- Export als Pdf (als Pdf speichern, als Pdf drucken)
- Datensynchronisation in ein Excel-Format

## Gantt-Diagramm drucken

Über die Druckfunktion des Browsers können Sie ein **Gantt-Diagramm** drucken. Diese Funktion wurde für Chrome optimiert.

Fügen Sie als erstes die von Ihnen benötigten Beschriftungen hinzu, zum Beispiel Startdatum, Enddatum oder Thema.

Wählen Sie den **Auto-Zoom** aus.

Optimieren Sie Ihre Anzeige, indem Sie das **Gantt-Diagramm** ganz nach links ziehen, sodass nur noch das **Gantt-Diagramm** zu sehen ist.

Drücken Sie danach **STRG + P**, um das **Gantt-Diagramm** zu drucken.

Stellen Sie sicher, dass Sie ein Format ausgewählt haben, das zu Ihrer Ansicht passt.

Aktivieren Sie **Hintergrundgrafik** in den Einstellungen.

Klicken Sie auf **Drucken**.

![Gantt-Diagramm drucken](media/8c9770d8215f3d4dafc18e59926649df27cc99aa.PNG "Gantt-Diagramm drucken")

Sollten Sie andere Browser verwenden, befolgen Sie bitte die Druckanweisungen des jeweiligen Browsers um das optimale Ergebnis zu erzielen.

## Gantt-Diagramm-Ansicht

**Zoomen in der Gantt-Diagramm-Ansicht**

Um die **Gantt-Diagramm-Ansicht** zu vergrößern oder zu verkleinern (heranzoomen, rauszoomen), klicken Sie auf die **Plus** - oder die **Minus-Schaltfläche** oben in Ihrem Diagramm.

![Zoom-Schaltflächen](media/1485c6cfe92e8d137863003bdfa383c73d18078b.png "Zoom-Schaltflächen")

**Automatisch Zoomen**

Wählen Sie die Schaltfläche für den **automatischen Zoom** oben im **Gantt-Diagramm**, um eine automatische Ansicht Ihres **Gantt-Diagramms** zu erhalten.

![Auto-Zoom-Schaltfläche](media/2e4ab13459f7705e6c072e45b286eb2686fa67a2.png "Auto-Zoom-Schaltfläche")

**Zen-Modus**

Der **Zen-Modus** ist ähnlich zu einem Vollbildmodus. Klicken Sie auf die Schaltfläche für den **Zen-Modus,** werden Ihnen nur noch die **Gantt-Diagramm-Ansicht** und die **Arbeitspaket-Tabelle** angezeigt. Dadruch kann es Ihnen leichter fallen, sich auf die Tätigkeiten in diesen Bereichen zu konzentrieren. Klicken Sie erneut auf die **Zen-Modus-Schaltfläche**, um diese Funktion zu deaktivieren.

![Zen-Modus-Schaltfläche](media/723409fac05637746d2058f9138f2c24029fc578.png "Zen-Modus-Schaltfläche")

## Projektübergreifende Ansichten

Es ist Ihnen möglich, mit den **Gantt-Diagrammen** in Projekte projektübergreifende Zeitpläne zu erstellen.

Erstellen Sie projektübergreifende Zeitpläne. Zeigen Sie alle Aktivitäten, Phasen und Meilensteine innerhalb Ihres Projekts und Ihrer Unterprojekte an. Sie können projektübergreifende Zeitpläne erstellen, indem Sie die Projekte filtern und in einen gemeinsamen Plan aufzeichnen.

**Unterprojekte einbeziehen:** Klicken Sie innerhalb des übergeordneten Projekts auf die **Filter-Schaltfläche** und wählen Sie die Unterprojekte aus, die Sie in Ihren Projektplan aufnehmen möchten. Es ist auch möglich eine Aggregation nach Projekten zu wählen.

Verwenden Sie die Filter- und Gruppierungsoptionen bei der Konfiguration der Arbeitspaket-Tabelle, um die Zeitleiste anzupassen. Vergessen Sie nicht, die Informationen zu **speichern**.

![Unterprojekte](media/d923e76b156ece58d192557f37ddcb5cca27aee7.PNG "Unterprojekte")

Wenn Sie Ihre **Arbeitspaket-Tabelle** anpassen und Ihre Arbeitspakete filtern, sortieren oder gruppieren möchten, lesen Sie bitte den Abschnitt Konfiguration der [Arbeitspakete erstellen](../der-gesamte-projektmanagement-lebenszyklus/projektstart-oder-durchfuehrung/arbeitspakete-erstellen.md).

## Aggregation nach Projekten

Sie können einen **schnellen Überblick aller Projekte** über das Gantt-Diagramm anzeigen lassen. Navigieren Sie dazu in das Modul Arbeitspakete eines Projekts.

**Gruppieren Sie die Liste** nach Projekten, indem Sie die Arbeitspaket-Tabellen-Konfiguration verwenden oder auf das kleine Dreieck neben “Projekt” in der Kopfzeile der Tabelle klicken.

![Gruppieren nach Projekten](media/95d1dd4753ebc19aacf4020bcba39772f849c8c3.PNG "Gruppieren nach Projekten")

Blenden Sie das **Gantt-Diagramm** ein, indem Sie auf die Schaltfläche in der oberen rechten Ecke klicken.

![Gantt-Diagramm einblenden](media/4ddf7629629740fe7d275cbb960f79e1eeb1a5f7.PNG "Gantt-Diagramm einblenden")

Benutzen Sie das **Minus** neben dem Projektnamen oder die Schaltfläche in der oberen rechten Ecke, um einige oder alle Projekte zusammenzufassen.

![zu- aufklappen](media/851e28dac99d99b3da87d85c779f7bc53dcf0c66.PNG "zu- aufklappen")

Auf diese Weise können Sie aggregierte Ansichten der Meilensteine der Projekte erhalten.

![Aggregierte Ansicht](media/dbc15ccd025976b9752f66f07c712ebc7bcfaa5d.PNG "Aggregierte Ansicht")

**Hinweis**: Wenn Sie diese Funktion nutzen möchten, müssen Meilensteine zu Ihren Terminen und Projekten vorhanden sein. Für andere Arbeitspakettypen ist diese Funktion noch nicht verfügbar. Ihre angezeigten Projekte hängen davon ab, welche Filter Sie eingestellt haben und welche Berechtigungen Sie besitzen. Sehen können Sie private Projekte und Projekte, bei denen Sie Mitglied sind, sowie öffentliche Projekte. Es gibt Fälle (viele Arbeitspakete pro Projekt), in denen Sie die Anzahl der Objekte pro Seite in der unteren rechten Ecke erhöhen müssen, um mehrere Projekte anzuzeigen. Ändern Sie die Optionen für die Paginierung in den Systemeinstellungen, falls dies erforderlich ist.

![Anzahl Objekte pro Seite](media/bf22f0409d15b683a21cf764657deb171590b392.PNG "Anzahl Objekte pro Seite")
