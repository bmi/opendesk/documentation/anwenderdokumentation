# Roadmap

## Produktmanagement und Release-Planung

Die Seite **Roadmap** ist als eine Übersichtsseite definiert und nach Versionen sortiert, die die zugeweisenen Arbeitspakete anzeigt. Wenn das Arbeitspaket-Modul aktiviert ist und eine Version erstellt wurde, wird diese auch in einer Projektnavigation angezeigt. Dies kann man in den Projektkonfigurationen einstellen.

**Hinweis**: Eine Roadmap wird nur angezeigt, wenn man innerhalb eines Projekts eine Version angelegt hat. Das geschieht über das Projektmenü.

Die Roadmap wird im Modul **Projekte** geplant und verwaltet. Hierbei haben Sie die Möglichkeit, Ihre Roadmap mit Stakeholdern zu teilen, Feedback für dieses Projekt einzuholen und einen kompletten Release-Plan zu erstellen.

Sie haben in der Roadmap eine Übersicht über alle Arbeitspakete sowie deren Status. Jede Projekt-Version kann aus der Liste der Roadmap geprüft werden. Haben Sie Projekte abgeschlossen, werden diese in der Liste durchgestrichen.

![Roadmap Übersichtsseite](media/40a30b24ba49cde7b92c0533b3908e624ac05643.png "Roadmap Übersichtsseite")

Die Roadmap bietet Ihnen- Informationen über den Fortschritt eines Projektes und dessen zugehörigen Arbeitspaketen. Es wird der Prozentsatz der abgeschlossenen und/ oder offenen Arbeitspakete sowie der Gesamtfortschritt gezeigt.

Weitere Informationen zur Projekt-Version können sie erhalten, indem Sie auf den Titel der Version klicken.

![Roadmap - weitere Informationen](media/da3844a54517f3306812146bb356d2b5de6aba73.png "Roadmap - weitere Informationen")

Das Diagramm unter **Status** zeigt die vorhandenen Arbeitspakete, welche nach eines bestimmten Attributs sortiert sind. Sie können in der Dropdown-Liste das entsprechende Attribut auswählen:

![Auswählen eines Attributs](media/c301b0cad5b2657c0e5d9d82fb057bf246269c08.png "Auswählen eines Attributs")
