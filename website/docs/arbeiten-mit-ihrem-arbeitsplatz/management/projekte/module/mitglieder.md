# Mitglieder

Finden Sie heraus, wie Sie Mitglieder in ein Projekt einladen oder bestehende Accounts zu Ihrem Projekt hinzufügen können.

## Bestehende Accounts hinzufügen

Um bestehende Mitglieder zu einem Projekt hinzuzufügen, wählen Sie zuerst das Projekt aus, zu dem Sie Mitglieder hinzufügen möchten. Wählen Sie im Projektmenü auf der linken Seite das Modul **Mitglieder** aus.

In der Mitgliederliste können Sie die aktuellen Mitglieder dieses Projekts einsehen.

![Bestehende Mitglieder verwalten](media/315e4e3816011cb13fe3efd07816c652ee7420e1.png "Bestehende Mitglieder verwalten")

Klicken Sie auf **+Mitglied** in der oberen rechten Ecke.

Geben Sie den Namen der Person ein, welche Sie zu dem Projekt hinzufügen möchten. Sie können auch mehrere Mitglieder auf einmal auswählen und hinzufügen. Jedes Mitglied muss einer Rolle zugewiesen werden. Klicken Sie anschließend auf **Hinzufügen.**

Bevor Sie auf **Hinzufügen** klicken, stellen Sie sicher, dass Sie den Namen des neuen Mitglieds ausgewählt und eine Rolle zugewiesen haben.

![Neues Mitglied hinzufügen](media/e2a88d0b90663b4b20003fb038ed663e0547d583.png "Neues Mitglied hinzufügen")

## Neue Accounts hinzufügen

Es ist auch möglich, Personen einzuladen, die noch kein **Projekte-Konto** besitzen.

## Personen einladen im Modul Mitglieder

Wählen Sie zunächst das Projekt aus, zu dem Sie Mitglieder hinzufügen möchten. Navigieren Sie im Projektmenü auf der linken Seite zum Modul **Mitglieder**.

Geben Sie die E-Mail-Adresse des neuen Mitglieds ein. Falls kein bestehender Projekte-Account gefunden wird, wird automatisch der Hinweis **Einladen** vor die E-Mail-Adresse gesetzt. Bestätigen Sie durch Auswahl des Textes **Einladung an ... senden**. Weisen Sie dem neuen Mitglied eine Rolle zu und klicken Sie auf **Hinzufügen**.

Die eingeladene Person erhält eine Einladung per E-Mail mit einem Link, um ein Konto für **Projekte** zu erstellen.

![Mitglieder per E-Mail einladen](media/72ef5dcfa730117e45b54944ee69e38bf20876aa.png "Mitglieder per E-Mail einladen")

## Einladen aus der Kopfzeile

Um Personen einzuladen, die noch keinen Projekte-Account haben, haben Sie auch die Möglichkeit dies über das Header-Menü durchzuführen. Klicken Sie dafür auf das violette **Plus (+)** und wählen Sie **Nutzer einladen** aus.

![Nutzerinnen und Nutzer einladen](media/5f191a14101c65465cb24b000fa1ae9063134e06.png "Nutzerinnen und Nutzer einladen")

Sobald Sie auf **Nutzer einladen** geklickt haben, werden Sie zu einem Pop-up-Fenster weitergeleitet. Dort können Sie entscheiden, zu welchem Projekt Sie die neuen Mitglieder einladen möchten und welche Rollen sie erhalten sollen.

![Nutzerinnen und Nutzer einladen](media/88c5a388c386966fadf361b30f9698603962a5a0.png "Nutzerinnen und Nutzer einladen")

## Mitglieder über ein Arbeitspaket hinzufügen

Über die Tabelle der Arbeitspakete können Sie ebenfalls Nutzerinnen und Nutzer als Mitglieder hinzufügen. Wählen Sie dazu in der Tabelle der Arbeitspakete unter **ZUGEWIESEN AN** die Option **Einladen.**

![Externe Nutzerinnen und Nutzer einladen](media/72ef5dcfa730117e45b54944ee69e38bf20876aa.png "Externe Nutzerinnen und Nutzer einladen")

## Mitglieder bearbeiten

Um ein angelegtes Mitglied innerhalb eines Projekts zu ändern, gehen Sie wie folgt vor: Wählen Sie das betreffende Projekt aus und öffnen Sie das Mitgliedermodul. Hier können Sie die Rolle eines bestehenden Mitglieds bearbeiten. Klicken Sie dafür auf das Stiftsymbol am Ende der Zeile des entsprechenden Mitglieds und wählen Sie die neue Rolle aus. Klicken Sie anschließend auf **Wechseln,** um Ihre Änderungen zu speichern.

![Mitglied bearbeiten](media/53d5d1c89752b6de73b4bfc0bcd4a7540007c603.png "Mitglied bearbeiten")

## Mitglieder entfernen

Um Mitglieder aus einem Projekt zu entfernen, folgen Sie diesen Schritten: Wählen Sie das entsprechende Projekt aus und öffnen Sie das Mitgliedermodul im Projektmenü auf der linken Seite. Klicken Sie auf das Mülltonnensymbol, das sich auf der rechten Seite der Zeile des Mitglieds befindet, das Sie entfernen möchten.

![Mitglieder entfernen](media/e694e4ffae2a16641e7810a828ba30127f4abe33.png "Mitglieder entfernen")

## Rollen und Berechtigungen

Mitglieder in einem Projekt haben unterschiedliche Rollen, die jeweils verschiedene Berechtigungen beinhalten.

Eine Rolle ist definiert als eine Gruppe von Berechtigungen, die durch einen eindeutigen Namen identifiziert werden. Projektmitglieder werden einem Projekt zugeordnet, indem der Name einer Person, einer Gruppe oder eines Platzhalters zusammen mit der Rolle angegeben wird, die sie im Projekt einnehmen sollen.

Um Arbeitspakete einem Projektmitglied zuzuweisen, muss die Rolle des entsprechenden Accounts oder Platzhalters die Berechtigung zur Zuweisung von Arbeitspaketen haben. Dies ist die Standardkonfiguration für Standardrollen. Sie können diese Einstellung im Abschnitt "Rollen und Berechtigungen" in der Systemadministration überprüfen.

### Benutzerinnen und Benutzer

Eine Benutzerin oder ein Benutzer ist jede Person die sich bei Projekte anmelden kann.

### Berechtigungen

Berechtigungen entscheiden darüber was Benutzerinnen und Benutzer in Projekte sehen und bearbeiten können. Berechtigungen werden an Benutzerinnen und Benutzer für bestimmte Rollen zugewiesen.

### Rollen

Eine Rolle bündelt eine bestimmte Auswahl an Berechtigungen. Es ist eine gute Möglichkeit um ohne viel Aufwand Berechtigungen an mehrere Benutzerinnen und Benutzer einer Organisation zu verteilen, die die Selben Berechtigungen zur Bearbeitung ihrer Projekte benötigen.

Eine Benutzerin oder ein Benutzer kann eine oder mehrere Rollen mit unterschiedlichen Berechtigungen besitzen. Mithilfe von diesen ist es dieser Person möglich, in unterschiedlichen Projekte verschiedene Informationen zu sehen und zu bearbeiten.

### Administratorinnen und Administratoren

Administratorinnen und Administratoren haben vollen Zugang zu allen Einstellungen und allen Projekten die in Projekte zur verfügung stehen. Die Berechtigungen von Administratorinnen und Administratoren können nicht geändert werden.

- **Umfang der Rolle:** Volle Kontrolle über alle Aspekte die die Anwendung bietet
- **Berechtigungsbeispiele:** Bearbeiten der globalen Rollen, Bearbeiten von Rollen in Projekten, Vergabe von Administrationsrechten an andere Benutzerinnen und Benutzer
- **Anpassungsoptionen:** Können nicht geändert werden

### Globale Rolle

Globale Rollen erlauben es Administratorinnen und Administratoren Verwaltungsaufgaben an andere Benutzerinnen und Benutzer zu verteilen.

- **Umfang der Rolle:** Berechtigungen für ausgewählte Verwaltungsaufgaben
- **Berechtigungsbeispiele:** Verwaltung von Benutzerinnen und Benutzern, Erstellen von Projekten
- **Anpassungsoptionen:** Administratorinnen und Administratoren können neue globale Rollen erstellen und an diese Rollen globale Berechtigungen verteilen

### Projekt-Rolle

Eine Projekt-Rolle ist eine Reihe von Berechtigungen die an alle Benutzerinnern und Benutzer vergeben werden kann. Es können mehrere Rollen an eine Benutzerin oder einen Benutzer vergeben werden.

**Hinweis:** Ist ein Modul in einem Projekt nicht aktiviert, wird es der Benutzerin oder dem Benutzer nicht angezeigt, wenn die dafür benötigten Berechtigungen nicht vorhanden sind.

- **Umfang der Rolle:** Berechtigungen die sich auf bestimmte Projekte beschränken
- **Berechtigungsbeispiele:** Erstellen von Arbeitspaketen, Löschen von Wiki-Seiten
- **Anpassungsoptionen:** Erstellen von unterschiedlichen Projektrollen mit verschiedenen Berechtigungen

### Nicht-Mitglied

Ein Nicht-Mitglied ist die Standardrolle von Benutzerinnen und Benutzern, die noch nicht Teil eines Projektes sind. Dies gilt nur wenn das Projekt in den **Einstellungen** auf **öffenltich** gesetzt wurde.

**Hinweis:** Die Nicht-Mitglied-Rolle kann nicht gelöscht werden.

- **Umfang der Rolle:** Berechtigungen für ausgewählte Projekte für Benutzerinnen und Benutzer die angemeldet sind
- **Berechtigungsbeispiele:** Ansicht von Arbeitspaketen für Benutzerinnen und Benutzer die angemeldet sind
- **Anpassungsoptionen:** Ausgewählte Berechtigungen für Nicht-Mitglieder

### Anonym

Projekte erlaubt dir Projektinformationen mit anonymen Benutzerinnen und Benutzern zu teilen die nicht in Projekte angemeldet sind. Das ist hilfreich wenn es darum geht Projektziele und Aktivitäten in einer öffentlichen Community zu teilen.

**Hinweis:** Dies funktioniert nur wenn ausgewählt wurde das eine Authentifizierung in Ihrer Projekte Instanz nicht notwendig ist, da diese in den **Einstellungen** auf **öffentlich** gesetzt wurde. Die Anonym-Rolle kann nicht gelöscht werden.

- **Umfang der Rolle:** Berechtigungen die für ausgewählte Projekte vorgesehen sind, für Benutzerinnen und Benutzer die nicht angemeldet sein müssen
- **Berechtigungsbeispiele:** Ansicht von Arbeitspaketen für Benutzerinnen und Benutzer die nicht angemeldet sein müssen
- **Anpassungsoptionen:** Ausgewählte Berechtigungen für die Rolle Anonym

### Anpassung von Rollen mit ausgewählten Berechtigungen

Administratorinnen und Administratoren können neue Rollen mit ausgewählten Berechtiugen erstellen oder bereits existierende ändern.

### Berechtigungsübersicht

Der Berechtigungsübersicht ist ein guter Ausgangspunkt um eine Übersicht über die aktuellen Einstellungen der Rollen und Berechtigungen zu bekommen.

Die Berechtigungsübersicht finden sie unter: **Administration** - **Benutzer und Berechtigungen** - **Berechtigungsübersicht**.

### Erstellen einer neuen Projektrolle

Administratorinnen und Administratoren können neue Projektrollen unter: **Administration** - **Benutzer und Berechtigungen** - **Rollen und Rechte** erstellen. Klicken Sie auf die Schaltfläche **Neue Rolle** um eine neue Rolle zu erstellen.

Folgen Sie den weiteren Schritten um eine neue Rolle zu erstellen:

1. **Name:** Vergeben Sie einen neuen Namen für die Rolle.
1. **Globale Rolle:** Erstellen Sie eine neue globale Rolle.
1. **Workflow kopieren von:** Sie können eine bereits existierende Rolle auswählen und diese inklusive ihres Workflows kopieren, um daraus eine neue Rolle zu erzeugen.
1. **Berechtiungen:** Hier ist es Ihnen möglich die Berechtigungen zu vergeben. Sie legen fest welche Aktivitäten die neue Rolle sehen und bearbeiten kann. Die Berechtigungen basieren auf den aktivierten Modulen.
1. Um die neue Rolle zu erstellen klicken Sie auf **Anlegen**.

### Erstellen einer globalen Rolle

Administratorinnen und Administratoren können unter: **Administration** - **Benutzer und Berechtigungen** - **Rollen und Rechte** neue globale Rollen erstellen. Nachdem Sie ausgewählt haben, dass sie eine neue Rolle erstellen möchten, müssen Sie hierfür in der **Checkbox Globale Rolle** den Haken setzen. Die Ansicht ändert sich und zeigt Ihnen die möglichen globalen Berechtigungen die Sie auswählen können.

- **Projekte erstellen**
- **Hinweis:** Um ein Unterprojekt für ein bereits existierendes Projekt zu erstellen, wird eine Berechtigung für diesen Schritt benötigt.
- **Create backups**
- **Benutzer erstellen**
- **Benutzer bearbeiten**
- **Hinweis:** Dies erlaubt Administratorinnen und Administratoren anderen Rollen erweiterte Rechte zu geben  oder Recht zu entfernen. Diese Benutzerinnen und Benutzer können die Einstellungen anderer Benutzerinnen und Benutzer verändern ohne dabei Administratorinnen und Adminstratoren sein zu müssen. Dies stellt ein Sicherheitsrisiko dar und sollte mit Vorsicht behandelt werden.
- **Platzhalterbenutzerinnen und Benutzer erstellen, bearbeiten und löschen**
- **Hinweis:** Benutzerinnen und Benutzer mit diesen globalen Berechtigungen können nicht automatisch alle Platzhalterbenutzerinnen und Benutzer sehen und bearbeiten. Es ist den Platzhalterbenutzerinnen und Benutzern vorbehalten Projektmitlgieder zu sehen und zu bearbeiten.
- **Rollen bearbeiten und löschen:** Um eine existierende Rolle zu bearbeiten klicken Sie auf den Namen der Rolle auf der Übersicht. Ändern sie die gewünschten Punkte und speichern Sie Ihre Änderungen. Um eine Rolle zu löschen klicken Sie auf **Löschen** (Papierkorb) in der Rollen-Ansicht.
- **Hinweis:** Rollen die einer Benutzerin oder einem Benutzer zugewiesen sind können nicht gelöscht werden.

## Gruppen

Eine **Gruppe** ist definiert als eine Liste mit Benutzerinnen und Benutzern. Diese sind einem Projekt zugeordnet und den Listenmitgliedern ist eine ausgewählte Rolle zugewiesen.

Neue **Gruppen** können unter: **Administration** - **Benutzer** **und** **Berechtigungen** - **Gruppen** erstellt werden.

Projekte erlaubt Ihnen maßgeschneiderte **Gruppen** zu erstellen, in denen Benutzerinnen und Benutzer unterschiedliche Benutzerrechte für projektbezogene Rollen gewährt werden können. Anstatt einzelne Benutzerinnen und Benutzer einem Projekt hinzuzufügen ist es Ihnen möglich eine vollständige **Gruppe** hinzuzufügen wie z. B. Marketing. Sie können bereits existierende **Gruppen** ändern, neue erstellen oder Gruppenmitglieder hinzufügen oder entfernen, sowie **Gruppen** löschen.

![Benutzer und Berechtigungen](media/12ae8e5e8aef2feb6ab1c36cc1ce23fad72f3aed.png "Benutzer und Berechtigungen")

### Eine neue Gruppe hinzufügen

Nachdem Sie Gruppen unter Administration ausgewählt haben, öffent sich eine Liste in der Sie alle bereits existierenden Gruppen auffinden. Zusätzlich wird Ihnen die Schaltfläche **Neue Gruppe** angezeigt. Klicken Sie danach auf **Anlegen**.

![Gruppenliste](media/e7b2c3960ca77ad0010d9ffc71516ccb1510f619.PNG "Gruppenliste")

Klicken Sie auf die Schaltfläche **Neue Gruppe** und wählen Sie einen Namen für Ihre Gruppe aus.

![Gruppenname wählen](media/3018ae5528036609e02bb2f3d60738d20febd93a.PNG "Gruppenname wählen")

### Gruppenmitglieder hinzufügen, ändern oder Gruppe löschen

Um einer **Gruppe** Mitglieder hinzuzufügen oder eine **Gruppe** zu ändern klicken sie auf den Namen der **Gruppe**. Um eine **Gruppe** zu löschen klicken Sie auf die **Löschen-Schaltfläche** (Papierkorb).

![Gruppe löschen](media/4b77953e9dd63672327ffc28307e094fbd34542f.PNG "Gruppe löschen")

Wenn Sie auf den Namen der **Gruppe** klicken, öffent sich ein neue Fenster. In diesem ist es Ihnen möglich:

- Gruppennamen zu ändern
- Gruppenmitglieder hinzuzufügen oder zu entfernen
- Gruppen Projekten zuzuweisen

![Gruppe bearbeiten](media/dd43d9b75f882dffe6c475f27f09f469bfed3f5b.PNG "Gruppe bearbeiten")

### Einer Gruppe Mitglieder hinzufügen

Klicken Sie auf die Registerkarte **Benutzer**. Wählen Sie Benutzerinnen und Benutzer aus die Sie Ihrer **Gruppe** hinzufügen möchten. Durch die Eingabe des Benutzerinnen oder Benutzernamens unter **Neuer Benutzer** erhalten Sie mehrere Auswahlmöglichkeiten durch das **Drop-down-Menü**. Wenn Sie Ihr gewünschtes Gruppenmitglied gefunden haben, klicken Sie auf **Hinzufügen**.

Hinzugefügte Gruppenmitglieder können Sie rechts vom **Drop-down-Menü** sehen. Um ein Gruppenmitglied zu entfernen klicken Sie auf das **✖** rechts neben dem Namen des Gruppenmitglieds.

![Gruppenmitglieder hinzufügen](media/b467670dacf5a0410395a25dcf7891b9770865b2.png "Gruppenmitglieder hinzufügen")

Wenn Sie eine Benutzerin oder einen Benutzer einer **Gruppe** hinzufügen, wird diese oder dieser auch automatisch der Liste der verfügbaren Benutzerinnen under Benutzer für alle Projekte hinzugefügt. Das Entfernen eines Gruppenmitglieds aus einer **Gruppe** löscht diesen auch aus allen Projekten in denen diese **Gruppe** vorhanden war. Wenn das Gruppenmitglied nicht noch durch eine andere Rolle in das Projekt eingebunden war, wird dieses komplett aus dem Projekt entfernt.

### Eine Gruppe einem Projekt hinzufügen

Wählen Sie die Registerkarte **Projekte** aus. Wählen Sie im **Drop-down-Menü** von unter **Neues Projekt** ihr gewünschtes Projekt aus. Klicken Sie die Rollen an die in Ihrer Gruppe vorhanden sein sollen. Danach klicken Sie auf **Hinzufügen**. Die Gruppenmitglieder wurden dem Projekt hinzugefügt und können die ihnen zugewiesenen Rollen verfolgen.

![Projekt zuweisen](media/dff802bcc08b8e5395a8afa1ef11ac8927d0d737.PNG "Projekt zuweisen")

### Globale Rollen einer Gruppe

Klicken Sie auf die Registerkarte **Globale Rollen**. Wählen Sie die **Globalen Rollen** aus die Sie für Ihre **Gruppe** verwenden wollen. Klicken Sie auf hinzufügen. Um **Globale Rollen** verwenden zu können müssen diese zuvor für das System erstellt worden sein.

### Auswirkungen von Gruppen auf Projektmitglieder

**Gruppen** haben Auswirkungen auf Projektmitgliederlisten und Benutzerinnen- und Benutzerdetails. Änderungen an **Gruppen** oder Projektmitgliedern oder Benutzerinnen und Benutzern können Auswirkungen auf die jeweils anderen Punkte haben.

### Gruppenprofile

Ähnlich zu Benutzerinnen und Benutzern, haben **Gruppen** eine Profilseite auf der ihre Namen und Mitglieder angezeigt werden. Jedes Gruppenmitglied ist nur für Benutzerinnen und Benutzer sichtbar, wenn die dafür notwendigen Berechtigungen vorhanden sind.

Um das Gruppenprofil zu sehen gibt es mehrere Mögllichkeiten:

- über die Gruppenliste
- über die Projektübersicht
- oder auch über die Arbeitspakete

![Gruppenprofil aus Projektübersicht](media/0ae3a2bb7b127d30ee387c27e32f05deb0e8473a.png "Gruppenprofil aus Projektübersicht")
