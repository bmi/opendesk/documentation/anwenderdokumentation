# Den Kalender verwenden

## Arbeitspakete anzeigen

In einem neuen Kalender werden Ihnen alle Arbeitspakete innerhalb des aktuellen Projekt als horizontale Streifen angezeigt. Die Streifen ziehen sich über die gesamte Zeit, einschließlich des Start- und des Enddatums, die für das Arbeitspaket geplant ist. Die Farbe weist auf den Arbeitpaket-Typ hin (orange ist eine Phase, grün ist ein Meilenstein usw.). Der Titel des Arbeitspakets wird im Streifen angezeigt, soweit die Länge dies zulässt.

Der aktuelle Monat wird automatisch ausgewählt.

1. Mit den Steuerelementen Vorschau/Nächster Pfeil (←, →) in der linken oberen Ecke können Sie sich in der Zeit vorwärts oder rückwärts bewegen.

1. Der **today** Button bringt Sie zurück zum aktuellen Datum.

Mit dem Umschalter month (Monat) / week (Woche) in der oberen rechten Ecke können Sie zwischen der Monats- und der Wochenansicht wechseln.  Mehrere Arbeitspakete am selben Tag werden auf einen vertikalen Stapel organisiert. Durch Klick auf einen Arbeitspaket-Streifen öffnet sich die Detailansicht des Arbeitspakets in der rechten Bildschirmhälfte.

![Kalender-Detailansicht-Arbeitspaket](media/5c972218dbac5436cf64e7f3b39899ba969de65a.jpg "Kalender-Detailansicht-Arbeitspaket")

## Arbeitspakete erstellen

Erstellen Sie neue Arbeitspakete direkt im Kalender. Dazu klicken Sie auf ein einzelnes Datum. Sie können auch über einen Datumsbereich hinweg klicken und ziehen. Ein neues Formular für ein Arbeitspaket öffnet sich in der rechten Bildschirmhälfte. Das gewählte Datum oder der gewählte Datumsbereich ist bereits ausgefüllt. Denken Sie daran, Ihre Eingaben zu **speichern**.

![Kalender-Arbeitspakete-direkt-erstellen](media/12d72b1f471542a3aecbda9236ef27eb16d24647.jpg "Kalender-Arbeitspakete-direkt-erstellen")

## Arbeitspakete bearbeiten

Um den zeitlichen Rahmen eines Arbeitspaketes zu verändern, können Sie ganz einfach den Streifen des Pakets im Kalender bearbeiten:
- Ändern Sie das Startdatum, indem Sie den Ziehgriff am linken Rand des Streifens nehmen und ihn verlängern oder verkürzen.
- Ändern Sie das Enddatum, indem Sie den Ziehgriff am rechten Rand des Streifens nehmen und ihn verlängern oder verkürzen.

Verwenden Sie den Anfasser und ziehen Sie einen ganzen Streifen, um das Arbeitspaket zeitlich nach vorne oder hinten zu verschieben, ohne die Dauer zu verändern.  **Hinweis**: Der Kalender markiert Nicht-Arbeitstage mit einer anderen Hintergrundfarbe. In der Regel kann ein Arbeitspaket nicht so verschoben oder verändert werden, dass es an diesen Tagen beginnt oder endet. Sie können diese Einschränkung in den Einstellungen zu Daten und Dauer der Arbeitspakete bearbeiten (siehe dort). Der Schalter **Nur Arbeitstage** muss dabei inaktiv gestellt werden.

Eine dreitägige Aufgabe, die am Donnerstag beginnt und am Montag endet, verteilt sich zum Beispiel auf 5 Kalendertage. Wenn Sie dasselbe Arbeitspaket so verschieben, dass es an einem Dienstag beginnt und an einem Donnerstag endet, erstreckt es sich über 3 Kalendertage. In beiden Fällen beträgt die Dauer 3 Tage.

## Filter

Um die Arbeitspakete, die in Ihrem Kalender angezeigt werden, zu filten, können Sie eine beliebige Anzahl von Filtern verwenden. So richten Sie sich Ihren Kalender so ein, dass er nur die für Sie relevanten Arbeitspakete anzeigt. Das kann für Sie selbst (privater Kalender) oder auch für das Team sein (öffentlicher Kalender; dieser ist für alle Projektmitglieder sichtbar).

Klicken Sie oben rechts auf die Schaltfläche **Filter**. Die Details öffnen sich. Wählen Sie aus dem Dropdown von **+ Filter hinzufügen** weitere Kriterien aus.

![Kalender-Filter-Auswahl](media/b16e9d294fcc923ec2f648f66cf9878bd4e46282.jpg "Kalender-Filter-Auswahl")

## Andere Projekte einbeziehen

Sie können im Kalender auch die Arbeitspakete aus anderen Projekten anzeigen lassen. Gehen Sie zur Schaltfläche Enthaltene Projekte und wählen Sie aus dem Dropdown die gewünschten Projekte aus oder ab, die Sie anzeigen lassen wollen. Sie können auch automatisch alle Unterprojekte für gewählte Projekte einbinden. Dazu setzen Sie ein Häkchen bei **Alle Unterprojekte einbeziehen** am unteren Rand des Dialogfeldes.

![Kalender-Projekte-einbinden](media/b97de6a2a5a1ff19ce96a0aa97e6da178dd71845.jpg "Kalender-Projekte-einbinden")

## Einen Kalender in die Projektübersicht einbetten

Sie können die Projektübersicht auch um eine Kalenderanzeige ergänzen. Wählen Sie dazu auf der Übersichtsseite an der gewünschten Stelle das Pluszeichen aus, um ein weiteres Widget hinzuzufügen. Ein neues Fenster öffnet sich. Dort wählen Sie Kalender aus:

![Kalender-als-Widget-in-der-Projektübersicht](media/23602d49916e6ccb4af0a3fb956886eac301e8b8.jpg "Kalender-als-Widget-in-der-Projektübersicht")

## Zen-Modus

Um ungestört zu arbeiten, können Sie den Kalender auch im Vollbildmodus anzeigen lassen. Die Navigationsleiste und die Seitenleiste sind dann ausgeblendet. Den Vollbildmodus erreichen Sie durch Klick auf die **quadratische graue Schaltfläche mit vier Pfeilen (Zen-Modus aktivieren)**, die in jede Ecke deuten. Sie ist oben rechts im Menü zu finden. Drücken Sie die Escape-Taste (**Esc**), um den Zen-Modus zu beenden.

![Kalender-Zen-Modus-Aktivieren](media/6440b1581e6296817fd301fc92c53a428295db90.jpg "Kalender-Zen-Modus-Aktivieren")
