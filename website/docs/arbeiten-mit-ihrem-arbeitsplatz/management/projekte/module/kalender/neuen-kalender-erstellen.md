# Neuen Kalender erstellen

Im Kalender-Modul erhalten Sie einen Überblick über die Arbeitspakete, die für einen bestimmten Zeitraum (Monat, Woche, ...) geplant sind. So können Sie besser im Blick halten, welche Aufgaben anstehen und wie sich das Projekt entwickelt hat und weiter entwickeln wird. Bitte aktivieren Sie zunächst das Kalendermodul in Ihrer **Projektkonfiguration**, um Kalender nutzen zu können.

![Projektkonfiguration-Kalender-Ausgewählt](media/feb6279e791dcc1e411eafbecd4cb86318b4465c.jpg "Projektkonfiguration-Kalender-Ausgewählt")

## Einen neuen Kalender erstellen

Klicken Sie in der linken Seitenleiste auf **Kalender** um zum entsprechenden Modul mit einer Liste aller vorhandenen Kalender zu gelangen. Zu Beginn wird diese Liste leer sein.

Wählen Sie die Schaltfläche **+ Kalender**, um einen neuen Kalender zu erstellen. Diese Schaltfläche ist oben rechts in der Nähe Ihres Avatars und unten in der Seitenleiste auf der linken Seite zu finden.

- Klicken Sie auf einen bestehenden (gespeicherten) Kalender, um ihn anzusehen.
- Passen Sie die Sichtbarkeit eines Kalenders an: Dazu gehen Sie auf das **Drei-Punkte-Menü (mehr)** oben rechts und wählen **Sichtbarkeits-Einstellungen**.

**Hinweis**: Jeder Kalender, der als **favorisiert** gekennzeichnet wird, wird links in der Seitenleiste unter **Favorit** aufgeführt. Jeder Kalender, der als **Öffentlich** gekennzeichnet wird, wird links in der Seitenleiste unter **Öffentlich** aufgeführt. Jeder Kalender, der nicht als **Öffentlich** markiert ist, gilt als **Privat** und wird auf der linken Seite unter **Privat** aufgeführt.

![Kalender-Menü-Sichtbarkeitseinstellungen-markiert](media/3700de4d0a8844b0a9ce50cb4a4d36dfabe82c5c.jpg "Kalender-Menü-Sichtbarkeitseinstellungen-markiert")

![Kalender-Sichtbarkeit-Auswahl-öffentlich-favorisiert](media/878af935fb3f35819d69d6f2c1d02df8e167993c.jpg "Kalender-Sichtbarkeit-Auswahl-öffentlich-favorisiert")
