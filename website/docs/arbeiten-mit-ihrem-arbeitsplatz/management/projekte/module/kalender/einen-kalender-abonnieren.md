# Einen Kalender abonnieren

Sie können einen Projekte-Kalender über einen externen Client abonnieren und darauf zugreifen. Solche Clients sind zum Beispiel Thunderbird, Open-Xchange, Apple Kalender oder Google Calendar. So können Sie Ihren Projektplan von jedem kompatiblen Gerät aus beobachten, ohne sich direkt in Projekte einzuloggen.

**Hinweis:** Sie können nur gespeicherte Kalender abonnieren. Daher müssen Sie einen neu erstellten Kalender erst speichern, bevor Sie ihn abonnieren können.

So abonnieren Sie einen Kalender:

1. Klicken Sie auf die **Drei-Punkte-Schaltfläche (mehr)** in der Symbolleiste und wählen Sie **Kalender** **abonnieren**.

1. Es öffnet sich ein Fenster. Vergeben Sie einen Namen für diesen Kalender. Beachten Sie, dass Kalendernamen nur einmal vergeben werden können. Es empfiehlt sich, den Namen danach zu wählen, von wo aus Sie den Kalender abonnieren (z. B. “Tablet” oder “Dienst-Handy”).

1. Klicken Sie auf **URL kopieren**. Damit wird ein Kalendertoken erstellt und die Kalender-URL in die Zwischenablage kopiert.

Fügen Sie diese URL in den gewünschten Kalender-Client ein, um den Projekte-Kalender zu abonnieren.  

**Wichtig:** Bitte teilen Sie diese URL nicht mit anderen. Jede Person kann mit diesem Link die Details des Arbeitspakets einsehen, ohne ein Konto oder Passwort zu benötigen.

![Kalender-abonnieren-und-Namen-vergeben](media/f42bb34ec286c7a100ac47756725b25ca0eb099e.jpg "Kalender-abonnieren-und-Namen-vergeben")

![Kalendertoken-erstellt-und-in-der-Zwischenablage](media/e919733a9b3839050eb0bfd6c003afd5394d80ef.jpg "Kalendertoken-erstellt-und-in-der-Zwischenablage")
