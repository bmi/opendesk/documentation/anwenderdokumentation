# Der gesamte Projektmanagement-Lebenszyklus

Das Modul Projekte bietet alle wichtigen Funktionen zur Unterstützung von Projektteams während des gesamten Projektmanagement-Zyklus.

Das Modul Projekte ermöglicht einen reibungslosen Ablauf der Projektarbeit und -kommunikation. Von der Idee bis zum Abschluss und der Dokumentation können Sie alles in Projekte erledigen.

| Projektphase                    | Dokumentationen für                                                                                                                                                                                                                  |
| ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Projektkonzept und -initiierung | Sammeln von Ideen. Festlegung des Projektumfangs und der zu erbringenden Leistungen. Darunter: Projekt einrichten, erste Ideen dokumentieren, Projektbeschreibung, Mitglieder einladen.                                              |
| Projektdefinition und -planung  | Erstellen einer Projektübersicht mit detaillierten Informationen. Anlegen einer Roadmap oder einen Projektplans                                                                                                                      |
| Projektstart oder -durchführung | Verwalten Sie alle Projektaktivitäten wie Aufgaben, Ergebnisse, Risiken, Funktionen, Bugs oder Änderungswünsche. Verwenden Sie agile Boards mit Ihren Teams, dokumentieren Sie Meetings oder teilen Sie Neuigkeiten.                 |
| Projektleistung und -kontrolle  | Erstellen und verwalten eines Projektbudgets. Verfolgung und Bewertung von Zeit und Kosten. Benutzerdefinierte Berichte für einen genauen, aktuellen Einblick in die Projektleistung und die zugewiesenen Ressourcen.                |
| Projektabschluss                | Dokumentieren Sie Projektergebnisse, Gelerntes, bewährte Praktiken und fassen Sie die wichtigsten Ergebnisse eines Projekts einfach zusammen. Archivieren Sie Projekte, um später auf Inhalte und Gelerntes zurückgreifen zu können. |

### Hier geht es weiter mit:

- [Projektkonzept und Projektinitiierung](der-gesamte-projektmanagement-lebenszyklus/projektkonzept-und-projektinitiierung.md)
- [Projektdefinition und -planung](der-gesamte-projektmanagement-lebenszyklus/projektdefinition-und-planung.md)
- [Projektstart oder -durchführung](der-gesamte-projektmanagement-lebenszyklus/projektstart-oder-durchfuehrung.md)
- [Projektper­for­mance und Projektkontrolle](der-gesamte-projektmanagement-lebenszyklus/projektperformance-und-projektkontrolle.md)
- [Projekte beenden](der-gesamte-projektmanagement-lebenszyklus/projekte-beenden.md)
