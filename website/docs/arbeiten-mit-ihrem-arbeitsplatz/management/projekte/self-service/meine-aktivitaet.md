# Meine Aktivität

Sie gelangen zu einer Übersicht Ihrer Aktivitäten, indem Sie rechts oben auf Ihr Profilbild bzw Ihr Namenskürzel navigieren und dort **Meine Aktivität** aus dem Dropdown auswählen. Sie sehen nun alle Ihre aktuellen Aktionen und Projekte, an denen Sie beteiligt sind.

In der Regel sehen Sie zwei Listen:

- **Projekte** zeigen alle Projekte, in denen Sie Mitglied sind.
- **Aktivität** zeigt alle Ihre Aktivitäten an, die in Projekte aufgezeichnet werden.  

**Hinweis:** Bitte beachten Sie, dass nur Aktivitäten von Projekten angezeigt werden, die das Modul **Aktivität** aktiviert haben. Dieses können Sie in den Projektkonfigurationen unter Module vornehmen.

![Aktivität](media/a88cf57f2d1bd40dfea48cfe0df48b2db303e931.jpg "Aktivität")
