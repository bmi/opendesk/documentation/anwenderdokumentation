# Ersteinrichtung

## Ersteinrichtung von Projekte für Administratoren

Dieser Abschnitt wird Sie durch einige grundlegende Empfehlungen zur Einrichtung von Projekte als Systemadministrator führen und Ihnen zeigen wie Sie diese Kenntnisse nutzen können, um Projekte für Benutzerinnen und Benutzer vorzubereiten. Für die Einrichtung des Backends und die ersten technischen Konfigurationen für die On-Premise-Editionen, sehen Sie sich bitte den entsprechenden Abschnitt im Installationshandbuch an.
