# API und Webhooks

Hier können Sie alle API-bezogenen Einstellungen konfigurieren. API-Token erlauben es Drittanbieter-Anwendungen, mit Projekte zu kommunizieren. Wenn Sie noch kein API-Token erstellt haben, ist diese Liste leer. **Bitte beachten Sie**: Sie benötigen möglicherweise Administratorrechte, um eine API erstellen zu können.

Ebenso können Sie hier Webhooks zu Projekte hinzufügen und verwalten.

## API

Verwalten Sie hier den REST-Webdienst. Damit können Sie gezielt steuern, ob fremde Anwendungen vom Browser aus auf Ihre Projekte-API-Endpunkte Zugriff erhalten. Sie können auch die maximale Seitengröße festlegen, mit der die API antworten wird. Es wird nicht möglich sein, API-Anfragen durchzuführen, die mehr Werte auf einer einzigen Seite zurückgeben.

![Formular-Schnittstelle-API](media/e8884b55e4c7ad42dcd6422015ca6be7471d61fb.jpg "Formular-Schnittstelle-API")

## Documentation

Wenn die Dokumentationsseite aktiviert ist, sehen Sie eine interaktive APIv3 Dokumentation unter dem folgenden Link.

![API-Dokumentation](media/72a6f1e8e736f9e568c895f8a1ec660eef681632.jpg "API-Dokumentation")

## Cross-Origin Resource Sharing (CORS)

Um CORS-Header zu aktivieren, die von der Projekt APIv3 zurückgegeben werden, aktivieren Sie das Kontrollkästchen auf dieser Seite. Dadurch wird es auch für abhängige Authentifizierungsendpunkte aktiviert, wie z. B. OAuth-Endpunkte und dergleichen.

Anschließend müssen Sie die zulässigen Werte für den Origin-Header eingeben, auf die Projekte den Zugriff zulassen soll. Dies ist notwendig, da authentifizierte Ressourcen von Projekte nicht für alle Origins mit dem Header-Wert zugänglich sein können.

![API-CORS-Formular](media/7779342e70b0ad89b8d929829e6437c592873c93.jpg "API-CORS-Formular")

## Webhook

Um andere Anwendungen in Projekte zu integrieren, können Sie Webhooks einrichten. Zum Beispiel wäre, die gebuchten Zeiten von Arbeitspaketen in eine Abrechnungsanwendung zu übertragen.

Klicken Sie auf **+ Webhook**, um einen neuen Webhook hinzuzufügen.

![Webhooks-Übersicht-und-Hinzufügen](media/6cd8e0809e6df2b7813a358a27d687fc204bbfa0.jpg "Webhooks-Übersicht-und-Hinzufügen")

Und so stellen Sie die Webhooks ein:

- Wählen Sie einen Namen, der den Webhook identifiziert.
- Payload URL definiert den Endpunkt, der aufgerufen wird, wenn der Webhook ausgelöst wird.
- Wählen Sie frei eine zusätzliche Beschreibung, um den Zweck des jeweiligen Webhooks näher zu bestimmen.
- Durch die Definition eines Signaturgeheimnisses garantieren Sie, dass der Absender der Payload-Anfrage tatsächlich Projekte ist. Der Client wird dann dieses Signaturgeheimnis überprüfen.
- Aktivieren Sie, wenn der Webhook aktiv sein soll.
- Legen Sie fest, für welche Ereignisse der Webhook aktiviert werden soll, z.B. Webhook für das Aktualisieren oder Erstellen von Projekten oder Arbeitspaketen oder für das Erstellen von Zeiteinträgen.
- Wählen Sie aus, für welche Projekte der Webhook aktiv sein soll. Sie können alle Projekte oder nur bestimmte Projekte auswählen. Wenn Sie z.B. das Projekt "Systemadministrationsleitfaden" auswählen, wird ein Ereignis (z.B. Erstellen eines neuen Zeiteintrags) über den Webhook abgefeuert. Dies geschieht nur, wenn ein Benutzer oder eine Benutzerin innerhalb der ausgewählten Projekte Zeit einträgt.

Denken Sie daran am Ende **Anlegen** zu klicken um Ihre Änderungen zu speichern.

**Hinweis**: Sie müssen nach unten scrollen, um **Anlegen** zu finden. Dort können Sie auch Ihre Eingabe abbrechen.

![Formular-Webhook-hinzufügen](media/c5b8a4799b5914f5924ec30b2ac317511a7d91fa.jpg "Formular-Webhook-hinzufügen")
