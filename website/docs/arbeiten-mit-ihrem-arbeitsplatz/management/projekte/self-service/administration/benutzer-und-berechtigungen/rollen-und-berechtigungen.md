# Rollen und Berechtigungen

## Benutzerinnen und Benutzer

Eine Benutzerin oder ein Benutzer ist jede Person die sich bei Projekte anmelden kann.

## Berechtigungen

Berechtigungen entscheiden darüber was Benutzerinnen und Benutzer in Projekte sehen und bearbeiten können. Berechtigungen werden an Benutzerinnen und Benutzer für bestimmte Rollen zugewiesen.

## Rollen

Eine Rolle bündelt eine bestimmte Auswahl an Berechtigungen. Es ist eine gute Möglichkeit um ohne viel Aufwand Berechtigungen an mehrere Benutzerinnen und Benutzer einer Organisation zu verteilen, die die Selben Berechtigungen zur Bearbeitung ihrer Projekte benötigen.

Eine Benutzerin oder ein Benutzer kann eine oder mehrere Rollen mit unterschiedlichen Berechtigungen besitzen. Mithilfe von diesen ist es dieser Person möglich, in unterschiedlichen Projekte verschiedene Informationen zu sehen und zu bearbeiten.

## Administratorinnen und Administratoren

Administratorinnen und Administratoren haben vollen Zugang zu allen Einstellungen und allen Projekten die in Projekte zur verfügung stehen. Die Berechtigungen von Administratorinnen und Administratoren können nicht geändert werden.

- **Umfang der Rolle:** Volle Kontrolle über alle Aspekte die die Anwendung bietet
- **Berechtigungsbeispiele:** Bearbeiten der globalen Rollen, Bearbeiten von Rollen in Projekten, Vergabe von Administrationsrechten an andere Benutzerinnen und Benutzer
- **Anpassungsoptionen:** Können nicht geändert werden

## Globale Rolle

Globale Rollen erlauben es Administratorinnen und Administratoren Verwaltungsaufgaben an andere Benutzerinnen und Benutzer zu verteilen.

- **Umfang der Rolle:** Berechtigungen für ausgewählte Verwaltungsaufgaben
- **Berechtigungsbeispiele:** Verwaltung von Benutzerinnen und Benutzern, Erstellen von Projekten
- **Anpassungsoptionen:** Administratorinnen und Administratoren können neue globale Rollen erstellen und an diese Rollen globale Berechtigungen verteilen

## Projekt-Rolle

Eine Projekt-Rolle ist eine Reihe von Berechtigungen die an alle Benutzerinnern und Benutzer vergeben werden kann. Es können mehrere Rollen an eine Benutzerin oder einen Benutzer vergeben werden.

**Hinweis:** Ist ein Modul in einem Projekt nicht aktiviert, wird es der Benutzerin oder dem Benutzer nicht angezeigt, wenn die dafür benötigten Berechtigungen nicht vorhanden sind.

- **Umfang der Rolle:** Berechtigungen die sich auf bestimmte Projekte beschränken
- **Berechtigungsbeispiele:** Erstellen von Arbeitspaketen, Löschen von Wiki-Seiten
- **Anpassungsoptionen:** Erstellen von unterschiedlichen Projektrollen mit verschiedenen Berechtigungen

## Nicht-Mitglied

Ein Nicht-Mitglied ist die Standardrolle von Benutzerinnen und Benutzern, die noch nicht Teil eines Projektes sind. Dies gilt nur wenn das Projekt in den **Einstellungen** auf **öffenltich** gesetzt wurde.

**Hinweis:** Die Nicht-Mitglied-Rolle kann nicht gelöscht werden.

- **Umfang der Rolle:** Berechtigungen für ausgewählte Projekte für Benutzerinnen und Benutzer die angemeldet sind
- **Berechtigungsbeispiele:** Ansicht von Arbeitspaketen für Benutzerinnen und Benutzer die angemeldet sind
- **Anpassungsoptionen:** Ausgewählte Berechtigungen für Nicht-Mitglieder

## Anonym

Projekte erlaubt dir Projektinformationen mit anonymen Benutzerinnen und Benutzern zu teilen die nicht in Projekte angemeldet sind. Das ist hilfreich wenn es darum geht Projektziele und Aktivitäten in einer öffentlichen Community zu teilen.

**Hinweis:** Dies funktioniert nur wenn ausgewählt wurde das eine Authentifizierung in Ihrer Projekte Instanz nicht notwendig ist, da diese in den **Einstellungen** auf **öffentlich** gesetzt wurde. Die Anonym-Rolle kann nicht gelöscht werden.

- **Umfang der Rolle:** Berechtigungen die für ausgewählte Projekte vorgesehen sind, für Benutzerinnen und Benutzer die nicht angemeldet sein müssen
- **Berechtigungsbeispiele:** Ansicht von Arbeitspaketen für Benutzerinnen und Benutzer die nicht angemeldet sein müssen
- **Anpassungsoptionen:** Ausgewählte Berechtigungen für die Rolle Anonym

## Anpassung von Rollen mit ausgewählten Berechtigungen

Administratorinnen und Administratoren können neue Rollen mit ausgewählten Berechtiugen erstellen oder bereits existierende ändern.

## Berechtigungsübersicht

Der Berechtigungsübersicht ist ein guter Ausgangspunkt um eine Übersicht über die aktuellen Einstellungen der Rollen und Berechtigungen zu bekommen.

Die Berechtigungsübersicht finden sie unter: **Administration** - **Benutzer und Berechtigungen** - **Berechtigungsübersicht**.

## Erstellen einer neuen Projektrolle

Administratorinnen und Administratoren können neue Projektrollen unter: **Administration** - **Benutzer und Berechtigungen** - **Rollen und Rechte** erstellen. Klicken Sie auf die Schaltfläche **Neue Rolle** um eine neue Rolle zu erstellen.

Folgen Sie den weiteren Schritten um eine neue Rolle zu erstellen:

1. **Name:** Vergeben Sie einen neuen Namen für die Rolle.
1. **Globale Rolle:** Erstellen Sie eine neue globale Rolle.
1. **Workflow kopieren von:** Sie können eine bereits existierende Rolle auswählen und diese inklusive ihres Workflows kopieren, um daraus eine neue Rolle zu erzeugen.

**Berechtiungen:** Hier ist es Ihnen möglich die Berechtigungen zu vergeben. Sie legen fest welche Aktivitäten die neue Rolle sehen und bearbeiten kann. Die Berechtigungen basieren auf den aktivierten Modulen.  Um die neue Rolle zu erstellen klicken Sie auf **Anlegen**.

## Erstellen einer globalen Rolle

Administratorinnen und Administratoren können unter: **Administration** - **Benutzer und Berechtigungen** - **Rollen und Rechte** neue globale Rollen erstellen. Nachdem Sie ausgewählt haben, dass sie eine neue Rolle erstellen möchten, müssen Sie hierfür in der **Checkbox Globale Rolle** den Haken setzen. Die Ansicht ändert sich und zeigt Ihnen die möglichen globalen Berechtigungen die Sie auswählen können.

- **Projekte erstellen**
- **Hinweis:** Um ein Unterprojekt für ein bereits existierendes Projekt zu erstellen, wird eine Berechtigung für diesen Schritt benötigt.
- **Create backups**
- **Benutzer erstellen**
- **Benutzer bearbeiten**
- **Hinweis:** Dies erlaubt Administratorinnen und Administratoren anderen Rollen erweiterte Rechte zu geben  oder Recht zu entfernen. Diese Benutzerinnen und Benutzer können die Einstellungen anderer Benutzerinnen und Benutzer verändern ohne dabei Administratorinnen und Adminstratoren sein zu müssen. Dies stellt ein Sicherheitsrisiko dar und sollte mit Vorsicht behandelt werden.
- **Platzhalterbenutzerinnen und Benutzer erstellen, bearbeiten und löschen**
- **Hinweis:** Benutzerinnen und Benutzer mit diesen globalen Berechtigungen können nicht automatisch alle Platzhalterbenutzerinnen und Benutzer sehen und bearbeiten. Es ist den Platzhalterbenutzerinnen und Benutzern vorbehalten Projektmitlgieder zu sehen und zu bearbeiten.
- **Rollen bearbeiten und löschen:** Um eine existierende Rolle zu bearbeiten klicken Sie auf den Namen der Rolle auf der Übersicht. Ändern sie die gewünschten Punkte und speichern Sie Ihre Änderungen. Um eine Rolle zu löschen klicken Sie auf **Löschen** (Papierkorb) in der Rollen-Ansicht.
- **Hinweis:** Rollen die einer Benutzerin oder einem Benutzer zugewiesen sind können nicht gelöscht werden.
