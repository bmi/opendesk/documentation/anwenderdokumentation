# Benutzereinstellungen

Der Abschnitt für Benutzereinstellungen umfasst allgemeine Einstellungen wie die Standardsprache, das Löschen von Benutzern und die Benutzerzustimmung.

Benutzereinstellungen sind für Administratorinnen und Administratoren über die Projekte-Administration zugänglich.

![Benutzereinstellungen](media/7062ccee6f5ebeaa215d8a98b40f49f9202740de.jpg "Benutzereinstellungen")

## Standardeinstellungen

Der Abschnitt für Standardeinstellungen umfasst die Standardbenutzereinstellungen.

Hier können Sie die Standardsprache für neue Benutzerinnen und Benutzer sowie die Standard-Zeitzone festlegen.

Die Standardsprache wird Benutzerinnen und Benutzern angezeigt, wenn sie sich zum ersten Mal bei Projekte anmelden. Sie können dann eine andere Sprache auswählen.

Sie können auch festlegen, ob Erfolgsmeldungen (z.B. auf der Arbeitspaket-Seite) automatisch ausgeblendet werden sollen.

Diese Einstellungen können von Benutzerinnen und Benutzern später in ihrem Benutzerkonto geändert werden.

![Standardsprache, Standard-Zeitzone](media/f26d2573d440de490389895376e6bcc6c1d6c733.jpg "Standardsprache, Standard-Zeitzone")

## Anzeigeformat

Der Abschnitt für das Anzeigeformat behandelt, wie das Format des Benutzernamens angezeigt werden sollte.

## Benutzer löschen

![Benutzerlöschung](media/6355bd6dd6016e1c6e35ba317722375a3f531abb.jpg "Benutzerlöschung")

Im Abschnitt **Löschen** können Sie festlegen, wer Benutzerkonten löschen darf.

Standardmäßig können nur Administratorinnen und Administratoren Konten löschen. Wenn diese Option aktiviert ist, können Administratorinnen und Administratoren zur Benutzerliste navigieren, ein Benutzerkonto auswählen und auf die Option **Löschen** oben rechts klicken, um ein Konto zu löschen.

Zusätzlich können Sie die Option **Nutzer dürfen ihre Konten löschen** auswählen. Wenn diese Option aktiviert ist, können Benutzer ihre eigenen Benutzerkonten von der Seite **Mein Konto** aus löschen.

Wenn Sie verhindern möchten, dass Benutzer ihre eigenen Konten löschen, wird empfohlen, diese Option zu deaktivieren.

![Benutzer löschen](media/949b78903b857b47517a4e7cc00fd432b6b3c1b8.PNG "Benutzer löschen")

## Nutzer-Einwilligung

![Nutzer-Einwilligung](media/81b4a64230e42e9e1d085885408fc0dd884aacc3.jpg "Nutzer-Einwilligung")

Datenschutz und Sicherheit haben in Projekte höchste Priorität. Um den Vorschriften der DSGVO zu entsprechen, kann in Projekte ein Einwilligungsformular konfiguriert werden. Wenn die Option **Einwilligung erforderlich** aktiviert ist, wird Benutzern beim ersten Mal, wenn sie sich bei Projekte anmelden, ein Einwilligungsformular angezeigt.

Standardmäßig wird in dem Einwilligungsformular auf die Datenschutz- und Sicherheitsrichtlinie von Projekte verwiesen. Wenn Sie zusätzliche Informationen haben, zu denen Sie Ihre Benutzerinnen und Benutzer um Einwilligung bitten möchten, können Sie diese im Abschnitt **Informationen zur Einwilligung** verlinken.

Darüber hinaus können Sie die E-Mail-Adresse eines Ansprechpartners für Einwilligungen angeben. Diese Benutzerin oder Benutzer kann dann benachrichtigt werden, wenn eine Änderung oder ein Löschen von Daten erforderlich ist.
