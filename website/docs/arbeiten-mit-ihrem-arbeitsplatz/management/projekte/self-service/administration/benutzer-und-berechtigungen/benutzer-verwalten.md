# Benutzer verwalten

## Benutzer verwalten

Das Sperren und Entsperren von Benutzerinnen und Benutzern wird ebenfalls aus der Benutzerliste aus durchgeführt. Um den Zugriff einer Benutzerin oder eines Benutzers zu deaktivieren, klicken Sie auf die **Sperren-Schaltfläche** (Schloss) neben einer Benutzerin oder einem Benutzer. Verwenden Sie die **Entsperren-Schaltfläche** (Schloss), um den Zugriff der Benutzerin oder des Benutzers wiederherzustellen.

Wenn Sie Enterprise Cloud oder Enterprise On-Premises verwenden, wird das Sperren einer Benutzerin oder eines Benutzers eine Benutzerlizenz freigeben, sodass Sie einer anderen Benutzerin oder einem anderem Benutzer innerhalb Ihres gebuchten Plans zum System hinzufügen können.

**Hinweis**: Die bisherigen Aktivitäten einer gesperrten Benutzerin oder eines gesperrten Benutzers werden weiterhin im System angezeigt.

## Benutzerliste

Unter **Benutzer** finden Sie den Ort, an dem Benutzerinnen und Benutzer verwaltet werden. Sie können zu dieser Liste hinzugefügt, bearbeitet oder gelöscht werden. Die Liste kann auch nach gefiltert werden.Durch Klicken auf die Spaltenüberschriften können Sie die Sortierreihenfolge ändern. Pfeile zeigen die Sortierreihenfolge an, nach oben für aufsteigend (a-z/0-9) und nach unten für absteigend (z-a/9-0). Die Seitensteuerung wird am unteren Ende der Liste angezeigt. In der Liste können Sie sehen, wer Systemadministrator7in bzw- Systemadministrator in Projekte ist.

![Benutzerliste](media/1862a1da7681db9e99d97a0c44c8ea337a0cc2fe.jpg "Benutzerliste")

## Benutzer filtern

Am Anfang der Benutzerliste finden Sie eine Filter-Schaltfläche.Filtern Sie nach Status, Gruppe oder Namen und klicken Sie dann auf die Schaltfläche **Anwenden**, um die Liste zu filtern. Klicken Sie auf die Schaltfläche **Zurücksetzen**, um die Filterfelder zurückzusetzen und die Liste neu zu laden.

**Status** - Wählen Sie aus **Aktiv**, **Alle, Eingeladen** oder **Temporär gesperrt** aus. Jede Auswahl zeigt die Anzahl der Benutzerinnen und Benutzer an.
**Gruppe** - Wählen Sie aus der Liste der vorhandenen Gruppen aus.
**Name** - Geben Sie einen beliebigen Text ein, dieser kann einen Platzhalter % für 0 oder mehr Zeichen enthalten. Der Filter gilt für Benutzernamen, Vornamen, Nachnamen und E-Mail-Adresse.

## Sperren und Entsperren von Benutzern

Das Sperren und Entsperren von Benutzerinnen und Benutzern wird ebenfalls aus der Benutzerliste aus durchgeführt. Um den Zugriff einer Benutzerin oder eines Benutzers zu deaktivieren, klicken Sie auf die **Sperren-Schaltfläche** (Schloss) neben einer Benutzerin oder einem Benutzer. Verwenden Sie die **Entsperren-Schaltfläche** (Schloss), um den Zugriff der Benutzerin oder des Benutzers wiederherzustellen.

Wenn Sie Enterprise Cloud oder Enterprise On-Premises verwenden, wird das Sperren einer Benutzerin oder eines Benutzers eine Benutzerlizenz freigeben, sodass Sie einer anderen Benutzerin oder einem anderem Benutzer innerhalb Ihres gebuchten Plans zum System hinzufügen können.

**Hinweis**: Die bisherigen Aktivitäten einer gesperrten Benutzerin oder eines gesperrten Benutzers werden weiterhin im System angezeigt.

![Benutzer sperren](media/fc5efc84ebd96e21b7576cc4237b4b5586ff4d3b.jpg "Benutzer sperren")

Wenn eine Benutzerin oder ein Benutzer wiederholt fehlgeschlagene Anmeldeversuche ausführt, wird die Benutzerin oder Benutzer vorübergehend gesperrt, und in der Benutzerinnen und Benutzerliste wird eine Schaltfläche für **Fehlgeschlagene Anmeldeversuche zurücksetzen** angezeigt. Klicken Sie auf die Schaltfläche, um ihn zu entsperren, oder warten Sie, und er wird automatisch entsperrt. Werfen Sie einen Blick auf den Abschnitt **Andere Authentifizierungseinstellungen für fehlgeschlagene Versuche und Sperrzeit**, um weitere Informationen zu erhalten.

## Benutzerin oder Benutzer erstellen

Neue Benutzerinnen und Benutzer können von einer Administratorin oder einem Administrator oder von den Benutzerinnen und Benutzern selbst (wenn aktiviert) erstellt und konfiguriert werden.

### Benutzer einladen (als Administrator)

Klicken Sie in der Benutzerliste auf die Schaltfläche **Neuer** **Benutzer**, um das Formular für neue Benutzerinnen und Benutzer zu öffnen.

![Neuer Benutzer](media/9a1d119feb7ec90007f384c2203c3be99af8a33b.jpg "Neuer Benutzer")

Geben Sie die E-Mail-Adresse, den Vornamen und den Nachnamen der neuen Benutzerin oder des neuen Benutzers ein. Setzen Sie ein Häkchen, um ihn oder sie zu einer Administratorin-Benutzer oder einem Administrator-Benutzer zu machen.

**Hinweis**: Das E-Mail-Feld muss ein gültiges Format haben und eindeutig sein, sonst wird es beim Klicken auf die Schaltfläche abgelehnt.

Klicken Sie auf die Schaltfläche **Anlegen**, um die Benutzerin oder den Benutzer hinzuzufügen und die Detailseite dieser Benutzerin oder des Benutzers anzuzeigen. Klicken Sie auf die Schaltfläche **Anlegen und weiter**, um die Benutzerin oder den Benutzer hinzuzufügen und im Formular für neue Benutzerinnen und Benutzer zu bleiben, um eine weitere Benutzerin oder einen weiteren Benutzer hinzuzufügen. In jedem Fall wird die neue Benutzerin oder der neue Benutzer per E-Mail eingeladen. Wenn Sie den letzten von mehreren Benutzerinnen oder Benutzern hinzufügen, können Sie auf **Erstellen** klicken oder auf den Link **Benutzer** im Menü auf der linken Seite klicken. Die Benutzerliste wird angezeigt. Klicken Sie auf den Namen jeder Benutzerin oder jedes Benutzers, um deren Details zu bearbeiten.

## Benutzerin oder Benutzer erstellen (über Selbstregistrierung)

Um Benutzerinnen und Benutzern die Möglichkeit zu geben, ihre eigenen Benutzerkonten zu erstellen, aktivieren Sie die Selbstregistrierung in den Authentifizierungseinstellungen. Eine Person kann dann ihr eigenes Benutzerkonto von der Startseite aus erstellen, indem sie oben rechts auf die Schaltfläche **Anmelden** klickt und dann auf den Link **Ein neues Konto erstellen** im Anmeldefeld.

Geben Sie Werte in alle Felder ein (sie dürfen nicht leer bleiben). Das E-Mail-Feld muss eine gültige E-Mail-Adresse sein, die in diesem System nicht verwendet wird. Klicken Sie auf die Schaltfläche **Erstellen**. Je nach den Einstellungen wird das Konto erstellt, es kann jedoch sein, dass es noch von einem Administrator aktiviert werden muss

## Benutzerinnen und Benutzer aktivieren

Öffnen Sie die Benutzerliste unter **Benutzer**. Wenn eine Benutzerin oder ein Benutzer sein eigenes Konto erstellt hat (und es nicht automatisch aktiviert wurde), wird es in der Benutzerliste mit einem Link **Aktivieren** auf der rechten Seite angezeigt. Klicken Sie auf diesen Link und fahren Sie fort, die Details für diese Benutzerin oder diesen Benutzer wie unten beschrieben hinzuzufügen. Es gibt auch eine **Aktivieren** Schaltfläche oben auf der Detailseite der Benutzerin oder des Benutzers.

## Anfangsdaten festlegen

Sie können die Details eines neu erstellten Benutzers bearbeiten. Nützliche Felder könnten Benutzername, Sprache und Zeitzone sein. Sie können auch Projekte, Gruppen und Tarife ausfüllen oder dies dem Projekt-Ersteller überlassen. Berücksichtigen Sie auch die Authentifizierungseinstellungen.

## Erneute Benutzereinladung per E-Mail senden

Wenn eine Benutzerin oder ein Benutzer die E-Mail-Einladung nicht erhalten hat oder seine Authentifizierungsmethode auf E-Mail ändern möchte, können Sie die Einladung bei Bedarf erneut an die Benutzerin oder den Benutzer senden. In der Benutzerliste klicken Sie auf den Namen der Benutzerin oder des Benutzers, an den Sie die E-Mail mit dem Einladungslink zum System erneut senden möchten.

Klicken Sie oben rechts auf die Schaltfläche **Einladung senden**, um die E-Mail erneut zu senden

![Einladung senden](media/680e03b1a22ee8a91c365350bc0bc4fe6f747efb.jpg "Einladung senden")

## Benutzereinladungen löschen

Um die Einladung einer Benutzerin oder eines Benutzers ungültig zu machen oder zurückzuziehen, klicken Sie auf den Benutzernamen und dann in der oberen rechten Ecke auf **Löschen**. Dadurch wird verhindert, dass die eingeladene Benutzerin oder der eingeladene Benutzer sich anmeldet.

**Bitte beachten Sie:** Dies funktioniert nur für Benutzerinnen und Benutzer, die sich noch nicht angemeldet haben. Wenn die Benutzerin oder der Benutzer bereits aktiv ist, wird dadurch sein gesamtes Profil und Konto gelöscht. Das Löschen von Benutzerinnen und Benutzern kann nicht rückgängig gemacht werden.

## Benutzereinstellungen verwalten

Sie können individuelle Benutzerdetails verwalten, indem Sie auf den Benutzernamen in der Liste klicken. Diese Einstellungen überschreiben die individuellen Benutzereinstellungen, die in ihren **Mein Konto** -Einstellungen festgelegt wurden.

## Allgemeine Einstellungen

Im Allgemeinen Tab werden folgende Felder angezeigt:

![Allgemeine Einstellungen](media/7ab4bbac9167e74a670f483885756ee946ea7011.jpg "Allgemeine Einstellungen")

Im Allgemeinen Tab werden folgende Felder angezeigt:

1. Stammdaten des Benutzers

- **Status** - dieser wird vom System festgelegt.
- **Benutzername** - dieser wird standardmäßig auf die E-Mail-Adresse einer neuen Benutzerin oder eines neuen Benutzers gesetzt (sofern die Person die Selbstregistrierung nicht verwendet hat). Dieser kann auf dieser Seite geändert werden. Benutzerinnen und Benutzer können ihren eigenen Benutzernamen nicht ändern.
- **Vorname, Nachname, E-Mail** - diese Felder werden von der Seite **Neuer Benutzer** ausgefüllt. Benutzerinnen und Benutzer können sie unter ihrem **Profil** ändern; sie sind obligatorisch.
- **Sprache** - diese wird standardmäßig aus den Benutzereinstellungen übernommen. Benutzerinnen und Benutzer können dies auf ihrer **Profilseite** ändern.
- **Administrator** - aktivieren oder deaktivieren Sie diese globale Rolle. Benutzerinnen und Benutzer können dies nicht ändern.
- **Benutzerdefinierte Felder** - wenn solche erstellt wurden, werden sie hier angezeigt. Sie können sie beispielsweise für Abteilung oder Telefonnummer verwenden. Andernfalls können auf diese Weise benutzerdefinierte Felder erstellt werden.

**Benutzereinwilligung** - wenn dies konfiguriert wurde (d. h. wenn das Kästchen neben 'Einwilligung erforderlich' aktiviert ist), wird der Einwilligungsstatus hier angezeigt. 

2. **Authentifizierung** - der Inhalt dieses Abschnitts hängt von der verwendeten Authentifizierungsmethode ab (z. B. Passwort, OpenID, Kerberos, etc.).

3. **Preferenzen** - Benutzerinnen und Benutzer können diese auf ihrer **Profilseite** ändern. Die Zeitzone wird standardmäßig aus der gewählten Sprache übernommen. **Erfolgsmeldungen automatisch ausblenden** bedeutet, dass Benachrichtigungen nach einigen Sekunden automatisch entfernt werden, nicht dass es überhaupt keine Erfolgsmeldungen gibt.

4. Vergessen Sie nicht, Ihre Änderungen zu **speichern**.

## Zurücksetzen des Passworts

Um ein neues Passwort für zu erstellen (z.B. wenn er/sie es verloren hat), wechseln Sie zur Authentifizierungssektion des Allgemeinen Tabs. Sie können entweder ein zufälliges Passwort zuweisen (aktivieren Sie das Kontrollkästchen oben) oder ein neues Passwort manuell festlegen und es an die Benutzerin oder den Benutzer senden (vorzugsweise über eine sichere Kommunikation). Sie sollten auch das Kontrollkästchen **Passwortänderung bei der nächsten Anmeldung erzwingen** in Betracht ziehen.

## Person zu einem Projekt hinzufügen

Um ein Projekt sehen und daran arbeiten zu können, muss ein Person Mitglied eines Projekts sein und mit einer bestimmten Rolle diesem Projekt hinzugefügt werden.

Im Projekte-Tab wählen Sie das neue Projekt aus der Dropdown-Liste aus, wählen die Rollen für dieses Projekt aus und klicken auf die violette Schaltfläche **Hinzufügen**.

![Benutzer zu einem Projekt hinzufügen](media/c8aa831421b70bfc8d2354be86d01072e6f43779.jpg "Benutzer zu einem Projekt hinzufügen")

## Person zu Gruppen hinzufügen

Im Gruppen-Tab sehen Sie die Gruppen, denen der Personen angehört. Wenn eine Gruppe angezeigt wird, klicken Sie auf den Gruppennamen-Link.

Wenn keine Gruppen angezeigt werden (d. h. die Benutzerin oder der Benutzer gehört noch keiner Gruppe an), klicken Sie auf den Link **Gruppen verwalten**, um Gruppen zu bearbeiten.

**Hinweis**: Der Gruppen-Tab wird nur angezeigt, wenn in Projekte mindestens eine Benutzergruppe vorhanden ist.

## Globale Rollen

Um einer Person eine globale Rolle hinzuzufügen, muss im System mindestens eine globale Rolle erstellt werden (eine Rolle mit dem aktivierten Feld **Globale Rolle**).

Im Tab **Globale Rollen** wählen Sie die globalen Rolle(n) für diese Person aus oder deaktivieren sie. Klicken Sie auf die Schaltfläche **Hinzufügen.**

![Globale Rollen](media/5f6bf746246f41642ecae68aec0dcb7746a374c8.jpg "Globale Rollen")

## Benachrichtigungseinstellungen

Unter dem Tab **Benachrichtigungseinstellungen** können Sie die Benachrichtigungseinstellungen für Benutzerinnen und Benutzer bearbeiten. Jeder Person kann diese Einstellungen unter **Mein Konto** selbst anpassen.

## Email-Erinnerungen

Unter dem Tab **Email-Erinnerungen** können Sie die Einstellungen für Email-Erinnerungen bearbeiten. Jeder Person kann diese Einstellungen unter **Mein Konto** selbst anpassen.

## Stundensatz-Historie

Der Tab **Stundensatz-Historiezeigt** die stündlichen Raten, die für die Benutzerin oder den Benutzer definiert wurden. Die Standardrate wird auf Projekte angewendet, für die keine Rate festgelegt ist. Alle Projekte, denen die Benutzerin oder der Benutzer angehört, werden mit den Raten der Person aufgeführt.

Das **Gültig ab** -Datum beeinflusst die Rate, die beim Erstellen eines Budgets und beim Erfassen von Arbeitszeiten verwendet wird.

Wenn Sie für die Benutzerin oder den Benutzer auf verschiedenen Projekten unterschiedliche stündliche Raten festlegen möchten, können Sie die Standardrate in den jeweiligen Projekten durch eine andere Rate überschreiben.

Um eine neue stündliche Rate einzugeben, klicken Sie auf das Aktualisierungssymbol neben der **Stundensatz-Historiezeigt**. Sie können entweder eine Standardstundensatz festlegen oder eine Rate für ein bestimmtes Projekt definieren

1. Geben Sie ein Datum ein, ab dem die Rate gültig ist.
1. Geben Sie die (stündliche) Rate ein. Die Währung kann nur in den entsprechenden Einstellungen geändert werden.
1. Sie können eine stündliche Rate löschen.
1. Sie können eine Rate für einen anderen Zeitraum hinzufügen.
1. Speichern Sie Ihre Änderungen

## Avatar

Der Tab **Avatar** zeigt das Standard-Symbol, das für diese Benutzerin oder diesen Benutzer angezeigt werden soll. Ein benutzerdefiniertes Bild kann als Avatar hochgeladen werden. Außerdem können die Benutzerinnen und Benutzer auch ihren Gravatar verwenden. Die Benutzerin oder der Benutzer kann dies in seinem Profil verwalten. Diese Funktionen können in den **Avatar-Einstellungen** deaktiviert werden.

### Zwei-Faktor-Authentifizierung (2FA)

Dieser Tab zeigt an, ob eine Benutzerin oder ein Benutzer in seinem Konto ein Gerät für die Zwei-Faktor-Authentifizierung aktiviert hat. Sie können die Geräte sehen und sie bei Bedarf löschen.

## Authentifizierung

Die verfügbaren Authentifizierungsmethoden beeinflussen den Inhalt des Abschnitts **Authentifizierung** im Allgemeinen Tab der Benutzerdetails.

Verwenden Sie das Feld zur Selbstregistrierung, um die folgenden Steuerelemente über den Zugriff einer neuen Benutzerin oder eines neuen Benutzers zu haben.

## Manuelle Kontofreischaltung

Im Abschnitt **Authentifizierung** der Benutzerdetails gibt es die Felder **Zufälliges Passwort zuweisen**, **Passwort**, **Bestätigung** und **Passwortänderung erzwingen**.

Wenn Sie sich in der Nähe des neuen Benutzers befinden, können Sie ein Passwort und eine Bestätigung eingeben und dem Benutzer mitteilen, was es ist. Er kann sich dann anmelden. Es wird empfohlen, auch das Kontrollkästchen **Passwortänderung erzwingen** zu aktivieren, damit der Benutzer nach der Anmeldung aufgefordert wird, sein Passwort zu ändern. Sie können den neuen Benutzer anrufen oder ihm eine E-Mail senden, ohne Projekte zu verwenden, um ihm das Passwort mitzuteilen. In diesem Fall ist es wichtiger, das Kontrollkästchen **Passwortänderung erzwingen** zu aktivieren. Aktivieren Sie das Kontrollkästchen **Zufälliges Passwort zuweisen** und wahrscheinlich auch das Kontrollkästchen **Passwortänderung erzwingen**. Wenn die Details gespeichert werden, sendet Projekte eine E-Mail an den neuen Benutzer mit seinem Passwort.

## Aktivierung des Kontos

Per E-Mail lassen Sie alle Felder leer. Wenn die Details gespeichert werden, sendet eine E-Mail an die neue Benutzerin oder den neuen Benutzer mit einem Link, der der Benutzerin oder den Benutzer zu Projekte einlädt. Sie klicken auf den Link, um zur Registrierungsseite zu gelangen und die Erstellung ihres Kontos abzuschließen

## Benutzerinnen oder Benutzer löschen

Zwei **Einstellungen** ermöglichen es, Benutzerinnen und Benutzern aus dem System zu löschen:

- Benutzerkonten können von Administratorinnen und Administratoren gelöscht werden - wenn aktiviert, wird eine Schaltfläche **Löschen** auf der Benutzerdetails-Seite angezeigt

Benutzerinnen und Benutzer dürfen ihre Konten löschen - wenn aktiviert, wird im **Mein Konto** -Bereich eine Menüeintragschaltfläche **Konto löschen** angezeigt. Um das Konto einer anderen Benutzerin oder eines anderen Benutzers zu löschen, öffnen Sie die Benutzerliste. Klicken Sie auf den Benutzernamen der Benutzerin oder des Benutzers, den Sie löschen möchten. Klicken Sie oben rechts auf die Schaltfläche **Löschen**.

Dann werden Sie aufgefordert, den Benutzernamen einzugeben, um die Benutzerin bzw. den Benutzer dauerhaft aus dem System zu löschen, und dies mit Ihrem Passwort zu bestätigen.

**Hinweis**: Das Löschen eines Benutzerinnenkontos bzw. eines Benutzerkontos ist eine dauerhafte Aktion und kann nicht rückgängig gemacht werden. Die früheren Aktivitäten dieser Benutzerin bzw. dieses Benutzers werden immer noch im System angezeigt, jedoch der Benutzerin bzw. dem Benutzer als **Gelöscht** zugewiesen. Dies gilt auch für die Module Zeit und Kosten sowie Budget. Aufgewendete Zeit bleibt innerhalb eines Arbeitspakets weiterhin sichtbar als **Gelöscht**. Zeit- und Kostenberichte enthalten Einträge mit Verweis auf die Benutzerin bzw. den Benutzer **Gelöscht**. Auch Laborbudgets, die für die Benutzerin bzw. für den Benutzer eingerichtet wurden, werden unter dem **Benutzer** **Gelöscht** angezeigt. Wenn Sie den Namen der Benutzerin oder des Benutzers in Verbindung mit den genannten Aktivitäten, der aufgewendeten Zeit und dem Budget weiterverfolgen möchten, können Sie die Benutzerin oder den Benutzer einfach sperren, um seinen Namen in den historischen Daten beizubehalten.
