# Avatare

Um zu sehen welche Avatare in Projekte genutzt werden können navigieren Sie zu: **Administration** - **Benutzer und Berechtigungen** - **Avatars**.

Sie können auswählen ob Benutzerinnen und Benutzer **Gravatare** oder **selbsterstellte Profilbilder** verwenden können oder nicht.

**Avatare** können über die Benutzereinstellungen verändert werden.

![Avatare](media/345e9dab79f66590accf7f9935be49e6d3414896.PNG "Avatare")
