# Gruppen verwalten

Eine **Gruppe** ist definiert als eine Liste mit Benutzerinnen und Benutzern. Diese sind einem Projekt zugeordnet und den Listenmitgliedern ist eine ausgewählte Rolle zugewiesen.

Neue **Gruppen** können unter: **Administration** - **Benutzer** **und** **Berechtigungen** - **Gruppen** erstellt werden.

Projekte erlaubt Ihnen maßgeschneiderte **Gruppen** zu erstellen, in denen Benutzerinnen und Benutzer unterschiedliche Benutzerrechte für projektbezogene Rollen gewährt werden können. Anstatt einzelne Benutzerinnen und Benutzer einem Projekt hinzuzufügen ist es Ihnen möglich eine vollständige **Gruppe** hinzuzufügen wie z. B. Marketing. Sie können bereits existierende **Gruppen** ändern, neue erstellen oder Gruppenmitglieder hinzufügen oder entfernen, sowie **Gruppen** löschen.

## Eine neue Gruppe hinzufügen

Nachdem Sie Gruppen unter Administration ausgewählt haben, öffent sich eine Liste in der Sie alle bereits existierenden Gruppen auffinden. Zusätzlich wird Ihnen die Schaltfläche **Neue Gruppe** angezeigt. Klicken Sie danach auf **Anlegen**.

![Gruppenliste](media/e7b2c3960ca77ad0010d9ffc71516ccb1510f619.PNG "Gruppenliste")

Klicken Sie auf die Schaltfläche **Neue Gruppe** und wählen Sie einen Namen für Ihre Gruppe aus.

![Gruppenname wählen](media/3018ae5528036609e02bb2f3d60738d20febd93a.PNG "Gruppenname wählen")

## Gruppenmitglieder hinzufügen, ändern oder Gruppe löschen

Um einer **Gruppe** Mitglieder hinzuzufügen oder eine **Gruppe** zu ändern klicken sie auf den Namen der **Gruppe**. Um eine **Gruppe** zu löschen klicken Sie auf die **Löschen-Schaltfläche** (Papierkorb).

![Gruppe löschen](media/4b77953e9dd63672327ffc28307e094fbd34542f.PNG "Gruppe löschen")

Wenn Sie auf den Namen der **Gruppe** klicken, öffent sich ein neue Fenster. In diesem ist es Ihnen möglich:

- Gruppennamen zu ändern
- Gruppenmitglieder hinzuzufügen oder zu entfernen
- Gruppen Projekten zuzuweisen

![Gruppe bearbeiten](media/dd43d9b75f882dffe6c475f27f09f469bfed3f5b.PNG "Gruppe bearbeiten")

## Einer Gruppe Mitglieder hinzufügen

Klicken Sie auf die Registerkarte **Benutzer**. Wählen Sie Benutzerinnen und Benutzer aus die Sie Ihrer **Gruppe** hinzufügen möchten. Durch die Eingabe des Benutzerinnen oder Benutzernamens unter **Neuer Benutzer** erhalten Sie mehrere Auswahlmöglichkeiten durch das **Drop-down-Menü**. Wenn Sie Ihr gewünschtes Gruppenmitglied gefunden haben, klicken Sie auf **Hinzufügen**.

Hinzugefügte Gruppenmitglieder können Sie rechts vom **Drop-down-Menü** sehen. Um ein Gruppenmitglied zu entfernen klicken Sie auf das **✖** rechts neben dem Namen des Gruppenmitglieds.

![Gruppenmitglieder hinzufügen](media/f832e33f0c5ea64c446a27ce1055e2c6e2c43388.PNG "Gruppenmitglieder hinzufügen")

Wenn Sie eine Benutzerin oder einen Benutzer einer **Gruppe** hinzufügen, wird diese oder dieser auch automatisch der Liste der verfügbaren Benutzerinnen under Benutzer für alle Projekte hinzugefügt. Das Entfernen eines Gruppenmitglieds aus einer **Gruppe** löscht diesen auch aus allen Projekten in denen diese **Gruppe** vorhanden war. Wenn das Gruppenmitglied nicht noch durch eine andere Rolle in das Projekt eingebunden war, wird dieses komplett aus dem Projekt entfernt.

## Eine Gruppe einem Projekt hinzufügen

Wählen Sie die Registerkarte **Projekte** aus. Wählen Sie im **Drop-down-Menü** von unter **Neues Projekt** ihr gewünschtes Projekt aus. Klicken Sie die Rollen an die in Ihrer Gruppe vorhanden sein sollen. Danach klicken Sie auf **Hinzufügen**. Die Gruppenmitglieder wurden dem Projekt hinzugefügt und können die ihnen zugewiesenen Rollen verfolgen.

![Projekt zuweisen](media/dff802bcc08b8e5395a8afa1ef11ac8927d0d737.PNG "Projekt zuweisen")

## Globale Rollen einer Gruppe

Klicken Sie auf die Registerkarte **Globale Rollen**. Wählen Sie die **Globalen Rollen** aus die Sie für Ihre **Gruppe** verwenden wollen. Klicken Sie auf hinzufügen. Um **Globale Rollen** verwenden zu können müssen diese zuvor für das System erstellt worden sein.

## Auswirkungen von Gruppen auf Projektmitglieder

**Gruppen** haben Auswirkungen auf Projektmitgliederlisten und Benutzerinnen- und Benutzerdetails. Änderungen an **Gruppen** oder Projektmitgliedern oder Benutzerinnen und Benutzern können Auswirkungen auf die jeweils anderen Punkte haben.

## Gruppenprofile

Ähnlich zu Benutzerinnen und Benutzern, haben **Gruppen** eine Profilseite auf der ihre Namen und Mitglieder angezeigt werden. Jedes Gruppenmitglied ist nur für Benutzerinnen und Benutzer sichtbar, wenn die dafür notwendigen Berechtigungen vorhanden sind.

Um das Gruppenprofil zu sehen gibt es mehrere Mögllichkeiten:

- über die Gruppenliste
- über die Projektübersicht
- oder auch über die Arbeitspakete

![Gruppenprofil aus Projektübersicht](media/43c240567826b94ab26ca9fb99a7f5e60760f1f4.PNG "Gruppenprofil aus Projektübersicht")
