# Aufzählungen

Im Bereich **Aufzählungen** Ihres Administrationsbereichs können Sie, je nach Berechtigung, verschiedene Kategorien um Auswahlmöglichkeiten ergänzen oder diese bearbeiten. Dazu zählen u.a.:

1. Dokumenten-Kategorien: Hier können Sie ergänzen, welche Kategorien für Dokumente zur Auswahl stehen sollen. Sie können in der Auswahl eine Standardkategorie festlegen, die Kategorien aktiv bzw. inaktiv schalten, die Anzeigen-Reihenfolge bearbeiten oder die Kategorie ganz löschen.
1. Arbeitspaket-Prioritäten: Hier können Sie ergänzen, welche weiteren Prioritäten zur Auswahl stehen sollen. Sie können eine Standardkategorie festlegen, die Prioritäten aktiv bzw. inaktiv schalten, ihnen Farben zuweisen, die Anzeigen-Reihenfolge bearbeiten oder die Priorität ganz löschen.

Aktivitäten für Zeiterfassung: Hier können Sie ergänzen, welche Tätigkeiten zur Zeiterfassung zur Auswahl stehen sollen. Sie können in der Auswahl einen Standardwert festlegen, die Aktivitäten aktiv bzw. inaktiv schalten, die Anzeigen-Reihenfolge bearbeiten oder die Aktivität ganz löschen.  Für alle Bereiche gilt: Klicken Sie das **Pluszeichen (+)**, wenn Sie die Aufzählung **ergänzen** möchten, klicken Sie auf den **Titel** der Auswahlmöglichkeit um diese zu **bearbeiten** und klicken Sie auf das **Papierkorb-Symbol** am Ende der Zeile um den Eintrag zu **löschen**.

![Aufzählungen-Überblick](media/ca9360ed7d4e9808a4d333bb6fcdfbfeeaa1f99b.jpg "Aufzählungen-Überblick")
