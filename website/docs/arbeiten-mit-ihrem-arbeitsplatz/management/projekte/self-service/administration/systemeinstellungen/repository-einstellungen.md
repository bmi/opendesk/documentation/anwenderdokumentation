# Projektarchive

**Wichtiger Hinweis**: Die Unterstützung für verwaltete GIT- und SVN-Archive in Projekte ist derzeit ausschließlich in installationsbasierten Paketinstallationen verfügbar. In Docker-basierten Installationen wird diese Funktion nicht unterstützt.

Um die Einstellungen für Projekt-Archive anzupassen, gehen Sie zu den Systemkonfigurationen auf der Registerkarte **Projektarchiv** , wo Sie die folgenden Optionen einstellen können:

**Automatisches Abrufen von Archiv-Änderungen:** Diese Option ermöglicht das automatische Anzeigen von Commits in einem Projekte-Archiv.

**Cache für die Größe des Archiv-Datenträgers:** Um die Größe des Archiv-Datenträgers zu zwischenspeichern (zum Beispiel auf der Archiv-Seite angezeigt).

**Aktivieren des Archiv-Verwaltungs-Webservices:** Diese Option ermöglicht die Kommunikation mit SVN- oder Git-Clients. Wenn sie deaktiviert ist, kann kein Archiv erstellt werden.

**API-Schlüssel definieren:** Dieser API-Schlüssel wird einmal erstellt und für die Kommunikation mit einem SVN- oder Git-Client verwendet.

**Auswahl des SCM (Source Code Management):** Sie können Git und/oder Subversion auswählen.

**Automatischer Archiv-Anbietertyp:** Legen Sie den Standard-Archiv-Typ fest, der in neuen Projekten verwendet wird.

**Codierungen für Archive definieren.**

**Maximale Anzahl der anzuzeigenden Revisionen im Datei-Log definieren.**

**Maximale Anzahl der im Archiv-Browser angezeigten Dateien definieren.**

**Caching für Authentifizierungsanfragen von Versionskontrollsoftware aktivieren:** Diese Option ermöglicht das Speichern der Authentifizierung, damit ein Benutzer sich nicht jedes Mal authentifizieren muss, wenn auf ein Archiv zugegriffen wird (zum Beispiel während Commits).

## Checkout-Anweisungen für Subversion und Git

1. Wählen Sie aus, ob die Chekout-Anweisungen auf der Archiv-Seite angezeigt werden sollen oder nicht.
1. Basis-URL für das Checkout festlegen. Definieren Sie die Basis-URL, die für Archive neuer Projekte verwendet werden soll.
1. Hilfetext für Checkout-Anweisung. Wird verwendet, um einen (optionalen) Anleitungstext für Archive anzugeben (kann in den Projekteinstellungen weiter spezifiziert werden).

## Arbeitspaket-Beziehungen und Status in Commit-Log-Meldungen

**Referenzschlüsselwörter für Arbeitspakete in Commit-Nachrichten definieren:** Diese dienen dazu, Schlüsselwörter festzulegen, die verwendet werden, um Revisionen mit Arbeitspaketen zu verknüpfen.

**Fixierungsschlüsselwörter für Arbeitspakete in Commit-Nachrichten definieren:** Fixierungsschlüsselwörter ermöglichen Status- oder Fortschrittsänderungen durch bestimmte Schlüsselwörter in Commit-Nachrichten. Zum Beispiel Ändern eines Arbeitspakets in den Status **Geschlossen** und Setzen auf 100%.

**Festlegen, welcher Status einem Arbeitspaket zugeordnet wird, wenn ein Fixierungswort in einer Commit-Nachricht verwendet wird.**

**Festlegen, welcher Prozentanteil als Erledigt auf ein Arbeitspaket angewendet wird, wenn ein Fixierungswort in einer Commit-Nachricht für dieses Arbeitspaket verwendet wird.**

**Zeiterfassung aktivieren:** Durch Aktivieren dieser Option wird die Protokollierung von Zeiten für ein Arbeitspaket über Commit-Nachrichten ermöglicht.

**Aktivität für erfasste Zeit definieren:** Diese Aktivität wird für die Zeiterfassung über einen Commit verwendet.
