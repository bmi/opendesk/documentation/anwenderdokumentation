# Projektsystemkonfigurationen

Um die Systemprojektkonfigurationen anzupassen, gehen Sie zu **Administration** -&gt; **Systemkonfigurationen** -&gt; **Projekte**, wo Sie die folgenden Optionen anpassen können.

## Einstellungen für neue Projekte

1. Überprüfen Sie, ob neue Projekte standardmäßig öffentlich sind. Das bedeutet, dass Benutzerinnen und Benutzer ohne Konto auf das Projekt zugreifen können, ohne sich anzumelden.
1. Wählen Sie aus, welche Module standardmäßig für neu erstellte Projekte aktiviert sein sollen.
1. Die Rolle, die einer Benutzerin oder einem Benutzer in einem neuen Projekt zugewiesen wird, wenn die Benutzerin oder der Benutzer ein neues Projekt erstellt, aber keine (globale) Administratorin bzw. kein (globaler) Administrator ist. Dies ergibt Sinn, wenn einer Benutzerin oder einem Benutzer die Berechtigung zum Erstellen eines neuen Projekts über eine globale Rolle erteilt wird.

![Projekteinstellungen](media/bd20b80e9fd512548a90bcdeb8f62392751990c6.png "Projekteinstellungen")

## Einstellungen für Projektliste

1. Wählen Sie aus, welche Spalten standardmäßig in der Übersichtsliste der Projekte sichtbar sein sollen.
1. Definieren Sie die Einstellungen für die Projekt-Portfolioübersicht.
1. Speichern Sie Ihre Anpassungen mit dem **Speichern-Button**.

![Einstellungen für Projektliste](media/dcf5d153526322c6183f26cf804de8d41be2a915.png "Einstellungen für Projektliste")
