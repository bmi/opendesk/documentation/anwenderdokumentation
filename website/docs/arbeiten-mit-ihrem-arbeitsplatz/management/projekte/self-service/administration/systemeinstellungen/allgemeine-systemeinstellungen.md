# Allgemeine Systemkonfigurationen

In Projekte haben Sie die Möglichkeit, allgemeine Systemkonfigurationen vorzunehmen. Unter **Systemkonfigurationen** auf der Registerkarte **Allgemein** können Sie die folgenden Optionen anpassen:

**Appklikationstitel:** Dieser Titel wird auf der Startseite der Anwendung angezeigt.

**Objekte pro Seite:** Definiert die Anzahl der Objekte, die auf einer einzelnen Seite angezeigt werden. Diese Einstellung wird für die Seitenübersicht in der Arbeitspaket-Tabelle verwendet. Sie können mehrere Werte, durch Kommas getrennt, eingeben. Beachten Sie, dass höhere Werte die initiale Ladezeit für Arbeitspaketseiten verlängern können.

**Tage auf Projektaktivität anzeigen:** Legt fest, wie weit zurück Projekttätigkeiten verfolgt und in der Aktivität des Projekts angezeigt werden.

**Host-Name:** Definiert den Host-Namen der Anwendung. In vielen Installationsarten wird diese Einstellung über eine Umgebungsvariable konfiguriert und ist nicht von der Benutzerin oder vom Benutzer zu bearbeiten.

**Cache für formatierten Text:** Erlaubt das Speichern von formatiertem Text im Cache, um das Laden von Wiki-Seiten zu beschleunigen.

**Feeds aktivieren:** Aktiviert RSS-Feeds auf Wiki-Seiten, in Foren und bei Neuigkeiten über einen RSS-Client. Sie können das Feed-Inhaltslimit festlegen.

**Exportlimit für Arbeitspakete und Projekte:** Definiert die maximale Anzahl von Elementen, die ein strukturierter Export für Arbeitspakete und Projekte enthalten kann. Ein höherer Wert ermöglicht das gleichzeitige Exportieren von mehr Elementen, geht jedoch mit einem höheren RAM-Verbrauch einher.

**Maximale Größe von Textdateien, die inline angezeigt werden:** Definiert die maximale Dateigröße, bis zu der verschiedene Versionen einer Datei nebeneinander angezeigt werden, wenn zwei Versionen in einem Repository verglichen werden (Diff).

**Maximale Anzahl der anzuzeigenden Diff-Zeilen:** Definiert die maximale Anzahl der Zeilen, die angezeigt werden, wenn zwei Versionen in einem Repository verglichen werden.

**Anzeige von Sicherheitsplakette:** Aktiviert die Anzeige einer Plakette mit dem Status Ihrer Installation im Informations-Verwaltungspanel und auf der Startseite. Diese Funktion wird nur für Administratoren angezeigt.

**Hinweis:** Wenn aktiviert, überprüft die Sicherheitsplakette Ihre aktuelle Projekte-Version mit der offiziellen Projekte-Versionen-Datenbank, um Sie über Aktualisierungen oder bekannte Sicherheitslücken zu informieren. Für weitere Informationen zur Funktionsweise der Überprüfung, den benötigten Daten und wie Sie diese Überprüfung deaktivieren können, lesen Sie bitte die Konfigurationsdokumentation. Vergessen Sie nicht, Ihre Eingaben zu **speichern**.

![Allgemeine Systemeinstellungen](media/6a8451de84790baae047066a9c22eb23ae86c6a8.png "Allgemeine Systemeinstellungen")

## Willkommenstext

Erstellen Sie einen Willkommens-Text, um den Benutzern auf der Startseite Ihrer Anwendung die wichtigsten Informationen anzuzeigen.

1. Fügen Sie einen Titel für das Willkommens-Textfeld ein.
1. Geben Sie eine Beschreibung für den Willkommens-Textblock ein.
1. Setzen Sie einen Haken, wenn Sie den Willkommenstext auf der Startseite anzeigen lassen möchten.
1. Klicken Sie anschließend auf **Speichern.**

![Willkommenstext bearbeiten](media/dedfa729a4378fc762796df594ea8b55b043df36.png "Willkommenstext bearbeiten")
