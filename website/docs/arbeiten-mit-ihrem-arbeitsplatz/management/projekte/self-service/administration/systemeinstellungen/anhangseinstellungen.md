# Anhänge

## Anhänge – Maximale Größe und erlaubte Dateitypen

Sie können die maximale Anhangsgröße (in kB) erhöhen oder verringern, indem Sie die entsprechende Größe in [kB] in das Eingabefeld **Max. Dateigröße** eintragen. Außerdem können Sie eine Positivliste der Dateiuploads anlegen, um verschiedene Dateitypen zu zulassen (z.B. *.jpg). Dazu machen Sie entsprechende Eingaben im Feld **Positivliste für Dateiuploads. Speichern** Sie Ihre Anpassungen.

![Anhangseinstellungen](media/e698d96922777030a0d99cbc7ffc4457304ad97b.png "Anhangseinstellungen")
