# Sprachen

Auf der Seite **Sprachen** können Sie die Sprachen auswählen, die Sie aktivieren möchten. Diese ausgewählten Sprachen stehen dann den individuellen Benutzern über ihre Benutzereinstellungen zur Verfügung und dienen als Standardsprache für das gesamte Programm.
