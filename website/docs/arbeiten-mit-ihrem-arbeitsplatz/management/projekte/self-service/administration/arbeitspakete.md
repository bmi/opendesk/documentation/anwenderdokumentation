# Arbeitspakete

In diesem Abschnitt erfahren Sie, wie Sie Arbeitspakete konfigurieren können.

### Hier geht es weiter mit:

- [Einstellungen](arbeitspakete/settings.md)
- [Typen](arbeitspakete/typen.md)
- [Status](arbeitspakete/status.md)
- [Workflow](arbeitspakete/workflow.md)
