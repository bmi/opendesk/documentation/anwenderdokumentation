# Backup

## Vorbereitung für das Backup

Sofern nicht in der **Konfiguration** deaktiviert, können Benutzerinnen und Benutzer von Projekte-Installationen Backups aus dem Verwaltungsbereich erstellen. Sie müssen entweder Administratorin oder Administrator sein oder die globale Berechtigung dazu haben.

Sie können selbst Backups Ihrer Projekte-Installation erstellen. Gehen Sie dazu in die Verwaltung und dann zu Backup, um zu beginnen.

![Backup](media/49f61dd10b3609b840100c6db1556cf050d87885.jpg "Backup")

Um ein Backup erstellen zu können, muss zuerst ein sogenanntes Backup-Token generiert werden. Dies soll eine zusätzliche Sicherheitsebene hinzufügen, da das Backup der gesamten Installation sensible Daten umfasst.

Sie müssen dann ein Backup-Token erstellen, indem Sie auf **+ Backup-Token** klicken.

![+Backup-Tocken](media/b6fb45ec72217c51ee599ce18e5649c18433e38e.jpg "+Backup-Tocken")

Sie werden aufgefordert, Ihr Passwort zu bestätigen, wenn Sie versuchen, ein Token zu generieren oder zurückzusetzen. Das Backup-Token wird nur einmal angezeigt, nachdem es generiert wurde. Stellen Sie sicher, dass Sie es an einem sicheren Ort aufbewahren.

Das System generiert der Token, das Sie dann in das unten angeforderte Feld eintragen. Anschließend können Sie die Schaltfläche **Backup anfordern** drücken.

![Backup Tocken erstellen](media/f967c5aa845cca1f10ad12c0761fe10896b77cee.jpg "Backup Tocken erstellen")

![Backup Projekte](media/42037de2ccd15f6eae27b78dd4c335397bde1336.jpg "Backup Projekte")

Jedes Mal, wenn Sie ein Backup anfordern, muss dieses Token angegeben werden. Dies gilt auch, wenn ein Backup über die API angefordert wird, wo zusätzlich zum API-Token auch das Backup-Token angegeben werden muss.

## Benachrichtigungen

Jedes Mal, wenn ein Backup-Token erstellt oder zurückgesetzt wird, wird eine E-Mail-Benachrichtigung an alle Administratorinnen und Administratoren gesendet, um alle darüber zu informieren, dass es eine neue Benutzerin oder einen neuen Benutzer mit Zugriff auf Backups gibt.

Nachdem Sie das Backup angefordert haben, erhalten Sie eine E-Mail-Benachrichtigung mit einem Link zum Herunterladen Ihres Backups. Dafür benötigen Sie eine zusätzliche Authentifizierung (Benutzername und Passwort sowie 2-Faktor-Authentifizierung, falls aktiviert), um die Backup-Dateien herunterzuladen.

## Sicherung über APIv3 abrufen

Es ist manchmal gut, Backups an anderen Orten zu haben (z.B. lokal vs. Cloud). Sie können in Projekte eine Sicherung über die APIv3 abrufen.

Voraussetzungen:

1. Das Token muss bereits generiert und in einem sicheren Schlüsselspeicher aufbewahrt werden
1. Der API-Schlüssel muss bekannt sein und in einem sicheren Schlüsselspeicher aufbewahrt werden

Sie könnten unser **Beispiel-Bash-Skript** verwenden und es in Ihren Crond integrieren, um es täglich auszuführen.
