# Informationen

## Systeminformationen

Sie erhalten einen Überblick über den aktuellen Systemstatus und weitere Informationen. Navigieren Sie über Ihren Avatar oben rechts auf **Administration** und klicken Sie anschließend in Ihrer linken unteren Menüleiste auf **Informationen**.

1. Zeigt die Produktversion (Projekte-Konfiguration) an.
1. Zeigt die Core Version Ihrer Projekte-Installation an.

![Administration](media/9c3fec071696bda0f7dee69bd5196524e3685ce8.jpg "Administration")

![Information](media/d1dae3436079507a718d7aeb7d0e4d57689dc146.jpg "Information")

![System information](media/10b6c3be096610310f1666539f5cef0e6f6021d1.jpg "System information")

## Sicherheitsabzeichen

Das Sicherheitsabzeichen zeigt den aktuellen Status Ihrer Projekte-Installation an. Es informiert die Administratoren einer Projekte-Installation darüber, ob neue Versionen oder Sicherheitsupdates für Ihre Plattform verfügbar sind.

Wenn diese Option aktiviert ist, wird ein Abzeichen mit Ihrem Installationsstatus unter Verwaltung &gt; Informationen direkt neben der Versionsnummer und auf dem Startbildschirm angezeigt. Es wird nur Administratoren angezeigt.

Das Abzeichen vergleicht Ihre aktuelle Projekte-Version mit der offiziellen Projekte-Release-Datenbank, um Sie über etwaige Updates oder bekannte Schwachstellen zu informieren. Um sicherzustellen, dass das neueste verfügbare Update zurückgegeben werden kann, wird die Überprüfung Ihren Installationstyp, die aktuelle Version, den Datenbanktyp, den Unternehmensstatus und eine anonyme eindeutige ID der Instanz umfassen. Um das Abzeichen zu lokalisieren, wird das Gebietsschema des Benutzers gesendet. Es werden keine persönlichen Informationen Ihrer Installation oder eines Benutzers darin übermittelt, verarbeitet oder gespeichert.

Um die Anzeige des Abzeichens zu deaktivieren, gehen Sie über ihren Avatar oben rechts auf **Administration** anschließend in der Liste auf der linken Seite auf **Systemkonfigurationen** und anschließend auf **Allgemein.** Oder übergeben Sie das Konfigurationsflag security_badge_displayed: false.

## Zusätzliche Systeminformationen

Es gibt einige automatische Überprüfungen des Systems, um die Sicherheit und korrekte Einrichtung Ihrer Konfiguration zu gewährleisten, wenn Sie über **Administration** und anschließend **Informationen** navigieren.

Wenn ein Punkt nicht erfüllt ist, z. B. das Ändern des Standard-Administrator-Kontos, erhalten Sie eine Warnmeldung in Form eines Fehler-Icons.

![System](media/f34b3d5421cd6ca129b4417c2d5bae586ae997b9.jpg "System")

## Speicherinformationen

Sie erhalten Informationen über das Speicher-Dateisystem in Ihrer Projekte-Anwendung, wenn Sie zu **Administration** und anschließend zu **Informationen** navigieren.

Sie sehen den verbleibenden Festplattenspeicher sowie den verwendeten Festplattenspeicher in Ihrer Projekte-Installation.
