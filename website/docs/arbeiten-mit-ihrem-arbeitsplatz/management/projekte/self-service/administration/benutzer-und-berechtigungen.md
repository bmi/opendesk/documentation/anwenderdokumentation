# Benutzer und Berechtigungen

In diesem Abschnitt des Systemadministrationshandbuchs erfahren Sie, wie Sie Benutzerinnen und Benutzer, Platzhalter-Benutzerinnen und Benutzer und Berechtigungen in Projekte verwalten können.

### Hier geht es weiter mit:

- [Benutzereinstellungen](benutzer-und-berechtigungen/benutzereinstellungen.md)
- [Benutzer verwalten](benutzer-und-berechtigungen/benutzer-verwalten.md)
- [Gruppen verwalten](benutzer-und-berechtigungen/gruppen-verwalten.md)
- [Rollen und Berechtigungen](benutzer-und-berechtigungen/rollen-und-berechtigungen.md)
- [Avatare](benutzer-und-berechtigungen/avatare.md)
