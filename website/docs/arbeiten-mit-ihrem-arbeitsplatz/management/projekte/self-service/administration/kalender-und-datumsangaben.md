# Kalendarien und Daten

## Kalender und Daten

Konfigurieren Sie Werktage, Datumsformate und Kalenderabonnements.

Navigieren Sie über Ihren Avatar auf der oberen linken Bildschirmseite zu **Administration** und klicken Sie anschließend auf der linken Seite des Bildschirms auf **Kalendarien und Daten.**

## Werktage

Administratorinnen und Administratoren können definieren, welche Tage der Woche auf Instanzebene als Arbeitstage gelten. Mit anderen Worten, diese Einstellung definiert, was Projekte als eine normale Arbeitswoche bei der Planung von Arbeitspaketen ansehen sollte.

Um diese Einstellung zu ändern, navigieren Sie zu **Werktage** innerhalb der Einstellungen für **Kalender und Daten**.

**Hinweis**: Standardmäßig gelten die fünf Tage von Montag bis Freitag als Arbeitstage und Samstag und Sonntag als Nicht-Arbeitstage.

![Werktage](media/ab5305e8d78e5301ecc11d8697e74133b005af4b.jpg "Werktage")

Um diese Einstellung zu ändern, deaktivieren Sie die Tage, die Sie als Nicht-Arbeitstage definieren möchten, und aktivieren Sie die Tage, die Sie als Arbeitstage betrachten möchten, und klicken Sie auf **Speichern**.

## Arbeitsfreier Tag

Sie können auch bestimmte Daten (wie nationale oder lokale Feiertage) als **Arbeitsfreier Tage** festlegen. Arbeitspakete im regulären Planungsmodus werden diese Tage überspringen und sie werden nicht in die Berechnung der Dauer einbezogen.

![Feiertage und Schließungen](media/18fcb83324df35be34941cf4dfbf67b892fd48f8.jpg "Feiertage und Schließungen")

Klicken Sie auf **+ Arbeitsfreier Tag**, um ein neues Datum zur Liste hinzuzufügen. Im daraufhin erscheinenden Datumsauswahl-Dialog geben Sie diesem neuen Nicht-Arbeitstag einen Namen und wählen das spezifische Datum aus, das Sie hinzufügen möchten. Klicken Sie auf **Hinzufügen**. Dieser Tag wird nun für alle Benutzer der Instanz ein arbeitsfreier Tag sein.

![Arbeitsfreier Tag](media/319fd094e36c43458a96ca2d1415b33e97f71b60.jpg "Arbeitsfreier Tag")

Klicken Sie am Ende der Seite auf **Änderungen anwenden**, damit die Änderungen wirksam werden.

### Auswirkungen auf die Planung

Als Einstellung auf Instanzebene wird jede Änderung hier die Planung aller Arbeitspakete in allen Projekten dieser Instanz beeinflussen. Es ist derzeit nicht möglich, Arbeitstage auf Projektebene zu definieren.

Es ist jedoch möglich, diese Einstellung auf Ebene einzelner Arbeitspakete über den Datumsauswahl-Dialog zu überschreiben.

**Wichtig**: Das Ändern dieser Einstellung führt automatisch zur Neuplanung von Arbeitspaketen am nächsten verfügbaren Arbeitstag, nachdem Sie auf **Speichern** geklickt haben. Wenn Sie beispielsweise Freitag als Arbeitstag abwählen, bedeutet dies, dass Arbeitspakete, die Freitag einschlossen, jetzt einen Tag in der Zukunft enden werden, und solche, die am Freitag begannen oder endeten, werden nun am Montag beginnen und enden.

Je nach Anzahl der Projekte und Arbeitspakete in Ihrer Instanz kann dieser Prozess von ein paar Minuten bis zu Stunden dauern.

Das Ändern dieser Einstellung kann zu unerwarteten Änderungen in der Planung führen und eine erhebliche Anzahl von Benachrichtigungen für Zuweiser, Verantwortliche und Beobachter von Arbeitspaketen generieren, deren Termine sich als Ergebnis ändern.

Wir empfehlen, diese Einstellung nur zu ändern, wenn Sie sich absolut sicher sind und sich der möglichen Konsequenzen bewusst sind.

### Auswirkungen auf Kalender

Die hier definierten Nicht-Arbeitstage werden in der Regel mit einer dunkleren Hintergrundfarbe auf dem **Datumsauswahl-Dialog für Arbeitspakete**, im **Gantt-Diagramm** und in den Modulen **Teamplaner** und **Kalender** andersfarbig dargestellt.

## Datumsformat

Administratorinnen und Administratoren können das Standardzeit- und Datumsformat in Projekte ändern. Dies beeinflusst, wie Kalender und Daten berechnet und angezeigt werden.

Um diese Einstellung zu ändern, navigieren Sie zu **Datumsformat** innerhalb der Einstellungen für **Kalender und Daten**.

![Datumsformat](media/8dcc80f32c59bee05e56c669c5ea2cc4616c2576.jpg "Datumsformat")

**Datum**: Standard basiert auf der Sprache des Benutzers. Sie können verschiedene Formate wählen, um Daten im System anzuzeigen. **Zeit**: Standard basiert auf der Sprache des Benutzers. Sie können verschiedene Formate wählen, um die Zeit im System anzuzeigen. **Wochenanfang am**: Konfigurieren Sie, an welchem Datum die Woche beginnt (z.B. in der Kalenderansicht). Standard ist Basierend auf der Sprache des Benutzers. Sie können auch wählen, dass eine Woche immer montags, sonntags oder samstags beginnt. Wenn Sie Woche beginnt am einstellen, müssen Sie auch Erste Woche im Jahr enthält festlegen, sonst erhalten Sie wahrscheinlich Inkonsistenzen bei der Wochennummerierung in Kalendern und dem Gantt-Diagramm. **Die Erste Woche im Jahr enthält**: Wählen Sie den Tag aus, der in der ersten Woche des Jahres enthalten sein muss. Dieser Wert wird zusammen mit Woche beginnt am verwendet, um die erste Woche des Jahres zu bestimmen.

- Für den europäischen ISO-8601-Standard stellen Sie Woche beginnt am auf Montag und Erste Woche im Jahr auf Donnerstag.
- Für die USA und Kanada stellen Sie Woche beginnt am auf Sonntag und Tag des Jahres am 6.
- Wenn Sie beide auf Basierend auf der Sprache des Benutzers lassen, werden die Informationen von moment.js verwendet: Für weitere Informationen sehen Sie bitte auch dieses Dokument von moment.js an.

Weitere Informationen zur Berechnung der ersten Woche des Jahres nach ISO-Normen finden Sie auf Wikipedia.  Denken Sie daran, Ihre Änderungen zu **speichern.** **Hinweis**: Konfigurationsoptionen im Zusammenhang mit der Aggregationszeit (der Zeitraum, in dem verschiedene Benutzeraktivitäten als eine Reihe von Aktionen angezeigt werden) wurden in den Abschnitt E-Mails und Benachrichtigungen verschoben.

## Kalenderabonnements

Administratorinnen und Administratoren können Benutzerinnen und Benutzern mit den erforderlichen Berechtigungen erlauben, Projekte-Kalender zu abonnieren und über einen externen Kalender-Client mithilfe von iCalendar auf Arbeitspaketinformationen zuzugreifen. Wenn die Einstellung **iCalendar-Abonnements aktivieren** inaktiv ist, kann niemand ein Kalenderabonnement abschließen.

Um diese Einstellung zu ändern, navigieren Sie zu **Kalenderabonnements** innerhalb der Einstellungen für **Kalender und Daten**.

![Kalenderabonnements](media/5776bcb49058cb94fa0854e6d64b3ee2b5fdf074.jpg "Kalenderabonnements")
