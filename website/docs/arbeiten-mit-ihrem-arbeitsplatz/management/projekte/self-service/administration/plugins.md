# Plugins

## Plugins

Die Projekte-Konfiguration enthält eine bestimmte Anzahl von Plugins, die unter -&gt; **Administration** -&gt; **Plugins** aufgelistet sind.

Wir empfehlen, die Plugins so zu verwenden, wie es in unseren Bereitstellungspaketen vorgeschlagen wird. Detailliertere Informationen (über aktuelle Änderungen, Autor usw.) erhalten Sie, wenn Sie den Links folgen

![Module und Plugins](media/a7d6736d219b659945dc85b96bc012eb76830b17.jpg "Module und Plugins")
