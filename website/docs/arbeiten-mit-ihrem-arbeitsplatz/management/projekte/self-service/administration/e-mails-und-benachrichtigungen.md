# E-Mails und Benachrichtigungen

## E-Mails und Benachrichtigungen

Konfigurieren Sie die Einstellungen für E-Mails und Benachrichtigungen in Projekte, d.h. E-Mail-Benachrichtigungen und die Konfiguration eingehender E-Mails.

Navigieren Sie zu **Administration** → **E-Mails und Benachrichtigungen.**

## Zusammenfassungen

![Zusammenfassungen](media/275fcca4f28f0030cdbeef43b70134a4c8de0576.jpg "Zusammenfassungen")

Die Einstellung **Benutzeraktionen aggregiert innerhalb** gibt einen Zeitraum an, in dem alle Benachrichtigungen über die Aktionen eines bestimmten Benutzers zu einer einzigen Benachrichtigung gebündelt werden. Einzelne Aktionen eines Benutzers (z.B. zweimaliges Aktualisieren eines Arbeitspakets) werden zu einer einzigen Aktion zusammengefasst, wenn der Zeitunterschied zwischen ihnen geringer ist als der angegebene Zeitraum. Sie werden innerhalb der Anwendung als eine einzige Aktion angezeigt. Dies verzögert auch die Benachrichtigungen um denselben Zeitraum und reduziert die Anzahl der versendeten E-Mails.

## Mailbenachrichtigung

![Mailbenachrichtigung](media/df9e6435db62d0b22578c878b19676670604f344.jpg "Mailbenachrichtigung")

1. **Absender-E-Mail-Adresse.** Diese E-Mail-Adresse wird als Absender für die von Projekte gesendeten E-Mail-Benachrichtigungen angezeigt (zum Beispiel, wenn ein Arbeitspaket geändert wird).
1. **E-Mails als Blindkopie (BCC) senden** aktivieren.
1. Definieren, ob die E-Mail **nur reinen Text (kein HTML) senden** formatiert werden soll.

## Konfigurieren Sie E-Mail-Kopfzeile und E-Mail-Fußzeile

Konfigurieren Sie Ihre E-Mail-Kopfzeile und E-Mail-Fußzeile, die für E-Mail-Benachrichtigungen aus dem System versendet werden.

![Konfigurieren Sie Ihre E-Mail-Kopfzeile und E-Mail-Fußzeile](media/58b2b13b8f574b90b34ba94cf28a761b3915ed07.jpg "Konfigurieren Sie Ihre E-Mail-Kopfzeile und E-Mail-Fußzeile")

1. **Formulieren Sie Kopfzeile und oder Fußzeile** für die E-Mail-Benachrichtigungen. Diese werden für alle E-Mail-Benachrichtigungen von Projekte verwendet (z.B. beim Erstellen eines Arbeitspakets).
1. Wählen Sie eine **Sprache** aus, für die die E-Mail-Kopfzeile und -Fußzeile gelten soll.
1. Senden Sie eine **Test-E-Mail**.

**Bitte beachten Sie**: Diese Test-E-Mail testet nicht die Benachrichtigungen für Änderungen an Arbeitspaketen usw.

Vergessen Sie nicht, Ihre Änderungen zu **speichern**.

## Einstellungen für eingehende E-Mails

Hier können Sie die folgenden Optionen konfigurieren.

![Eingehende E-Mails](media/425cc4b4f5cddea5cbc644a11b51d365588b179d.jpg "Eingehende E-Mails")

1. Definieren Sie, nach welchen Zeilen eine **E-Mail gekürzt werden** soll. Diese Einstellung ermöglicht es, E-Mails nach den eingegebenen Zeilen zu kürzen.
1. Geben Sie einen **regulären Ausdruck** an, um E-Mails zu kürzen.
1. **Ignorieren Sie E-Mail-Anhänge** mit den in dieser Liste angegebenen Namen.

Vergessen Sie nicht, die Änderungen zu **speichern**.
