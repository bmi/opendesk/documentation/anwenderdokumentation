# LDAP-Gruppensynchronisierung

In Projekte können Sie LDAP-Gruppenmitgliedschaften synchronisieren, die über die LDAP-Objektklasse groupOfNames definiert sind. Diese Anleitung geht davon aus, dass Sie:
- mindestens eine Gruppe in Projekte definiert haben (Weitere Informationen zum Erstellen und Bearbeiten von Gruppen finden Sie in der Anleitung "Gruppen verwalten"),
- Ihre LDAP-Authentifizierungsquelle eingerichtet haben (siehe die Anleitung "LDAP-Verbindungen")

mindestens einen LDAP-Eintrag mit einer groupOfNames-Objektklasse haben und die Mitglieder dieser Gruppe das Attribut memberOf: &lt;DN der Gruppe&gt; Attribut enthalten, um die Mitglieder eines Gruppeneintrags zu bestimmen. LDAP-Instanzen, die nur member-Attribute, aber nicht die inverse memberOf-Eigenschaft haben, werden derzeit nicht unterstützt.  **Bitte beachten Sie:** Projekte unterstützt keine anderen Attribute als die memberOf-Eigenschaft zur Definition von Gruppen. Bitte stellen Sie sicher, dass die Benutzerobjekte die memberOf-Eigenschaft haben, damit die Synchronisation funktioniert.

Der Einfachheit halber gehen wir in diesem Leitfaden davon aus, dass Ihre LDAP-Struktur wie folgt aussieht:

Sie haben zwei Gruppen cn=groupA,ou=groups,ou=example,ou=com und cn=groupB,ou=groups,ou=example,ou=com mit zwei bzw. einem Benutzer*.* Ihr Basis-DN ist ou=people,ou=example,ou=com.

## LDAP-Gruppensynchronisierung

Die LDAP-Gruppensynchronisation ergänzt die von Administratorinnen und Administratoren definierten Mitgliedschaften in einer bestehenden Projekte-Gruppe. Dabei sind wichtige Dinge zu beachten:

- Es können nur bestehende Gruppen und Benutzerinnen bzw. Benutzer in Projekte synchronisiert werden. Die Funktionalität erstellt nicht einfach alle Einträge in der LDAP-Gruppenbasis und synchronisiert auch keine Benutzerinnen bzw. Benutzer, die nicht in Projekte existieren.
- Die Gruppensynchronisation muss von einer Administratorin bzw. einem Administrator aktiviert werden, indem sie bzw. er eine synchronisierte LDAP-Gruppe erstellt, die die Projekte-Gruppe mit einem LDAP-Eintrag verknüpft.
- Nur synchronisierte Mitgliedschaften werden aus der Projekte-Gruppe entfernt. Wenn Sie eine Benutzerin bzw. einen Benutzer außerhalb Ihrer LDAP-Authentifizierung zu einer Projekte-Gruppe hinzufügen möchten, können Sie dies tun, ohne dass die Mitgliedschaft entfernt wird.

## Konfigurieren von LDAP-Gruppensynchronisierungsfiltern

Anstelle der manuellen Synchronisierung von Gruppen aus einem bestimmten DN können Sie auch Filterobjekte erstellen, die das LDAP nicht nur nach Gruppenmitgliedern, sondern auch nach den Gruppen selbst abfragen.

Wenn die Synchronisierungsaufgabe ausgeführt wird, wird der Filter gegen das LDAP abgefragt und die resultierenden Gruppenobjekte werden als synchronisierte Gruppen und als Projekte-Gruppen erstellt.

![Synchronisationsfilter LDAP-Gruppe](media/d756a4d5a5a702c63fc58f67ec25ad22bf4f527a.jpg "Synchronisationsfilter LDAP-Gruppe")

## Einen Synchronisationsfilter erstellen

Um einen neuen synchronisierten Filter zu erstellen, verwenden Sie die Schaltfläche oben rechts auf der Indexseite. Dort wählen Sie Ihre LDAP-Authentifizierungsquelle aus, die abgefragt werden soll. Die folgenden Eigenschaften können eingestellt werden:

- **Name**: Name des LDAP-Filters, nur für organisatorische Zwecke
- **Attribut für Gruppenname**: Das Attribut, das für die Benennung der zugehörigen Projekte-Gruppen verwendet wird.
- **Benutzer synchronisieren**: Aktivieren Sie diese Option, wenn Sie möchten, dass die Mitglieder aller synchronisierten Gruppen, die dieser Filter erstellt, automatisch in Projekte erstellt werden. Wenn diese Option nicht aktiviert ist, können nur Mitglieder einer Gruppe synchronisiert werden, die auch als Benutzer in Projekte vorhanden sind.
- **LDAP-Verbindung**: Wählen Sie die LDAP-Verbindung, die dieser synchronisierte Filter verwenden soll. Die durch die Gruppensynchronisierung erstellten Benutzerinnen und Benutzer werden mit diesem LDAP verbunden und können sich für die Authentifizierung mit diesem verbinden.
- **Suchbasis-DN**: (optional) Geben Sie den Basis-DN des LDAP-Unterbaums ein, in dem Sie die Suche durchführen möchten. Wenn Sie diesen Wert nicht angeben, wird stattdessen der Basis-DN der LDAP-Verbindung verwendet. Der hier angegebene DN muss den Basis-DN der LDAP-Verbindung enthalten, um gültig zu sein.
- **LDAP-Filter**: Der LDAP-Filterstring, der für die Identifizierung von LDAP-Gruppeneinträgen, die mit Projekte synchronisiert werden sollen, verwendet werden soll.  Klicken Sie auf Anlegen, um die Erstellung des synchronisierten Filters abzuschließen. Dieser Filter wird stündlich als Teil des Hintergrundjobs ausgeführt, bevor die eigentliche Gruppensynchronisierung läuft.

**Hinweis**: Wenn Sie manuell eine synchronisierte Gruppe erstellen, die auch von einem Filter gefunden wird, werden ihre Eigenschaften (z. B. die Einstellung Benutzer synchronisieren) von der Filtereinstellung überschrieben.

## Konfigurieren synchronisierter LDAP-Gruppen

Um zum Verwaltungsbereich der LDAP-Gruppensynchronisierung zu gelangen, gehen Sie in Ihrer Adminstration zu dem Menüpunkt LDAP-Authentifizierung.

**Definieren Sie Gruppenbasis und Schlüsseleinstellungen**

Damit das LDAP-Gruppen-Plugin Ihre Gruppeneinträge finden kann, müssen Sie zunächst den Gruppenschlüssel auf cn (das identifizierende Attribut der Gruppeneinträge) und die Gruppenbasis auf ou=groups,ou=example,ou=com setzen, wie im folgenden Screenshot gezeigt.

![Hinzufügen einer synchronisierten LDAP-Gruppe](media/60651d71c56b3b264fab3e22d3bb321312becd1d.jpg "Hinzufügen einer synchronisierten LDAP-Gruppe")

## Eine synchronisierte Gruppe erstellen

Um eine neue synchronisierte Gruppe zu erstellen, verwenden Sie die Schaltfläche oben rechts auf der Seite. Dort wählen Sie Ihre LDAP-Authentifizierungsquelle aus, die die Gruppe enthält, sowie die bestehende Projekte-Gruppe, mit der die Mitglieder synchronisiert werden sollen. Die folgenden Optionen können eingestellt werden:

- **LDAP-Verbindung**: Wählen Sie die LDAP-Verbindung aus, die diese synchronisierte Gruppe verwenden soll. Die durch die Gruppensynchronisierung erstellten Benutzerinnen und Benutzer werden an dieses LDAP gebunden und können sich für die Authentifizierung daran binden.
- **DN**: Geben Sie den vollständigen Distinguished Name (DN) der Gruppe ein, die Sie synchronisieren möchten. Zum Beispiel: cn=team1,ou=groups,dc=example,dc=com.
- **Benutzer synchronisieren**: Aktivieren Sie diese Option, wenn Sie möchten, dass die Mitglieder dieser Gruppe automatisch in Projekteangelegt werden. Wenn diese Option nicht aktiviert ist, können nur Mitglieder der Gruppe synchronisiert werden, die auch als Benutzerin bzw. Benutzer im Modul Projekte vorhanden sind.
- **Gruppe**: Wählen Sie eine Projekte-Gruppe, mit der die Mitglieder der LDAP-Gruppe synchronisiert werden sollen.

Klicken Sie auf Anlegen, um die Erstellung der synchronisierten Gruppe abzuschließen. Die LDAP-Mitgliedschaften der einzelnen Benutzerinnen und Benutzer werden stündlich durch einen Hintergrundjob auf Ihrer Paketinstallation synchronisiert. Änderungen und Ausgaben werden in /var/log/openproject/cron-hourly.log protokolliert.

## Fehlerbehbung

**LDAP-Gruppen werden nicht synchronisiert**

Bitte überprüfen Sie den DN der Gruppen und der LDAP-Verbindung. Der Basis-DN der LDAP-Verbindung und der DN müssen eine gemeinsame Hierarchie haben. Andernfalls wird der Gruppen-DN von der Verbindung nicht gefunden, da der Basis-DN für alle nachfolgenden Abfragen während der Lebensdauer der Verbindung verwendet wird.

**Benutzer werden nicht synchronisiert**

Damit die Benutzerinnen und Benutzer automatisch synchronisiert werden, müssen die folgenden Bedingungen erfüllt sein:

1. Bei der Verbindung oder der LDAP-Gruppe muss die Option "Benutzer synchronisieren" aktiviert sein. Diese Einstellung hat Vorrang vor dem LDAP-Verbindungsattribut "Benutzer synchronisieren" und ermöglicht eine fein abgestufte Kontrolle darüber, mit welchen Gruppen die Benutzer synchronisiert werden.
1. Die Gruppe muss ihre Mitglieder mit der LDAP-Eigenschaft "member" definieren, und die Benutzerinnen und Benutzer müssen die Eigenschaft "memberOf" oder die virtuelle Eigenschaft haben. Projekte sucht nach Benutzerinnen und Benutzern mit dem folgenden Filter: (memberOf=&lt;DN der Gruppe&gt;). Sie können ldapsearch verwenden, um zu überprüfen, ob dies wie erwartet funktioniert.
1. Die in den Gruppen definierten Benutzerinnen und Benutzer müssen alle erforderlichen Attribute aufweisen, um erstellt werden zu können. Dies sind: Login, E-Mail, Vor- und Nachname. Wenn eines dieser Attribute fehlt, kann die Benutzerin bzw. der Benutzer nicht in der Datenbank gespeichert werden.
1. Wenn Ihre Unternehmenslizenz das Benutzerlimit überschreitet, können neue Benutzerinnen und Benutzer auch nicht über LDAP synchronisiert werden. Es wird ein entsprechender Log-Eintrag erstellt.
