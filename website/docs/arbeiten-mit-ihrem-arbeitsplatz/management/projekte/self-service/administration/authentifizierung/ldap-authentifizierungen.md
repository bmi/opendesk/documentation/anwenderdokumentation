# LDAP-Verbindungen

## Verwalten von LDAP-Verbindungen

**Beachten Sie:** Um auf das Administrationspanel zugreifen und die LDAP-Verbindungen verwalten zu können, müssen Sie eine Systemadministratorin bzw. ein Systemadministrator sein.

Um die Liste aller verfügbaren LDAP-Authentifizierungen (Lightweight Directory Access Protocol) anzuzeigen, navigieren Sie zu - &gt; Administration und wählen Sie -&gt; Authentifizierung -&gt; LDAP-Verbindungen aus. Sie sehen dann eine Liste aller bereits erstellten Verbindungen.

## Eine neue LDAP-Verbindung hinzufügen

Um eine neue LDAP-Verbindung zu erstellen, klicken Sie auf den violetten Button **+ LDAP-Verbindung**.

![LDAP-Verbindungen Übersicht](media/69a25f0bc75d9097aea9add96c3b0d33c8c774fd.jpg "LDAP-Verbindungen Übersicht")

Anschließend können Sie die LDAP-Konfiguration angeben. Dies kann ein beliebiger Verzeichnisdienst sein, der mit dem LDAPv3-Standard kompatibel ist, wie z. B. Microsoft Active Directory oder openLDAP. Die Konfiguration ist abhängig von der spezifischen Datenbank/Anwendung, über die die Authentifizierung mit Projekte erfolgen soll.

Die folgenden Screenshots enthalten eine beispielhafte Konfiguration für einen neuen LDAP-Authentifizierungsmodus. Im Folgenden werden alle verfügbaren Optionen kurz erklärt.

## LDAP Verbindungsdetails und Sicherheit

![LDAP Konfiguration: Neue Verbindung](media/9cc85c9cee328a853e1a6ce4641862e0a792dc8b.jpg "LDAP Konfiguration: Neue Verbindung")

In dem oberen Abschnitt der **LDAP-Einstellungen** müssen Sie die Verbindungsdetails Ihres LDAP-Servers sowie die zu verwendende Verbindungsverschlüsselung angeben.

- **Name**: Beliebiger Identifier, der genutzt wird, um zu zeigen von welcher Authentifizierungsquelle ein Nutzer stammt
- **Host**: Vollständiger Host-Name des LDAP-Servers
- **Port:** LDAP-Port. In der Regel 389 für LDAP und StartTLS und 636 für LDAP per SSL Verbindung
- **Verbindungsverschlüsselung**: Wählen Sie die passende Verschlüsselung aus **Empfohlene Option: STARTTLS** führt ein TLS-Verbindungsuprade durch, nachdem eine Verbindung zum LDAP-Server über den unverschlüsselten Port (standardmäßig 389) hergestellt wurde
- Für **LDAPS-Verbindungen** (LDAP over SSL) verwenden Sie LDAPS , dies ist ein SSL-Verschlüsselungsmuster, das SSL-Zertifikate verwendet und eine Verbindung zu einem separaten Port auf dem LDAP-Server herstellt. Einige ältere LDAP-Server unterstützen nur diese Option, aber diese Option ist bei den meisten LDAP-Servern zugunsten der STARTTLS-Methode veraltet.
- Für unverschlüsselte Verbindungen wählen Sie **keine**. Es wird keine TLS/SSL-Verbindung aufgebaut, Ihre Verbindung ist unsicher und es findet keine Verifizierung statt.
- [Klicken Sie hier](https://www.rubydoc.info/gems/ruby-net-ldap/Net/LDAP), um mehr darüber zu erfahren, was diese Optionen für die Verbindungssicherheit bedeuten.
- **SSL-Verschlüsselungsoptionen**: Bietet zusätzliche Optionen für LDAPS- und STARTTLS-Verbindungen. Beachten Sie, dass diese Optionen nicht für die Option Verbindungsverschlüsselung keine gelten. **SSL-Zertifikat überprüfen**: Standardmäßig werden bei STARTTLS und LDAPS die Vertrauensketten der SSL-Zertifikate während der Verbindung überprüft. Da der Erfahrung nach viele LDAP-Server selbstsignierte Zertifikate verwenden, schlägt die Aktivierung dieser Option ohne Angabe des SSL-Zertifikats fehl. Es wird jedoch empfohlen, dieses Kontrollkästchen für alle in der Produktion verwendeten LDAP-Verbindungen zu aktivieren.
- **LDAP-Server SSL-Zertifikat**: Wenn das Zertifikat des LDAP-Servers auf dem System, auf dem der Projekte-Server läuft, nicht vertrauenswürdig ist, haben Sie die Möglichkeit, ein oder mehrere PEM-kodierte X509-Zertifikate anzugeben. Bei diesem Zertifikat kann es sich um das eigene Zertifikat des LDAP-Servers handeln, oder um eine Zwischen- oder Stammzertifizierungsstelle, der Sie für diese Verbindung vertrauen.

## LDAP Nutzeranmeldedaten

![LDAP Systemkonto](media/aca7a4276c9a73ec52225bd9baa8d73b917ec546.jpg "LDAP Systemkonto")

Als Nächstes müssen Sie eine Systembenutzerin bzw. einen Systembenutzer eingeben, die bzw. der zu Identifizierungs- und Synchronisierungszwecken READ-Zugriff auf die Benutzerinnen und Benutzer hat. Beachten Sie, dass die meisten Operationen mit dem LDAP während der Authentifizierung nicht mit diesen Anmeldedaten, sondern mit den von der Benutzerin bzw. vom Benutzer im Anmeldeformular angegebenen Anmeldedaten durchgeführt werden, um eine reguläre Benutzerverbindung mit dem LDAP herzustellen.

- **Konto**: Der vollständige DN einer Systembenutzerin bzw. eines Systembenutzers, die bzw. der zum Nachschlagen von Benutzerdetails im LDAP verwendet wird. Sie bzw. er muss Leseberechtigungen unter dem Basis-DN haben. Dieser wird nicht für die Benutzerbindung bei der Authentifizierung verwendet.
- **Passwort**: Das Bindungspasswort des obigen DN der Systembenutzerin bzw. des Systembenutzers.

## LDAP Details

![LDAP Details](media/343e905ad466929588d8409b2f091050992e70d9.jpg "LDAP Details")

Als Nächstes können Sie festlegen, nach welchen Bereichen Projekte im LDAP sucht und ob Benutzerinnen und Benutzer automatisch in Projekte angelegt werden sollen, wenn sie darauf zugreifen. Schauen wir uns nun die verfügbaren Optionen an:

- **Basis-DN**: Geben Sie den Basis-DN ein, in dem nach Benutzern und Gruppen im LDAP-Baum gesucht werden soll
- **Filter string**: Geben Sie eine optionale LDAP-RFC4515-Filterzeichenfolge ein, um die zurückgegebene Benutzermenge weiter einzuschränken. Damit können Sie den Zugriff auf das Modul Projekte mit einem sehr flexiblen Filter einschränken. Bei der Gruppensynchronisation werden nur Benutzerinnen und Benutzer hinzugefügt, die diesem Filter entsprechen.
- **Benutzer automatisch erzeugen**: Aktivieren Sie diese Option, um Benutzerinnen und Benutzer automatisch in Projekte anzulegen, wenn sie sich zum ersten Mal in dem Modul anmelden. Dabei wird die unten stehende LDAP-Attribut-Zuordnung verwendet, um die erforderlichen Attribute auszufüllen. Die Benutzerin bzw. der Benutzer wird zu einem Registrierungsbildschirm weitergeleitet, um die erforderlichen Attribute zu vervollständigen, wenn sie im LDAP fehlen.

## Zuordnen von Attributen

![LDAP Zuordnen von Attributen](media/9287866163c077d5f23ef619c6bdf2e5df080137.jpg "LDAP Zuordnen von Attributen")

Die Attribut-Zuordnung wird verwendet, um Attribute von Projekte mit Attributen des LDAP-Verzeichnisses zu identifizieren. Mindestens das Login-Attribut ist erforderlich, um DNs aus den Anmeldedaten zu erstellen.

- **Benutzername**: Das Login-Attribut im LDAP. wird verwendet, um den DN aus login-attribute=value, zu konstruieren. In den meisten Fällen wird dies uid sein.
- **Vorname**: Der Attributname im LDAP, der auf den Vornamen abgebildet wird. In den meisten Fällen wird dies givenName sein. Bleibt er leer, wird die Benutzerin bzw. der Benutzer bei der Registrierung zur Eingabe aufgefordert, wenn die automatische Benutzererstellung aktiviert ist.
- **Nachname**: Der Attributname im LDAP, der dem Nachnamen zugeordnet ist. In den meisten Fällen wird dies sn sein. Bleibt er leer, wird die Benutzerin bzw. der Benutzer bei der Registrierung zur Eingabe aufgefordert, wenn die automatische Benutzererstellung aktiviert ist.
- **E-Mail**: Der Attributname im LDAP, der der E-Mail-Adresse der Benutzerin bzw. des Benutzers zugeordnet ist. Dies ist normalerweise mail. Wenn diese Option leer bleibt, wird die Benutzerin bzw. der Benutzer bei der Registrierung zur Eingabe aufgefordert, wenn die automatische Benutzererstellung aktiviert ist.

**Admin**: Geben Sie ein Attribut an, das, wenn es einen wahren Wert hat, dazu führt, dass die Benutzerin bzw. der Benutzer in Projekte ein Admin-Konto wird. Lassen Sie es leer, um den Admin-Status nicht über LDAP-Attribute zu vergeben.  Klicken Sie abschließend auf **Anlegen**, um den LDAP-Authentifizierungsmodus zu speichern. Sie werden zur Indexseite mit dem erstellten Authentifizierungsmodus weitergeleitet. Klicken Sie auf die Schaltfläche Test, um eine Testverbindung mit den Anmeldeinformationen der Systembenutzerin bzw. des Systembenutzers zu erstellen.

## Mehrere LDAP Verbindungen

Projekte unterstützt mehrere LDAP-Verbindungen, aus denen Benutzerinnen und Benutzer stammen können. Die Authentifizierungsquelle der Benutzerin bzw. des Benutzers wird bei der ersten Erstellung gespeichert (kann aber im Admin-Backend gewechselt werden). Dadurch wird sichergestellt, dass die richtige Verbindung / LDAP-Quelle für die Benutzerin bzw. den Benutzer verwendet wird.

**Duplikate in den eindeutigen Attributen** (Login, E-Mail) sind nicht erlaubt und ein zweiter Benutzer mit denselben Attributen kann sich nicht anmelden. Bitte stellen Sie sicher, dass bei allen LDAP-Verbindungen ein eindeutiges Attribut verwendet wird, das nicht zu widersprüchlichen Anmeldungen führt.

## LDAP-Verbindungen durch Seeding / Umgebungsvariablen

Projekte ermöglicht es Ihnen, eine LDAP-Verbindung (und optional synchronisierte Gruppenfilter für Enterprise-Editionen) über eine Konfiguration oder Umgebungsvariable zu definieren.

## LDAP Nutzer-Synchronisierung

Standardmäßig synchronisiert Projekte alle 24 Stunden die Benutzerdaten (Name, E-Mail, Login) und den Kontostatus aus dem LDAP über einen Hintergrundjob.

**Statussynchronisation aktivieren**

Wenn Sie den Kontostatus aus dem LDAP synchronisieren möchten, können Sie die Statussynchronisierung mit der folgenden Konfiguration aktivieren:

ldap_users_sync_status: true (oder die ENV-Variable OPENPROJECT_LDAP__USERS__SYNC__STATUS=true) Es wird sichergestellt, dass die Benutzerin bze. der Benutzer aktiv ist, wenn sie bzw. er in LDAP gefunden werden kann. Kann die Benutzerin bzw. der Benutzer nicht im LDAP gefunden werden, wird das zugehörige Projekte-Konto gesperrt.

**Deaktivieren des Synchronisierungsjobs**

Wenn Sie die Synchronisierung aus irgendeinem Grund nicht durchführen möchten, können Sie die Ausführung des Synchronisierungsauftrags auch mit der folgenden Variable unterbinden:

ldap_users_disable_sync_job: true (oder die ENV-Variable OPENPROJECT_LDAP__USERS__DISABLE__SYNC__JOB=true)
