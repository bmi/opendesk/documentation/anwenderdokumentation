# Einstellungen

Um die Authentifizierungseinstellungen des Systems anzupassen müssen Sie zunächst über Administration zu Authentifizierung navigieren und dort Einstellungen auswählen.

Unter den Authentifizierungseinstellungen können Sie folgende Anpassungen vornehmen:

## Allgemeine Authentifizierungseinstellungen

1. Wählen Sie aus, ob die **Authentifizierung für den Zugriff auf Projekte erforderlich** ist. **Achtung**: Wenn Sie dieses Kontrollkästchen deaktivieren, ist Ihre Projekte-Instanz für die Allgemeinheit sichtbar, ohne dass Sie sich anmelden müssen. Die Sichtbarkeit der einzelnen Projekte hängt von dieser Einstellung ab.
1. Wählen Sie eine Option für die **Selbstregistrierung**. Die Selbstregistrierung kann entweder **deaktiviert** oder mit den folgenden Kriterien zugelassen werden: **Kontoaktivierung per E-Mail** bedeutet, dass die Benutzerin oder der Benutzer eine E-Mail erhält und die Aktivierung bestätigen muss.
1. **Manuelle Kontoaktivierung** bedeutet, dass eine Systemadministratorin oder ein Systemadministrator die neu registrierte Benutzerin oder den neu registrierten Benutzer manuell aktivieren muss.
1. **Automatische Kontoaktivierung** bedeutet, dass eine neue registrierte Benutzerin bzw. ein neu registrierter Benutzer automatisch aktiviert wird.
1. Legen Sie fest, ob die **E-Mail-Adresse als Anmeldename** verwendet werden soll.

Legen Sie fest, nach **wie vielen Tagen die Aktivierungs-E-Mail abläuft**, die an neue Benutzerinnen und neue Benutzer gesendet wird. Danach haben Sie die Möglichkeit, die Aktivierungs-E-Mail über die Benutzereinstellungen erneut zu versenden.

**Hinweis**: Standardmäßig wird die Selbstregistrierung nur auf interne Benutzerinnen und Benutzer angewandt (Anmeldung mit Benutzernamen und Kennwort). Wenn Sie über einen Identitätsanbieter wie LDAP, SAML oder OpenID Connect verfügen, verwenden Sie die entsprechenden Einstellungen in deren Konfiguration, um zu steuern, welche Benutzerinnen und welche Benutzer für die automatische Benutzererstellung in Frage kommen.

![Allgemeine Einstellungen Authentifizierung](media/2cf6a8e4af2b40ec7309e540cd8da64129b8f973.jpg "Allgemeine Einstellungen Authentifizierung")

## Fußzeile für Registrierung-Mails festlegen

Sie können eine Fußzeile für Ihre Registrierungs-E-Mails definieren unter -&gt; Adminstration -&gt; Authentifizierung -&gt; Einstellungen.

1. Wählen Sie aus, für welche **Sprache** Sie die Registrierungsfußzeile definieren möchten.
1. Geben Sie einen **Text für die Registrierungsfußzeile** ein.

![Fußzeile für Registrierungsnachricht](media/a991ad61e2e9227b54e4f21cf3a1f00df6ea40be.jpg "Fußzeile für Registrierungsnachricht")

## Passworteinstellungen konfigurieren

Sie können verschiedene Optionen anpassen, um die Kennworteinstellungen im Modul Projekte festzulegen.

1. Definieren Sie die **Mindestlänge des Passworts**.
1. Definieren Sie die Passwortstärke und legen Sie fest, **welche Zeichenarten zwingend Bestandteil** des Passwortes sein müssen.
1. Definieren Sie die **Mindestanzahl der erforderlichen Zeichenarten**.
1. Legen Sie die **Anzahl der Tage** fest, nach denen ein **Passwortwechsel** erzwungen werden soll.
1. Definieren Sie die **Anzahl der zuletzt verwendeten Passwörter**, die eine Benutzerin bzw. ein Benutzer **nicht wieder verwenden** darf.
1. Aktivieren Sie die Funktion **Passwort vergessen**. Auf diese Weise kann eine Benutzerin bzw. ein Benutzer ihr bzw. sein eigenes Passwort per E-Mail zurücksetzen.

![Passwörter Einstellungen](media/85b172a07908458a4f255799b657afef75988fe1.jpg "Passwörter Einstellungen")

## Andere Authentifizierungseinstellungen

Es können eine Reihe weiterer Authentifizierungseinstellungen festgelegt werden.

1. Definieren Sie die Anzahl der **fehlgeschlagenen Anmeldeversuche, nach denen ein Benutzer vorübergehend gesperrt wird**.
1. Legen Sie die **Dauer der Zeit** fest, für die der Benutzer **nach fehlgeschlagenen Anmeldeversuchen gesperrt** wird.
1. Aktivieren oder deaktivieren Sie die **Automatische Anmeldung**. Damit kann eine Benutzerin bzw. Benutzer eingeloggt bleiben, auch wenn sie bzw. er die Website verlässt. Wenn diese Option aktiviert ist, erscheint auf dem Anmeldebildschirm die Option **Angemeldet bleiben**, die ausgewählt werden kann.
1. Aktivieren Sie die Option **Session läuft ab**. Wenn Sie diese Option wählen, öffnet sich ein zusätzliches Feld, in dem Sie die **Dauer der Inaktivität vor Ablauf der Sitzung** festlegen können.
1. Legen Sie fest, dass **Benutzeranmeldung, Name und E-Mail-Adresse** für alle Anfragen **protokolliert** werden sollen.
1. Vergessen Sie nicht, Ihre Änderungen zu **speichern**.

![Sonstige Einstellungen](media/2b30b89ef1411176dd8a83fc07d858ce30af0fca.jpg "Sonstige Einstellungen")
