# Zwei-Faktor-Authentifizierung

## Einfache Zwei-Faktor-Authentifizierung mittels TOTP

Um die **Zwei-Faktor-Authentifizierung** für Projekte zu aktivieren und zu konfigurieren, navigieren Sie zu -&gt; Administration -&gt; Authentifizierung und wählen Sie Zwei-Faktor-Authentifizierung aus.

In der Nutzeroberfläche können Sie folgende Optionen konfigurieren:

1. **Erzwinge 2FA** (Zwei-Faktor-Authentifizierung) für jede Nutzerin und jeden Nutzer. Alle Benutzerinnen und Benutzer werden gezwungen, bei ihrer nächsten Anmeldung ein 2FA-Gerät zu registrieren.
1. **2FA-Anmeldung speichern**. Die Anmeldung wird für eine bestimmte Anzahl von Tagen gespeichert (z. B. 30 Tage).
1. Drücken Sie den violetten **Anwenden** Button, um Ihre Änderungen zu speichern.

![2FA Einstellungen](media/3e0349a0476533fff3bc9e8148f5f3a0f0dd29a7.jpg "2FA Einstellungen")

Normalerweise können Sie mit einem anderen Gerät wie einem Mobiltelefon oder einem Tablet eine TOTP-Anwendung verwenden, um das Token zu erzeugen, das als zusätzliche Sicherheitsebene zu Ihrem Passwort benötigt wird. Hier sind einige Anwendungen, die für Projekte 2FA funktionieren:
- Open Source andOTP (für Android Geräte) im [PlayStore](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&amp;gl=US).
- Open Source OTP Auth (für Apple Geräte) im [App Store](https://apps.apple.com/us/app/otp-auth/id659877384).
- Google Authenticator
- Microsoft Authenticator

## Zeitverschiebung

Der Time-based One-time Password Algorithmus (kurz TOTP) basiert auf dem Prinzip, dass der zweite Faktor und der Server, der den Authentifizierungsprozess abwickelt, ungefähr synchron sind. Standardmäßig beträgt die zulässige Zeitverschiebung (Differenz in Sekunden zwischen Client und Server) 30 Sekunden, was bedeutet, dass der Server Zeitverschiebungen von jeweils 30 Sekunden in der Vergangenheit und in der Zukunft akzeptiert.

Wenn Sie versuchen, ein neues Gerät zu registrieren und immer wieder Fehler erhalten, obwohl der Code korrekt zu sein scheint, ist höchstwahrscheinlich die Zeitverschiebung zwischen dem Gerät und dem Server der Grund dafür.
