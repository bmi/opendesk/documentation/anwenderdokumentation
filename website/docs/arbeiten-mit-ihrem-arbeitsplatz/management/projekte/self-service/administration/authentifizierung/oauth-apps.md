# OAuth-Apps

Um Projekte so zu konfigurieren, dass es als Server für eine OAuth-Client-Anwendung fungiert, navigieren Sie bitte zu --&gt; Administration --&gt; Authentifizierung --&gt; OAuth-Anwendungen.

## Fügen Sie eine neue OAuth-Anwendung

Um eine neue OAuth-Anwendung hinzuzufügen, klicken Sie auf den violetten Button **+ Hinzufügen**.

![OAuth-Anwendungen Übersicht](media/91c7dbec74e58a1ea050159c63271e19d498f1ea.jpg "OAuth-Anwendungen Übersicht")

Sie können die folgenden Optionen konfigurieren, um sie zu Ihrer OAuth-Anwendung hinzuzufügen.

1. Geben Sie den **Namen** ihrer OAuth-Anwendung ein.
1. Definieren Sie eine **Redirect-URL**, zu der Benutzer nach der Autorisierung des OAuth2-Zugriffs in Projekte weitergeleitet werden. Diese URL wird von Ihrer Client-Anwendung bereitgestellt.
1. Legen Sie die **Bereiche** fest, auf die die OAuth-Client-Anwendung zugreifen kann. Eine Mehrfachauswahl ist möglich. Wenn kein Bereich angekreuzt wird, wird standardmäßig api_v3 angenommen.
1. Wählen Sie aus, ob die Anwendung **vertraulich** sein soll. Dies ist in der Regel bei Client-Anwendungen der Fall, die auf einem Server oder Desktop laufen, aber die Client-Anwendung muss dies sicherstellen.
1. (Optional) Wählen Sie **Client Credentials User** und definieren Sie eine Benutzerin oder einen Benutzer, in deren bzw. dessen Namen Anfragen ausgeführt werden sollen.
1. Klicken Sie auf **Erstellen**, um Ihre OAuth-Anwendung hinzuzufügen.

![Neue OAuth-Anwendung](media/5659244f6eee563594c80382c207b57740c814ac.jpg "Neue OAuth-Anwendung")

Vergessen Sie nicht, Ihre Client-ID und Ihr Client Secret an einem sicheren Ort zu notieren. Sie werden sie später noch brauchen.

## OAuth-Endpunkte

Die Authentifizierungsendpunkte des Projekte OAuth2-Servers sind:

- Authentifizierung URL: https://example.com/oauth/authorize
- Zugriffstoken URL: https://example.com/oauth/token
