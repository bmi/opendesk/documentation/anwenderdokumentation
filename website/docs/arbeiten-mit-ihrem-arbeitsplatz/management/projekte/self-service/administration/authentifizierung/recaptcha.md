# reCAPTCHA

## reCAPTCHA Konfigurierung

Um **reCAPTCHA** für Projekte zu aktivieren und zu konfigurieren, navigieren Sie zu -&gt; Administration -&gt; Authentifizierung und wählen Sie -&gt; reCAPTCHA.

Wenn diese Option aktiviert ist, wird bei der Anmeldung ein Captcha-Formular für alle Benutzerinnen und Benutzer angezeigt, die noch kein Captcha abgeschlossen haben. Unter folgendem Link finden Sie weitere Informationen zu reCAPTCHA und deren Versionen sowie zur Erstellung der Website und der geheimen Schlüssel: [https://www.google.com/recaptcha](https://www.google.com/recaptcha).

Sie können die folgenden Schritte durchführen, um **reCAPTCHA** zu konfigurieren:

1. Aktivieren Sie **reCAPTCHA** für Projekte. Sie können zwischen reCAPTCHA v2 und reCAPTCHA v3 wählen.
1. Geben Sie den **Website-Schlüssel** ein.
1. Geben Sie den **geheimen Schlüssel** ein.
1. Drücken Sie auf den violetten **Anwenden** Button, um Ihre Änderungen zu speichern.

![reCAPTCHA Einstellungen](media/1a9c0e09891757825b4f3c71a26c85d53a711a7c.jpg "reCAPTCHA Einstellungen")
