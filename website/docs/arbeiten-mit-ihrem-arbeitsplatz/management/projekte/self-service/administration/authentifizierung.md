# Authentifizierung

Konfigurieren Sie die **Authentifizierungseinstellungen** und Authentifizierungsanbieter für das Modul Projekte. Um die Authentifizierungseinstellungen anzupassen navigieren Sie zu Ihrem Benutzernamen und wählen Sie anschließend **Adminstration** -&gt; **Authentifizierung**.

![Authentifizierung Einstellungen](media/2a0347a445f7e437b266fbcbf31ba8b32a5e99f3.jpg "Authentifizierung Einstellungen")

### Hier geht es weiter mit:

- [Einstellungen](authentifizierung/einstellungen.md)
- [OAuth-Apps](authentifizierung/oauth-apps.md)
- [Zwei-Faktor-Authentifizierung](authentifizierung/zwei-faktor-authentifizierung.md)
- [reCAPTCHA](authentifizierung/recaptcha.md)
- [LDAP-Verbindungen](authentifizierung/ldap-authentifizierungen.md)
- [LDAP-Gruppensynchronisierung](authentifizierung/ldap-gruppensynchronisierung.md)
