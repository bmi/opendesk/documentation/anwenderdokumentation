# Einstellungen

## Arbeitspaket-Verfolgung

Sie können Ihre und andere Arbeitspakete konfigurieren, indem Sie die Kästchen an den entspechenden Stellen aktivieren oder deaktivieren:

Setzen Sie dafür ein Häkchen bei:

- **Arbeitspaket-Beziehungen zwischen Projekten erlauben**
- **Arbeitspakete von Unterprojekten im Hauptprojekt anzeigen**
- **Neue Arbeitspakete haben "Heute" als Anfangsdatum**
- **Berechne den Arbeitspaket-Fortschritt mittels**
- **Standard Hervorhebung**

Wählen Sie bei den beiden letzten zu aktivierenden Kästchen aus dem Dropdown-Menü die entsprechenden Optionen aus, um Ihre Konfiguration abzuschließen.

## Darstellungsanpassungen der Arbeitspaketlisten

Passen Sie Ihre Darstellung der Arbeitspaketlisten an, indem Sie in der Tabelle neben den **Arbeitspaket-Eigenschaften** unter **Standardmäßig anzeigen** das Kästchen aktivieren oder deaktivieren.

![Arbeitspaket-Darstellungen anpassen](media/eac30453647cf3e67378736f2c53c8c78062da0e.png "Arbeitspaket-Darstellungen anpassen")

Vergessen Sie nicht, Ihre Anpassungen zu **Speichern.**
