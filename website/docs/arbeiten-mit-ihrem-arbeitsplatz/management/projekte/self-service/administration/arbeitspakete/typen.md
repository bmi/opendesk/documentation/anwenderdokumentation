# Typen

Sie können vorhandene Arbeitspaket-Typen einsehen, bearbeiten, löschen und neu erstellen.

![Arbeitspaket-Typen](media/708dcc5809e37a978b4bde79863466a397ab686d.png "Arbeitspaket-Typen")

## Arbeitspaket-Typ bearbeiten

Um ein Arbeitspaket-Typ zu bearbeiten klicken Sie in der Tabellen-Ansicht auf den entsprechenden Typen, den Sie bearbeiten möchten. Sie gelangen so auf eine neue Seite. Sie haben dort die Möglichkeit den Namen, die Farbe und den Standardtext für die Beschreibung zu ändern. Außerdem können Sie das Kästchen bei **In Roadmap standardmäßig angezeigt, Typ für neue Projekte standardmäßig aktiviert** und **Ist ein Meilenstein** aktivieren oder deaktivieren.

Klicken Sie anschließend auf **Speichern**, um Ihre Anpassungen festzuhalten.

![Arbeitspaket-Typen bearbeiten](media/bc6ae6c9d7be29d4750f0babd414a5b24be41837.png "Arbeitspaket-Typen bearbeiten")

Sie können auch einen komplett neuen Arbeitspaket-Typen erstellen, indem Sie oben rechts auf **+Typ** klicken. Sie werden nun zu einer neuen Seite weitergeleitet.

![Arbeitspaket-Typen anlegen](media/9d0c52ac578e1406d2ca0fac59e35756c90b3ed8.png "Arbeitspaket-Typen anlegen")

Bitte füllen Sie die Eingabefelder aus, wählen Sie die Optionen aus dem **Dropdown-Menü** aus und aktivieren/deaktivieren die Kästchen. Klicken Sie anschließend auf **Anlegen**, um Ihre Eingaben zu speichern.

## Arbeitspaket-Typ löschen

**Löschen** Sie Arbeitspakekt-Typen, indem Sie auf den **Papierkorb** ganz rechts in der Tabellen-Ansicht klicken.

![Arbeitspaket-Typen löschen](media/15f30bfec9d1fe2325483f35fea6fa3165924896.png "Arbeitspaket-Typen löschen")

## Arbeitspaket-Typen sortieren

Sie haben außerdem die Möglichkeit Ihre Arbeitspaket-Typen zu sortieren. Klicken Sie dafür auf die Pfeile unter Sortierung in der Tabellen-Ansicht.

![Arbeitspaket-Typen sortieren](media/b417eaee47871c4a34dd417cc3941cda934a75ec.png "Arbeitspaket-Typen sortieren")
