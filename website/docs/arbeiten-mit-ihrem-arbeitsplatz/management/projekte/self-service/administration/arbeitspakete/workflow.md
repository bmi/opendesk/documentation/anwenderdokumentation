# Workflow

In **Projekte** wird ein Workflow durch die zulässigen Übergänge zwischen Status für eine bestimmte Rolle und einen Arbeitseinheiten-Typ definiert. Anders ausgedrückt legt es fest, welche Statusänderungen eine bestimmte Rolle je nach Art der Arbeitseinheit durchführen kann.

Beispielsweise könnte ein bestimmter Arbeitseinheiten-Typ, wie zum Beispiel eine Aufgabe, den folgenden Workflow haben: Neu -&gt; In Bearbeitung -&gt; Geschlossen -&gt; Aufgehalten -&gt; Abgelehnt -&gt; Geschlossen. Allerdings kann dieser Workflow je nach der Rolle innerhalb eines Projekts unterschiedlich sein.

## Workflows bearbeiten

Um Workflows zu bearbeiten führen Sie folgende Schritte durch:

1. Wählen Sie die Rolle aus dem Dropdown-Menü aus, für die Sie den Workflow bearbeiten möchten.
1. Wählen Sie den Arbeitseinheiten-Typ aus dem Dropdown-Menü aus, für den Sie den Workflow bearbeiten möchten.
1. Überprüfen Sie, ob Sie nur die Status angezeigt bekommen möchten, die von diesem Typ verwendet werden (diese Option ist automatisch ausgewählt, kann aber jederzeit abgewählt werden). **Hinweis**: Wenn Sie einen neuen Status erstellt haben und diesen dem Workflow eines bestimmten Arbeitseinheiten-Typs hinzufügen möchten, müssen Sie diese Option abwählen. Nur so erscheinen auch Status, die von diesem Typ (noch) nicht verwendet werden, in der Liste und können dem Workflow hinzugefügt werden.
1. Klicken Sie auf **Bearbeiten**, um Ihre Anpassungen zu speichern.

![Workflows bearbeiten](media/c0134fa3d18a9ba650e2d857a2613c3153807dd1.png "Workflows bearbeiten")

Außerdem können Sie folgende Anpassungen durchführen:
- Definieren, welche Statusänderungen von der ausgewählten Rolle für den bestimmten Arbeitseinheiten-Typ erlaubt sind.

Festlegen, ob eine bestimmte Rolle spezifische Statusänderungen vornehmen darf, wenn der zugewiesene Benutzer auch der Autor der Arbeitseinheit ist.  Es besteht auch die Möglichkeit, zusätzliche Statusübergänge zuzulassen, wenn der Benutzer als Zuständiger für eine Arbeitseinheit fungiert.

Speichern Sie Ihre Anpassungen mit dem **Speichern-Button**.

![Workflows bearbeiten](media/66ab5b726613eef1319e075307b0f54f88dbff4d.png "Workflows bearbeiten")

## Kopieren eines Workflows

Um einen vorhandenen Workflow zu kopieren, klicken Sie auf **Kopieren** in der oberen rechten Ecke.

Nachdem Sie einen existierenden Workflow kopiert haben, können Sie festlegen, welcher bestehende Workflow auf welchen anderen Workflow kopiert werden soll.

![Workflow kopieren](media/133abc855801ddfdd812f9d352c95a3476cc8b54.png "Workflow kopieren")

## Zusammenfassung der Workflows

Eine Übersicht der erlaubten Statusübergänge für einen Arbeitseinheiten-Typ und eine bestimmte Rolle erhalten Sie, indem Sie in der Workflow-Übersicht auf **Zusammenfassung** klicken.

![Workflows zusammenfassen](media/21bbdd45301b9940dd663182622b56096377186e.png "Workflows zusammenfassen")

Anschließend sehen Sie eine Übersicht aller Workflows. In einer Matrix werden die Anzahl der möglichen Statusübergänge für jeden Typ und jede Rolle dargestellt.

![Workflows zusammenfassen](media/3edeb4ef81389addb792fa447d73bb78288d81af.png "Workflows zusammenfassen")
