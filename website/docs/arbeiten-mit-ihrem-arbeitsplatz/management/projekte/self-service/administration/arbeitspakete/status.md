# Status

Sie können vorhandene Arbeitspaket-Status einsehen, bearbeiten, löschen und neu erstellen.

![Arbeitspaket-Status](media/6a27979047f34ed125670fcd32c51f2bbdab2ef8.png "Arbeitspaket-Status")

## Arbeitspaket-Status bearbeiten

Um ein Arbeitspaket-Status zu bearbeiten klicken Sie in der Tabellen-Ansicht auf den entsprechenden Status, den Sie bearbeiten möchten. Sie gelangen so auf eine neue Seite. Sie haben dort die Möglichkeit den **Namen** zu ändern. Außerdem können Sie das Kästchen bei **Arbeitspakete geschlossen** und **Arbeitspakete schreibgeschützt** aktivieren oder deaktivieren.

Klicken Sie anschließend auf Speichern, um Ihre Anpassungen festzuhalten.

![Arbeitspaket-Status bearbeiten](media/b26730bf0f9630e3e3a47fa6e3a1e26c63e95720.png "Arbeitspaket-Status bearbeiten")

Sie können auch einen komplett neuen Arbeitspaket-Typen erstellen, indem Sie oben rechts auf **+Status** klicken. Sie werden nun zu einer neuen Seite weitergeleitet.

![Arbeitspaket-Status neu erstellen](media/ab605789aee253aba2f2a37accc7140b48c6d8bb.png "Arbeitspaket-Status neu erstellen")

Bitte füllen Sie die Eingabefelder aus, wählen Sie die Optionen aus dem Dropdown-Menü aus und aktivieren/deaktivieren die Kästchen. Klicken Sie anschließend auf **Anlegen**, um Ihre Eingaben zu speichern.
