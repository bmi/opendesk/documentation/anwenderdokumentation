# Farben

## Farben

Um eine Reihe von Farben in Projekte zu definieren, navigieren Sie zu -&gt; **Administration** -&gt; **Farben**.

Sie können in Projekte eine Reihe von vordefinierten Farben konfigurieren, die Sie beispielsweise für die Farbgebung von Arbeitspakettypen oder zur Hervorhebung von Attributen, wie zum Beispiel dem Status, wählen können.

### Definieren Sie eine neue Farbe in Projekte

Um eine neue Farbe hinzuzufügen, drücken Sie den violetten **+Farbe** Button oben rechts.

![Farben](media/555e0665506032c5cd42caf55b1044e22aae6464.jpg "Farben")

1. Fügen Sie einen Namen für Ihre neue Farbe hinzu.
2. Geben Sie den Hexcode für Ihre neue Farbe ein.
3. Klicken Sie **Speichern**.

![Neue Farbe](media/3a0c5ea29deca31faa61797ddbd26afe347f10c0.jpg "Neue Farbe")
