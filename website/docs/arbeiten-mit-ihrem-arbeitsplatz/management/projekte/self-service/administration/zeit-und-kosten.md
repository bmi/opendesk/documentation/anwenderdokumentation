# Zeit und Kosten

## Zeit und Kosten

Sie können in Projekte Kosten konfigurieren und Kostenarten erstellen, um Einheitskosten zu verfolgen. Außerdem können Sie die Währung konfigurieren, die Sie für Ihre Kostenberichte verwenden möchten.

Navigieren Sie über **Administration** -&gt; **Zeit und Kosten.**

### Einstellungen

Navigieren Sie zu **Administration** -&gt; **Zeit und Kosten** -&gt; **Einstellungen**, um Einstellungen für Kosten in Projekte zu definieren.

1. Konfigurieren Sie die im System verwendete Währung, z.B. EUR.
2. Geben Sie das **Format** der Währung an, ob die Zahl vor oder nach der Währung stehen soll, z.B. 10 EUR oder $ 10.
3. Klicken Sie **Anwenden**, um Ihre Änderungen zu speichern.

![Zeit und Kosten](media/b983d90efd7bf46ab5acda58db957b858eee1aca.jpg "Zeit und Kosten")

## Erstellen und Verwalten von Kostentypen

Sie können Kostentypen erstellen und verwalten, um Einheitskosten in Projekte zu Arbeitspaketen zuzuordnen.

Navigieren Sie über Ihren Avatar oben rechts auf **Administration** anschließend in der Liste auf der linken Seite auf **Zeit und Kosten** und klicken Sie daraufhin auf **Kostentypen**, um Einheitskostenarten zu erstellen und zu verwalten.

Klicken Sie auf **+ Kostentyp**, um eine neue Kostenart zu erstellen.

Sie können folgende Optionen festlegen:

1. Geben Sie einen **Namen** für die Kostenart ein.
1. Definieren Sie den **Einheitennamen** für diese Kostenart, z.B. Euro, Stück, Tag usw.
1. Stellen Sie den **pluralisierten Einheitennamen** ein, z.B. Euros, Stücke, Tage usw.
1. Wählen Sie, ob die **Kostenart als Standardkostenar** t beim Buchen neuer Einheitskosten verwendet werden soll.
1. Definieren Sie die **Stundensatz-Historie** der Rate und setzen Sie ein Datum, ab dem diese Kostenart gültig sein soll. Legen Sie einen Tarif (in der angegebenen Einheit) für dieses Datum fest.
1. Mit dem **+** Icon können Sie zusätzliche Tarife für andere Daten hinzufügen.
1. Das **Lösch** Icon entfernt einen Tarif für ein bestimmtes Datum.
1. Vergessen Sie nicht, Ihre Änderungen zu **speichern**.

![Kostentyp](media/02e4a01c7c7ab1089b240ac7fb44d504a8218db5.jpg "Kostentyp")

In der Übersichtsliste aller Kostenarten können Sie Folgendes definieren:

1. **Filtern** Sie nach Kostenarten zu einem bestimmten festen Datum in der Liste.
1. Wählen Sie aus, ob auch gesperrte Kostenarten angezeigt werden sollen.
1. Drücken Sie **Anwenden**: Auf diese Weise erhalten Sie die Kostenarten mit dem Tarif (zum gewählten festen Datum) in der darunterliegenden Liste angezeigt.
1. Klicken Sie auf den Namen einer Kostenart, um einen **vorhandene Kostentyp** zu bearbeiten.
1. **Setzen Sie einen aktuellen Tarif** (für den aktuellen Gültigkeitszeitraum) und drücken Sie das Speicher-Icon, um ihn anzuwenden.
1. **Sperren** Sie eine Kostenart. Bitte beachten Sie, dass Sie Kostenarten nur sperren, aber nicht löschen können.

![Kostentypen](media/f0c60fe682ea22f6357ca438a61ae14ba6d657ce.jpg "Kostentypen")

**Hinweis**: Mit Kostenarten können Sie auch alle Arten von Einheiten zu Arbeitspaketen buchen, z.B. Urlaubstage, Abwesenheitstage, Reisetage usw. Wählen Sie einfach 1 als Einheit. Auf diese Weise können Sie z.B. Urlaubstage gegen ein Urlaubsbudget erfassen und die Tage in der Kostenberichterstattung auswerten.
