# Benutzerdefinierte Felder für Projekte

Eine Benutzerin oder ein Benutzer kann sich ausgewählte Informationen eines Arbeitspakets oder eines Projekts über **benutzerdefinierte Felder** anzeigen lassen.

## Ein benutzerdefiniertes Feld für Projekte erstellen

Um ein neues **benutzerdefiniertes Feld** für ein Projekt zu erstellen wählen Sie: **Administration** - **Benutzerdefinierte Felder** aus. Wählen Sie die **Registerkarte Projekte** aus. Klicken Sie auf **Neues benutzerdefiniertes Feld**.

Es öffnet sich ein neues Fenster.

![Benutzerdefiniertes Feld für Projekte erstellen](media/7367bd334697abca5ed20f03a6cc13db9a2ae6db.PNG "Benutzerdefiniertes Feld für Projekte erstellen")

- **Name:** Wählen Sie hier einen Namen für Ihr benutzerdefiniertes Feld aus
- **Format:** Wählen Sie hier ein Format für das benutzerdefinierte Feld aus, über das Drop-down-Menü stehen Ihnen mehrere Möglichkeiten zur verfügung
- **Länge:** Geben Sie die minimale und die maximale Länge für ihr Feld an
- **Regulärer Ausdruck:** Geben Sie hier an, welche Werte in Ihrem Feld erlaubt sind
- **Standardwert:** Geben Sie an, welche Werte standardmäßig in diesem Feld sein sollten
- **Erforderlich:** Geben Sie an, ob es erforderlich sein muss, dass dieses Feld ausgefüllt wird
- **Sichtbar:** Geben Sie an, ob das benutzerdefinierte Feld sichtbar sein soll

**Durchsuchbar:** Geben Sie an, ob dieses Feld durchsuchbar sein soll  Klicken Sie auf **Speichern,** nachdem Sie Ihre Angaben eingetragen haben.

![Optionen im benutzerdefinierten Feld in Projekte](media/6809362587271955c34e3777804cfc23aebfc540.PNG "Optionen im benutzerdefinierten Feld in Projekte")

## Einfügen des benutzerdefinierten Feldes in ein Projekt

Wenn das **benutzerdefinierte Feld** erstellt wurde, kann ein Wert für dieses in den Projekteinstellungen gesetzt werden.

Um dies zu tun navigieren Sie zu den Projektinformationen. Als Projektmanager können Sie diese Informationen ändern.

![Projektinformationen](media/57b50680f4882baba1fcc17176c5ee471e1a7730.PNG "Projektinformationen")

## Anzeigen von benutzerdefinierten Feldern in Projekten

Wenn Sie sichtbar in den Einstellungen des **benutzerdefinierten Feldes** ausgewählt haben, können Sie das Feld in der Ansicht alles Projekte auswählen.

Navigieren Sie zur Projektliste und wählen Sie oben Links **Alle Projekte** aus.

![Projektliste](media/537d33dfc7002032a1a13e3ee0fd478f3f661136.png "Projektliste")

![Alle Projekte anzeigen](media/9fba54b1106fb192445d516a188856049dfe2ca3.PNG "Alle Projekte anzeigen")

In der Projektliste wird Ihnen nur dann eine Spalte Ihres erstellten **benutzerdefinierten Feld** angezeigt, wenn Sie über die Enterprise Edition verfügen.

![Spalten für benutzerdefinierte Felder](media/61a3b6ee689c852d6882f6228895a26800e64dc5.PNG "Spalten für benutzerdefinierte Felder")
