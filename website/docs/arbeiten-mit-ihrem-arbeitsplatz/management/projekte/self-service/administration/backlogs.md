# Backlogs

## Backlogs-Konfiguration

Konfigurieren Sie Ihre Backlogs-Einstellungen in Projekte. Navigieren Sie zu **Administration** -&gt; **Backlogs** -&gt; **Einstellungen**, um Ihre Backlogs einzurichten.

Sie können Folgendes in den Backlogs-Einstellungen konfigurieren:

1. Legen Sie die Arbeitspakettypen fest, die als **Story-Typen** verwendet werden sollen. Halten Sie Strg gedrückt, um mehrere Typen auszuwählen. Die Story-Typen erscheinen in der Backlogs-Ansicht (Produkt-Backlog, Wunschliste, Sprint) und können direkt in der Backlogs-Ansicht erstellt und priorisiert werden, z.B. EPIC, FEATURE, BUG.
1. Stellen Sie den **Aufgaben-Typ** ein. Der Aufgabentyp erscheint im Aufgabenboard zur Verwaltung in Ihren täglichen Stand-up-Meetings. Bitte beachten Sie: Sie können einen Arbeitspakettyp nicht als Story-Typ und als Aufgabentyp verwenden.
1. Definieren Sie, ob ein **Burn-down** - oder **Burn-up** -Diagramm angezeigt werden soll.
1. Legen Sie eine Vorlage für die **Sprint-Wiki-Seite** fest. Wenn Sie eine Wiki-Seite mit diesem Namen erstellen, z.B. sprintwiki, richten Sie eine neue Wiki-Seite basierend auf dieser Vorlage ein, wenn Sie sie über das Sprint-Dropdown-Menü in der Backlogs-Ansicht öffnen. Auf diese Weise können Sie einfach Vorlagen für Sprint-Review-Meetings oder Retrospektiven erstellen.
1. Klicken Sie **Anwenden**, um Ihre Änderungen zu speichern.

![Backlogs-Konfiguration](media/55325ad411d991148859d612c95cb14261bfa927.jpg "Backlogs-Konfiguration")
