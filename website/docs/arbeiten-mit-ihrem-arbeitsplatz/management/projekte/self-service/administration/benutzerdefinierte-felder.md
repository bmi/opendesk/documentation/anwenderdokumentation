# Benutzerdefinierte Felder

Sie können **benutzerdefinierte Felder** für unterschiedliche Bereiche erstellen. Zum Beispiel Arbeitspakete, Projekte, Benutzerinnen und Benutzer oder auch Gruppen.

**Benutzerdefinierte Felder** ermöglichen es Ihnen, Ihr Projekt an Ihre Bedürfnisse anzupassen. Dadurch ist es Ihnen zum Beispiel möglich, unterschiedliche Arbeitspakete zu erstellen, die an die Anforderungen Ihrer Organisation angepasst sind.

## Ein benutzerdefiniertes Feld erstellen

Um eine neues **benutzerdefiniertes Feld** zu erstellen wählen Sie: **Administration** - **Benutzerdefinierte Felder** aus.

Wählen Sie die **Registerkarte** aus für die Sie ein **benutzerdefiniertes Feld** erstellen wollen. Eine Liste mit allen **benutzerdefinierten Felder** n wird Ihnen angezeigt. Beachten Sie, dass für unterschiedliche Sektionen **benutzerdefinierte Felder** angezeigt werden (Registerkarte).

Klicken Sie in Ihrer gewünschten **Registerkarte** auf die Schaltfläche **Benutzerdefiniertes Feld** um eine neues **Benutzerdefiniertes Feld** zu erstellen.

![Benutzerdefiniertes Feld erstellen](media/26f1ecb1cb5e4bd7777bdb129032b1ee70898f0a.PNG "Benutzerdefiniertes Feld erstellen")

Abhängig davon, für welchen Bereich das **benutzerdefinierte Feld** erstellt wurde, stehen unterschiedliche Optionen zur Verfügung.

![Optionen benutzerdefiniertes Feld](media/1b6086718cbb7a275687b6a4e32b38dfe4d514de.PNG "Optionen benutzerdefiniertes Feld")

- **Name:** Wählen Sie hier einen Namen für Ihr benutzerdefiniertes Feld aus
- **Format:** Wählen Sie hier ein Format für das benutzerdefinierte Feld aus, über das Drop-Down-Menü stehen Ihnen mehrere Möglichkeiten zur verfügung
- **Länge:** Geben Sie die minimale und die maximale Länge für ihr Feld an
- **Regulärer Ausdruck:** Geben Sie hier an, welche Werte in Ihrem Feld erlaubt sind
- **Standardwert:** Geben Sie an, welche Werte standardmäßig in diesem Feld sein sollten
- **Erforderlich:** Geben Sie an, ob es erforderlich sein muss, dass dieses Feld ausgefüllt wird
- **Für alle Projekte:** Geben Sie an, ob dieses Feld in allen Projekten angelegt werden soll
- **Als Filter benutzen:** Geben Sie an, ob dieses Feld als Filter benutzt werden kann
- **Durchsuchbar:** Geben Sie an, ob dieses Feld durchsuchbar sein soll  Klicken Sie auf **Speichern** nachdem Sie Ihre Angaben eingetragen haben.

## Ein benutzerdefiniertes Feld einem Arbeitspaket oder Projekt hinzufügen

**Hinweis: Benutzerdefinierte Felder** müssen in Arbeitspaketen und Projekten aktiviert werden um genutzt werden zu können. **Benutzerdefinierte Felder** sind in Arbeitspaketen nur sichtbar wenn folgende Vorraussetzungen erfüllt sind: 

- Das **benutzerdefinierte Feld** muss dem Arbeitspakte hinzugefügt worden sein (Einstellungen). 
- Das **benutzerdefinierte Feld** wurde im Projekt aktiviert.

1. Ein **benutzerdefiniertes Feld** einem Arbeitspaket hinzufügen (Einstellungen). Sie können dies direkt über die Spalte **IN TYP ENTHALTEN** in der **benutzerdefinierte Felder-Liste** lösen.

![In Typ enthalten Spalte](media/818154caeba1c3e8a44982c58ca7c3d551394539.PNG "In Typ enthalten Spalte")

2. Aktivieren Sie das **benutzerdefinierte Feld** für ein Projekt in den Projektkonfigurationen. Dies ist nicht relevant, wenn Sie zuvor für das Feld **Für alle Projekte** ausgewählt haben.

Mit diesen Einstellungen haben Sie alle Einstellungen bearbeitet, die zur Verwendung eines **benutzerdefinierten Feldes** notwendig sind. Sie können auswählen, welche Felder für Ihr Projekt notwendig sind, diese erstellen und bei Bedarf ändern.

## Ein benutzerdefiniertes Feld bearbeiten oder löschen

Um ein bereits existierendes **benutzerdefiniertes Feld** zu bearbeiten wählen Sie die passende Registerkarte aus und klicken Sie auf den Namen des Feldes. Um ein **benutzerdefiniertes Feld** zu löschen klicken Sie auf die **Löschen** - **Schaltfläche** (Papierkorb).

![Feld bearbeiten oder löschen](media/970d4d6be4d17f3af7a11f2cb5a1c2413b2730be.PNG "Feld bearbeiten oder löschen")

### Hier geht es weiter mit:
- [Benutzerdefinierte Felder für Projekte](benutzerdefinierte-felder/benutzerdefinierte-felder-fuer-projekte.md)
