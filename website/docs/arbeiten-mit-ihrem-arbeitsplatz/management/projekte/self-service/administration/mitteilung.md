# Ankündigung

## Ankündigung

Sie können in Projekte **Ankündigungen** konfigurieren, die für einen bestimmten Zeitraum für Benutzerinnen und Benutzer angezeigt werden, wenn sie sich anmelden. Gehen Sie dafür auf **Administration**, wenn Sie auf Ihren Avatar in der oberen rechten Ecke klicken und anschließend auf **Ankündigung** auf der linken Seite des Bildschirmes.

1. Geben Sie einen Text für Ihre Ankündigungsnachricht ein.
2. Legen Sie ein **Datum** fest, bis zu dem die Ankündigung angezeigt werden soll.
3. Setzen Sie die Ankündigung auf **aktiv**. Nur dann wird die Ankündigung angezeigt.
4. **Speichern** Sie Ihre Änderungen.

![Ankündigung](media/1997c164d7b3a1339be87ff2504a16db8ad391bf.jpg "Ankündigung")

Außerdem wird die aktive Ankündigung bis zum festgelegten Datum den Benutzerinnen und Benutzern auf der Startseite der Anwendung angezeigt.

![Ankündigung auf der Startseite](media/608ca74c85fd0e88dd6fc5ac46f99e3db113eaab.jpg "Ankündigung auf der Startseite")
