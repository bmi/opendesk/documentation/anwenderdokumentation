# Abmelden

## Abmelden

Nachdem Sie sich wie unter [An- und Abmelden](../../../../erste-schritte/an-und-abmelden.md) beschrieben angemeldet haben, sind die automatisch auch im Modul **Projekte** angemeldet. Um sich von diesem Modul abzumelden, klicken Sie rechts oben auf ihr Profil und wählen Sie die Option **Abmelden.**

![Abmelden aus dem Modul](media/b076c1573f7aa8bf0858539203ae80468f8e9efa.png "Abmelden aus dem Modul")
