# Mein Konto

Sie finden unter **Mein Konto** Einstellungen zu Ihrem Account, indem Sie in der rechten oberen Ecke auf Ihr Profilbild navigieren und dort Mein Konto aus dem **Dropdown-Menü** auswählen. Hier sehen Sie nun folgende Unterpunkte:

- **Profil**
- **Einstellungen**
- **Zwei-Faktor-Authentifizierung**
- **Zugriffstokens**
- **Benachrichtigungseinstellungen**
- **E-Mail-Erinnerungen**
- **Profilfoto**

## Profil

Sie haben die Möglichkeit Informationen zu Ihrem Profil hinzuzufügen. In den Zeilen **Vorname** und **Nachname** sind diese vorausgefüllt und können nicht geändert werden. In der Zeile **E-Mail** können Sie ihre E-Mail-Adresse einfügen oder ändern. Außerdem können Sie je nach Bedarf ein Häkchen bei **E-Mail-Adresse nicht anzeigen** setzen. Klicken Sie auf **Speichern**, um Ihre Profildaten zu speichern.

![Ansicht Mein Konto](media/c991bd55d4a9cc36860ca5f19390965a6146275c.png "Ansicht Mein Konto")

## Einstellungen

Ändern Sie unter dem Punkt **Sprache** mittels des Dropdown-Menüs die Sprache Ihrer Oberfläche. Hier können Sie aus verschiedenen Sprachen auswählen. Außerdem können Sie Ihre **Zeitzone** ändern. Es stehen Ihnen auch hier im Dropdown-Menü verschiedene Zeitzonen zur Verfügung. Sie können Ihren **Modus** ändern, indem Sie zwischen **Hell** und **Hell (hoher Kontrast)** entscheiden können. Unter dem Punkt **Kommentare anzeigen** können Sie **Neueste zuerst** oder **Älteste zuerst** wählen. Je nach Bedarf können Sie ein Häkchen bei **Beim Verlassen einer Arbeitspaket-Seite mit ungespeichertem Text warnen** und **Benachrichtigungen erfolgreicher Aktionen automatisch ausblenden** setzen.

Für die angezeigte Farbe der Aufgaben haben Sie unter **BACKLOGS** die Möglichkeit eine Farbe in Form eines Hex-Codes anzugeben und für Ihre Ansicht können Sie das Häkchen bei **Zeige Versionen eingeklappt** aktivieren.

## Zwei-Faktor-Authentifizierung

Richten Sie für Ihren Account die **Zwei-Faktor-Authentifizierung** ein, indem Sie entweder oben rechts auf die Schaltfläche **+2FA-Gerät** oder unter dem Punkt **2FA-Geräte** auf das **Plus(+)** klicken. Wenn Sie auf auf eine der beiden Schaltflächen geklickt haben, befinden Sie sich auf der Seite mit der Überschrift **Neues 2FA-Gerät hinzufügen** . Dort können Sie ein neues Gerät registrieren und sich ein Passwort erstellen, welches von Zeit und Zeit geändert werden muss. Außerdem können Sie **2FA Backup-Codes** erstellen, indem Sie auf **2FA Backup-Codes generieren** klicken.

![Zwei-Faktor-Authentifizierung](media/55a999d2f3eef49dfa6031eb3315f84a93a47661.png "Zwei-Faktor-Authentifizierung")

## Zugriffstokens

Generieren Sie einen Zugriffstoken, um Drittanbieter-Anwendungen mit **Projekte** zu verknüpfen. Um ein neues Token zu erstellen, klicken Sie auf **+API-Token**. Sie können außerdem für iCALENDAR, OAUTH, RSS und den DATEISPEICHER einen Token erstellen. Hierbei wird jeweils externen Benutzern dieser Anwendungen einen Zugriff auf Ihre Daten gewährt.

## Sitzungsverwaltung

Sie können eine Liste der Geräte einsehen, mit denen Sie sich mit Ihrem Konto angemeldet haben. Widerrufen Sie ggfl. Anmeldungen, die nicht von Ihnen getätigt wurden. Klicken Sie dafür am Zeilenende der jeweiligen Sitzungen auf das **Mülltonnen-Symbol**, um diese zu entfernen.

## Benachrichtigungseinstellungen

Nehmen Sie Einstellungen zu Ihren Benachrichtigungen vor. Sie können jeweils Häkchen aktivieren, um Benachrichtigungen zu Arbeitspaketen und Projekten zu erhalten, an denen Sie beteiligt oder nicht beteiligt sind.

Außerdem können Sie Benachrichtigungseinstellungen für ein bestimmtes Projekt hinzufügen, indem Sie auf **+Einstellung für Projekt hinzufügen** klicken. Dort können Sie in dem Textfeld **Bitte auswählen** den Titel des Projekts eingeben und projektspezifische Benachrichtigungen vornehmen. Klicken Sie anschließend auf **Speichern**, um die Einstellungen beizubehalten.

![Projektspezifische Benachrichtigungseinstellungen](media/b5578f558a540ffa73513eef7b8956cc76b08913.png "Projektspezifische Benachrichtigungseinstellungen")

## E-Mail-Erinnerungen

Unter E-Mail-Erinnerungen können Sie einstellen, ob und welche Erinnerungs-E-Mails Sie bekommen möchten. Sie können ein Häckchen setzen, dass Sie eine E-Mail erhalten wollen, wenn Sie jemand erwähnt. Sie können außerdem entscheiden, wann Sie diese E-Mails erhalten möchten und wann nicht. Wenn Sie bei **Tägliche E-Mail-Erinnerungen aktivieren** den Haken setzen, bekommen Sie täglich E-Mail-Benachrichtigungen. Geben Sie bitte die gewünschte Zeit für die E-Mail-Benachrichtigung an.

Sie können auch Tage festlegen, an denen Sie E-Mails bekommen möchten bzw. keine E-Mails bekommen möchten. Aktivieren Sie dafür die entsprechenden Wochentage. Wenn Sie tägliche E-Mail-Erinnerungen vorübergehend pausieren möchten, setzen Sie bitte dort das Häkchen.

Einstellungen für E-Mail-Benachrichtigungen anderer Objekte z.B. wenn Neuigkeiten, Kommentare, Dokumente, Nachrichten im Forum, Wiki-Seiten oder Mitgliedschaften hinzugefügt werden, können Sie ebenfalls vorgenommen werden.

## Profilfoto

Sie haben unter **Profilfoto** einen voreingestellten Gravatar. Sie haben aber die Möglichkeit ihren Gravatar für Ihre E-Mail-Adresse zu ändern. Klicken Sie dafür auf den folgenden Link: [gravatar.com](https://gravatar.com).

Sollten Sie lieber ein selbsterstelltes Profilbild einfügen wollen, können Sie auf die Schaltfläche **Datei auswählen** klicken und aus Ihrem Datenspeicher ein Foto von Ihnen auswählen und dieses hochladen. Klicken Sie anschließend auf **Aktualisieren**, um das Foto zu speichern.
