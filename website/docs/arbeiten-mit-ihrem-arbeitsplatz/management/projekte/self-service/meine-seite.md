# Meine Seite

## Überblick

Ihr persönliches Dashboard **Meine Seite** zeigt Ihnen alle wichtigen Informationen zu Ihren Projekten an. Sie sehen zum Beispiel Nachrichten, Ihre Arbeitspakete, gebuchte Zeiten oder einen Kalender. Selbstverständlich kann Meine Seite nach Ihren Bedürfnissen konfiguriert werden.

Gehen Sie rechts oben zu Ihrem Profilbild bzw. Namenskürzel und öffnen Sie das Dropdown-Menü mit einem Klick. Wählen Sie die erste Option **Meine Seite** um Ihr Dashboard zu öffnen. In der Standardeinstellung sehen Sie hier zunächst zwei projektübergreifende Listen aller **Arbeitspakete, die Ihnen zugewiesen wurden** und **Arbeitspakete, die von Ihnen erstellt wurden**.

## Widgets hinzufügen

Fügen Sie ein Widget zu **Meine Seite** hinzu, indem Sie auf das **Pluszeichen (+)** oben rechts auf der Seite klicken. Nun werden auf Ihrer Seite mehrere Pluszeichen sichtbar. Dies sind die Stellen, an denen Sie neue Widgets hinzufügen können. Entscheiden Sie sich für den gewünschten Ort und klicken Sie auf das Pluszeichen. Ein neues Dialogfenster öffnet sich und Sie können aus der Liste der Widgets das bzw. die gewünschten aussuchen. Durch einen Klick auf den Titel des Widgets wird dieses direkt eingefügt. Zur Auswahl stehen:

- Arbeitspaket-Tabelle
- Benutzerdefinierter Text
- Dokumente
- Kalender
- Meine gebuchte Zeit
- Mir zugewiesene Arbeitspakete
- Neuigkeiten
- Von mir beobachtete Arbeitspakete
- Von mir erstellte Arbeitspakete
- Von mir verantwortete Arbeitspakete

![Widget-hinzufügen](media/38bb61b572cbd51b839f8bb66687a577b378ba33.jpg "Widget-hinzufügen")

## Position und Größe der Widgets bearbeiten

**Position**

Ändern Sie die Position eines Widgets auf dem Dashboard mit der Drag &amp; Drop-Funktion. Klicken Sie auf die **Punkte neben dem Titel des Widgets**. Der Mauszeiger verwandelt sich zu einem Anfasser (Hand). Ziehen Sie das Widget nun zur gewünschten Stelle.

**Größe**

Wenn Sie auf die Punkte in der **unteren rechten Ecke eines Widgets** klicken, verwandelt sich der Mauszeiger in einen Doppelpfeil. So können Sie die Größe des Widgets ändern, indem Sie das Widget mit der Maus entsprechend ziehen.

## Konfigurieren Sie die Ansicht eines Widgets (für Arbeitspaket-Tabellen)

Optimieren Sie die Ansicht eines Arbeitspakets in den Widgets auf **Meine Seite** so, dass nur die von Ihnen benötigten Informationen angezeigt sind. Klicken Sie in einem Arbeitspaket-Widget auf das Drei-Punkte-Menü oben rechts und wählen aus dem Dropdown **Ansicht konfigurieren**. Im nächsten Fenster wählen Sie die Kriterien für Ihre Arbeitspakete aus.

![Ansicht-eines-Widgets-konfigurieren](media/acf9aef73115a15a20edc65d8ee5741c2d703c71.jpg "Ansicht-eines-Widgets-konfigurieren")

![Konfiguration-des-Widgets-für-Arbeitspakete](media/7d243462e556f88599895d048993d5f42f4eca17.jpg "Konfiguration-des-Widgets-für-Arbeitspakete")

## Entfernen der Widgets

**Entfernen** Sie ein Widget von **Meine Seite**, indem Sie auf das **Drei-Punkte-Menü** in der oberen rechten Ecke klicken und aus dem Dropdown **Widget entfernen** wählen.

![widget-entfernen](media/247e12e79d865e05bb4cb9d02d0eec9c7582347b.jpg "widget-entfernen")

## Zeit-Widget

Sie können auch ganz bequem Ihre Zeiten auf **Meine Seite** eintragen. Fügen Sie dazu das Widget **Meine gebuchte Zeit** hinzu. In diesem Widget können Sie direkt neue Zeiteinträge erstellen, indem Sie auf den Tag klicken, die Zeiten per Drag&amp;Drop auf ein anderes Datum ziehen und die Zeiten bearbeiten oder löschen.

![Widget-Meine-gebuchte-Zeit](media/2cbca8410371a73076c9e51e29037c2bbb820203.jpg "Widget-Meine-gebuchte-Zeit")
