# Administration

## Systemadministrator-Handbuch

Um Änderungen an Einstellungen vorzunehmen, die Ihre gesamte Projekte-Umgebung betreffen, müssen Sie zur Projekte Verwaltung navigieren.

Wenn Sie Administrator Ihrer Projekte-Umgebung sind, können Sie auf die Verwaltung zugreifen, indem Sie auf Ihren Benutzer-Avatar in der oberen rechten Ecke klicken und im Dropdown-Menü **Administration** auswählen.

Klicken Sie auf eine der Kategorien, um mit der Konfiguration fortzufahren.

![Übersicht](media/47255ba13e7fb19ab9fdf3a3c4430edac0b1fb57.jpg "Übersicht")

## Übersicht

| Benutzer und Berechtigungen    | Wie Benutzerinnen und Benutzer, Gruppen und Berechtigungen in Projekte verwendet werden                                  |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------ |
| Arbeitspakete                  | Einstellungen zu Arbeitspaketen, Typen, Status und Arbeitsabläufe                                                        |
| Benutzerdefinierte Felder      | Benutzerdefinierte Felder für Arbeitspakete, aufgewendete Zeit, Projekte, Versionen, Benutzer, Gruppen und mehr          |
| Hilfe-Texte für Attribute      | Fügen Sie Hilfe-Texte hinzu um Attribute zu erklären (Inklusive Benutzerdefinierte Felder) in Projekte und Arbeitspakete |
| Aufzählungen                   | Setzen Sie Aufzählungen, z. B. Prioritäten zu Arbeitspaketen, Zeiterfassungsaktivitäten, Dokumentenkategorien und mehr   |
| Kalendarien und Daten          | Stellen Sie Ihre Arbeitstage, das Datumsformat und die Kalenderabonnements ein                                           |
| Systemkonfiguration            | Stellen Sie Ihre Systemkonfigurationen nach Ihren Wünschen ein (Willkommenstexte, Sprachen und vieles mehr)              |
| E-Mails und Benachrichtigungen | Organisation von E-Mails und Benachrichtigungen                                                                          |
| API und Webhooks               | Organisation von APIs und Einstellungen zu Webhooks                                                                      |
| Authentifizierung              | Setzen Sie Ihre Authentifizierungsmethoden in Projekte                                                                   |
| Ankündigung                    | Wie man Ankündigungen erstellt                                                                                           |
| Design                         | Erstellen Sie Ihr eigenes Design                                                                                         |
| Farben                         | Wählen Sie Farben für Ihr System aus (Arbeitspakete, Statusfarben)                                                       |
| Enterprise edition             | Update von Projekte und andere administrative Einstellungen                                                              |
| Zeit und Kosten                | Erstellen Sie Kostentypen und wählen Sie eine Währung aus                                                                |
| Backlogs                       | Setzen Sie die Einstellungen für Ihr Backlog in Projekte                                                                 |
| Datei-Speicher                 | Setzen Sie die Einstellungen für Ihren Datei-Speicher                                                                    |
| Plugins                        | Setzen Sie die Einstellungen für Ihre Plugins in Projekte                                                                |
| Backup                         | Sehen Sie sich den Status und die letzten Systeminformationen an                                                         |
| Information                    | So verbinden Sie Projekte mit anderen Anwendungen                                                                        |

### Hier geht es weiter mit:

- [Ersteinrichtung](administration/ersteinrichtung.md)
- [Benutzer und Berechtigungen](administration/benutzer-und-berechtigungen.md)
- [Arbeitspakete](administration/arbeitspakete.md)
- [Benutzerdefinierte Felder](administration/benutzerdefinierte-felder.md)
- [Aufzählungen](administration/aufzaehlungen.md)
- [Kalendarien und Daten](administration/kalender-und-datumsangaben.md)
- [Systemkonfigurationen](administration/systemeinstellungen.md)
- [E-Mails und Benachrichtigungen](administration/e-mails-und-benachrichtigungen.md)
- [API und Webhooks](administration/api-und-webhooks.md)
- [Authentifizierung](administration/authentifizierung.md)
- [Ankündigung](administration/mitteilung.md)
- [Farben](administration/farben.md)
- [Zeit und Kosten](administration/zeit-und-kosten.md)
- [Backlogs](administration/backlogs.md)
- [Plugins](administration/plugins.md)
- [Backup](administration/backup.md)
- [Informationen](administration/informationen.md)
