# Erste Schritte

Um mit **Projekte** zu beginnen, gibt es ein paar einfache Schritte zu befolgen:

1. Erstellen Sie ein Konto und melden Sie sich an

1. Erstellen Sie ein neues Projekt

1. Teammitglieder zur Zusammenarbeit einladen

1. Arbeitspakete erstellen

1. Einrichten eines Projektplans
