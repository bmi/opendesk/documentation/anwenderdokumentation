# Projekte-Modul öffnen

Die **Projektübersicht** öffnen Sie über Ihre persönliche Startseite. Klicken Sie auf den Module-Button am linken oberen Bildschirmrand. Der nun offene Reiter zeigt eine Übersicht der verschiedenen Module. Klicken Sie auf **Projekte**.

## Modul-Übersicht

![Reiter mit Übersicht der Module](media/dc81d82a07fe3281ac5d6704ab200f34a5e135c6.PNG "Modul-Übersicht")

## Projekt-Übersicht

![Übersicht der Projekte](media/900d451e2bc8ea788e65ca4a62e7ed7a3a5e6c20.PNG "Übersicht der Projekte")
