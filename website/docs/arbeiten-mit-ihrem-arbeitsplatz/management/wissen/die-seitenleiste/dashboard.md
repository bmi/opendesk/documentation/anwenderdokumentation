# Dashboard

Auf dem Dashboard werden wichtige Informationen zusammenfasst. Sie erhalten dort einen direkten Zugriff auf häufig verwendete Funktionen.

Sie erreichen das Dashboard von der Startseite aus. Klicken Sie in der Seitenleiste links unter **Anwendungen** auf den entsprechenden Menüpunkt.

![Dashboard auf der Startseite](media/29511ab7144a6b06e81beceffaf7a1d5b2c263ea.jpg "Dashboard auf der Startseite")

## Navigieren im Dashboard

Das Dashboard ist in mehrere Schlüsselbereiche unterteilt, die für eine effektive Navigation und Verwaltung sorgen. Der Bereich **Seiten** bietet einen direkten Zugriff auf wichtige Seiten wie die Startseite, die Hilfe-Sektion für Anleitungen, den Sandkasten zum Experimentieren und Ihre persönliche Wiki-Seite.

![Dashboard](media/6fbd0037671b7f5a6a2b9199a1a3e90ee2665ac8.png "Dashboard")

## Überblick über Aktivitäten

Im **Aktivitätenbereich** werden Sie über neueste Profiländerungen und Seitenbearbeitungen informiert. Es ist auch erkennbar, welche Seiten bei den Benutzerinnen und Benutzern beliebt sind.

## Tags (Schlagwörter) und Dokumentenmanagement

Der **Tag-Bereich** erleichtert die Organisation und Suche nach Dokumenten durch eine strukturierte Tagging-Funktion.

Im **Dokumentenbereich** können Sie neue Dokumente hinzufügen und bestehende verwalten, was eine strukturierte Dokumentation und Archivierung ermöglicht.

## Seiten erstellen und bearbeiten

Auch auf dem Dashboard haben Sie schnellen Zugriff auf Funktionen zum Erstellen und Bearbeiten von Seiten. Sie finden die Schaltflächen **Erstellen** sowie **Bearbeiten** oben rechts neben dem Titel **Dashboard**. Für ausführlichere Hinweise zum Erstellen und Bearbeiten von Seiten schauen Sie bitte in das entsprechende Kapitel der Dokumentation.
