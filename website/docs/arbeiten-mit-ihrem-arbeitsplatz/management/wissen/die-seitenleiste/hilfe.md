# Hilfe

Das Modul **Wissen** verfügt über einen eigenen Hilfebereich. Sie erreichen ihn, indem Sie im Menü links unter **Anwendungen** auf **Hilfe** klicken.

![Die Seitenleiste mit den Überpunkten &quot;Anwendungen&quot; (einschließlich &quot;Hilfe&quot;) und &quot;Navigation&quot;](media/665d50d22803d342762a45067f76b042186a71c0.png "Die Seitenleiste")

Sie gelangen auf eine Übersichtsseite und können Informationen zu verschiedenen Themen abrufen. Einige Links führen zu englischsprachigen Seiten.

Auf den Unterseiten Vorlagen, Makros und Anwendungen finden Sie die Schaltfläche **Probieren Sie es aus!**. Sie öffnet einen Editor, in dem Sie die entsprechende Funktion direkt anwenden können.

Folgende Abschnitte sind auf der Übersichtsseite zu finden:

- Erste Schritte: Dieser Link führt zu einer englischsprachigen Seite, auf der Sie sich mit den Grundfunktionen des Moduls vertraut machen können
- Videos (englisch): Über diesen Link gelangen Sie zu einer Übersicht von englischsprachigen Videoanleitungen, die Sie einzeln nach Bedarf abrufen können
- Seitenbearbeitung: Dieser Link öffnet den Sandkasten (näheres dazu im entsprechenden Abschnitt der Hilfe), in dem Sie die Bearbeitung von Seiten ausprobieren und üben können
- Vorlagen: Über diesen Link erfahren Sie mehr über Vorlagen, also Seiten, die Sie einrichten und später als Grundlage neuer Seiten wiederverwenden können. Sie finden hier auch Beispiele für vorgefertigte Vorlagen. Der Link **Mehr erfahren** führt zu einer englischsprachigen Anleitung
- Makros: Über diesen Link erfahren Sie mehr über Makros und können sich Beispiele ansehen. Makros sind automatisierte Funktionsabläufe, die Ihnen die Arbeit erheblich erleichtern können. Beispielsweise können Sie mithilfe eines Makros automatisch ein Inhaltsverzeichnis für Ihre Seite anlegen, statt von Hand eine entsprechende Übersicht mit Verweisen zu jeder einzelnen Überschrift zu erstellen
- Anwendungen: Erfahren Sie Näheres zu Anwendungen. Sie können ganz ohne Programmierkenntnisse eigene Anwendungen erstellen oder nach einer geeigneten Anwendung suchen, die von Community-Mitgliedern erstellt wurde. Unter **Beispiele** finden Sie Links zur englischsprachigen Anleitung einiger häufiger Anwendungen
- Inhaltsorganisation: Hier erfahren Sie, wie Sie die Seiten in Ihrem Wiki hierarchisch ordnen
- Versionskontrolle: Hier erfahren Sie, wie Sie frühere Seitenversionen anzeigen, vergleichen und zurücksetzen können
- Importieren/Exportieren: Hier erfahren Sie, wie Sie Seiteninhalte aus dem Modul exportieren oder in das Modul importieren können

![Die Hilfe-Seite und ihre einzelnen Punkte im Überblick](media/4f2725c3fec8617e4145f4fcdb104a12df57e089.png "Die Hilfe-Seite")
