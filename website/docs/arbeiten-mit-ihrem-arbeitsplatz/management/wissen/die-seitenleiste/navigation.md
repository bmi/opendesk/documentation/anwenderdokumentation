# Navigation

Im Abschnitt **Navigation** sehen Sie die Baumstruktur Ihrer Seiten und können per Mausklick zu jeder gewünschten Seite navigieren. Klicken Sie auf **Startseite**, um zur Startseite Ihres Wikis zu gelangen.

![Der Navigations-Bereich in der Seitenleiste](media/6f22d60b1d78e2f0bf03deb57400c9f9f7757c12.png "Der Navigations-Bereich")

Seiten, die Unterseiten enthalten, erkennen Sie an einem kleinen **Pfeilsymbol** links neben dem Seitentitel. Mit Klick auf den Pfeil können Sie diese Unterseiten aufklappen. Sie können auch auf den Titel der übergeordneten Seite klicken, um diese zu öffnen und gleichzeitig etwaige Unterseiten einzublenden.

![Der Navigations-Bereich mit aufgeklappten Unterseiten](media/2518914d844b849ce9736d43d308cca85f5288e9.png "Der Navigations-Bereich mit aufgeklappten Unterseiten")

Je nachdem, wie komplex Ihr Wiki ist, können Sie mehrere solcher Hierarchieebenen einrichten. Das heißt, dass Unterseiten wiederum eigene Unterseiten haben können usw.

## Einfluss der Navigation auf die Erstellung von Seiten

Beim Erstellen neuer Seiten sollten Sie darauf achten, welche Seite Sie gerade geöffnet haben. Wenn Sie sich auf der Startseite befinden und dann auf die Schaltfläche **Erstellen** klicken, wird Ihre neue Seite auf der höchsten Hierarchieebene erstellt. Befinden Sie sich hingegen auf einer von Ihnen erstellten Seite, so wird eine Unterseite dieser Seite erstellt.

**Beispiel:** Sie haben aktuell die Seite „Testseite 1“ geöffnet.

![Testseite 1 ist geöffnet](media/efdc16d0147fa41831499f14e4f0081dc2d11d33.png "Testseite 1")

Im Dialog **Seite erstellen** sehen Sie unter **Speicherort**, dass „Testseite 1“ als Speicherort Ihrer neuen Seite vorausgewählt ist. Die neue Seite wird also eine Unterseite von „Testseite 1“. Sie können diese Auswahl allerdings ändern, indem Sie rechts auf die Schaltfläche **Einen neuen Speicherort auswählen** klicken.

![Testseite 1 ist im Dialog &quot;Seite erstellen&quot; als Speicherort für die neue Unterseite vorausgewählt](media/a65abb589fddd4903e3c32617ac1ea00f89b6b8e.png "Unterseite erstellen")

Näheres zum Erstellen von Seiten erfahren Sie unter **Seiten – Seiten erstellen**.
