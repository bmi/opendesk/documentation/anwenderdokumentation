# Sandkasten

Der Sandkasten ist eine vorgefertigte Seite, die beispielhaft zeigt, welche Formatierungen Sie auf Seiten vornehmen können und welche Funktionen Ihnen zur Verfügung stehen. Sie können im Sandkasten sämtliche Funktionen ausprobieren, um sich mit dem Anlegen und Bearbeiten von Seiten vertraut zu machen.

Klicken Sie in der Seitenleiste links auf **Sandkasten**, um den Sandkasten zu öffnen.

![Die Seitenleiste mit den Überpunkten &quot;Anwendungen&quot; (einschließlich &quot;Sandkasten&quot;) und &quot;Navigation&quot;](media/665d50d22803d342762a45067f76b042186a71c0.png "Die Seitenleiste")

Folgen Sie nun einfach den Anweisungen auf der Sandkastenseite, um das Arbeiten mit Seiten zu üben.

![Der Sandkasten](media/a5d0d492135224ebee80b93aef3c3a06388ed659.png "Der Sandkasten")
