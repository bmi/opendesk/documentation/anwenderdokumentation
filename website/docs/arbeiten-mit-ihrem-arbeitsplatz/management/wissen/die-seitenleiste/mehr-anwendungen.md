# Mehr Anwendungen

Im Modul Wissen haben Sie die Möglichkeit, binnen weniger Minuten ohne Programmierkenntnisse Ihre eigenen kollaborativen Web-Anwendungen zu erstellen. Klicken Sie dafür zunächst im Seitenleisten-Abschnitt **Anwendungen** auf **Mehr Anwendungen** und dann auf **Erstellen Sie Ihre eigene!**.

![Die Option &quot;Mehr Anwendungen&quot; und der Befehl &quot;Erstellen Sie Ihre eigene!&quot; in der Seitenleiste](media/c5fe729bf9eb1c6955771dadad14da4a957e8a3d.png "Mehr Anwendungen")

Sie gelangen auf die Seite **Anwendung erstellen in wenigen Minuten**.

![Die Seite &quot;Anwendung erstellen in wenigen Minuten&quot;](media/897608e1e7fa954480524b7db399c7ff221052f3.png "Anwendung erstellen in wenigen Minuten")

## Anwendung erstellen

Um mit der Erstellung Ihrer ersten eigenen Anwendung zu beginnen, klicken Sie auf die Schaltfläche **Anwendung erstellen** . Danach müssen Sie als ersten Schritt einen Namen für die Anwendung eingeben.

Ein Assistent wird Sie durch die nächsten Schritte führen. Folgen Sie den Anweisungen auf der Seite.

![Die Namenseingabe für eine neue Anwendung sowie Anweisungen des Assistenten, der durch die weiteren Schritte führt](media/13450a819a0fe4414ca56304af8e6bd1fa948089.png "Anwendung erstellen")

## Liste eigener Anwendungen

Unten auf der Seite **Anwendung erstellen in wenigen Minuten** finden Sie den Abschnitt **Anwendungen**. Hier sind alle Anwendungen aufgelistet, die von Nutzerinnen und Nutzern selbst erstellt wurden. Klicken Sie auf die Überschriften **Anwendung** (Sortierung/Filterung nach Anwendungsname), **Letzer Autor** oder **Änderungsdatum**, um die Anwendungen nach diesen Kriterien zu **sortieren**. Wenn Sie wiederholt auf die jeweilige Überschrift klicken, können Sie zwischen auf- und absteigender Sortierung wechseln.

Unter den genannten Überschriften finden Sie jeweils ein Textfeld, in dem Sie einen passenden Suchbegriff eingeben können, um die Liste einzugrenzen. So können Sie beispielsweise nur Anwendungen anzeigen lassen, die an einem bestimmten Datum bearbeitet wurden. Schon während der Eingabe eines Suchbegriffs wird die Liste entsprechend gefiltert.

Unter der Überschrift **Aktionen** erhalten Sie Direktzugriff auf die Funktion **Bearbeiten**, um Änderungen an der gewünschten Anwendung vorzunehmen.
