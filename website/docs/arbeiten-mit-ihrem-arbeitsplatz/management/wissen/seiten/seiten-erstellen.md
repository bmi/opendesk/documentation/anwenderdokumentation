# Seiten erstellen

## Erstellen einer Seite

Es gibt mehrere Möglichkeiten eine Seite auf Wissen zu erstellen. Im Folgenden finden Sie alle verfügbaren Methoden.

- Verwendung der Schaltfläche **Erstellen**
- Verwendung von Links auf der Seite

## Verwendung der Schaltfläche Erstellen

 Klicken Sie zuerst auf der Startseite von Wissen das Symbol **Erstellen**:

![Erstellen button](media/22d520adc81545477e97ffba85a708fefd38f860.jpg "Erstellen button")

Daraufhin wird Ihnen dieser Bildschirm angezeigt:

![Seite erstellen](media/3665022b02ff7f0af849b721d685a5d6a645d715.jpg "Seite erstellen")

Hier können Sie einen Titel vergeben und den Typ des Dokuments auswählen den Sie erstellen möchten.

## Verwendung von Links auf der Seite

Links können als Verweise auf noch nicht existierende Seiten genutzt werden. Wenn sie zu diesem Zweck verwendet werden, erscheint der Link unterstrichen mit einem Fragezeichen rechts daneben.

Ein Klick auf einen solchen Link fordert Sie auf, einen Seitentyp auszuwählen. Nachdem Sie auf **Erstellen** geklickt haben, wird Ihnen die Seite im Bearbeitungsmodus angezeigt. Die neue Seite wird erstellt, wenn Sie auf **Speichern &amp; Anzeigen** klicken. Diese Methode ist besonders praktisch, da sie gleichzeitig zur Erstellung neuer Inhalte auch die Struktur Ihres Wikis aufbaut.
