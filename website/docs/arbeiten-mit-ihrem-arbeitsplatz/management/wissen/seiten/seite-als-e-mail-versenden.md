# Seite als E-Mail versenden

Sie können Seiten per E-Mail mit anderen Nutzerinnen und Nutzern teilen. Öffnen Sie dazu die gewünschte Seite und klicken Sie auf die Schaltfläche **Weitere Aktionen (drei Punkte)**. Wählen Sie dann den Menüpunkt **Als E-Mail versenden**.

![Der Punkt &quot;Als E-Mail versenden&quot; im Kontextmenü](media/fc73e9dac9a517a4b36dc684470a6e8a66eda80a.png "Als E-Mail versenden")

Das Fenster **Diese Seite versenden** öffnet sich.

Klicken Sie zunächst in das Textfeld unter **Senden an**, um auszuwählen mit wem Sie die Seite teilen möchten. Daraufhin wird eine Liste eingeblendet, in der Sie die gewünschten Personen auswählen können. Sie können auch einen Namen eingeben, um gezielt nach bestimmten Personen zu suchen oder von Hand die E-Mail-Adresse einer Empfängerin bzw. eines Empfängers eingeben. Wiederholen Sie den Vorgang, um mehrere Empfängerinnen und Empfänger auszuwählen.

Wenn Sie selbst eine Kopie der E-Mail erhalten möchten, setzen Sie einen Haken bei **Sende mir eine Kopie**.

Unter **Füge das aktuelle Dokument ein** können Sie auswählen, ob die Empfängerinnen und Empfänger lediglich einen Link zur Seite erhalten sollen (**Nur als Link**) oder die gesamte Seite in der E-Mail enthalten sein soll (**Im Volltext der Nachricht**).

Unter **Die folgende Nachricht wird gesendet werden** finden Sie ein Textfeld, in dem Sie eine persönliche Nachricht eingeben können. Erklären Sie hier z. B. den Empfängerinnen und Empfänger, warum die Seite für sie relevant ist.

Klicken Sie abschließend auf **Senden**.

![Das Fenster &quot;Diese Seite versenden&quot;](media/dee183338ffa3e41a4a6c48b92db094481fe91c6.png "Diese Seite versenden")
