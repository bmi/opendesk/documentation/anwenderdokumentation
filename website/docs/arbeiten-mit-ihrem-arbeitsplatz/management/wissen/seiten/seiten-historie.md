# Seiten-Historie

Jedes Mal, wenn Sie eine Seite in Wissen ändern, wird diese Änderung in der Versionshistorie der Seite gespeichert. Sie können zwei Versionen einer Seite vergleichen, um zu sehen, was sich zwischen ihnen verändert hat. Es geht nie Inhalt verloren — alles, was Sie im Laufe der Zeit zu Ihrem Wiki hinzufügen, wird gesichert. Wenn sich eine kürzliche Änderung als falsch herausstellt, können Sie problemlos zu einer früheren Version der Seite zurückkehren.

![Versionen](media/c34aab4d5ced6cdaf44029170013779388d19980.jpg "Versionen")

Um zwei Versionen einer Seite zu vergleichen, suchen Sie nach dem Tab **Historie** am unteren Rand der Seite. Sie finden auch den Menüpunkt **Historie**, indem Sie oben auf einer Seite auf den Button **Weitere Aktionen** klicken.

Um zwei beliebige Versionen einer Seite zu vergleichen, wählen Sie diese beiden Versionen aus und klicken Sie auf den Button **Ausgewählte Versionen vergleichen**, der sich am unteren Rand der Seite befindet. Eine Seite wird Ihnen die Unterschiede im Inhalt, den Anhängen und Kommentaren zwischen den beiden Versionen der Seite zeigen. Klicken Sie auf den **Zurück-Button** Ihres Browsers, um dorthin zurückzukehren, wo Sie waren. Um den Inhalt einer früheren Version der Seite anzusehen, klicken Sie in der Spalte **Version** auf die Nummer der Seitenversion. Um zu einer früheren Version der Seite zurückzukehren, klicken Sie auf den Button **Zurücksetzen**.
