# Seiten bearbeiten

Wissen ist ein Wiki, wodurch der Inhalt seiner Seiten leicht zu bearbeiten und zu modifizieren ist.

![Bearbeiten-Botton](media/5809f0d9b44d2746e6d988aa024c4f18aedb7498.jpg "Bearbeiten-Botton")

- Klicken Sie auf den **Bearbeiten-Button**.
- Sie können den Titel und den Inhalt der Wiki-Seite bearbeiten, während Sie im Ansichtsmodus bleiben, mit dem zusätzlichen Vorteil, dass Sie den Kontext nicht verlieren. Die Sektionsbearbeitung hilft Ihnen, einen Abschnitt zu bearbeiten, ohne die Seite scrollen zu müssen.
- Wenn Sie mit Ihren Änderungen fertig sind, können Sie unter **Zusammenfassung hinzufügen** eine Notiz eingeben, die erklärt, was Sie geändert haben. Wählen Sie dann einen der folgenden Button: **Speichern &amp; Weiterarbeiten**, **Speichern &amp; Anzeigen** oder **Vorschau.**
- Nachdem Sie die Seite gespeichert haben, ist die neue Version mit Ihrer Bearbeitung sofort online.

## Die Bedeutung von Links

Eine der Hauptstärken eines Wikis ist, dass Sie leicht Links zwischen Seiten erstellen können. Die einfache Linkerstellung ermöglicht es Ihnen, eine Struktur in Ihrem Wiki zu erstellen und Seiten zu verweisen. Links machen Inhalte einfacher durchsuch- und auffindbar. Deshalb ist es wichtig, Links zwischen Ihren Seiten zu erstellen. Um neue interne Wiki-Links beim Bearbeiten einer Seite zu erstellen, führen Sie folgende Schritte durch:

- Im Rich-Text-Bearbeitungsmodus (WYSIWYG) markieren Sie das Wort oder die Wörter, die Sie verlinken möchten. 
- Klicken Sie auf den **Link-Button**, dann auf den **Seite-Button** und wählen Sie die Seite aus dem Baum aus, auf die Sie verlinken möchten.
- Anschließend klicken Sie auf **Auswählen**, dann auf **OK**.