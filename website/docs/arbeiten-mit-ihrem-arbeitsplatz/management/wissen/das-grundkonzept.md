# Das Grundkonzept

Wissen unterteilt sich in Seiten und Wikis. Die Seitenmenüs werden über Panels verwaltet.

## Seite

In Wissen bildet eine Seite die Grundeinheit für Inhalte. Seiten sind in folgende Bereiche unterteilt:

- Aktionsleiste (im oberen Bereich): ermöglicht die Interaktion mit der aktuellen Seite
- Titel (wie die Seite benannt ist)
- Autor und Datum (welche Benutzerin oder welcher Benutzer hat die Seite zuletzt geändert und an welchem Datum)
- Inhaltsbereich
- Panels (entweder links oder rechts auf der Seite, auf beiden Seiten oder versteckt)
- Fußzeilenbereich: dort finden Sie Ersteller und Datum (wer war die Erstellerin oder der Ersteller der Seite und zu welchem Datum) sowie Tags (einige Kategorien, in die die Seite passt)

Im Registerkartenbereich am unteren Rand finden Sie:

- Kommentare und Anmerkungen (was andere über die Seite zu sagen haben),
- Anhänge (Dateien, die auf die Seite hochgeladen wurden)
- Historie (jede Version der aktuellen Seite)
- Informationen (enthaltene Seiten, Backlinks)

## Aktionen auf einer Seite

Nutzerinnen und Nutzer können folgende Aktionen durchführen.

**Bearbeiten der Seite:**

- Mit dem WYSIWYG-Editor: Ein Rich-Text-Editor, mit dem Sie den Inhalt Ihrer Wiki-Seite leicht ändern können.
- Mit dem Wiki-Editor: für Nutzerinnen und Nutzer, die es gewohnt sind, in Auszeichnungssyntax zu schreiben

**Drucken und Exportieren der Seite über das Menü Weitere Aktionen:**

- Lassen Sie sich eine Druckvorschau der aktuellen Seite anzeigen

Jede Seite kann in den folgenden Formaten exportiert werden: HTML, PDF, ODT und RTF (für MS Office)

**Die Seite ansehen:**

- Beobachten Sie die aktuelle Seite, um über Änderungen informiert zu werden. Klicken Sie dazu auf das Herz "Gefällt mir" am Ende des Haupttextes. So erhalten Sie Benachrichtigungen per E-Mail bei Änderungen.

**Weitere mögliche Aktionen:**

- Unterseiten erstellen
- Zugriffsrechte für die Seite ändern
- Bearbeiten von Objekten und Klassen der Seite
- Umbenennen, Kopieren und Löschen der Seite

## Wiki

Zunächst befinden Sie sich auf dem Hauptwiki. Es ist jedoch möglich, weitere Wikis (auch Subwikis genannt) zu erstellen. Ein Wiki ist eine Sammlung von Wikiseiten.

Dies kann nützlich sein, wenn Sie eine saubere Trennung zwischen verschiedenen Arten von Inhalten wünschen. Es gibt keine Beschränkungen der Anzahl der Wikis. In jedem Wiki können außerdem so viele Seiten wie sie benötigen erstellt werden. In der Regel beginnen Sie mit einem einzigen Wiki. Wenn der Bedarf oder die Inhalte entsprechend stark wachsen, können Sie die Inhalte in mehrere Wikis gliedern.

## Panels

Ein Panel ist ein Widget, das Sie auf jeder Seite Ihres Wikis sehen können. Panels werden innerhalb von Spalten angezeigt. Standardmäßig sehen Sie auf der linken Seite die Bereiche Anwendungen und Navigation und auf der rechten Seite einige weitere Bereiche.

Sie können die Bereiche auf verschiedene Weise steuern:

- Wählen Sie, welche Spalten angezeigt werden sollen (rechte Spalte, linke Spalte, beide Spalten, keine) 
- Wählen Sie die Breite der anzuzeigenden Spalten (klein, mittel, groß) 
- Wählen Sie, welche Felder Sie in Ihre Seitenspalten einfügen möchten

Panels sind nützlich, um Menüs zu erstellen oder Funktionen zu Ihren Seiten hinzuzufügen. Sie können für jeden Bereich Ihres Wikis verschiedene Bereiche festlegen.
