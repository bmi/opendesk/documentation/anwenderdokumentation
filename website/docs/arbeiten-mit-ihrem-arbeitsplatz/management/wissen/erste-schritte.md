# Einstieg

## Erste Schritte

Willkommen in der Einführung zum Modul **Wissen**. Dieser Leitfaden bietet grundlegende Informationen zur Einrichtung und Nutzung von Wissen.

Das Modul **Wissen** kann für viele verschiedene Zwecke verwendet werden: als kollaboratives Intranet, Wissensdatenbank, Content-Management-System (CMS), Instrument zur Wettbewerbsanalyse, Plattform für öffentliche Diskussionen, gemeinsame Erstellung von Schulungsmaterialien, Erstellung von Webseiten und vieles mehr.

##  Grundlegende Informationen

Wenn Sie sich in Ihr Wiki einloggen, sehen Sie als Erstes die Startseite des Wikis.

1. Wissen ist in Wikis und Seiten unterteilt. Innerhalb eines Wikis können Sie eine Hierarchie von Seiten erstellen, indem Sie Seiten in anderen Seiten erstellen. Diese Funktion wird als verschachtelte Seiten bezeichnet. In diesem Bereich können Sie neue Seiten hinzufügen, indem Sie auf die Schaltfläche oben rechts **Erstellen** klicken.
1. Zum Ändern einer Seite klicken Sie auf die Schaltfläche **Bearbeiten** oben rechts. Nehmen Sie Ihre Änderungen vor und speichern Sie die Seite. Die geänderte Seite ersetzt nun die vorherige Version. Jede Version der Seite wird in der Seitenhistorie gespeichert und kann bei Bedarf wiederhergestellt werden. Weitere Seitenaktionen, wie Verwalten, Kopieren, Umbenennen, Löschen, Exportieren, sind auch über das Menü auf der rechten Seite verfügbar.
1. In der oberen rechten Ecke erreichen Sie Ihr **Profil** oder das **Benachrichtigungsmenü**.
1. Wenn Sie das **Menü** in der rechten Leiste ausklappen, können Sie sich **einloggen, registrieren**, Ihr **Profil aufrufen** oder zur **Wiki-Verwaltung** in **Home** und **Global** gehen. Sie können auch das Wiki, **Dokumentenindex**, **Benutzerverzeichnis** oder **Anwendungen** aufrufen sowie das Wiki Verzeichnis einsehen.
1. Sie können den Inhalt der **Startseite** leicht bearbeiten/ändern, um diesen durch Ihren eigenen Inhalt zu ersetzen.
1. Seitliche Menüs werden mit Panels gehandhabt. Ein Panel ist ein Widget, das Sie links und/oder rechts auf jeder Seite Ihres Wikis in der Standardversion sehen können. Standardmäßig sehen Sie links die Panels **Anwendungen** und **Navigation.** Auf der rechten Seite finden Sie **Tipps** und **Benötigen Sie Hilfe?** und ebenso **Meine letzten Änderungen.**
1. Der Registerkartenbereich unten auf der Seite bietet standardmäßig **Kommentare**, **Anhänge**, **Seitenhistorie** und **Seiteninformationen.**

![Startseite](media/bd73b0a303db7d9f47b1cfd769fce67c4ab9403e.jpg "Startseite")
