# Das Benutzerverzeichnis

Im Benutzerverzeichnis finden Sie alle Personen, die aktuell für die Nutzung Ihres Wikis registriert sind. Das Sortieren und Filtern der Liste funktioniert genauso wie beim Dokumentenindex (siehe entsprechende Anleitung). Als Kriterien stehen Ihnen hier **Benutzer-ID**, **Vorname** und **Nachname** zur Verfügung.

![Eingabe eines Suchbegriffes im Benutzerverzeichnis](media/214327194baa1b3931121ccd859fe60afd013598.png "Das Benutzerverzeichnis")
