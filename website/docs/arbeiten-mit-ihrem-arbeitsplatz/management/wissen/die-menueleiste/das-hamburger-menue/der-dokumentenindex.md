# Der Dokumentenindex

Im Dokumentenindex sind alle Seiten aufgelistet, die aktuell im Wiki vorhanden sind, einschließlich der Profilseiten von Nutzerinnen und Nutzern.

## Dokumente sortieren und filtern

Klicken Sie auf die Überschriften **Titel**, **Speicherort**, **Datum** oder **Letzer Autor**, um die Seiten nach diesen Kriterien zu **sortieren**. Wenn Sie wiederholt auf die jeweilige Überschrift klicken, können Sie zwischen auf- und absteigender Sortierung wechseln.

Unter den genannten Überschriften finden Sie jeweils ein Textfeld, in dem Sie einen passenden Suchbegriff eingeben können, um die Liste einzugrenzen. So können Sie beispielsweise nur Seiten anzeigen lassen, die einen bestimmten Begriff im Titel haben oder an einem bestimmten Datum bearbeitet wurden. Schon während der Eingabe eines Suchbegriffs wird die Liste entsprechend gefiltert.

![Eingabe eines Suchbegriffes im Dokumentenindex](media/3026469b819c24cd6eb4b3646328ec257af53eee.png "Der Dokumentenindex")

## Aktionen und Likes

Unter der Überschrift **Aktionen** erhalten Sie mittels entsprechender Schaltfläche Direktzugriff auf nützliche Funktionen. Sie können direkt vom Dokumentenindex aus Seiten kopieren, umbenennen oder löschen. Seiten, die Sie nicht selbst erstellt haben, können Sie nur kopieren.

Unter **Likes** sehen Sie, wie vielen Personen die jeweilige Seite gefällt.

## Baumstruktur einsehen

Um die Baumstruktur des gesamten Wikis einzusehen, navigieren Sie oben zum Reiter **Baum**. Mit dem kleinen Pfeil neben den einzelnen Seitentiteln können Sie untergeordnete Seiten einblenden.

![Die Baumstruktur im Dokumentenindex](media/2f3a8dbee2c8671d3d83af21a085a42999ef35b3.png "Die Baumstruktur im Dokumentenindex")

## Anhänge sortieren und filtern

Wählen Sie den Reiter **Anhänge** aus, um eine Liste aller Dateianhänge im Wiki zu erhalten. Auch diese Liste können Sie sortieren und filtern. Als Kriterien stehen Ihnen hier **Typ**, **Dateiname**, **Speicherort**, **Dateigröße**, **Datum** und **Autor** zur Verfügung.

![Anhänge im Dokumentenindex (Tabellenlayout)](media/35d7e530732ade93fb31e9d10395dcf6df2256ef.png "Anhänge im Dokumentenindex \(Tabellenlayout\)")

## Weitere Optionen für Anhänge

Mit der Schaltfläche **Mehr Aktionen (drei Punkte)** erhalten Sie zusätzliche Optionen für Anhänge:

- Aktionen: Hier können Sie die Liste aktualisieren
- Layouts: Das übersichtliche Layout **Tabelle** ist vorausgewählt. Wenn Sie zum Layout **Karte** wechseln, werden die Angaben zu den einzelnen Anhängen in Form kompakter Karteikarten angezeigt
- Panels: Unter **Eigenschaften** können Sie einzelne der Sortierungs-/Filterkriterien abwählen, indem Sie die Haken bei den Begriffen entfernen. Unter **Sortieren** und **Filtern** haben Sie die Möglichkeit, die Liste noch genauer zu sortieren oder einzugrenzen, indem Sie die Sortierung bzw. Filterung für mehrere Kriterien hintereinander einstellen.

![Anhänge im Dokumentenindex (Kartenlayout)](media/4d3c8f8595d6dc2d1598700f5f783bdbe8524ac1.png "Anhänge im Dokumentenindex \(Kartenlayout\)")
