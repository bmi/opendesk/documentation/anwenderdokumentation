# Das Wiki-Verzeichnis

Wählen Sie im Hamburger-Menü den Punkt **Wiki-Verzeichnis**, um auf die Seite **Wikis durchsuchen** zu gelangen. Das Sortieren und Filtern der Liste funktioniert genauso wie beim Dokumentenindex (siehe entsprechende Anleitung). Als Kriterien stehen Ihnen hier **Name**, **Beschreibung**, **Besitzer** und **Erstelldatum** zur Verfügung. Mit der Schaltfläche **Mehr Aktionen (drei Punkte)** erhalten Sie zusätzliche Optionen, wie in der Anleitung zum Dokumentenindex unter **Weitere Optionen für Anhänge** beschrieben.

![Das Wikiverzeichnis (Wikis durchsuchen)](media/fc2b138e60960d64207b42a5fad637e54f2d75f8.png "Das Wikiverzeichnis")
