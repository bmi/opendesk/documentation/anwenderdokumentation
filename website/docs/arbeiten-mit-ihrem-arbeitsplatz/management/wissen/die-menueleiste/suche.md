# Suche

Mithilfe der Suchfunktion können Sie schnell gewünschte Inhalte finden. Klicken Sie dafür zunächst auf die Schaltfläche **Suche**.

![Die Schaltfläche &quot;Suche&quot;](media/e3452b0eddf301b673d9e4e43aa2e169fac199e2.png "Die Schaltfläche &quot;Suche&quot;")

Ein Textfeld erscheint. Geben Sie einen Suchbegriff ein. Dies kann der Titel einer Seite, ein Stichwort aus dem Seiteninhalt oder auch der Name einer Nutzerin oder eines Nutzers sein. Etwa wenn Sie nach Seiten suchen, die von bestimmten Personen bearbeitet wurden. Schon während der Eingabe werden Ihnen Vorschläge angezeigt.

![Eingabe eines Suchbegriffs und Ergebnisvorschläge](media/875ea993f37efa61ff2c80d51301f24dab5c726e.png "Suchvorschläge")

Wenn Sie unter den Vorschlägen bereits den gesuchten Inhalt sehen, können Sie ihn direkt per Mausklick öffnen. Sie können auch mit den Pfeiltasten auf Ihrer Tastatur zum gewünschten Inhalt navigieren und ihn mit der Eingabetaste auswählen.

Je nachdem, wie viele Treffer Sie erhalten, kann es hilfreicher sein, sich alle Suchergebnisse auf einer neuen Seite anzeigen zu lassen. Dort können Sie die Suche bei Bedarf auch noch weiter eingrenzen. Klicken Sie dazu nach Eingabe Ihres Suchbegriffes auf **Alle Ergebnisse zeigen** oder drücken Sie die **Eingabetaste**.

![Detaillierte Suchergebnisse mit Werkzeugen zur Anzeige, Sortierung und Eingrenzung](media/5dacc611fe54fb5da5e5f91df0a0342ddef65c04.png "Detaillierte Suchergebnisse")

In den detaillierten Suchergebnissen stehen Ihnen verschiedene Optionen zur Anzeige, Sortierung und Eingrenzung Ihrer Suche zur Verfügung.

## Anzeigen

- Normalerweise wird in der Liste mit Suchergebnissen jeweils eine Vorschau angezeigt, in der Ihr Suchbegriff hervorgehoben wird. Klicken Sie auf **Hervorheben**, um diese Vorschau auszuschalten. Nun werden lediglich Titel und Speicherort der Seite sowie die letzte Person angezeigt, von der die Seite bearbeitet wurde
- Klicken Sie auf **Facetten**, um zur besseren Übersicht den Bereich **Suche verfeinern** auf der rechten Seite auszublenden. Näheres zu diesem Bereich finden Sie weiter unten
- Durch erneuten Klick auf **Hervorheben** bzw. **Facetten** können Sie die Einstellung wieder rückgängig machen

![Suchergebnisse ohne Vorschau (Hervorheben und Facetten ausgeschaltet)](media/298995aa30b32ed0d2f6b0ec339adf2d86e80f18.png "Suchergebnisse ohne Vorschau")

## Sortierung

Wählen Sie hier aus, wie Ihre Suchergebnisse sortiert werden sollen. Folgende Sortierungskriterien stehen zur Auswahl:

- Relevanz
- Titel
- Letztes Bearbeitungsdatum
- Letzter Autor, d. h. die letzte Person, von der die Seite zuletzt bearbeitet wurde  

Klicken Sie wiederholt auf die jeweiligen Kriterien, um zwischen auf- und absteigender Sortierung umzuschalten.

## Suche verfeinern

Wenn die Liste mit Suchergebnissen noch zu lang und unübersichtlich ist, können Sie Ihre Suche hier weiter eingrenzen. Dazu stehen Ihnen folgende Kriterien zur Auswahl, die Sie mit einem Klick auf den kleinen Pfeil neben dem jeweiligen Kriterium aufklappen können:

- Ergebnistyp: In aller Regel werden Sie dieses Kriterium nicht benötigen, da hier alle Seiten als Dokumente aufgeführt werden
- Speicherort: Grenzen Sie Ihre Suche auf Seiten ein, die sich in einem bestimmten Bereich der Baumstruktur befinden, beispielsweise Unterseiten von „Testseite 1“
- Sprache: Grenzen Sie Ihre Suche auf Seiten in einer bestimmten Sprache ein, falls in Ihrem Wiki mehrere Sprachen verwendet werden
- Letzter Autor: Grenzen Sie Ihre Suche auf Seiten ein, die von bestimmten Nutzerinnen und Nutzern bearbeitet wurden
- Ersteller: Grenzen Sie Ihre Suche auf Seiten ein, die von bestimmten Nutzerinnen und Nutzern erstellt wurden
- Letztes Bearbeitungsdatum: Grenzen Sie Ihre Suche auf Seiten ein, die in einem bestimmten Zeitraum bearbeitet wurden. Sie haben dabei die Wahl zwischen **Heute**, **Letzte 7 Tage**, **Letzte 30 Tage** sowie **Frei angebbarer Zeitraum**. 	

Wenn Sie die letzte Option auswählen, klicken Sie in die Felder **Startdatum mit Zeit** und **Enddatum mit Zeit**. Ein Kalender öffnet sich, in dem Sie das gewünschte Datum auswählen können. Klicken Sie unten im Kalender auf die Schaltfläche **Select Time (Uhrsymbol)**, um eine Uhrzeit hinzuzufügen. Bestätigen Sie Ihre Auswahl mit einem Klick auf **Anwenden**.

- Erstelldatum: Grenzen Sie Ihre Suche auf Seiten ein, die in einem bestimmten Zeitraum erstellt wurden. Die Auswahlmöglichkeiten sind die gleichen wie unter **Letztes Bearbeitungsdatum**. 

Sie können mehrere Kriterien gleichzeitig auswählen, um Ihre Suche möglichst genau einzugrenzen. Klicken Sie auf **Zurücksetzen**, um alle ausgewählten Kriterien auszuschalten. Wenn Sie sämtliche Optionen auf einmal einsehen möchten, klicken Sie auf **Alles aufklappen**.

![Der Bereich &quot;Suche verfeinern&quot; samt Kalenderansicht für einen frei angebbaren Zeitraum](media/224e83efcf5c9745ab8f06a6073bf41c06c18f3a.png "Suche verfeinern")

Wenn Sie die gewünschte Seite gefunden haben, öffnen Sie diese mit einem Klick auf den Seitentitel.
