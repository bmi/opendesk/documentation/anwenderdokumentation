# Das Hamburger-Menü

Die Schaltfläche **Menüleiste (Hamburger-Menü)** ganz rechts in der Menüleiste bietet schnellen Zugriff auf einige nützliche Funktionen.

![Die Schaltfläche &quot;Menüleiste&quot; (Hamburger-Menü)](media/b66c05faefef9c1ec8cbc6dad75fd397aed78d71.png "Hamburger-Menü")

Öffnen Sie das Menü mit einem Mausklick.

![Das geöffnete Hamburger-Menü](media/5cec049042cf9abbbad7266a67101353f14892eb.png "Das geöffnete Hamburger-Menü")

## Funktionsübersicht

Folgende Funktionen stehen im oberen Bereich des Menüs zur Verfügung:

- Klicken Sie auf Ihr Profilbild oder Ihren Benutzernamen, um in Ihr Profil zu gelangen
- Abmelden: Melden Sie sich aus der Arbeitsumgebung ab  

Folgende Funktionen stehen im Abschnitt **Home** zur Verfügung:

- Dokumentenindex: Zeigt eine durchsuchbare Liste aller Dokumente an, die aktuell in Ihrem Wiki enthalten sind
- Benutzerverzeichnis: Zeigt eine durchsuchbare Liste aller Personen an, die aktuell für die Nutzung des Wikis registriert sind
- Anwendungen: Zeigt eine Liste aller Anwendungen an, die aktuell für Ihr Wiki installiert sind  

Folgende Funktionen stehen im Abschnitt **Global** zur verfügung:

- Wiki-Verzeichnis: Zeigt eine durchsuchbare Liste aller verfügbaren Wikis an
- Was gibt's Neues: Zeigt Informationen zu aktuellen Softwareversionen und damit verbundenen Änderungen an  

Nähere Angaben zu Dokumentenindex, Benutzer- und Wiki-Verzeichnis finden Sie auf den entsprechenden Unterseiten dieser Anleitung.

### Hier geht es weiter mit:

- [Der Dokumentenindex](das-hamburger-menue/der-dokumentenindex.md)
- [Das Benutzerverzeichnis](das-hamburger-menue/das-benutzerverzeichnis.md)
- [Das Wiki-Verzeichnis](das-hamburger-menue/das-wiki-verzeichnis.md)
