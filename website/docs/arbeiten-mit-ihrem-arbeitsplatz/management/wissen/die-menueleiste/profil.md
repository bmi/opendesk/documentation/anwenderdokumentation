# Profil

Um Ihr Profil einzusehen und zu bearbeiten, klicken Sie in der Menüleiste auf die Schaltfläche mit Ihrem Profilbild. Falls Sie noch kein Profilbild ausgewählt haben, befindet sich hier ein Platzhalterbild. Als Alternativtext für die Schaltfläche ist Ihr Name hinterlegt.

![Die Schaltfläche für das Profil, hier mit Platzhalterbild](media/c117dd25af166556c0b3e4969f9ca3d38eb06806.png "Profilschaltfläche")

## Übersicht

Ihre Profilansicht besteht aus drei Teilen:

- Links können Sie Ihr **Profilbild** ändern und auf verschiedene Einstellungen zugreifen
- In der Mitte können Sie Ihre Profilinformationen einsehen und ändern, aufgeteilt in **Persönliche Informationen**, **Kontaktinformationen** und **Externe Links**
- Rechts sehen Sie Ihre letzten **Aktivitäten**, beispielsweise Seiten, die Sie bearbeitet oder erstellt haben

![Die Profilansicht im Überblick](media/f8f639b0515d409acdce61a3d8d690976476643f.png "Die Profilansicht im Überblick")

## Profilbild hinzufügen, ändern oder entfernen

Oben links im Profil haben Sie die Möglichkeit, ein Profilbild hinzuzufügen oder ein vorhandenes Profilbild auszutauschen. Klicken Sie dazu auf die Schaltfläche **Foto ändern** oben rechts im vorhandenen Bild. Sie gelangen in ein neues Fenster.

![Fenster, in dem das Profilbild geändert werden kann](media/3fb93e61d87b2bde5a6c00c980f991f77f296232.png "Profilbild ändern")

Im rechten Fensterbereich sehen Sie eine Auswahl an bereits vorhandenen Bildern. Um ein Bild auszuwählen, klicken Sie auf die Schaltfläche **Auswählen (grüner Haken)** über dem jeweiligen Bild. Um das Bild größer anzuzeigen, klicken Sie auf das Bild selbst. Mit der Schaltfläche **Entfernen (rotes X)** können Sie Bilder löschen. Das Platzhalterbild **Standard** steht jederzeit zur Verfügung und kann nicht gelöscht werden.

Um ein neues Bild hochzuladen, klicken Sie im linken Fensterbereich auf **Datei auswählen**. Der Dateibrowser öffnet sich. Navigieren Sie zum Ordner, in dem Ihr Bild gespeichert ist. Wählen Sie das gewünschte Bild aus und klicken Sie auf **Öffnen**. Sie können das Bild auch mit einem Doppelklick direkt öffnen. Abschließend müssen Sie noch auf **Hochladen und auswählen** klicken.

## Profilinformationen einsehen und bearbeiten

Hier können Sie die bisher gespeicherten Informationen in Ihrem Profil einsehen. Diese werden anderen Nutzerinnen und Nutzern angezeigt. Wenn Sie Angaben hinzufügen oder ändern möchten, klicken Sie oben rechts auf die Schaltfläche **Profil bearbeiten (Stift-Symbol)**.

- **Persönliche Informationen:** Geben Sie Ihren Namen und Ihre Firma an. Je nach Konfiguration können Sie Ihren Namen möglicherweise nicht ändern. Hier können Sie Ihr Profil auch durch einen Kommentar ergänzen, der sich mithilfe der Werkzeugleiste formatieren lässt
- **Kontaktinformationen:** Geben Sie Ihre E-Mail-Adresse, Telefonnummer und Postadresse an. Auch die Adresse lässt sich mithilfe der Werkzeugleiste formatieren
- **Externe Links:** Wenn Sie einen persönlichen Blog haben (ggf. mit Blog-Feed), können Sie hier entsprechende Links hinzufügen

![Ein Ausschnitt der Bearbeitungsansicht des Profils](media/06cea059b72e5cf05c3ef46cae886b33c23b49ef.png "Profil bearbeiten")

## Speichern und Vorschau

Wenn Sie alle gewünschten Angaben gemacht haben, stehen Ihnen unten mehrere Optionen zur Verfügung:

- **Speichern &amp; Ansehen:** Speichern Sie Ihre Angaben und kehren Sie zur Profilansicht zurück
- **Speichern:** Speichern Sie Ihre Angaben, ohne die Bearbeitung zu verlassen, z. B. wenn Sie vor der Fertigstellung Ihre Angaben zwischenspeichern möchten
- **Vorschau:** Sehen Sie sich Ihr bearbeitetes Profil an, so wie es anderen Nutzerinnen und Nutzern angezeigt werden würde. So können Sie vor dem Speichern Ihre Angaben noch einmal übersichtlich überprüfen. Von der Vorschau aus stehen Ihnen wiederum die Optionen **Speichern &amp; Ansehen**, **Speichern** sowie **Abbrechen** zur Verfügung. Wenn Sie Ihre Änderungen noch nachbearbeiten möchten, klicken Sie auf **Zurück zum Bearbeiten**
- **Abbrechen:** Damit wird die Bearbeitung abgebrochen und Sie kehren zurück zur Profilansicht, ohne dass Ihre Änderungen gespeichert werden
- **Zusammenfassung hinzufügen:** Beschreiben Sie optional, welche Änderungen Sie vorgenommen haben, um diese später nachvollziehen zu können oder anderen Nutzerinnen und Nutzern mitzuteilen. Kommentare, die Sie hier eingeben, tauchen später in der Historie Ihrer Profilseite auf. Näheres dazu finden Sie unter **Seiten – Seiten-Historie**
- **Kleine Änderungen:** Setzen Sie hier einen Haken, wenn Sie nur geringfügige Änderungen vorgenommen haben, für die keine Zusammenfassung erforderlich ist. Für Ihre Profilseite werden Sie die letzten beiden Funktionen in aller Regel jedoch nicht benötigen
