# Benachrichtigungen

Mithilfe von Benachrichtigungen bleiben Sie über relevante Inhalte und Aktivitäten auf dem Laufenden. Die meisten Benachrichtigungen beziehen sich auf Änderungen an Seiten, die Sie beobachten oder selbst erstellt haben.

## Benachrichtigungen in der Menüleiste

Die Schaltfläche **Benachrichtigungen (Glockensymbol)** in der Menüleiste zeigt mit einer Zahl an, wie viele neue Benachrichtigungen Sie haben. Wenn Sie dort keine Zahl sehen, haben Sie aktuell keine neuen Benachrichtigungen.

![Benachrichtigungen in der Menüleiste](media/816bef654c6f8f99f97966255eab98a3b654209d.png "Benachrichtigungen in der Menüleiste")

Mit Klick auf die Schaltfläche öffnet sich ein Kontextmenü mit den folgenden Optionen:

- **RSS:** Falls Sie RSS-Feeds abonniert haben, können Sie diese hier abrufen
- **Einstellungen:** Stellen Sie ein, wie häufig und auf welchen Kanälen Sie Benachrichtigungen erhalten und über welche Art von Aktivität Sie benachrichtigt werden möchten
- **Diese Seite abonnieren:** Wählen Sie aus, ob Sie die aktuell geöffnete Seite abonnieren möchten. Für Seiten, die Sie selbst erstellt haben, ist diese Option standardmäßig eingeschaltet. Sie erhalten Benachrichtigungen über Änderungen an abonnierten Seiten, sofern Sie solche Benachrichtigungen nicht in den Einstellungen ausgeschaltet haben
- **Diese Seite mitsamt Unterseiten abonnieren:** Wählen Sie aus, ob Sie neben der geöffneten Seite auch etwaige Unterseiten abonnieren möchten. Standardmäßig ist diese Option ausgeschaltet
- **Dieses Wiki abonnieren:** Wählen Sie aus, ob Sie das gesamte Wiki abonnieren möchten. In dem Fall erhalten Sie Benachrichtigungen über Änderungen an sämtlichen Seiten im Wiki, sofern Sie solche Benachrichtigungen nicht in den Einstellungen ausgeschaltet haben. Standardmäßig ist diese Option ausgeschaltet
- **Alle löschen:** Entfernen Sie alle Benachrichtigungen, die weiter unten aufgelistet sind. 

Falls es aktuelle Benachrichtigungen gibt, werden diese weiter unten im Kontextmenü aufgelistet. In dieser Liste sehen Sie die zugehörigen Seitentitel, die Bearbeiterin bzw. den Bearbeiter sowie eine ungefähre Angabe dazu, wann die Änderung vorgenommen wurde. Sie können die Seiten, auf die sich einzelne Benachrichtigungen beziehen, mit einem Mausklick auf den Seitentitel öffnen. Außerdem können Sie auf den Namen der Bearbeiterin bzw. des Bearbeiters klicken, um ihr bzw. sein Benutzerprofil zu öffnen. Dort können Sie alle weiteren letzten Aktivitäten der Person einsehen.

Für jede Benachrichtigung stehen Ihnen auf der rechten Seite 2 Schaltflächen zur Verfügung:

- Ereignis als gelesen markieren (Hakensymbol): Hiermit geben Sie an, dass Sie die Benachrichtigung zur Kenntnis genommen haben. Sie wird daraufhin nicht mehr als neue Benachrichtigung gewertet. Die Zahl bei der Schaltfläche **Benachrichtigungen (Glockensymbol)** reduziert sich entsprechend
- Zeige Ereignis-Details (drei Punkte): Hiermit blenden Sie Informationen dazu ein, um was für ein Ereignis es sich handelt (ob z. B. eine Seite bearbeitet, erstellt oder kommentiert wurde) und wann dies erfolgt ist. Wenn Änderungen an einer vorhandenen Seite vorgenommen wurden, können hier auch mehrere Zeitpunkte auftauchen. Klicken Sie auf einen der Zeitpunkte, um zu einer Übersicht über die Änderungen und Versionen der Seite zu gelangen

![Das Kontextmenü für Benachrichtigungen in der Menüleiste](media/6eeb094340123c13c4abd26a0903720738c6f32d.png "Kontextmenü Benachrichtigungen")

![Eingeblendete Ereignisdetails einer Benachrichtigung](media/278ec8a35292c2fb4944b1c191784c87ba543fd1.png "Ereignisdetails")

## Erwähnungen

Sie können Benachrichtigungen erhalten, wenn Sie von anderen Nutzerinnen und Nutzern namentlich erwähnt werden, beispielsweise auf Seiten oder in Kommentaren.

![Benachrichtigung über eine Erwähnung](media/e6547c050a49a03e82b6dd2700c96648c054315b.png "Erwähnungen")

## E-Mail-Benachrichtigungen

Sie können Benachrichtigungen per E-Mail erhalten, wenn andere Nutzerinnen und Nutzer Seiten per E-Mail mit Ihnen teilen. Wie Sie selbst Seiten per E-Mail teilen können, erfahren Sie unter **Seiten – Seite als E-Mail versenden**.
