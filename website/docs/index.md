<h1>Anwenderdokumentation openDesk</h1>

# Einstieg

- [Erste Schritte](erste-schritte.md): Wenn Sie openDesk zum ersten Mal benutzen.
- [Arbeiten mit openDesk](arbeiten-mit-ihrem-arbeitsplatz.md): Von Office bis hin zur Videokonferenz – erfahren Sie genau, wie openDesk funktioniert.

# Fachdokumentation

Die Dokumentationen der einzelnen Komponenten finden Sie hier:

| Anwendungsgruppe             | Anwendungsname                        | Komponentendokumentation                                                                    |
| ---------------------------- | ------------------------------------- | ------------------------------------------------------------------------------------------- |
| Kommunikation & Organisation | E-Mail, Kalender, Kontakte & Aufgaben | [OX AppSuite](https://www.open-xchange.com/resources/oxpedia)                               |
| Produktivität                | Dateien                               | [Nextcloud](https://docs.nextcloud.com/)                                                    |
| Produktivität                | Diagramme                             | [CryptPad](https://docs.cryptpad.org/en/) mit [diagrams.net](https://www.diagrams.net/doc/) |
| Produktivität                | Weboffice                             | [Collabora Online](https://help.collaboraoffice.com/)                                       |
| Management                   | Projekte                              | [OpenProject](https://www.openproject.org/docs/user-guide/)                                 |
| Management                   | Wissen                                | [XWiki](https://www.xwiki.org/xwiki/bin/view/Documentation)                                 |
| Direkte Kommunikation        | Chat & Zusammenarbeit                 | [Element](https://element.io/user-guide) mit Nordeck widgets                                |
| Direkte Kommunikation        | Sofort Videokonferenz                 | [Jitsi](https://jitsi.github.io/handbook/docs/category/user-guide/)                         |
| Portal                       | -                                     | [Univention](https://docs.software-univention.de/n/en/index.html)                           |

# Beteiligung

Die Anwenderdokumentation ist wie openDesk Open Source. Sie können auf [Open CoDE die Originaldateien](https://gitlab.opencode.de/bmi/opendesk/documentation/anwenderdokumentation) einsehen und sich an der Verbesserung beteiligen.

# Lizenzen

Lizenzinformation zu dieser Dokumentation entnehmen Sie bitte der [licenses.md](./licenses.md).
