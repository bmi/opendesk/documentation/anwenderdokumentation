# An- und Abmelden

## Arbeitsplatz starten

Ihr Arbeitsplatz wird über einen Internet-Browser (Opera, Microsoft Edge oder Google Chrome) aufgerufen. Geben Sie einfach in die Adressleiste Ihre Anmelde-Adresse ein.

## Anmeldung

Zur Anmeldung geben Sie Ihren **Benutzernamen** oder Ihre **E-Mail** und das Passwort ein. Nach der **Anmeldung** können Sie über das **Hamburger-Menü** (drei Balken) ganz oben rechts die **Benutzereinstellungen** verwalten.

![Anmelden](anmeldung.png "Anmelden")

## Abmeldung

Auf der Startseite Ihres Arbeitsplatzes und in jedem Modul können Sie sich über das **Hamburger-Menü** (drei Balken) ganz oben rechts abmelden.

![Abmelden](abmeldung.png "Abmelden")
