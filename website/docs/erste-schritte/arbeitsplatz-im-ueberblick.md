# Arbeitsplatz im Überblick

Ihr Arbeitsplatz ist ein für den öffentlichen Sektor entwickelter digitaler Arbeitsplatz bestehend aus webbasierten Open-Source-Modulen mit vielen Funktionen, die Mitarbeiterinnen und Mitarbeitern aus dem öffentlichen Sektor benötigen. Strenge Datenschutzbestimmungen fordern vom Staat, jederzeit die volle Kontrolle über eigene und anvertraute Daten zu behalten. Darum wird Ihr Arbeitsplatz in sicheren Rechenzentren von öffentlich-rechtlichen IT-Dienstleistern sowie in gesicherten Clouds betrieben. Ihr Arbeitsplatz stellt eine echte Alternative zu den Office-Produkten anderer Anbieter auf dem Markt dar.

![Modulübersicht auf der Startseite von OpenDesk](c138d45ee9b60521456074d02500e943fb5bb678.png "Modulübersicht auf der Startseite")
