site_name: openDesk Anwenderdokumentation
repo_url: https://gitlab.opencode.de/bmi/opendesk/documentation/anwenderdokumentation
repo_name: Anwenderdokumentation auf Open CoDE
copyright: © 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH

theme:
  name: material
  logo: assets/openDesk_Profilbild.png
  favicon: assets/openDesk_Profilbild.png
  custom_dir: 'overrides'

nav:
  - Startseite: index.md
  - Erste Schritte: 
    - Inhaltsverzeichnis: erste-schritte.md 
    - Arbeitsplatz im Überblick: erste-schritte/arbeitsplatz-im-ueberblick.md
    - An- und Abmelden: erste-schritte/an-und-abmelden.md
  - Arbeiten mit Ihrem Arbeitsplatz: 
    - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz.md
    - Self-Service: 
      - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/self-service.md
      - Überblick: arbeiten-mit-ihrem-arbeitsplatz/self-service/ueberblick.md
      - Benutzereinstellungen: arbeiten-mit-ihrem-arbeitsplatz/self-service/benutzereinstellungen.md
      - Profildaten bearbeiten: arbeiten-mit-ihrem-arbeitsplatz/self-service/profildaten-bearbeiten.md
      - Kontozugang schützen: arbeiten-mit-ihrem-arbeitsplatz/self-service/kontozugang-schuetzen.md
      - Passwort ändern: arbeiten-mit-ihrem-arbeitsplatz/self-service/passwort-aendern.md
      - Sprache ändern: arbeiten-mit-ihrem-arbeitsplatz/self-service/sprache-aendern.md
      - Globale Benachrichtigungen: arbeiten-mit-ihrem-arbeitsplatz/self-service/globale-benachrichtigungen.md
      - Datenschutz: arbeiten-mit-ihrem-arbeitsplatz/self-service/datenschutz.md
      - Impressum: arbeiten-mit-ihrem-arbeitsplatz/self-service/impressum.md
    - Kommunikation und Organisation: 
      - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation.md
      - E-Mail:
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/email/erste-schritte.md
        - E-Mail verwalten: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/email/e-mail-verwalten.md
        - Weiteres: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/email/weiteres.md
      - Kalender:
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kalender/erste-schritte.md
        - Termin einstellen: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kalender/termin-einstellen.md
        - Termin verwalten: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kalender/termin-verwalten.md
        - Termin drucken: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kalender/termin-drucken.md
      - Kontakte:
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kontakte/erste-schritte-1.md
        - Verteilerliste hinzufügen: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kontakte/verteilerliste-hinzufuegen.md
        - Adressbuch hinzufügen: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kontakte/adressbuch-hinzufuegen.md
        - Kontakte verwalten: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/kontakte/kontakte-verwalten.md
      - Aufgaben:
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/aufgaben/erste-schritte.md
        - Neue Aufgaben anlegen: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/aufgaben/neue-aufgaben-anlegen.md
        - Arbeiten mit Aufgaben: arbeiten-mit-ihrem-arbeitsplatz/kommunikation-und-organisation/aufgaben/arbeiten-mit-aufgaben.md
    - Produktivität:
      - Dateien: 
        - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien.md
        - Überblick: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ueberblick.md
        - Ordner und Dateien erstellen und hochladen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-erstellen-und-hochladen.md
        - Neues Diagramm erstellen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/neues-diagrams.net-diagramm-erstellen.md
        - Vorlagenordner: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/vorlagenordner.md
        - Ordner und Dateien löschen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-loeschen.md
        - Ordner und Dateien verwalten: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-verwalten.md
        - Ordner und Dateien herunterladen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-herunterladen.md
        - Ordner und Dateien verschieben oder kopieren: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-verschieben-oder-kopieren.md
        - Ordner und Dateien teilen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/dateien/ordner-und-dateien-teilen.md
      - Aktivitäten: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/aktivitaeten.md
      - Office: 
        - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/office.md
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/office/erste-schritte.md
        - Neue Dateien schnell erstellen und speichern: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/office/neue-dateien-schnell-erstellen-und-speichern.md
        - E-Mail aus dem Produktivitäts-Modul versenden: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/office/e-mail-aus-dem-produktivitaets-modul-versenden.md
        - Persönliche Benachrichtigungseinstellungen: arbeiten-mit-ihrem-arbeitsplatz/produktivitaet/office/persoenliche-benachrichtigungseinstellungen.md
    - Management:
      - Projekte: 
        - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/management/projekte.md
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/management/projekte/erste-schritte.md
        - Self-Service: arbeiten-mit-ihrem-arbeitsplatz/management/projekte/self-service.md
        - Projekt-Modul öffnen: arbeiten-mit-ihrem-arbeitsplatz/management/projekte/projekt-modul-oeffnen.md
        - Der gesamte Projektmanagement-Lebenszyklus: arbeiten-mit-ihrem-arbeitsplatz/management/projekte/der-gesamte-projektmanagement-lebenszyklus.md
        - Module: arbeiten-mit-ihrem-arbeitsplatz/management/projekte/module.md
      - Wissen: 
        - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/management/wissen.md
        - Erste Schritte: arbeiten-mit-ihrem-arbeitsplatz/management/wissen/erste-schritte.md
        - Das Grundkonzept: arbeiten-mit-ihrem-arbeitsplatz/management/wissen/das-grundkonzept.md
        - Seiten: arbeiten-mit-ihrem-arbeitsplatz/management/wissen/seiten.md
        - Die Seitenleiste: arbeiten-mit-ihrem-arbeitsplatz/management/wissen/die-seitenleiste.md
        - Die Menüleiste: arbeiten-mit-ihrem-arbeitsplatz/management/wissen/die-menueleiste.md    
    - Direkte Kommunikation: 
      - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit.md
      - Oberfläche und Widgets:
        - Chat & Zusammenarbeit starten: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/chat-zusammenarbeit-starten.md
        - Schlüssel einrichten: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/schluessel-einrichten.md
        - Überblick: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/ueberblick.md
        - Chat: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/chat.md
        - Whiteboard: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/whiteboard.md
        - Spaces: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/spaces.md
        - Bildschirm individualisieren: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/bildschirm-individualisieren.md
        - Breakoutsessions: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/breakoutsessions.md
        - Abstimmungen: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/oberflaeche-und-widgets/abstimmungen.md      
      - Ablauf einer Konferenz:
        - Einführung: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/einfuehrung.md
        - Eine Veranstaltung einrichten: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/eine-veranstaltung-einrichten.md
        - Besprechungsraum: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/besprechungsraum.md
        - Einer Konferenz beitreten: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/einer-konferenz-beitreten.md
        - Meeting-Steuerung und Breakoutsessions: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/meeting-steuerung-und-breakoutsessions.md
        - Erklärung der Symbolleiste und Tastenkürzel: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/erklaerung-der-symbolleiste-und-tastenkuerzel.md
        - Konferenz steuern: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/konferenz-steuern.md
        - Personen verwalten: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/personen-verwalten.md
        - Sicherheitsoptionen: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/ablauf-einer-konferenz/sicherheitsoptionen.md
      - Hinweise:
        - Nutzungshinweise, Datenschutz und Sicherheitsmaßnahmen: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/hinweise/nutzungshinweise-datenschutz-und-sicherheitsmassnahmen.md
        - Sprache einstellen: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/chat-zusammenarbeit/hinweise/sprache-einstellen.md
    - Sofort Videokonferenzen: 
      - Inhaltsverzeichnis: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen.md
      - Was ist Sofort Videokonferenzen?: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen/allgemeine-anleitungen/was-ist-sofort-videokonferenzen.md
      - Voraussetzungen und Vorbereitungen: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen/allgemeine-anleitungen/voraussetzungen-und-vorbereitungen.md
      - Übersicht über die wichtigsten Symbole: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen/allgemeine-anleitungen/uebersicht-ueber-die-wichtigsten-symbole-1.md
      - Teilnehmerinnen und Teilnehmer: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen/teilnehmerinnen-und-teilnehmer.md
      - Moderatorinnen und Moderatoren: arbeiten-mit-ihrem-arbeitsplatz/direkte-kommunikation/sofort-videokonferenzen/moderatorinnen-und-moderatoren.md

plugins:
  - search
  - glightbox
markdown_extensions:
  - attr_list
  - md_in_html
  - admonition
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true
  - toc:
      title: Inhaltsverzeichnis
