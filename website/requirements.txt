mkdocs>=1.1.2
mkdocs-material==9.1.6
mkdocs-glightbox==0.3.3
mkdocs-git-revision-date-localized-plugin==1.2.0
mkdocs-static-i18n==1.1.1